package com.orangebyte.bahetiadmin.modules.viewmodels;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider.NewInstanceFactory;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.modules.db.local.AppDatabase;
import com.orangebyte.bahetiadmin.modules.db.local.DatabaseCreator;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.repository.RemoteRepository;
import com.orangebyte.bahetiadmin.modules.repository.SubCategoryRepository;
import com.orangebyte.bahetiadmin.modules.ui.AllContract.SubcategoryDataStreams;

import java.util.List;

import im.delight.android.webview.BuildConfig;

public class SubCategoryViewModel extends ViewModel implements SubcategoryDataStreams {
    private LiveData<List<SubCategoryEntity>> subCatObservable;
    private SubCategoryRepository subCatRepository;

    public static class SubCategoryFactory extends NewInstanceFactory {
        SubCategoryRepository mSUbCatRepository;

        public SubCategoryFactory() {
            this(null, null);
        }

        public SubCategoryFactory(AppDatabase appDatabase) {
            this(SubCategoryRepository.getInstance(RemoteRepository.getInstance(), appDatabase.subCatDao()), BuildConfig.VERSION_NAME);
        }

        public SubCategoryFactory(@Nullable SubCategoryRepository mSUbCatRepository, @Nullable String abc) {
            this.mSUbCatRepository = mSUbCatRepository;
        }

        public <T extends ViewModel> T create(Class<T> cls) {
            return (T) new SubCategoryViewModel(this.mSUbCatRepository);
        }
    }

    public SubCategoryViewModel() {
        this(null);
    }

    public SubCategoryViewModel(@Nullable SubCategoryRepository subCatRepository) {
        if (subCatRepository != null) {
            this.subCatRepository = subCatRepository;
        }
        this.subCatObservable = subscribeToLocalData();
    }

    public LiveData<List<SubCategoryEntity>> getSubCats() {
        return this.subCatObservable;
    }

    public LiveData<List<SubCategoryEntity>> addSubCategory(SubCategoryEntity c, int chk) {
        onDatabaseCreated();
        return addSubCat(c, chk);
    }

    public LiveData<List<SubCategoryEntity>> updateSubCatStatus(SubCategoryEntity c) {
        onDatabaseCreated();
        return updtSubCatStatus(c);
    }

    public LiveData<List<SubCategoryEntity>> deleteSubCategoryById(SubCategoryEntity ct) {
        onDatabaseCreated();
        return deleteSubCatById(ct);
    }

    private LiveData<List<SubCategoryEntity>> subscribeToLocalData() {
        return Transformations.switchMap(DatabaseCreator.getInstance().isDatabaseCreated(), new Function<Boolean, LiveData<List<SubCategoryEntity>>>() {
            public LiveData<List<SubCategoryEntity>> apply(Boolean isDbCreated) {
                if (!Boolean.TRUE.equals(isDbCreated)) {
                    return null;
                }
                SubCategoryViewModel.this.onDatabaseCreated();
                return SubCategoryViewModel.this.subscribeSubCatObservable();
            }
        });
    }

    private LiveData<List<SubCategoryEntity>> subscribeSubCatObservable() {
        return this.subCatRepository.loadSubCategoryList();
    }

    private LiveData<List<SubCategoryEntity>> addSubCat(SubCategoryEntity cat, int chk) {
        return this.subCatRepository.addSubCatgory(cat, chk);
    }

    private LiveData<List<SubCategoryEntity>> updtSubCatStatus(SubCategoryEntity cat) {
        return this.subCatRepository.updateSubCatStatus(cat);
    }

    private LiveData<List<SubCategoryEntity>> deleteSubCatById(SubCategoryEntity c) {
        return this.subCatRepository.deleteCategoryByID(c);
    }

    private void onDatabaseCreated() {
        AppDatabase appDatabase = DatabaseCreator.getInstance().getDatabase();
        if (this.subCatRepository == null) {
            this.subCatRepository = SubCategoryRepository.getInstance(RemoteRepository.getInstance(), appDatabase.subCatDao());
        }
    }
}
