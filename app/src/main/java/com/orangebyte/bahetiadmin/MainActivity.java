package com.orangebyte.bahetiadmin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.analytics.FirebaseAnalytics.Event;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.admin.AdminDashboard;
import com.orangebyte.bahetiadmin.modules.admin.ForgetPassword;
import com.orangebyte.bahetiadmin.modules.employee.EmployeeDashboard;
import com.orangebyte.bahetiadmin.modules.entities.AdminEntity;
import com.orangebyte.bahetiadmin.modules.entities.LoginEntity;
import com.orangebyte.bahetiadmin.modules.manager.ManagerDashboard;
import com.orangebyte.bahetiadmin.modules.models.ServicesResponse;
import com.orangebyte.bahetiadmin.modules.repository.ServiceFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;

import im.delight.android.webview.BuildConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    Button btn_fg_psd;
    LoginEntity loginEntity = null;
    Button login_btn;
    String post = BuildConfig.VERSION_NAME;
    EditText pswd;
    SweetAlertDialog sweetAlertDialog;
    EditText username;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        this.login_btn.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                AdminEntity adminEntity = new AdminEntity();
                if (TextUtils.isEmpty(MainActivity.this.username.getText().toString())) {
                    BahetiEnterprises.retShowAlertDialod("Plese Enter UserName", MainActivity.this);
                } else if (TextUtils.isEmpty(MainActivity.this.pswd.getText().toString())) {
                    BahetiEnterprises.retShowAlertDialod("Plese Enter Password", MainActivity.this);
                } else {

                    adminEntity.setUsername(MainActivity.this.username.getText().toString());
                    adminEntity.setPassword(MainActivity.this.pswd.getText().toString());
                    adminEntity.setRid(MainActivity.this.getSharedPreferences("refreshedToken", 0).getString("refreshedToken", BuildConfig.VERSION_NAME));
                    MainActivity.this.sweetAlertDialog.show();
                    try {
                        ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/adminLogin/")
                                .adminLogin(adminEntity).enqueue(new Callback<ServicesResponse>() {
                            public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                                MainActivity.this.sweetAlertDialog.dismiss();
                                MainActivity.this.post = ((LoginEntity) ((ServicesResponse) response.body()).getLoginEntityList().get(0)).getPost();
                                MainActivity.this.loginEntity = (LoginEntity) ((ServicesResponse) response.body()).getLoginEntityList().get(0);
                                if (MainActivity.this.loginEntity != null) {
                                    SharedPreferences sh = MainActivity.this.getSharedPreferences("log", 0);
                                    sh.edit().putBoolean(Event.LOGIN, true).commit();
                                    sh.edit().putString("users", MainActivity.this.loginEntity.toString()).commit();
                                    sh.edit().putString("uid", MainActivity.this.loginEntity.getUid()).commit();
                                    sh.edit().putString("assignedMgrType", MainActivity.this.loginEntity.getAssigned_mgr_type()).commit();
                                    sh.edit().putString("type", MainActivity.this.loginEntity.getMgrtype()).commit();
                                    sh.edit().putString("mobile", MainActivity.this.loginEntity.getUmobileno()).commit();
                                    sh.edit().putString("password", MainActivity.this.loginEntity.getPassword()).commit();

                                    sh.edit().putString("unm", MainActivity.this.loginEntity.getUname()).commit();
                                    sh.edit().putString("ustatus", MainActivity.this.loginEntity.getUstatus()).commit();
                                    sh.edit().putString("can_edit", MainActivity.this.loginEntity.getCan_edit()).commit();
                                    sh.edit().putString("post", MainActivity.this.post).commit();
                                    if (MainActivity.this.post.equalsIgnoreCase("admin")) {
                                        MainActivity.this.startActivity(new Intent(MainActivity.this, AdminDashboard.class));
                                        MainActivity.this.finish();
                                        return;
                                    } else if (MainActivity.this.post.equalsIgnoreCase("employee")) {
                                        MainActivity.this.startActivity(new Intent(MainActivity.this, EmployeeDashboard.class));
                                        MainActivity.this.finish();
                                        return;
                                    } else {
                                        MainActivity.this.startActivity(new Intent(MainActivity.this, ManagerDashboard.class));
                                        MainActivity.this.finish();
                                        return;
                                    }
                                }
                                BahetiEnterprises.retShowAlertDialod("Not Registerd", MainActivity.this);
                            }

                            public void onFailure(Call<ServicesResponse> call, Throwable t) {
                                MainActivity.this.sweetAlertDialog.dismiss();
                                BahetiEnterprises.retShowAlertDialod("Failed To Login", MainActivity.this);
                                t.printStackTrace();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void initView() {
        this.username = (EditText) findViewById(R.id.username);
        this.pswd = (EditText) findViewById(R.id.pswd);
        this.login_btn = (Button) findViewById(R.id.login_btn);
        this.btn_fg_psd = (Button) findViewById(R.id.btn_fg_psd);



        this.sweetAlertDialog = new SweetAlertDialog(this, 5);
        this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
        this.sweetAlertDialog.setTitleText("Loading");
        this.sweetAlertDialog.setCancelable(false);


        btn_fg_psd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ForgetPassword.class));
            }
        });
    }
}
