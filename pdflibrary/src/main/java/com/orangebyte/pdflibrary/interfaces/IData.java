package com.orangebyte.pdflibrary.interfaces;

public interface IData {

	void 		setData(Object data);
	Object 	getData();

}
