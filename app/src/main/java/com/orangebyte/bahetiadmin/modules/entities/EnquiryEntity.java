package com.orangebyte.bahetiadmin.modules.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "enquiry")
public class EnquiryEntity extends IdentifiableEntity implements Serializable {
    @SerializedName("actn_tkn_st")
    @ColumnInfo(name = "actn_tkn_st")
    String actn_tkn_st;
    @SerializedName("address")
    @ColumnInfo(name = "address")
    String address;
    @SerializedName("assigned_emp_id")
    @ColumnInfo(name = "assigned_emp_id")
    String assigned_emp_id;
    @SerializedName("assigned_mgr_type")
    @ColumnInfo(name = "assigned_mgr_type")
    String assigned_mgr_type;
    @SerializedName("cat_id")
    @ColumnInfo(name = "cat_id")
    String cat_id;
    @SerializedName("cat_nm")
    @ColumnInfo(name = "cat_nm")
    String cat_nm;
    @SerializedName("descrip")
    @ColumnInfo(name = "descrip")
    String descrip;
    @SerializedName("emp_nm")
    @ColumnInfo(name = "emp_nm")
    String emp_nm;
    @SerializedName("emp_status")
    @ColumnInfo(name = "emp_status")
    String emp_status;
    @SerializedName("last_atnd_on")
    @ColumnInfo(name = "last_atnd_on")
    String last_atnd_on;
    @SerializedName("mgr_id")
    @ColumnInfo(name = "mgr_id")
    String mgr_id;
    @SerializedName("mob_no")
    @ColumnInfo(name = "mob_no")
    String mob_no;
    @SerializedName("mobile_no")
    @ColumnInfo(name = "mobile_no")
    String mobile_no;
    @SerializedName("model_no")
    @ColumnInfo(name = "model_no")
    String model_no;
    @SerializedName("ondate")
    @ColumnInfo(name = "ondate")
    String ondate;
    @SerializedName("product_id")
    @ColumnInfo(name = "product_id")
    String product_id;
    @SerializedName("status")
    @ColumnInfo(name = "status")
    String status;
    @SerializedName("sub_cat_name")
    @ColumnInfo(name = "sub_cat_name")
    String sub_cat_name;
    @SerializedName("subcat_id")
    @ColumnInfo(name = "subcat_id")
    String subcat_id;
    @SerializedName("title")
    @ColumnInfo(name = "title")
    String title;
    @SerializedName("user_id")
    @ColumnInfo(name = "user_id")
    String user_id;
    @SerializedName("usernm")
    @ColumnInfo(name = "usernm")
    String usernm;

    @SerializedName("com_no")
    @ColumnInfo(name = "com_no")
    private String com_no;

    private boolean isSelected;

    public EnquiryEntity() {
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public /* bridge */ /* synthetic */ long getId() {
        return super.getId();
    }

    public /* bridge */ /* synthetic */ void setId(long j) {
        super.setId(j);
    }

    public String getLast_atnd_on() {
        return this.last_atnd_on;
    }

    public void setLast_atnd_on(String last_atnd_on) {
        this.last_atnd_on = last_atnd_on;
    }

    public String getActn_tkn_st() {
        return this.actn_tkn_st;
    }

    public void setActn_tkn_st(String actn_tkn_st) {
        this.actn_tkn_st = actn_tkn_st;
    }


    public String getCom_no() {
        return com_no;
    }

    public void setCom_no(String com_no) {
        this.com_no = com_no;
    }

    @Ignore
    public EnquiryEntity(long id, String cat_id, String subcat_id, String product_id, String descrip, String user_id, String assigned_emp_id, String ondate, String status) {
        this.id = id;
        this.cat_id = cat_id;
        this.subcat_id = subcat_id;
        this.product_id = product_id;
        this.descrip = descrip;
        this.user_id = user_id;
        this.assigned_emp_id = assigned_emp_id;
        this.ondate = ondate;
        this.status = status;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getModel_no() {
        return this.model_no;
    }

    public void setModel_no(String model_no) {
        this.model_no = model_no;
    }

    public String getCat_nm() {
        return this.cat_nm;
    }

    public void setCat_nm(String cat_nm) {
        this.cat_nm = cat_nm;
    }

    public String getSub_cat_name() {
        return this.sub_cat_name;
    }

    public void setSub_cat_name(String sub_cat_name) {
        this.sub_cat_name = sub_cat_name;
    }

    public String getEmp_nm() {
        return this.emp_nm;
    }

    public void setEmp_nm(String emp_nm) {
        this.emp_nm = emp_nm;
    }

    public String getMob_no() {
        return this.mob_no;
    }

    public void setMob_no(String mob_no) {
        this.mob_no = mob_no;
    }

    public String getEmp_status() {
        return this.emp_status;
    }

    public void setEmp_status(String emp_status) {
        this.emp_status = emp_status;
    }

    public String getMgr_id() {
        return this.mgr_id;
    }

    public void setMgr_id(String mgr_id) {
        this.mgr_id = mgr_id;
    }

    public String getAssigned_mgr_type() {
        return this.assigned_mgr_type;
    }

    public void setAssigned_mgr_type(String assigned_mgr_type) {
        this.assigned_mgr_type = assigned_mgr_type;
    }

    public String getUsernm() {
        return this.usernm;
    }

    public void setUsernm(String usernm) {
        this.usernm = usernm;
    }

    public String getMobile_no() {
        return this.mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCat_id() {
        return this.cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getSubcat_id() {
        return this.subcat_id;
    }

    public void setSubcat_id(String subcat_id) {
        this.subcat_id = subcat_id;
    }

    public String getProduct_id() {
        return this.product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getDescrip() {
        return this.descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public String getUser_id() {
        return this.user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAssigned_emp_id() {
        return this.assigned_emp_id;
    }

    public void setAssigned_emp_id(String assigned_emp_id) {
        this.assigned_emp_id = assigned_emp_id;
    }

    public String getOndate() {
        return this.ondate;
    }

    public void setOndate(String ondate) {
        this.ondate = ondate;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
