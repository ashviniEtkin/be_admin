package com.orangebyte.bahetiadmin.modules.admin;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.entities.ManagerEntity;
import com.orangebyte.bahetiadmin.modules.viewmodels.ManagerViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.ManagerViewModel.ManagerFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;

import java.util.List;

public class AddManagerActivity extends AppCompatActivity implements LifecycleRegistryOwner {
    Button add;
    RadioGroup desi_rd_grp;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    EditText mgr_mob_no;
    EditText mgr_name;
    CheckBox offer_perm_cb;
    RadioButton rdo_sales_mgr;
    RadioButton rdo_service_mgr, rdo_super_mgr;
    SweetAlertDialog sweetAlertDialog;
    ManagerViewModel viewModel;
    ManagerEntity me1 = null;

    String chk_add_update = "";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_manager);
        initViews();
        chk_add_update = getIntent().getStringExtra(BahetiEnterprises.CHK_ADD_UPDATE);
        if (chk_add_update.equals("1")) {
            BahetiEnterprises.setActBar(getResources().getString(R.string.Update_manager), this, false);
            me1 = (ManagerEntity) getIntent().getSerializableExtra(BahetiEnterprises.FROM);
            add.setText(getString(R.string.update));
            updateData();
        } else {
            add.setText(getString(R.string.add));
            BahetiEnterprises.setActBar(getResources().getString(R.string.Add_manager), this, false);
        }
        this.add.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                AddManagerActivity.this.initModelView();
            }
        });
    }

    public void updateData() {
        if (me1.getPermi().equalsIgnoreCase("1")) {
            offer_perm_cb.setChecked(true);
        } else {
            offer_perm_cb.setChecked(false);
        }

        if (me1.getMgr_type().equalsIgnoreCase(getString(R.string.Super_Manager))) {
            rdo_super_mgr.setChecked(true);
        } else if (me1.getMgr_type().equalsIgnoreCase(getString(R.string.Service_Manager))) {
            rdo_service_mgr.setChecked(true);
        } else if (me1.getMgr_type().equalsIgnoreCase(getString(R.string.Sales_Manager))) {
            rdo_sales_mgr.setChecked(true);
        }

        mgr_mob_no.setText(me1.getMobile_no());
        mgr_name.setText(me1.getName());

    }


    public void initViews() {
        this.offer_perm_cb = (CheckBox) findViewById(R.id.offer_perm_cb);
        this.rdo_sales_mgr = (RadioButton) findViewById(R.id.rdo_sales_mgr);
        this.rdo_service_mgr = (RadioButton) findViewById(R.id.rdo_service_mgr);
        this.rdo_super_mgr = (RadioButton) findViewById(R.id.rdo_super_mgr);
        this.desi_rd_grp = (RadioGroup) findViewById(R.id.desi_rd_grp);
        this.mgr_name = (EditText) findViewById(R.id.mgr_name);
        this.mgr_mob_no = (EditText) findViewById(R.id.mgr_mob_no);
        this.add = (Button) findViewById(com.orangebyte.pdflibrary.R.id.add);
        this.sweetAlertDialog = new SweetAlertDialog(this, 5);
        this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
        this.sweetAlertDialog.setTitleText("Loading");
    }

    public void initModelView() {
        this.viewModel = (ManagerViewModel) ViewModelProviders.of(this, new ManagerFactory()).get(ManagerViewModel.class);
        ManagerEntity me = new ManagerEntity();

        me.setMgr_type(((RadioButton) findViewById(this.desi_rd_grp.getCheckedRadioButtonId())).getText().toString());
        me.setMobile_no(this.mgr_mob_no.getText().toString());
        me.setName(this.mgr_name.getText().toString());
        me.setPassword(this.mgr_mob_no.getText().toString());

        me.setStatus("1");
        if (this.offer_perm_cb.isChecked()) {
            me.setPermi("1");
        } else {
            me.setPermi("0");
        }
        if (TextUtils.isEmpty(this.mgr_name.getText().toString())) {
            BahetiEnterprises.retShowAlertDialod("Enter Manager Name", this);
        } else if (TextUtils.isEmpty(this.mgr_mob_no.getText().toString())) {
            BahetiEnterprises.retShowAlertDialod("Enter Valid Mobile Number", this);
        } else if (this.mgr_mob_no.getText().toString().length() < 10) {
            BahetiEnterprises.retShowAlertDialod("Enter Valid Mobile Number", this);
        } else {

            if (chk_add_update.equalsIgnoreCase("1")) {
                me.setId(me1.getId());
                addMgr(this.viewModel, me, 1);
            } else {
                addMgr(this.viewModel, me, 0);
            }
        }
    }

    private void addMgr(ManagerViewModel viewModel, ManagerEntity c, int chk) {
        try {
            this.sweetAlertDialog.show();
            viewModel.addMgr(c, chk).observe(this, new Observer<List<ManagerEntity>>() {
                public void onChanged(@Nullable List<ManagerEntity> managerEntities) {
                    if (!managerEntities.isEmpty()) {
                        AddManagerActivity.this.sweetAlertDialog.dismiss();
                        AddManagerActivity.this.onBackPressed();
                    } else if (managerEntities.size() == 0) {
                        AddManagerActivity.this.sweetAlertDialog.dismiss();
                        AddManagerActivity.this.onBackPressed();
                    } else {
                        AddManagerActivity.this.sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }
}
