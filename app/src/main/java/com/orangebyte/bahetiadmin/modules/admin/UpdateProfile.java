package com.orangebyte.bahetiadmin.modules.admin;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.entities.AdminEntity;
import com.orangebyte.bahetiadmin.modules.entities.EnquiryEntity;
import com.orangebyte.bahetiadmin.modules.models.ServicesResponse;
import com.orangebyte.bahetiadmin.modules.repository.RemoteCallback;
import com.orangebyte.bahetiadmin.modules.repository.ServiceFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;

import im.delight.android.webview.BuildConfig;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProfile extends AppCompatActivity {

    EditText mobile_number;
    Button update;
    SharedPreferences sh;
    String post = "";
    String uid = "";
    String mobile = "";
    String unm = "";
    TextView username, design;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        BahetiEnterprises.setActBar(getResources().getString(R.string.update_profile), this, false);

        init();

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(mobile_number.getText().toString())) {
                    mobile_number.setError(getResources().getString(R.string.enter_mobile_no));

                } else if (mobile_number.getText().toString().length() < 10) {
                    mobile_number.setError(getResources().getString(R.string.mobile_validation));
                } else {

                    AdminEntity adminEntity = new AdminEntity();
                    adminEntity.setId(Long.parseLong(uid));
                    adminEntity.setMobile_no(mobile_number.getText().toString());
                    if (post.equalsIgnoreCase(getString(R.string.Store_Manager))
                            || post.equalsIgnoreCase(getString(R.string.Super_Manager))
                            || post.equalsIgnoreCase(getString(R.string.Service_Manager))) {
                        adminEntity.setPost("manager");
                    } else {
                        adminEntity.setPost(post);
                    }
                    updateProfile(adminEntity);

                }

            }

        });

    }

    public void init() {
        mobile_number = (EditText) findViewById(R.id.mobile_number);
        update = (Button) findViewById(R.id.update);
        username = (TextView) findViewById(R.id.username);
        design = (TextView) findViewById(R.id.design);

        this.sh = getSharedPreferences("log", 0);
        this.post = this.sh.getString("post", "");
        mobile = sh.getString("mobile", "");
        uid = sh.getString("uid", "");
        unm = sh.getString("unm", "");
        try {
            mobile_number.setText(mobile);
            design.setText(post);
            username.setText(unm);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateProfile(AdminEntity adminEntity) {

        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.getProgressHelper().setBarColor((getResources().getColor(R.color.app_color)));
        sweetAlertDialog.setTitleText("Please Wait");
        sweetAlertDialog.setCancelable(false);
         sweetAlertDialog.show();
        try {
            //   RequestBody body = RequestBody.create(MediaType.parse("application/json"), adminEntity.toString());


            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/updateProfile/")
                    .updateProfile(adminEntity)
                    .enqueue(new Callback<ServicesResponse>() {
                        public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                            sweetAlertDialog.dismiss();
                            try {
                                boolean t = (Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                                sh.edit().putString("mobile", mobile_number.getText().toString()).commit();
                                init();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        public void onFailure(Call<ServicesResponse> call, Throwable t) {
                            t.printStackTrace();
                            sweetAlertDialog.dismiss();
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
