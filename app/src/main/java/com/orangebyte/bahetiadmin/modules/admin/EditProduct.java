package com.orangebyte.bahetiadmin.modules.admin;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.applications.PathUtil;
import com.orangebyte.bahetiadmin.modules.entities.BrandEntity;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.ProductEntity;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.models.ServicesResponse;
import com.orangebyte.bahetiadmin.modules.repository.ApiServices;
import com.orangebyte.bahetiadmin.modules.repository.ServiceFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.BrandViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import im.delight.android.webview.BuildConfig;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProduct extends AppCompatActivity implements LifecycleRegistryOwner {


    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);

    int RESULT_LOAD_IMG1 = 361;
    int RESULT_LOAD_IMG2 = 362;
    int RESULT_LOAD_IMG3 = 363;
    int RESULT_LOAD_IMG4 = 364;
    int RESULT_LOAD_IMG5 = 365;

    Uri selectedImageUri1 = null;
    Uri selectedImageUri2 = null;
    Uri selectedImageUri3 = null;
    Uri selectedImageUri4 = null;
    Uri selectedImageUri5 = null;


    EditText title, model_no, features, price;
    Button edit_product;
    Spinner category_spinner, subcategory_spinner, brand_spinner;
    List<SubCategoryEntity> allSubCat = new ArrayList();

    ImageView image1, image2, image3, image4, image5;
    ImageButton img1_cam, img2_cam, img3_cam, img4_cam, img5_cam;

    List<SubCategoryEntity> subCategoryEntityList = new ArrayList();

    ProductEntity productEntity = null;
    List<BrandEntity> brandEntityList = new ArrayList();
    List<CategoryEntity> categoryEntityList = new ArrayList();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);
        BahetiEnterprises.setActBar(getResources().getString(R.string.edit_product), this, false);

        productEntity = (ProductEntity) getIntent().getSerializableExtra(BahetiEnterprises.FROM);
        init();
        setCatgoryToSpinner();

        this.category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (allSubCat.isEmpty()) {
                    setSubCatgoryToSpinner(((CategoryEntity) categoryEntityList.get(i)).getId() + "");
                } else {
                    setToSubCatSpinner(allSubCat, ((CategoryEntity) categoryEntityList.get(i)).getId() + "");
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.subcategory_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setBrandToSpinner(categoryEntityList.get(category_spinner.getSelectedItemPosition()).getId() + "");
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    public void init() {
        title = (EditText) findViewById(R.id.title);
        model_no = (EditText) findViewById(R.id.model_no);
        features = (EditText) findViewById(R.id.features);
        price = (EditText) findViewById(R.id.price);
        edit_product = (Button) findViewById(R.id.edit_product);
        category_spinner = (Spinner) findViewById(R.id.category_spinner);
        subcategory_spinner = (Spinner) findViewById(R.id.subcategory_spinner);
        brand_spinner = (Spinner) findViewById(R.id.brand_spinner);

        image1 = (ImageView) findViewById(R.id.image1);
        image2 = (ImageView) findViewById(R.id.image2);
        image3 = (ImageView) findViewById(R.id.image3);
        image4 = (ImageView) findViewById(R.id.image4);
        image5 = (ImageView) findViewById(R.id.image5);


        img1_cam = (ImageButton) findViewById(R.id.img1_cam);
        img2_cam = (ImageButton) findViewById(R.id.img2_cam);
        img3_cam = (ImageButton) findViewById(R.id.img3_cam);
        img4_cam = (ImageButton) findViewById(R.id.img4_cam);
        img5_cam = (ImageButton) findViewById(R.id.img5_cam);

        title.setText(productEntity.getTitle());
        model_no.setText(productEntity.getModel_no());
        features.setText(Html.fromHtml(productEntity.getFeatures()));
        price.setText(productEntity.getPrice());

        try {
            Picasso.with(EditProduct.this).load(BahetiEnterprises.PRODUCT_IMAGE_URL + productEntity.getImage1()).into(image1);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            Picasso.with(EditProduct.this).load(BahetiEnterprises.PRODUCT_IMAGE_URL + productEntity.getImage2()).into(image2);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            Picasso.with(EditProduct.this).load(BahetiEnterprises.PRODUCT_IMAGE_URL + productEntity.getImage3()).into(image3);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            Picasso.with(EditProduct.this).load(BahetiEnterprises.PRODUCT_IMAGE_URL + productEntity.getImage4()).into(image4);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            Picasso.with(EditProduct.this).load(BahetiEnterprises.PRODUCT_IMAGE_URL + productEntity.getImage5()).into(image5);
        } catch (Exception e) {
            e.printStackTrace();
        }


       /* img1_cam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage(1);
            }
        });

        img2_cam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage(2);
            }
        });

        img3_cam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage(3);
            }
        });


        img4_cam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage(4);
            }
        });

        img5_cam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage(5);
            }
        });
*/

        edit_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProductEntity pe1 = new ProductEntity();
                if (TextUtils.isEmpty(title.getText().toString())) {
                    title.setError("Enter Title");
                } else if (TextUtils.isEmpty(model_no.getText().toString())) {
                    model_no.setError("Enter Model No.");
                } else if (TextUtils.isEmpty(features.getText().toString())) {
                    features.setError("Enter Feature");
                } else if (TextUtils.isEmpty(price.getText().toString())) {
                    price.setError("Enter Price");
                } else {
                    pe1.setId(productEntity.getId());
                    pe1.setTitle(title.getText().toString());
                    pe1.setFeatures(features.getText().toString());
                    pe1.setModel_no(model_no.getText().toString());
                    pe1.setPrice(price.getText().toString());

                    pe1.setCat_id(categoryEntityList.get(category_spinner.getSelectedItemPosition()).getId() + "");
                    pe1.setSub_cat_id(subCategoryEntityList.get(subcategory_spinner.getSelectedItemPosition()).getId() + "");
                    pe1.setBrand_id(brandEntityList.get(brand_spinner.getSelectedItemPosition()).getId() + "");

                    if (selectedImageUri1 != null) {
                        pe1.setImage1(title.getText().toString().replace(" ", "_") + System.currentTimeMillis() + ".jpg");
                    } else {
                        pe1.setImage1(productEntity.getImage1());
                    }
                    if (selectedImageUri1 != null) {
                        pe1.setImage2(title.getText().toString().replace(" ", "_") + System.currentTimeMillis() + ".jpg");

                    } else {
                        pe1.setImage2(productEntity.getImage2());

                    }

                    if (selectedImageUri1 != null) {
                        pe1.setImage3(title.getText().toString().replace(" ", "_") + System.currentTimeMillis() + ".jpg");

                    } else {
                        pe1.setImage3(productEntity.getImage3());
                    }

                    if (selectedImageUri4 != null) {
                        pe1.setImage4(title.getText().toString().replace(" ", "_") + System.currentTimeMillis() + ".jpg");

                    } else {
                        pe1.setImage4(productEntity.getImage4());
                    }

                    if (selectedImageUri5 != null) {
                        pe1.setImage5(title.getText().toString().replace(" ", "_") + System.currentTimeMillis() + ".jpg");

                    } else {
                        pe1.setImage5(productEntity.getImage5());
                    }

                    editProduct(pe1);


                }

            }
        });

    }

    /*protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, com.orangebyte.pdflibrary.R.styleable.AppCompatTheme_ratingBarStyleIndicator);
        }
    }*/


    public void chooseImage(int pos) {

        if (ActivityCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, com.orangebyte.pdflibrary.R.styleable.AppCompatTheme_ratingBarStyleIndicator);
            return;
        }
        Intent intent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        switch (pos) {

            case 1:
                startActivityForResult(Intent.createChooser(intent, "Select File"), this.RESULT_LOAD_IMG1);
                break;
            case 2:
                startActivityForResult(Intent.createChooser(intent, "Select File"), this.RESULT_LOAD_IMG2);
                break;
            case 3:
                startActivityForResult(Intent.createChooser(intent, "Select File"), this.RESULT_LOAD_IMG3);
                break;
            case 4:
                startActivityForResult(Intent.createChooser(intent, "Select File"), this.RESULT_LOAD_IMG4);
                break;
            case 5:
                startActivityForResult(Intent.createChooser(intent, "Select File"), this.RESULT_LOAD_IMG5);
                break;

            default:
                break;

        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == this.RESULT_LOAD_IMG1 && resultCode == RESULT_OK && data != null) {
            this.selectedImageUri1 = data.getData();
            Cursor cursor = managedQuery(this.selectedImageUri1, new String[]{"_data"}, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow("_data");
            cursor.moveToFirst();
            String selectedImagePath = cursor.getString(column_index);
            String filePath = BahetiEnterprises.getRealPathFromURI(data.getDataString());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            this.image1.setImageBitmap(BitmapFactory.decodeFile(filePath, options));
            this.image1.setScaleType(ImageView.ScaleType.FIT_XY);
            return;
        }
        if (requestCode == this.RESULT_LOAD_IMG2 && resultCode == -1 && data != null) {
            this.selectedImageUri2 = data.getData();
            Cursor cursor = managedQuery(this.selectedImageUri2, new String[]{"_data"}, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow("_data");
            cursor.moveToFirst();
            String selectedImagePath = cursor.getString(column_index);
            String filePath = BahetiEnterprises.getRealPathFromURI(data.getDataString());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            this.image2.setImageBitmap(BitmapFactory.decodeFile(filePath, options));
            this.image2.setScaleType(ImageView.ScaleType.FIT_XY);
            return;
        }
        if (requestCode == this.RESULT_LOAD_IMG3 && resultCode == -1 && data != null) {
            this.selectedImageUri3 = data.getData();
            Cursor cursor = managedQuery(this.selectedImageUri3, new String[]{"_data"}, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow("_data");
            cursor.moveToFirst();
            String selectedImagePath = cursor.getString(column_index);
            String filePath = BahetiEnterprises.getRealPathFromURI(data.getDataString());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            this.image3.setImageBitmap(BitmapFactory.decodeFile(filePath, options));
            this.image3.setScaleType(ImageView.ScaleType.FIT_XY);
            return;
        }
        if (requestCode == this.RESULT_LOAD_IMG4 && resultCode == -1 && data != null) {
            this.selectedImageUri4 = data.getData();
            Cursor cursor = managedQuery(this.selectedImageUri4, new String[]{"_data"}, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow("_data");
            cursor.moveToFirst();
            String selectedImagePath = cursor.getString(column_index);
            String filePath = BahetiEnterprises.getRealPathFromURI(data.getDataString());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            this.image4.setImageBitmap(BitmapFactory.decodeFile(filePath, options));
            this.image4.setScaleType(ImageView.ScaleType.FIT_XY);
            return;
        }
        if (requestCode == this.RESULT_LOAD_IMG5 && resultCode == -1 && data != null) {
            this.selectedImageUri5 = data.getData();
            Cursor cursor = managedQuery(this.selectedImageUri5, new String[]{"_data"}, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow("_data");
            cursor.moveToFirst();
            String selectedImagePath = cursor.getString(column_index);
            String filePath = BahetiEnterprises.getRealPathFromURI(data.getDataString());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            this.image5.setImageBitmap(BitmapFactory.decodeFile(filePath, options));
            this.image5.setScaleType(ImageView.ScaleType.FIT_XY);
            return;
        }
        Toast.makeText(getApplicationContext(), "you haven't select image", Toast.LENGTH_LONG).show();
    }

    public void setCatgoryToSpinner() {
        try {
            ((CategoryViewMdels) ViewModelProviders.of(this, new CategoryViewMdels.Factory()).get(CategoryViewMdels.class)).getCats().observe(this, new Observer<List<CategoryEntity>>() {
                public void onChanged(@Nullable List<CategoryEntity> categoryEntities) {
                    if (!categoryEntities.isEmpty()) {
                        setToSpinner(categoryEntities);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setSubCatgoryToSpinner(final String catId) {
        try {
            ((SubCategoryViewModel) ViewModelProviders.of(this, new SubCategoryViewModel.SubCategoryFactory()).get(SubCategoryViewModel.class)).getSubCats().observe(this, new Observer<List<SubCategoryEntity>>() {
                public void onChanged(@Nullable List<SubCategoryEntity> subCategoryEntities) {
                    if (!subCategoryEntities.isEmpty()) {
                        allSubCat = subCategoryEntities;
                        setToSubCatSpinner(subCategoryEntities, catId);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public void setBrandToSpinner(final String catId) {
        try {
            ((BrandViewModel) ViewModelProviders.of(this, new BrandViewModel.BrandFactory()).get(BrandViewModel.class)).getBrands().observe(this, new Observer<List<BrandEntity>>() {
                public void onChanged(@Nullable List<BrandEntity> brandEntityList) {
                    if (!brandEntityList.isEmpty()) {
                        setToBrandSpinner(brandEntityList, catId);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setToBrandSpinner(List<BrandEntity> brandEntities, String catId) {
        this.brandEntityList = brandEntities;
        List<String> catList = new ArrayList();

        try {
            for (BrandEntity subCategoryEntity : brandEntities) {
                //  if (subCategoryEntity.getCat_id().equalsIgnoreCase(catId)) {
                catList.add(subCategoryEntity.getBrand_nm());
                //}
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.brand_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));


        int i = 0;
        while (i < this.brandEntityList.size()) {
            try {
                if (((BrandEntity) this.brandEntityList.get(i)).getId() == ((long) Integer.parseInt(this.productEntity.getBrand_id()))) {
                    this.brand_spinner.setSelection(i);
                    i = this.brandEntityList.size();
                } else {
                    i++;
                }
            } catch (NumberFormatException e2) {
                e2.printStackTrace();
                return;
            }
        }
    }

    private void setToSpinner(List<CategoryEntity> categoryEntities) {
        this.categoryEntityList = categoryEntities;
        List<String> catList = new ArrayList();
        try {
            for (CategoryEntity categoryEntity : categoryEntities) {
                catList.add(categoryEntity.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.category_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
        int i = 0;
        while (i < this.categoryEntityList.size()) {
            try {
                if (((CategoryEntity) this.categoryEntityList.get(i)).getId() == ((long) Integer.parseInt(this.productEntity.getCat_id()))) {
                    this.category_spinner.setSelection(i);
                    i = this.categoryEntityList.size();
                } else {
                    i++;
                }
            } catch (NumberFormatException e2) {
                e2.printStackTrace();
                return;
            }
        }
    }

    private void setToSubCatSpinner(List<SubCategoryEntity> subCategoryEntities, String catId) {
        this.subCategoryEntityList.clear();
        List<String> catList = new ArrayList();
        try {
            for (SubCategoryEntity subCategoryEntity : subCategoryEntities) {
                if (subCategoryEntity.getCat_id().equalsIgnoreCase(catId)) {
                    catList.add(subCategoryEntity.getSub_cat_name());
                    this.subCategoryEntityList.add(subCategoryEntity);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.subcategory_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
        int i = 0;
        while (i < this.subCategoryEntityList.size()) {
            try {
                if (((SubCategoryEntity) this.subCategoryEntityList.get(i)).getId() == ((long) Integer.parseInt(this.productEntity.getSub_cat_id()))) {
                    this.subcategory_spinner.setSelection(i);
                    i = this.subCategoryEntityList.size();
                } else {
                    i++;
                }
            } catch (NumberFormatException e2) {
                e2.printStackTrace();
                return;
            }
        }
    }


    public void editProduct(ProductEntity prodEntity) {
        File file1 = null, file2 = null, file3 = null, file4 = null, file5 = null;
        RequestBody requestBody1 = null, requestBody2 = null, requestBody3 = null, requestBody4 = null, requestBody5 = null;

        MultipartBody.Part fileToUpload = null;
        try {
            if (selectedImageUri1 != null) {
                file1 = new File(PathUtil.getPath(BahetiEnterprises.getInstance(), selectedImageUri1));
            }
            if (selectedImageUri2 != null) {
                file2 = new File(PathUtil.getPath(BahetiEnterprises.getInstance(), selectedImageUri2));
            }
            if (selectedImageUri3 != null) {
                file3 = new File(PathUtil.getPath(BahetiEnterprises.getInstance(), selectedImageUri3));
            }
            if (selectedImageUri4 != null) {
                file4 = new File(PathUtil.getPath(BahetiEnterprises.getInstance(), selectedImageUri4));
            }

            if (selectedImageUri5 != null) {
                file4 = new File(PathUtil.getPath(BahetiEnterprises.getInstance(), selectedImageUri5));
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (file1 != null)
                requestBody1 = RequestBody.create(MediaType.parse("multipart/form-data"), file1);
            if (file2 != null)
                requestBody2 = RequestBody.create(MediaType.parse("multipart/form-data"), file2);
            if (file3 != null)
                requestBody3 = RequestBody.create(MediaType.parse("multipart/form-data"), file3);
            if (file4 != null)
                requestBody4 = RequestBody.create(MediaType.parse("multipart/form-data"), file4);
            if (file5 != null)
                requestBody5 = RequestBody.create(MediaType.parse("multipart/form-data"), file5);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            if (requestBody1 != null)
                fileToUpload = MultipartBody.Part.createFormData("uploaded_file_img1", file1.getName(), requestBody1);
            if (requestBody2 != null)
                fileToUpload = MultipartBody.Part.createFormData("uploaded_file_img2", file1.getName(), requestBody2);
            if (requestBody3 != null)
                fileToUpload = MultipartBody.Part.createFormData("uploaded_file_img3", file1.getName(), requestBody3);
            if (requestBody4 != null)
                fileToUpload = MultipartBody.Part.createFormData("uploaded_file_img4", file1.getName(), requestBody4);
            if (requestBody5 != null)
                fileToUpload = MultipartBody.Part.createFormData("uploaded_file_img5", file1.getName(), requestBody5);

        } catch (Exception e22) {
            e22.printStackTrace();
        }
        ApiServices myApiServices = ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/addOffer/");
        try {

            //id,cat_id,sub_cat_id,brand_id,title,model_no,price,features,image1,image2,image3,image4,image5
            Call<ServicesResponse> call;
            HashMap<String, RequestBody> map = new HashMap();
            map.put("id", BahetiEnterprises.retRequestBody(prodEntity.getId() + ""));
            map.put("cat_id", BahetiEnterprises.retRequestBody(prodEntity.getCat_id()));
            map.put("sub_cat_id", BahetiEnterprises.retRequestBody(prodEntity.getSub_cat_id()));
            map.put("brand_id", BahetiEnterprises.retRequestBody(prodEntity.getBrand_id()));
            map.put("title", BahetiEnterprises.retRequestBody(prodEntity.getTitle()));


            map.put("model_no", BahetiEnterprises.retRequestBody(prodEntity.getModel_no() + ""));
            map.put("price", BahetiEnterprises.retRequestBody(prodEntity.getPrice()));
            map.put("features", BahetiEnterprises.retRequestBody(prodEntity.getFeatures()));
            map.put("image1", BahetiEnterprises.retRequestBody(prodEntity.getImage1()));
            map.put("image2", BahetiEnterprises.retRequestBody(prodEntity.getImage2()));

            map.put("image3", BahetiEnterprises.retRequestBody(prodEntity.getImage3() + ""));
            map.put("image4", BahetiEnterprises.retRequestBody(prodEntity.getImage4()));
            map.put("image5", BahetiEnterprises.retRequestBody(prodEntity.getImage5()));


            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            sweetAlertDialog.setTitle(getString(R.string.loading));
            sweetAlertDialog.setTitleText("");
            sweetAlertDialog.setContentText("");
            sweetAlertDialog.show();
            call = myApiServices.editProduct(fileToUpload, map);
            call.enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    sweetAlertDialog.dismiss();
                    try {
                        Boolean.valueOf(((ServicesResponse) response.body()).isSuccess());

                    } catch (Exception e) {
                        e.printStackTrace();
                        //remoteCallback.onSuccess(Boolean.valueOf(true));
                    }
                    onBackPressed();
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    t.printStackTrace();
                    sweetAlertDialog.dismiss();
                    onBackPressed();
                    //  remoteCallback.onSuccess(Boolean.valueOf(true));
                }
            });
        } catch (Exception e222) {
            e222.printStackTrace();
        }
    }


    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }
}
