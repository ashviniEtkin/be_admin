package com.orangebyte.bahetiadmin.modules.admin;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.expandableview.ExpandableRelativeLayout;
import com.orangebyte.bahetiadmin.modules.entities.BrandEntity;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.OfferEntity;
import com.orangebyte.bahetiadmin.modules.entities.ProductEntity;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.viewmodels.BrandViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.BrandViewModel.BrandFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels.Factory;
import com.orangebyte.bahetiadmin.modules.viewmodels.OfferViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.OfferViewModel.OfferFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.ProductViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.ProductViewModel.ProductFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel.SubCategoryFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import com.pdfjet.Single;
import com.squareup.picasso.Picasso;

import im.delight.android.webview.BuildConfig;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddOffer extends AppCompatActivity implements LifecycleRegistryOwner {
    int RESULT_LOAD_IMG = 360;
    Button add;
    List<SubCategoryEntity> allSubCat = new ArrayList();
    List<BrandEntity> brandEntityList = new ArrayList();
    Spinner brand_spinner;
    List<CategoryEntity> categoryEntityList = new ArrayList();
    Spinner category_spinner;
    ExpandableRelativeLayout date_expandble_view;
    LinearLayout discount_row;
    EditText discount_type;
    List<ProductEntity> fbProductEntityList = new ArrayList();
    List<SubCategoryEntity> fbSubCategoryEntityList = new ArrayList();
    Spinner fb_category_spinner;
    LinearLayout fb_container;
    Spinner fb_product_spinner;
    Spinner fb_subcategory_spinner;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    int mDay;
    int mMonth;
    private OnDateSetListener mSeleteDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            AddOffer.this.select_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    private OnDateSetListener mStartDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            AddOffer.this.start_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    private OnDateSetListener mToDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            AddOffer.this.to_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    int mYear;
    TextView model_nm;
    ImageView offer_image;
    EditText offer_title;
    Spinner offer_type;
    List<ProductEntity> productEntityList = new ArrayList();
    ExpandableRelativeLayout product_expandble_view;
    Spinner product_spinner;
    ImageButton reset;
    ImageButton reset_brand;
    TextView select_date;
    Uri selectedImageUri = null;
    TextView start_date;
    List<SubCategoryEntity> subCategoryEntityList = new ArrayList();
    Spinner subcategory_spinner;
    TextView to_date;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_offer);
        BahetiEnterprises.setActBar(getResources().getString(R.string.add_offers), this, false);
        final ProductViewModel viewModel3 = (ProductViewModel) ViewModelProviders.of(this, new ProductFactory()).get(ProductViewModel.class);
        initView();
        setCatgoryToSpinner();
        this.add.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                AddOffer.this.initModelView();
            }
        });
        this.category_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (AddOffer.this.allSubCat.isEmpty()) {
                    AddOffer.this.setSubCatgoryToSpinner(((CategoryEntity) AddOffer.this.categoryEntityList.get(i)).getId() + BuildConfig.VERSION_NAME);
                } else {
                    AddOffer.this.setToSubCatSpinner(AddOffer.this.allSubCat, ((CategoryEntity) AddOffer.this.categoryEntityList.get(i)).getId() + BuildConfig.VERSION_NAME);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.subcategory_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                AddOffer.this.setProductToSpinner();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.product_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                AddOffer.this.model_nm.setText(((ProductEntity) AddOffer.this.productEntityList.get(i)).getModel_no());
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.fb_category_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (AddOffer.this.allSubCat.isEmpty()) {
                    AddOffer.this.setFbSubCatgoryToSpinner(((CategoryEntity) AddOffer.this.categoryEntityList.get(i)).getId() + BuildConfig.VERSION_NAME);
                } else {
                    AddOffer.this.setToFbSubCatSpinner(AddOffer.this.allSubCat, ((CategoryEntity) AddOffer.this.categoryEntityList.get(i)).getId() + BuildConfig.VERSION_NAME);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.fb_subcategory_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    ProductEntity pe = new ProductEntity();
                    pe.setCat_id(((CategoryEntity) AddOffer.this.categoryEntityList.get(AddOffer.this.fb_category_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
                    pe.setSub_cat_id(((SubCategoryEntity) AddOffer.this.fbSubCategoryEntityList.get(i)).getId() + BuildConfig.VERSION_NAME);
                    pe.setBrand_id("null");
                    viewModel3.getProduct(pe).observe(AddOffer.this, new Observer<List<ProductEntity>>() {
                        public void onChanged(@Nullable List<ProductEntity> productEntityList) {
                            if (productEntityList.isEmpty()) {
                                AddOffer.this.setFbProductDataToSpinner(new ArrayList());
                            } else {
                                AddOffer.this.setFbProductDataToSpinner(productEntityList);
                            }
                        }
                    });
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.fb_product_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.offer_type.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i1, long l) {
                if (i1 == 3) {
                    AddOffer.this.fb_container.setVisibility(0);
                    AddOffer.this.discount_row.setVisibility(8);
                    List<String> catList = new ArrayList();
                    try {
                        for (CategoryEntity categoryEntity : AddOffer.this.categoryEntityList) {
                            catList.add(categoryEntity.getName());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    AddOffer.this.fb_category_spinner.setAdapter(new ArrayAdapter(AddOffer.this.getApplicationContext(), R.layout.spinner_layout, catList));
                    return;
                }
                AddOffer.this.fb_container.setVisibility(8);
                AddOffer.this.discount_row.setVisibility(0);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    public void initModelView() {
        OfferViewModel viewModel = (OfferViewModel) ViewModelProviders.of(this, new OfferFactory()).get(OfferViewModel.class);
        OfferEntity oe = new OfferEntity();
        if (BahetiEnterprises.checkSpinner(this.category_spinner)) {
            oe.setCat_id("null");
        } else {
            oe.setCat_id(((CategoryEntity) this.categoryEntityList.get(this.category_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
        }
        if (BahetiEnterprises.checkSpinner(this.subcategory_spinner)) {
            oe.setSub_cat_id("null");
        } else {
            oe.setSub_cat_id(((SubCategoryEntity) this.subCategoryEntityList.get(this.subcategory_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
        }
        if (BahetiEnterprises.checkSpinner(this.product_spinner)) {
            oe.setProduct_id("null");
            oe.setBrand_id("null");
        } else {
            oe.setProduct_id(((ProductEntity) this.productEntityList.get(this.product_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
            oe.setBrand_id(((ProductEntity) this.productEntityList.get(this.product_spinner.getSelectedItemPosition())).getBrand_id() + BuildConfig.VERSION_NAME);
        }
        if (this.offer_type.getSelectedItemPosition() == 3) {
            oe.setFb_cat(((CategoryEntity) this.categoryEntityList.get(this.fb_category_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
            oe.setFb_sub_cat(((SubCategoryEntity) this.fbSubCategoryEntityList.get(this.fb_subcategory_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
            oe.setFb_product_id(((ProductEntity) this.fbProductEntityList.get(this.fb_product_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
            oe.setDiscount("-");
        } else {
            oe.setFb_cat(BuildConfig.VERSION_NAME);
            oe.setFb_sub_cat(BuildConfig.VERSION_NAME);
            oe.setFb_product_id(BuildConfig.VERSION_NAME);
            oe.setDiscount(this.discount_type.getText().toString());
        }
        oe.setStatus("1");
        oe.setTitle(this.offer_title.getText().toString());
        oe.setOffer_type(this.offer_type.getSelectedItem().toString());
        oe.setDiscount(this.discount_type.getText().toString());
        oe.setStart_date(BahetiEnterprises.tStampO(this.start_date.getText().toString()) + BuildConfig.VERSION_NAME);
        oe.setTo_date(BahetiEnterprises.tStampO(this.to_date.getText().toString()) + BuildConfig.VERSION_NAME);
        oe.setOffer_image(this.offer_title.getText().toString().replace(Single.space, "_") + System.currentTimeMillis() + ".jpg");
        if (this.selectedImageUri == null) {
            BahetiEnterprises.retShowAlertDialod("Please Choose Image", this);
        } else if (TextUtils.isEmpty(this.offer_title.getText().toString())) {
            BahetiEnterprises.retShowAlertDialod("Please Enter Offer Title", this);
        } else if (TextUtils.isEmpty(this.select_date.getText().toString()) && TextUtils.isEmpty(this.start_date.getText().toString()) && TextUtils.isEmpty(this.to_date.getText().toString())) {
            BahetiEnterprises.retShowAlertDialod("Please Select Date", this);
        } else if (TextUtils.isEmpty(this.start_date.getText().toString()) && !TextUtils.isEmpty(this.to_date.getText().toString())) {
            BahetiEnterprises.retShowAlertDialod("Please From Date", this);
        } else if (TextUtils.isEmpty(this.start_date.getText().toString()) || !TextUtils.isEmpty(this.to_date.getText().toString())) {
            if (!TextUtils.isEmpty(this.start_date.getText().toString()) && !TextUtils.isEmpty(this.to_date.getText().toString())) {
                oe.setStart_date(BahetiEnterprises.tStampO(this.start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                oe.setTo_date(BahetiEnterprises.tStampO(this.to_date.getText().toString()) + BuildConfig.VERSION_NAME);
            } else if (!TextUtils.isEmpty(this.start_date.getText().toString()) && TextUtils.isEmpty(this.to_date.getText().toString())) {
                oe.setStart_date(BahetiEnterprises.tStampO(this.start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                oe.setTo_date(BahetiEnterprises.tStampO(this.start_date.getText().toString()) + BuildConfig.VERSION_NAME);
            } else if (!TextUtils.isEmpty(this.start_date.getText().toString()) || TextUtils.isEmpty(this.to_date.getText().toString())) {
                oe.setStart_date(BahetiEnterprises.tStampO(this.select_date.getText().toString()) + BuildConfig.VERSION_NAME);
                oe.setTo_date(BahetiEnterprises.tStampO(this.select_date.getText().toString()) + BuildConfig.VERSION_NAME);
            } else {
                oe.setStart_date(BahetiEnterprises.tStampO(this.to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                oe.setTo_date(BahetiEnterprises.tStampO(this.to_date.getText().toString()) + BuildConfig.VERSION_NAME);
            }
            addOffer(viewModel, oe);
        } else {
            BahetiEnterprises.retShowAlertDialod("Please To Date", this);
        }
    }

    public void setCatgoryToSpinner() {
        try {
            ((CategoryViewMdels) ViewModelProviders.of(this, new Factory()).get(CategoryViewMdels.class)).getCats().observe(this, new Observer<List<CategoryEntity>>() {
                public void onChanged(@Nullable List<CategoryEntity> categoryEntities) {
                    if (!categoryEntities.isEmpty()) {
                        AddOffer.this.setToSpinner(categoryEntities);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setToFbSubCatSpinner(List<SubCategoryEntity> subCategoryEntities, String catId) {
        this.fbSubCategoryEntityList.clear();
        List<String> catList = new ArrayList();
        try {
            for (SubCategoryEntity subCategoryEntity : subCategoryEntities) {
                if (subCategoryEntity.getCat_id().equalsIgnoreCase(catId)) {
                    catList.add(subCategoryEntity.getSub_cat_name());
                    this.fbSubCategoryEntityList.add(subCategoryEntity);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.fb_subcategory_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    private void setFbProductDataToSpinner(List<ProductEntity> productEntities) {
        this.fbProductEntityList = productEntities;
        List<String> prodLIst = new ArrayList();
        try {
            for (ProductEntity productEntity : productEntities) {
                prodLIst.add(productEntity.getTitle());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.fb_product_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, prodLIst));
    }

    public void setSubCatgoryToSpinner(final String catId) {
        try {
            ((SubCategoryViewModel) ViewModelProviders.of(this, new SubCategoryFactory()).get(SubCategoryViewModel.class)).getSubCats().observe(this, new Observer<List<SubCategoryEntity>>() {
                public void onChanged(@Nullable List<SubCategoryEntity> subCategoryEntities) {
                    if (!subCategoryEntities.isEmpty()) {
                        AddOffer.this.allSubCat = subCategoryEntities;
                        AddOffer.this.setToSubCatSpinner(subCategoryEntities, catId);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setFbSubCatgoryToSpinner(final String catId) {
        try {
            ((SubCategoryViewModel) ViewModelProviders.of(this, new SubCategoryFactory()).get(SubCategoryViewModel.class)).getSubCats().observe(this, new Observer<List<SubCategoryEntity>>() {
                public void onChanged(@Nullable List<SubCategoryEntity> subCategoryEntities) {
                    if (!subCategoryEntities.isEmpty()) {
                        AddOffer.this.setToFbSubCatSpinner(subCategoryEntities, catId);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setProductToSpinner() {
        try {
            ProductViewModel viewModel2 = (ProductViewModel) ViewModelProviders.of(this, new ProductFactory()).get(ProductViewModel.class);
            ProductEntity pe = new ProductEntity();
            if (this.brand_spinner.getSelectedItemPosition() > 0) {
                pe.setBrand_id(((BrandEntity) this.brandEntityList.get(this.brand_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);
                pe.setCat_id(((CategoryEntity) this.categoryEntityList.get(this.category_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
                pe.setSub_cat_id("null");
            } else {
                pe.setSub_cat_id(((SubCategoryEntity) this.subCategoryEntityList.get(this.subcategory_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
                pe.setCat_id(((CategoryEntity) this.categoryEntityList.get(this.category_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
                pe.setBrand_id("null");
            }
            viewModel2.getProduct(pe).observe(this, new Observer<List<ProductEntity>>() {
                public void onChanged(@Nullable List<ProductEntity> productEntityList) {
                    if (productEntityList.isEmpty()) {
                        AddOffer.this.setProductDataToSpinner(new ArrayList());
                    } else {
                        AddOffer.this.setProductDataToSpinner(productEntityList);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setBrandToSpinner(final String catId) {
        try {
            ((BrandViewModel) ViewModelProviders.of(this, new BrandFactory()).get(BrandViewModel.class)).getBrands().observe(this, new Observer<List<BrandEntity>>() {
                public void onChanged(@Nullable List<BrandEntity> brandEntityList) {
                    if (!brandEntityList.isEmpty()) {
                        AddOffer.this.setToBrandSpinner(brandEntityList, catId);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setToSpinner(List<CategoryEntity> categoryEntities) {
        this.categoryEntityList = categoryEntities;
        List<String> catList = new ArrayList();
        try {
            for (CategoryEntity categoryEntity : categoryEntities) {
                catList.add(categoryEntity.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.category_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    private void setProductDataToSpinner(List<ProductEntity> productEntities) {
        this.productEntityList = productEntities;
        List<String> prodLIst = new ArrayList();
        try {
            for (ProductEntity productEntity : productEntities) {
                prodLIst.add(productEntity.getTitle());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.product_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, prodLIst));
    }

    private void setToSubCatSpinner(List<SubCategoryEntity> subCategoryEntities, String catId) {
        this.subCategoryEntityList.clear();
        List<String> catList = new ArrayList();
        try {
            for (SubCategoryEntity subCategoryEntity : subCategoryEntities) {
                if (subCategoryEntity.getCat_id().equalsIgnoreCase(catId)) {
                    catList.add(subCategoryEntity.getSub_cat_name());
                    this.subCategoryEntityList.add(subCategoryEntity);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.subcategory_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    private void setToBrandSpinner(List<BrandEntity> brandEntities, String catId) {
        this.brandEntityList = brandEntities;
        List<String> catList = new ArrayList();
        catList.add("Select");
        try {
            for (BrandEntity subCategoryEntity : brandEntities) {
              //  if (subCategoryEntity.getCat_id().equalsIgnoreCase(catId)) {
                    catList.add(subCategoryEntity.getBrand_nm());
              //  }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.brand_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    public void initView() {
        this.offer_title = (EditText) findViewById(R.id.offer_title);
        this.discount_type = (EditText) findViewById(R.id.discount_type);
        this.category_spinner = (Spinner) findViewById(R.id.category_spinner);
        this.subcategory_spinner = (Spinner) findViewById(R.id.subcategory_spinner);
        this.brand_spinner = (Spinner) findViewById(R.id.brand_spinner);
        this.product_spinner = (Spinner) findViewById(R.id.product_spinner);
        this.offer_type = (Spinner) findViewById(R.id.offer_type);
        this.select_date = (TextView) findViewById(R.id.select_date);
        this.start_date = (TextView) findViewById(R.id.start_date);
        this.to_date = (TextView) findViewById(R.id.to_date);
        this.model_nm = (TextView) findViewById(R.id.model_nm);
        this.offer_image = (ImageView) findViewById(R.id.offer_image);
        this.add = (Button) findViewById(com.orangebyte.pdflibrary.R.id.add);
        Calendar c = Calendar.getInstance();
        this.mYear = c.get(1);
        this.mMonth = c.get(2);
        this.mDay = c.get(5);
        this.discount_row = (LinearLayout) findViewById(R.id.discount_row);
        this.reset_brand = (ImageButton) findViewById(R.id.reset_brand);
        this.reset = (ImageButton) findViewById(R.id.reset);
        this.fb_category_spinner = (Spinner) findViewById(R.id.fb_category_spinner);
        this.fb_subcategory_spinner = (Spinner) findViewById(R.id.fb_subcategory_spinner);
        this.fb_product_spinner = (Spinner) findViewById(R.id.fb_product_spinner);
        this.fb_container = (LinearLayout) findViewById(R.id.fb_container);
        this.fb_container.setVisibility(8);
        this.reset_brand.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                AddOffer.this.brand_spinner.setSelection(0);
                AddOffer.this.setProductToSpinner();
            }
        });
        this.reset.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                AddOffer.this.start_date.setText(BuildConfig.VERSION_NAME);
                AddOffer.this.select_date.setText(BuildConfig.VERSION_NAME);
                AddOffer.this.to_date.setText(BuildConfig.VERSION_NAME);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == this.RESULT_LOAD_IMG && resultCode == -1 && data != null) {
            this.selectedImageUri = data.getData();
            Cursor cursor = managedQuery(this.selectedImageUri, new String[]{"_data"}, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow("_data");
            cursor.moveToFirst();
            String selectedImagePath = cursor.getString(column_index);
            String filePath = BahetiEnterprises.getRealPathFromURI(data.getDataString());
           // Options options = new Options();
            //options.inSampleSize = 8;
          //  this.offer_image.setImageBitmap(BitmapFactory.decodeFile(filePath, options));
            //this.offer_image.setScaleType(ScaleType.FIT_XY);

            Picasso.with(AddOffer.this).load(selectedImageUri).into(offer_image);
            this.offer_image.setScaleType(ScaleType.FIT_XY);
            return;
        }
        Toast.makeText(getApplicationContext(), "you haven't select image", 1).show();
    }

    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, com.orangebyte.pdflibrary.R.styleable.AppCompatTheme_ratingBarStyleIndicator);
        }
    }

    public void chooseImage(View view) {
        if (ActivityCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, com.orangebyte.pdflibrary.R.styleable.AppCompatTheme_ratingBarStyleIndicator);
            return;
        }
        Intent intent = new Intent("android.intent.action.PICK", Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"), this.RESULT_LOAD_IMG);
    }

    public void showProductView(View view) {
        int i = this.category_spinner.getSelectedItemPosition();
        if (this.brandEntityList.isEmpty()) {
            setBrandToSpinner(((CategoryEntity) this.categoryEntityList.get(i)).getId() + BuildConfig.VERSION_NAME);
        } else {
            setToBrandSpinner(this.brandEntityList, ((CategoryEntity) this.categoryEntityList.get(i)).getId() + BuildConfig.VERSION_NAME);
        }
        this.product_expandble_view = (ExpandableRelativeLayout) findViewById(R.id.product_expandble_view);
        this.product_expandble_view.toggle();
    }

    public void showDateView(View view) {
        this.date_expandble_view = (ExpandableRelativeLayout) findViewById(R.id.date_expandble_view);
        this.date_expandble_view.toggle();
    }

    public void showSelectDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this.mSeleteDate, this.mYear, this.mMonth, this.mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    public void showStartDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this.mStartDate, this.mYear, this.mMonth, this.mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    public void showToDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this.mToDate, this.mYear, this.mMonth, this.mDay);
        if (TextUtils.isEmpty(this.start_date.getText().toString())) {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        } else {
            datePickerDialog.getDatePicker().setMinDate(BahetiEnterprises.tStampO(this.start_date.getText().toString()));
        }
        datePickerDialog.show();
    }

    private void addOffer(OfferViewModel viewModel, OfferEntity c) {
        try {
            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, 5);
            sweetAlertDialog.setTitle("Loading");
            sweetAlertDialog.show();
            viewModel.addOffer(1, c, this.selectedImageUri).observe(this, new Observer<List<OfferEntity>>() {
                public void onChanged(@Nullable List<OfferEntity> offerEntities) {
                    if (!offerEntities.isEmpty()) {
                        sweetAlertDialog.dismiss();
                        AddOffer.this.onBackPressed();
                    } else if (offerEntities.size() == 0) {
                        sweetAlertDialog.dismiss();
                    } else {
                        sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
