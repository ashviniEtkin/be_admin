package com.orangebyte.bahetiadmin.modules.admin;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.adapters.NotiCenterAdapter;
import com.orangebyte.bahetiadmin.modules.entities.NotificationCenter;
import com.orangebyte.bahetiadmin.modules.viewmodels.NotiViewModel;

import java.util.ArrayList;
import java.util.List;

public class NotiCenter extends AppCompatActivity implements LifecycleRegistryOwner {

    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    RecyclerView notiList;
    String chk = "";
    String from = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noti_center);
        BahetiEnterprises.setActBar(getString(R.string.Notifications), NotiCenter.this, false);
        chk = getIntent().getStringExtra(BahetiEnterprises.FROM);
        if (chk.equalsIgnoreCase("2")) {
            from = "Manager";
        } else {
            from = "Other";
        }

        notiList = (RecyclerView) findViewById(R.id.notiList);

        NotiViewModel.NotiFactory factory1 = new NotiViewModel.NotiFactory();
        NotiViewModel viewModel1 = ViewModelProviders.of(NotiCenter.this, factory1).get(NotiViewModel.class);


        try {
            viewModel1.getAll().observe(this, new Observer<List<NotificationCenter>>() {
                @Override
                public void onChanged(@Nullable List<NotificationCenter> notificationCenters) {


                    NotiCenterAdapter adapter = new NotiCenterAdapter(getSortedList(notificationCenters), NotiCenter.this);
                    notiList.setLayoutManager(new LinearLayoutManager(NotiCenter.this));
                    notiList.setAdapter(adapter);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public List<NotificationCenter> getSortedList(List<NotificationCenter> notificationCenterList) {

        List<NotificationCenter> notiList = new ArrayList<>();

        for (int i = 0; i < notificationCenterList.size(); i++) {
            if (chk.equalsIgnoreCase("2")) {
                if (notificationCenterList.get(i).getFrom().equalsIgnoreCase("Manager")) {
                    notiList.add(notificationCenterList.get(i));
                }
            } else {
                if (!notificationCenterList.get(i).getFrom().equalsIgnoreCase("Manager")) {
                    notiList.add(notificationCenterList.get(i));
                }
            }
        }
        return notiList;

    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }
}
