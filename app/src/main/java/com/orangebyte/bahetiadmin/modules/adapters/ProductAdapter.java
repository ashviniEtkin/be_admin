package com.orangebyte.bahetiadmin.modules.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.admin.EditProduct;
import com.orangebyte.bahetiadmin.modules.entities.ProductEntity;
import com.orangebyte.bahetiadmin.views.CircleImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends Adapter<ProductAdapter.ProductHolder> {
    Context ctx;
    List<ProductEntity> productEntityList = new ArrayList();

    class ProductHolder extends ViewHolder {
        TextView prod_desc;
        CircleImageView prod_imag;
        TextView prod_model_no;
        TextView prod_price;
        LinearLayout product_container;
        TextView prod_title;

        public ProductHolder(View itemView) {
            super(itemView);
            this.prod_imag = (CircleImageView) itemView.findViewById(R.id.prod_image);
            this.prod_title = (TextView) itemView.findViewById(R.id.product_nm);
            this.prod_desc = (TextView) itemView.findViewById(R.id.product_desc);
            this.prod_model_no = (TextView) itemView.findViewById(R.id.product_model_no);
            this.prod_price = (TextView) itemView.findViewById(R.id.product_price);
            product_container = (LinearLayout) itemView.findViewById(R.id.product_container);
        }
    }

    public ProductAdapter(Context ctx, List<ProductEntity> productEntityList) {
        this.ctx = ctx;
        this.productEntityList = productEntityList;
    }

    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.product_row, parent, false));
    }

    public void onBindViewHolder(ProductHolder holder, final int position) {
        holder.prod_title.setText(((ProductEntity) this.productEntityList.get(position)).getTitle());
        holder.prod_desc.setText(Html.fromHtml(((ProductEntity) this.productEntityList.get(position)).getFeatures().trim()));
        holder.prod_model_no.setText(((ProductEntity) this.productEntityList.get(position)).getModel_no());
        holder.prod_price.setText("Rs.: " + ((ProductEntity) this.productEntityList.get(position)).getPrice());
        Picasso.with(this.ctx).load(Uri.parse(BahetiEnterprises.PRODUCT_IMAGE_URL + ((ProductEntity) this.productEntityList.get(position)).getImage1())).into(holder.prod_imag);

        holder.product_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent prod_intent = new Intent(ctx, EditProduct.class);
                prod_intent.putExtra(BahetiEnterprises.FROM, productEntityList.get(position));
                ctx.startActivity(prod_intent);
            }
        });


    }

    public int getItemCount() {
        return this.productEntityList.size();
    }
}
