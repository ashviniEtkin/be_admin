package com.orangebyte.bahetiadmin.modules.admin;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;


import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.modules.entities.UserEntity;
import com.orangebyte.bahetiadmin.modules.viewmodels.UserViewModel;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;

import java.util.List;

public class AddCustomer extends AppCompatActivity implements LifecycleRegistryOwner {

    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);

    EditText mobile_no, unm, emailId, password, address;
    Button submit;
    RadioGroup rg;
    RadioButton rdo_female, rdo_male;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    SweetAlertDialog sweetAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);
        init();


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  boolean fieldsOK = validate(new EditText[]{unm, emailId, password, mobile_no, address});
                final String email = emailId.getText().toString();

                if (TextUtils.isEmpty(unm.getText().toString())) {
                    unm.setError("Enter Username");
                } /*else if (TextUtils.isEmpty(emailId.getText().toString())) {
                    // BahetiEnterprises.retShowAlertDialod("Enter Email Id", AddCustomer.this);
                    emailId.setError("Enter Email Id");

                } else if (!email.matches(emailPattern)) {
                    Toast.makeText(getApplicationContext(), "Invalid email address", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(password.getText().toString())) {
                    //BahetiEnterprises.retShowAlertDialod("Enter Password", AddCustomer.this);
                    password.setError("Enter Password");
                } */ else if (TextUtils.isEmpty(mobile_no.getText().toString())) {
                    //BahetiEnterprises.retShowAlertDialod("Enter Mobile Number", AddCustomer.this);

                    mobile_no.setError("Enter Mobile Number");
                } else if (mobile_no.getText().toString().length() < 10) {
                    // BahetiEnterprises.retShowAlertDialod("Enter Valid Mobile NUmber", AddCustomer.this);
                    mobile_no.setError("Enter Valid Mobile NUmber");
                } else if (TextUtils.isEmpty(address.getText().toString())) {
                    // BahetiEnterprises.retShowAlertDialod("Enter Username", AddCustomer.this);
                    address.setError("Enter Address");
                } else {
                    int selectedId = rg.getCheckedRadioButtonId();

                    // find the radiobutton by returned id
                    RadioButton gender = (RadioButton) findViewById(selectedId);
                    UserEntity userEntity = new UserEntity();
                    userEntity.setAddress(address.getText().toString());
                    userEntity.setEmail(emailId.getText().toString());
                    userEntity.setMobile_no(mobile_no.getText().toString());
                    userEntity.setUsernm(unm.getText().toString());
                    userEntity.setPassword(mobile_no.getText().toString());
                    userEntity.setGender(gender.getText().toString());
                    userEntity.setRegId("-");
                    ;
                    initViewModels(userEntity);
                }
            }
        });
    }

    private boolean validate(EditText[] fields) {
        for (int i = 0; i < fields.length; i++) {
            EditText currentField = fields[i];
            if (currentField.getText().toString().length() <= 0) {
                return false;
            }
        }
        return true;
    }

    public void initViewModels(UserEntity userEntity) {
        sweetAlertDialog.show();
        UserViewModel.UserFactory factory1 = new UserViewModel.UserFactory();
        UserViewModel viewModel1 = ViewModelProviders.of(AddCustomer.this, factory1).get(UserViewModel.class);
        viewModel1.addUser(userEntity).observe(this, new Observer<List<Boolean>>() {
            @Override
            public void onChanged(@Nullable List<Boolean> booleans) {
                if (booleans.size() > 0) {
                    sweetAlertDialog.dismiss();
                    onBackPressed();
                }
            }
        });

    }

    public void init() {
        mobile_no = (EditText) findViewById(R.id.mobile_no);
        unm = (EditText) findViewById(R.id.unm);
        emailId = (EditText) findViewById(R.id.emailId);
        password = (EditText) findViewById(R.id.password);
        address = (EditText) findViewById(R.id.address);
        submit = (Button) findViewById(R.id.submit);
        rg = (RadioGroup) findViewById(R.id.rg);
        rdo_female = (RadioButton) findViewById(R.id.rdo_female);
        rdo_male = (RadioButton) findViewById(R.id.rdo_male);
        this.sweetAlertDialog = new SweetAlertDialog(this, 5);
        this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
        this.sweetAlertDialog.setTitleText("Loading");
        this.sweetAlertDialog.setCancelable(false);

    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }
}
