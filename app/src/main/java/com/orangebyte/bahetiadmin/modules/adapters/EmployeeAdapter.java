package com.orangebyte.bahetiadmin.modules.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import java.util.ArrayList;
import java.util.List;

public class EmployeeAdapter extends Adapter<EmployeeAdapter.EmployeeHolder> {
    Context ctx;
    List<EmployeeEntity> dataSet = new ArrayList();
    ClickListener listener;
    SharedPreferences sh;
    String post = "";
    public interface ClickListener {
        void onItemClicked(EmployeeEntity employeeEntity);
    }

    public class EmployeeHolder extends ViewHolder {
        ImageButton chng_status;
        TextView emp_design;
        TextView emp_nm;
        TextView emp_pass;
        TextView srl_no;
        LinearLayout password_container;

        public EmployeeHolder(View itemView) {
            super(itemView);
            this.srl_no = (TextView) itemView.findViewById(R.id.srl_no);
            this.emp_nm = (TextView) itemView.findViewById(R.id.emp_nm);
            this.emp_design = (TextView) itemView.findViewById(R.id.emp_design);
            this.chng_status = (ImageButton) itemView.findViewById(R.id.chng_status);
            this.emp_pass = (TextView) itemView.findViewById(R.id.emp_pass);
            password_container = (LinearLayout) itemView.findViewById(R.id.password_container);

        }
    }

    public EmployeeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EmployeeHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.employee_row, parent, false));
    }

    public void onBindViewHolder(EmployeeHolder holder, int position) {

        this.sh = ctx.getSharedPreferences("log", 0);
        this.post = this.sh.getString("post", "");

        holder.srl_no.setText((position + 1) + ".");
        holder.emp_design.setText(((EmployeeEntity) this.dataSet.get(position)).getAssigned_mgr_type());
        holder.emp_nm.setText(((EmployeeEntity) this.dataSet.get(position)).getEmp_nm());
        holder.emp_pass.setText(((EmployeeEntity) this.dataSet.get(position)).getPassword());

        if (post.equalsIgnoreCase("admin")) {
            holder.password_container.setVisibility(View.VISIBLE);
        } else {
            holder.password_container.setVisibility(View.GONE);

        }


        if (((EmployeeEntity) this.dataSet.get(position)).getStatus().equalsIgnoreCase("1")) {
            holder.chng_status.setImageResource(R.drawable.ic_unhide);
            holder.chng_status.setColorFilter(this.ctx.getResources().getColor(R.color.app_color));
            return;
        }
        holder.chng_status.setImageResource(R.drawable.ic_hide);
        holder.chng_status.setColorFilter(this.ctx.getResources().getColor(R.color.black));



    }

    public int getItemCount() {
        return this.dataSet.size();
    }

    public void setItems(List<EmployeeEntity> itemsList) {
        this.dataSet.clear();
        this.dataSet.addAll(itemsList);
        notifyDataSetChanged();
    }

    public EmployeeAdapter(ClickListener listener, Context ctx) {
        setHasStableIds(true);
        this.listener = listener;
        this.ctx = ctx;
    }
}
