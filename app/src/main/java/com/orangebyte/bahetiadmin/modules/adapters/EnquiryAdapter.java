package com.orangebyte.bahetiadmin.modules.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.admin.EnquiryDetails;
import com.orangebyte.bahetiadmin.modules.entities.EnquiryEntity;

import java.io.Serializable;
import java.util.List;

public class EnquiryAdapter extends Adapter<EnquiryAdapter.EnquiryHolder> {
    int chk;
    Context ctx;
    List<EnquiryEntity> enquiryEntity;
    CheckBox cb;

    public class EnquiryHolder extends ViewHolder {
        TextView asign_to;
        TextView comp_date;
        TextView comp_desc;
        TextView comp_status;
        CardView container_id;
        TextView cust_nm;
        TextView service_request_no;
        LinearLayout service_no_container,btn_layout;
        CheckBox checkb;
        public EnquiryHolder(View itemView) {
            super(itemView);
            this.cust_nm = (TextView) itemView.findViewById(R.id.cust_nm);
            this.comp_desc = (TextView) itemView.findViewById(R.id.comp_desc);
            this.comp_date = (TextView) itemView.findViewById(R.id.comp_date);
            this.comp_status = (TextView) itemView.findViewById(R.id.comp_status);
            this.container_id = (CardView) itemView.findViewById(R.id.container_id);
            this.asign_to = (TextView) itemView.findViewById(R.id.asign_to);
            service_no_container = (LinearLayout) itemView.findViewById(R.id.service_no_container);
            service_request_no = (TextView) itemView.findViewById(R.id.service_request_no);
            service_no_container.setVisibility(View.VISIBLE);
            this.checkb = (CheckBox) itemView.findViewById(R.id.checkb);
            this.btn_layout = (LinearLayout) itemView.findViewById(R.id.btn_layout);

        }
    }

    public EnquiryAdapter(List<EnquiryEntity> enquiryEntity, Context ctx, int chk, CheckBox cb) {
        this.enquiryEntity = enquiryEntity;
        this.ctx = ctx;
        this.chk = chk;
        this.cb = cb;
    }

    public EnquiryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EnquiryHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.complaint_row, parent, false));
    }

    public void onBindViewHolder(EnquiryHolder holder, final int position) {
        holder.cust_nm.setText(((EnquiryEntity) this.enquiryEntity.get(position)).getUsernm());
        holder.comp_desc.setText(((EnquiryEntity) this.enquiryEntity.get(position)).getDescrip());
        holder.comp_date.setText(BahetiEnterprises.retDate(((EnquiryEntity) this.enquiryEntity.get(position)).getOndate()));
        holder.comp_status.setText(((EnquiryEntity) this.enquiryEntity.get(position)).getStatus());
        holder.service_request_no.setText(enquiryEntity.get(position).getCom_no());


        BahetiEnterprises.setData(((EnquiryEntity) this.enquiryEntity.get(position)).getEmp_nm(), holder.asign_to);
        if (((EnquiryEntity) this.enquiryEntity.get(position)).getStatus().equalsIgnoreCase("Pending")) {
            if (BahetiEnterprises.showInRed(Long.parseLong(((EnquiryEntity) this.enquiryEntity.get(position)).getOndate()))) {
                holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.red_900));
            } else {
                holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.red_100));
            }
        } else if (((EnquiryEntity) this.enquiryEntity.get(position)).getStatus().equalsIgnoreCase("On Hold")) {
            holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.green_100));
        } else if (((EnquiryEntity) this.enquiryEntity.get(position)).getStatus().equalsIgnoreCase("In Progress")) {
            holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.teal_100));
        } else {
            holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.amber_200));
        }
        holder.container_id.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(EnquiryAdapter.this.ctx, EnquiryDetails.class);
                if (EnquiryAdapter.this.chk == 2) {
                    i.putExtra(BahetiEnterprises.FROM, "2");
                }
                i.putExtra(BahetiEnterprises.COMPLAINT_DETAILS, (Serializable) EnquiryAdapter.this.enquiryEntity.get(position));
                EnquiryAdapter.this.ctx.startActivity(i);
            }
        });


        if (this.chk == 0 || this.chk == 2) {
            holder.btn_layout.setVisibility(View.GONE);
            holder.checkb.setVisibility(View.GONE);
        } else {
            holder.btn_layout.setVisibility(View.VISIBLE);
            holder.checkb.setVisibility(View.VISIBLE);

        }




        try {
            holder.checkb.setChecked(((EnquiryEntity) this.enquiryEntity.get(position)).isSelected());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        this.cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                /*if (b = false && checkAll() == true) {
                    cb.setChecked(false);
                    setAllCheck(b);
                }*/

                boolean g = checkedAllSelected(enquiryEntity);
                if (b == false && g == true) {
                    setAllCheck(b);
                } else if (b == false && g == false) {
                    // setAllCheck(b);
                } else if (b == true && g == false) {
                    setAllCheck(b);
                }

            }
        });


        holder.checkb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                enquiryEntity.get(position).setSelected(b);

                boolean g = checkedAllSelected(enquiryEntity);

                if (g) {
                    cb.setChecked(true);
                } else {
                    cb.setChecked(false);
                }
               /* if (checkAll() == false) {
                    cb.setChecked(false);
                }
*/
            }
        });
    }

    public List<EnquiryEntity> getEnquiryEntity() {
        return enquiryEntity;
    }

    public boolean checkedAllSelected(List<EnquiryEntity> enquiryEntityList) {
        boolean s = false;
        int f = 0;
        for (int y = 0; y < enquiryEntityList.size(); y++) {
            if (enquiryEntityList.get(y).isSelected()) {
                f++;
            }
        }

        if (f == enquiryEntityList.size()) {
            return true;
        } else {
            return false;
        }

    }

    public void setAllCheck(boolean a) {
        for (int i = 0; i < this.enquiryEntity.size(); i++) {
            ((EnquiryEntity) this.enquiryEntity.get(i)).setSelected(a);
        }
        notifyDataSetChanged();
    }


    public int getItemCount() {
        return this.enquiryEntity.size();
    }
}
