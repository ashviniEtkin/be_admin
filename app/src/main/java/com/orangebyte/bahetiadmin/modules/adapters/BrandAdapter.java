package com.orangebyte.bahetiadmin.modules.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.admin.AddBrandActivity;
import com.orangebyte.bahetiadmin.modules.entities.BrandEntity;
import com.orangebyte.bahetiadmin.views.CircleImageView;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BrandAdapter extends Adapter<BrandAdapter.BrandHolder> {
    Context ctx;
    List<BrandEntity> dataSet = new ArrayList();
    ClickListener listener;

    public class BrandHolder extends ViewHolder {
        TextView brandName;
        ImageButton chng_status;
        ImageButton del;
        ImageButton edt;
        TextView srl_no,toll_txt;
        CircleImageView brand_logo;
        RecyclerView toll_free_List;

        public BrandHolder(View itemView) {
            super(itemView);
            this.brandName = (TextView) itemView.findViewById(R.id.category_nm);
            this.edt = (ImageButton) itemView.findViewById(R.id.edt);
            this.del = (ImageButton) itemView.findViewById(R.id.del);
            this.chng_status = (ImageButton) itemView.findViewById(R.id.chng_status);
            this.srl_no = (TextView) itemView.findViewById(R.id.srl_no);
            brand_logo = (CircleImageView) itemView.findViewById(R.id.brand_logo);
            toll_txt =(TextView) itemView.findViewById(R.id.toll_txt);
            toll_free_List = (RecyclerView) itemView.findViewById(R.id.toll_free_List);
        }
    }

    public interface ClickListener {
        void onItemClicked(BrandEntity brandEntity);
    }

    public BrandHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BrandHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.brand_row, parent, false));
    }

    public void onBindViewHolder(BrandHolder holder, final int position) {
        holder.srl_no.setText((position + 1) + ".");
        holder.brandName.setText(((BrandEntity) this.dataSet.get(position)).getBrand_nm());
        // holder.toll_free.setText(ctx.getString(R.string.toll_free_no) + " : " + BahetiEnterprises.getData(dataSet.get(position).getToll_free_no()));


        Picasso.with(ctx).load(BahetiEnterprises.Brand_URL + dataSet.get(position).getImg()).placeholder(R.drawable.logo).into(holder.brand_logo);
        ;
        if (((BrandEntity) this.dataSet.get(position)).getStatus().equalsIgnoreCase("1")) {
            holder.chng_status.setImageResource(R.drawable.ic_unhide);
            holder.chng_status.setColorFilter(this.ctx.getResources().getColor(R.color.app_color));
        } else {
            holder.chng_status.setImageResource(R.drawable.ic_hide);
            holder.chng_status.setColorFilter(this.ctx.getResources().getColor(R.color.black));
        }
        holder.edt.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(BrandAdapter.this.ctx, AddBrandActivity.class);
                i.putExtra(BahetiEnterprises.CHK_ADD_UPDATE, "1");
                i.putExtra(BahetiEnterprises.SUBCATEGORY, (Serializable) BrandAdapter.this.dataSet.get(position));
                BrandAdapter.this.ctx.startActivity(i);
            }
        });

        try {
            List<String> toll_freeList = Arrays.asList(dataSet.get(position).getToll_free_no().split(","));
            holder.toll_txt.setVisibility(View.VISIBLE);
            TollFreeAdapter adapter = new TollFreeAdapter(ctx, toll_freeList);
            holder.toll_free_List.setLayoutManager(new LinearLayoutManager(ctx));
            holder.toll_free_List.setAdapter(adapter);
        } catch (Exception e) {
            holder.toll_txt.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }

    public void setItems(List<BrandEntity> itemsList) {
        this.dataSet.clear();
        this.dataSet.addAll(itemsList);
        notifyDataSetChanged();
    }

    public BrandAdapter(ClickListener listener, Context ctx) {
        setHasStableIds(true);
        this.listener = listener;
        this.ctx = ctx;
    }

    public long getItemId(int position) {
        return this.dataSet.size() >= position ? ((BrandEntity) this.dataSet.get(position)).getId() : -1;
    }

    public int getItemCount() {
        return this.dataSet.size();
    }
}
