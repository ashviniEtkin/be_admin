package com.orangebyte.bahetiadmin.modules.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.db.daos.ManagerDAO;
import com.orangebyte.bahetiadmin.modules.entities.ManagerEntity;

import java.util.List;

public class ManagerRepository {
    private static ManagerRepository sInstance;
    private final ManagerDAO managerDAO;
    private final RemoteRepository remoteRepository;

    interface RemoteManagerListener {
        void onCheckResult(boolean z);

        void onRemotReceived(ManagerEntity managerEntity);

        void onRemoteReceived(List<ManagerEntity> list);
    }

    interface TaskListener {
        void onTaskFinished();
    }

    public static ManagerRepository getInstance(RemoteRepository remoteRepository, ManagerDAO managerDAO) {
        if (sInstance == null) {
            sInstance = new ManagerRepository(remoteRepository, managerDAO);
        }
        return sInstance;
    }

    private ManagerRepository(RemoteRepository remoteRepository, ManagerDAO managerDAO) {
        this.remoteRepository = remoteRepository;
        this.managerDAO = managerDAO;
    }

    public LiveData<List<ManagerEntity>> loadMgrsList(final ManagerEntity managerEntity) {
        fetchMgrList(new RemoteManagerListener() {
            public void onRemotReceived(ManagerEntity cmEntity) {
            }

            public void onRemoteReceived(List<ManagerEntity> scmEntities) {
                if (scmEntities != null) {
                    if (managerEntity.getMgr_type().equalsIgnoreCase("null")) {
                        ManagerRepository.this.insertAllTask(scmEntities);
                    }
                }
            }

            public void onCheckResult(boolean b) {
            }
        }, managerEntity);

        if (managerEntity.getMgr_type().equalsIgnoreCase("null"))
            return this.managerDAO.loadAll();
        else {
            return this.managerDAO.getManagetType(managerEntity.getMgr_type());
        }
    }

    public LiveData<List<ManagerEntity>> addUpdateMgr(final ManagerEntity managerEntity, final int chk) {
        addUpdateMgr(managerEntity, chk, new RemoteManagerListener() {
            public void onRemotReceived(ManagerEntity cmEntity) {
            }

            public void onRemoteReceived(List<ManagerEntity> list) {
            }

            public void onCheckResult(boolean b) {
                ManagerEntity me = new ManagerEntity();
                me.setName(managerEntity.getName());
                me.setMgr_type(managerEntity.getMgr_type());
                me.setMobile_no(managerEntity.getMobile_no());
                me.setPassword(managerEntity.getPassword());
                me.setStatus(managerEntity.getStatus());
                if (!(b && chk == 0) && b && chk == 1) {
                    me.setId(managerEntity.getId());
                    ManagerRepository.this.updateMgr(me);
                }
            }
        });
        return this.managerDAO.loadAll();
    }

    public LiveData<List<ManagerEntity>> updateMgrStatus(final ManagerEntity mgr_entity) {
        updateMgrStatus(mgr_entity, new RemoteManagerListener() {
            public void onRemotReceived(ManagerEntity cmEntity) {
            }

            public void onRemoteReceived(List<ManagerEntity> list) {
            }

            public void onCheckResult(boolean b) {
                if (b) {
                    ManagerEntity managerEntity = new ManagerEntity();
                    managerEntity.setId(mgr_entity.getId());
                    managerEntity.setName(mgr_entity.getName());
                    managerEntity.setMgr_type(mgr_entity.getMgr_type());
                    managerEntity.setMobile_no(mgr_entity.getMobile_no());
                    managerEntity.setPassword(mgr_entity.getPassword());
                    managerEntity.setStatus(mgr_entity.getStatus());
                    ManagerRepository.this.updateMgr(managerEntity);
                }
            }
        });
        return this.managerDAO.loadAll();
    }

    public LiveData<List<ManagerEntity>> deleteMgrByID(final ManagerEntity managerEntity) {
        deleteMgrByID(managerEntity, new RemoteManagerListener() {
            public void onRemotReceived(ManagerEntity managerEntity) {
            }

            public void onRemoteReceived(List<ManagerEntity> list) {
            }

            public void onCheckResult(boolean b) {
                if (b) {
                    ManagerEntity me = new ManagerEntity();
                    me.setId(managerEntity.getId());
                    me.setName(managerEntity.getName());
                    me.setMgr_type(managerEntity.getMgr_type());
                    me.setMobile_no(managerEntity.getMobile_no());
                    me.setPassword(managerEntity.getPassword());
                    me.setStatus(managerEntity.getStatus());
                    ManagerRepository.this.deleteMngr(me);
                }
            }
        });
        return this.managerDAO.loadAll();
    }

    private void insertAllTask(List<ManagerEntity> managerEntity) {
        insertManagers(managerEntity, null);
    }

    private void insertManagers(final List<ManagerEntity> managerEntity, @Nullable final TaskListener listener) {
        if (VERSION.SDK_INT >= 3) {
            new AsyncTask<Context, Void, Void>() {
                protected Void doInBackground(Context... params) {
                    ManagerRepository.this.managerDAO.deleteAll();
                    ManagerRepository.this.managerDAO.insertAll(managerEntity);
                    return null;
                }

                protected void onPostExecute(Void aVoid) {
                    if (VERSION.SDK_INT >= 3) {
                        super.onPostExecute(aVoid);
                    }
                    if (listener != null) {
                        listener.onTaskFinished();
                    }
                }
            }.execute(new Context[]{BahetiEnterprises.getInstance()});
        }
    }

    private void insertMgrs(ManagerEntity managerEntity) {
        insertMangr(managerEntity, null);
    }

    private void insertMangr(final ManagerEntity managerEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                ManagerRepository.this.managerDAO.insert(managerEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteMngr(ManagerEntity managerEntity) {
        deleteMgr(managerEntity, null);
    }

    private void deleteMgr(final ManagerEntity managerEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                ManagerRepository.this.managerDAO.delete(managerEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void updateMgr(ManagerEntity managerEntity) {
        updateMgrs(managerEntity, null);
    }

    private void updateMgrs(final ManagerEntity managerEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                ManagerRepository.this.managerDAO.update(managerEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteAllSubCategories() {
        deleteAllMgr(null);
    }

    private void deleteAllMgr(@Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                ManagerRepository.this.managerDAO.deleteAll();
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void fetchMgrList(@NonNull final RemoteManagerListener listener, ManagerEntity managerEntity) {
        this.remoteRepository.getManagerList(new RemoteCallback<List<ManagerEntity>>() {
            public void onSuccess(List<ManagerEntity> response) {
                listener.onRemoteReceived(response);
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        }, managerEntity);
    }

    private void addUpdateMgr(@NonNull ManagerEntity managerEntity, @NonNull int chk, @NonNull final RemoteManagerListener listener) {
        if (chk == 1) {
            this.remoteRepository.updateManager(managerEntity, new RemoteCallback<Boolean>() {
                public void onSuccess(Boolean response) {
                    listener.onCheckResult(response.booleanValue());
                }

                public void onUnauthorized() {
                }

                public void onFailed(Throwable throwable) {
                }
            });
        } else {
            this.remoteRepository.addManager(managerEntity, new RemoteCallback<Boolean>() {
                public void onSuccess(Boolean response) {
                    listener.onCheckResult(response.booleanValue());
                }

                public void onUnauthorized() {
                }

                public void onFailed(Throwable throwable) {
                }
            });
        }
    }

    private void updateMgrStatus(@NonNull ManagerEntity managerEntity, @NonNull final RemoteManagerListener listener) {
        this.remoteRepository.updateManagerStatus(managerEntity, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }

    private void deleteMgrByID(@NonNull ManagerEntity managerEntity, @NonNull final RemoteManagerListener listener) {
        this.remoteRepository.deleteManagerByID(managerEntity, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }
}
