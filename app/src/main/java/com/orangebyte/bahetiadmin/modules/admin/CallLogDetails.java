package com.orangebyte.bahetiadmin.modules.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.employee.CallLogAction;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.squareup.picasso.Picasso;

import im.delight.android.webview.BuildConfig;

public class CallLogDetails extends AppCompatActivity {
    TextView assigned_to;
    Button btn_ok;
    CallLogInstallationDemoEntity callLogInstallationDemoEntity;
    String chk = BuildConfig.VERSION_NAME;
    TextView comp_desc;
    TextView comp_status;
    TextView cust_address;
    TextView cust_comp_dt;
    TextView cust_mob_no;
    TextView cust_nm;
    TextView prod;
    TextView prod_category;
    TextView prod_model, extra_charges, product_in, additional_desc;
    TextView prod_subcat, paid_type, paid_amt, priority_txt, action_taken_of_st, comp_no;
    public static int chek_backpress = 0;

    ImageView signature_url, proof_url;
    LinearLayout prof_container;
    LinearLayout extra_charges_container;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_log_details);
        BahetiEnterprises.setActBar(getResources().getString(R.string.call_log_details), this, false);
        this.chk = getIntent().getStringExtra(BahetiEnterprises.FROM);
        initView();
        this.callLogInstallationDemoEntity = (CallLogInstallationDemoEntity) getIntent().getSerializableExtra(BahetiEnterprises.COMPLAINT_DETAILS);
        updateData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (chek_backpress == 1) {
            onBackPressed();
            chek_backpress = 0;
        }
    }

    public void initView() {
        extra_charges = (TextView) findViewById(R.id.extra_charges);
        product_in = (TextView) findViewById(R.id.product_in);
        additional_desc = (TextView) findViewById(R.id.additional_desc);
        comp_no = (TextView) findViewById(R.id.comp_no);

        extra_charges_container = (LinearLayout) findViewById(R.id.extra_charges_container);
        prof_container = (LinearLayout) findViewById(R.id.prof_container);
        proof_url = (ImageView) findViewById(R.id.proof_url);
        paid_type = (TextView) findViewById(R.id.paid_type);
        this.cust_nm = (TextView) findViewById(R.id.cust_nm);
        this.cust_mob_no = (TextView) findViewById(R.id.cust_mob_no);
        this.cust_address = (TextView) findViewById(R.id.cust_address);
        this.cust_comp_dt = (TextView) findViewById(R.id.cust_comp_dt);
        this.prod_category = (TextView) findViewById(R.id.prod_category);
        this.prod_subcat = (TextView) findViewById(R.id.prod_subcat);
        this.prod = (TextView) findViewById(R.id.prod);
        this.prod_model = (TextView) findViewById(R.id.prod_model);
        this.comp_desc = (TextView) findViewById(R.id.comp_desc);
        this.comp_status = (TextView) findViewById(R.id.comp_status);
        this.assigned_to = (TextView) findViewById(R.id.assigned_to);
        paid_amt = (TextView) findViewById(R.id.paid_amt);
        signature_url = (ImageView) findViewById(R.id.signature_url);
        priority_txt = (TextView) findViewById(R.id.priority_txt);
        this.btn_ok = (Button) findViewById(R.id.btn_ok);
        action_taken_of_st = (TextView) findViewById(R.id.action_taken_of_st);
        this.comp_desc.setMovementMethod(new ScrollingMovementMethod());
        this.btn_ok.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                try {
                    if (CallLogDetails.this.chk.equalsIgnoreCase("2")) {
                        if (!callLogInstallationDemoEntity.getStatus().equalsIgnoreCase(getResources().getString(R.string.Resolved_Closed))) {
                            Intent enqIntent = new Intent(CallLogDetails.this, CallLogAction.class);
                            enqIntent.putExtra(BahetiEnterprises.FROM, CallLogDetails.this.callLogInstallationDemoEntity);
                            CallLogDetails.this.startActivity(enqIntent);
                        }
                        return;
                    }
                    CallLogDetails.this.onBackPressed();
                } catch (Exception e) {
                    e.printStackTrace();
                    CallLogDetails.this.onBackPressed();
                }
            }
        });
    }

    public void updateData() {
        //comp_no
        BahetiEnterprises.setData(this.callLogInstallationDemoEntity.getCom_no(), this.comp_no);
        BahetiEnterprises.setData(this.callLogInstallationDemoEntity.getUsernm(), this.cust_nm);
        BahetiEnterprises.setData(this.callLogInstallationDemoEntity.getMobile_no(), this.cust_mob_no);
        BahetiEnterprises.setData(this.callLogInstallationDemoEntity.getAddress(), this.cust_address);
        BahetiEnterprises.setData(this.callLogInstallationDemoEntity.getDemodate(), this.cust_comp_dt);
        BahetiEnterprises.setData(this.callLogInstallationDemoEntity.getDescrip(), this.comp_desc);
        BahetiEnterprises.setData(this.callLogInstallationDemoEntity.getStatus(), this.comp_status);
        BahetiEnterprises.setData(this.callLogInstallationDemoEntity.getEmp_nm(), this.assigned_to);
        BahetiEnterprises.setData(this.callLogInstallationDemoEntity.getTitle(), this.prod);
        BahetiEnterprises.setData(this.callLogInstallationDemoEntity.getModel_no(), this.prod_model);
        BahetiEnterprises.setData(this.callLogInstallationDemoEntity.getSub_cat_name(), this.prod_subcat);
        BahetiEnterprises.setData(this.callLogInstallationDemoEntity.getCat_nm(), this.prod_category);
        BahetiEnterprises.setData(callLogInstallationDemoEntity.getPaid_type(), paid_type);
        BahetiEnterprises.setData(callLogInstallationDemoEntity.getPaid_amt(), paid_amt);
        BahetiEnterprises.setData(callLogInstallationDemoEntity.getPriority(), priority_txt);
        BahetiEnterprises.setData(callLogInstallationDemoEntity.getActn_tkn_st(), action_taken_of_st);
        BahetiEnterprises.setData(callLogInstallationDemoEntity.getExtra_charges(), extra_charges);
        BahetiEnterprises.setData(callLogInstallationDemoEntity.getExtra_charges_desc(), additional_desc);
        if (callLogInstallationDemoEntity.getStatus().equalsIgnoreCase(getString(R.string.Resolved_Closed))) {
            ((LinearLayout) findViewById(R.id.sign_container)).setVisibility(View.VISIBLE);
            Picasso.with(CallLogDetails.this).load(BahetiEnterprises.SIGNATURE_URL + callLogInstallationDemoEntity.getSignature_url()).into(signature_url);
            ((LinearLayout) findViewById(R.id.prof_container)).setVisibility(View.VISIBLE);
            Picasso.with(CallLogDetails.this).load(BahetiEnterprises.PROOF_URL + callLogInstallationDemoEntity.getProof_url()).into(proof_url);


        } else {
            ((LinearLayout) findViewById(R.id.sign_container)).setVisibility(View.GONE);
            ((LinearLayout) findViewById(R.id.prof_container)).setVisibility(View.GONE);

        }
    }
}
