package com.orangebyte.bahetiadmin.modules.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.db.daos.CategoryDAO;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.models.Cat;

import java.util.List;

public class CategoryRepository {
    private static CategoryRepository sInstance;
    private final CategoryDAO categoryDAO;
    private final RemoteRepository remoteRepository;

    interface RemoteCategoryListener {
        void onCheckResult(boolean z);

        void onRemoteCategoryReceived(CategoryEntity categoryEntity);

        void onRemoteCategoryReceived(List<CategoryEntity> list);
    }

    interface TaskListener {
        void onTaskFinished();
    }

    public static CategoryRepository getInstance(RemoteRepository remoteRepository, CategoryDAO categoryDAO) {
        if (sInstance == null) {
            sInstance = new CategoryRepository(remoteRepository, categoryDAO);
        }
        return sInstance;
    }

    private CategoryRepository(RemoteRepository remoteRepository, CategoryDAO categoryDAO) {
        this.remoteRepository = remoteRepository;
        this.categoryDAO = categoryDAO;
    }

    public LiveData<List<CategoryEntity>> loadList() {
        fetchCategoryList(new RemoteCategoryListener() {
            public void onRemoteCategoryReceived(CategoryEntity cmEntity) {
            }

            public void onRemoteCategoryReceived(List<CategoryEntity> cmEntities) {
                if (cmEntities != null) {
                    CategoryRepository.this.insertAllTask(cmEntities);
                }
            }

            public void onCheckResult(boolean b) {
            }
        });
        return this.categoryDAO.loadAll();
    }

    public LiveData<List<CategoryEntity>> addCatgory(final Cat c, final int chk) {
        addCate(c, chk, new RemoteCategoryListener() {
            public void onRemoteCategoryReceived(CategoryEntity cmEntity) {
            }

            public void onRemoteCategoryReceived(List<CategoryEntity> list) {
            }

            public void onCheckResult(boolean b) {
                CategoryEntity cm = new CategoryEntity();
                cm.setName(c.getCat_nm());
                cm.setAssign_to(c.getAssign_to());
                cm.setStatus(c.getStatus());
                if (b && chk == 0) {
                    CategoryRepository.this.insertTask(cm);
                } else if (b && chk == 1) {
                    cm.setId(Long.parseLong(c.getId()));
                    CategoryRepository.this.updateTask(cm);
                }
            }
        });
        return this.categoryDAO.loadAll();
    }

    public LiveData<List<CategoryEntity>> updateCatStatus(final Cat c) {
        updateCatStatus(c, new RemoteCategoryListener() {
            public void onRemoteCategoryReceived(CategoryEntity cmEntity) {
            }

            public void onRemoteCategoryReceived(List<CategoryEntity> list) {
            }

            public void onCheckResult(boolean b) {
                if (b) {
                    CategoryEntity cm = new CategoryEntity();
                    cm.setId(Long.parseLong(c.getId()));
                    cm.setName(c.getCat_nm());
                    cm.setAssign_to(c.getAssign_to());
                    cm.setStatus(c.getStatus());
                    CategoryRepository.this.updateTask(cm);
                }
            }
        });
        return this.categoryDAO.loadAll();
    }

    public LiveData<List<CategoryEntity>> deleteCategoryByID(final Cat c) {
        deleteCategoryByID(c, new RemoteCategoryListener() {
            public void onRemoteCategoryReceived(CategoryEntity cmEntity) {
            }

            public void onRemoteCategoryReceived(List<CategoryEntity> list) {
            }

            public void onCheckResult(boolean b) {
                if (b) {
                    CategoryEntity cm = new CategoryEntity();
                    cm.setId(Long.parseLong(c.getId()));
                    cm.setName(c.getCat_nm());
                    cm.setAssign_to(c.getAssign_to());
                    cm.setStatus(c.getStatus());
                    CategoryRepository.this.deleteTask(cm);
                }
            }
        });
        return this.categoryDAO.loadAll();
    }

    private void insertAllTask(List<CategoryEntity> catEntity) {
        insertAllTask(catEntity, null);
    }

    private void insertAllTask(final List<CategoryEntity> catEntity, @Nullable final TaskListener listener) {
        if (VERSION.SDK_INT >= 3) {
            new AsyncTask<Context, Void, Void>() {
                protected Void doInBackground(Context... params) {
                    CategoryRepository.this.categoryDAO.deleteAll();
                    CategoryRepository.this.categoryDAO.insertAll(catEntity);
                    return null;
                }

                protected void onPostExecute(Void aVoid) {
                    if (VERSION.SDK_INT >= 3) {
                        super.onPostExecute(aVoid);
                    }
                    if (listener != null) {
                        listener.onTaskFinished();
                    }
                }
            }.execute(new Context[]{BahetiEnterprises.getInstance()});
        }
    }

    private void insertTask(CategoryEntity catEntity) {
        insertTask(catEntity, null);
    }

    private void insertTask(final CategoryEntity categoryEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                CategoryRepository.this.categoryDAO.insert(categoryEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteTask(CategoryEntity categoryEntity) {
        deleteTask(categoryEntity, null);
    }

    private void deleteTask(final CategoryEntity categoryEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                CategoryRepository.this.categoryDAO.delete(categoryEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void updateTask(CategoryEntity categoryEntity) {
        updateTask(categoryEntity, null);
    }

    private void updateTask(final CategoryEntity categoryEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                CategoryRepository.this.categoryDAO.update(categoryEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteAllTask() {
        deleteAllTask(null);
    }

    private void deleteAllTask(@Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                CategoryRepository.this.categoryDAO.deleteAll();
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void fetchCategoryList(@NonNull final RemoteCategoryListener listener) {
        this.remoteRepository.getCategoryList(new RemoteCallback<List<CategoryEntity>>() {
            public void onSuccess(List<CategoryEntity> response) {
                listener.onRemoteCategoryReceived((List) response);
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }

    private void addCate(@NonNull Cat c, @NonNull int chk, @NonNull final RemoteCategoryListener listener) {
        this.remoteRepository.addCat(c, chk, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }

    private void updateCatStatus(@NonNull Cat c, @NonNull final RemoteCategoryListener listener) {
        this.remoteRepository.updateCatStatus(c, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }

    private void deleteCategoryByID(@NonNull Cat c, @NonNull final RemoteCategoryListener listener) {
        this.remoteRepository.deleteCategoryByID(c, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }
}
