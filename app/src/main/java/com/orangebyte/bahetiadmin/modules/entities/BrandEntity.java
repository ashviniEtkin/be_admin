package com.orangebyte.bahetiadmin.modules.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

import com.google.gson.annotations.SerializedName;
import com.orangebyte.bahetiadmin.modules.models.BrandModel;

import java.io.Serializable;

@Entity(tableName = "brand")
public class BrandEntity extends IdentifiableEntity implements Serializable {
    @SerializedName("brand_nm")
    @ColumnInfo(name = "brand_nm")
    private String brand_nm;
    @SerializedName("cat_id")
    @ColumnInfo(name = "cat_id")
    private String cat_id;
    @SerializedName("status")
    @ColumnInfo(name = "status")
    private String status;

    @SerializedName("toll_free_no")
    @ColumnInfo(name = "toll_free_no")
    String toll_free_no;

    @SerializedName("img")
    @ColumnInfo(name = "img")
    String img;


    @SerializedName("address")
    @ColumnInfo(name = "address")
    String address;


    public /* bridge */ /* synthetic */ long getId() {
        return super.getId();
    }

    public /* bridge */ /* synthetic */ void setId(long j) {
        super.setId(j);
    }

    public String getBrand_nm() {
        return this.brand_nm;
    }

    public String getCat_id() {
        return this.cat_id;
    }

    @Ignore
    public BrandEntity(long id, String brand_nm, String cat_id, String status, String toll_free_no, String img, String address) {
        this.id = id;
        this.brand_nm = brand_nm;
        this.cat_id = cat_id;
        this.status = status;
        this.toll_free_no = toll_free_no;
        this.img = img;
        this.address = address;
    }

    @Ignore
    public BrandEntity(BrandModel bm) {
        this.id = bm.getId();
        this.brand_nm = bm.getBrand_nm();
        this.cat_id = bm.getCat_id();
        this.status = bm.getStatus();
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getToll_free_no() {
        return toll_free_no;
    }

    public void setToll_free_no(String toll_free_no) {
        this.toll_free_no = toll_free_no;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public BrandEntity() {
    }

    public void setBrand_nm(String brand_nm) {
        this.brand_nm = brand_nm;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CategoryEntity)) {
            return false;
        }
        BrandEntity that = (BrandEntity) o;
        if (this.brand_nm != null) {
            return this.brand_nm.equals(that.brand_nm);
        }
        if (that.brand_nm != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (int) (this.id ^ (this.id >>> 32));
    }
}
