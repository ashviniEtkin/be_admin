package com.orangebyte.bahetiadmin.modules.viewmodels;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider.NewInstanceFactory;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.modules.db.local.AppDatabase;
import com.orangebyte.bahetiadmin.modules.db.local.DatabaseCreator;
import com.orangebyte.bahetiadmin.modules.entities.ManagerEntity;
import com.orangebyte.bahetiadmin.modules.repository.ManagerRepository;
import com.orangebyte.bahetiadmin.modules.repository.RemoteRepository;
import com.orangebyte.bahetiadmin.modules.ui.AllContract.ManagerDataStreams;

import java.util.List;

import im.delight.android.webview.BuildConfig;

public class ManagerViewModel extends ViewModel implements ManagerDataStreams {
    private ManagerEntity managerEntity;
    private ManagerRepository managerRepository;
    private LiveData<List<ManagerEntity>> managersObservable;

    public static class ManagerFactory extends NewInstanceFactory {
        ManagerRepository mManagerRepository;

        public ManagerFactory() {
            this(null, null);
        }

        public ManagerFactory(AppDatabase appDatabase) {
            this(ManagerRepository.getInstance(RemoteRepository.getInstance(), appDatabase.managerDao()), BuildConfig.VERSION_NAME);
        }

        public ManagerFactory(@Nullable ManagerRepository mManagerRepository, @Nullable String abc) {
            this.mManagerRepository = mManagerRepository;
        }

        public <T extends ViewModel> T create(Class<T> cls) {
            return (T) new ManagerViewModel(this.mManagerRepository);
        }
    }

    public ManagerViewModel() {
        this(null);
    }

    public ManagerViewModel(@Nullable ManagerRepository managerRepository) {
        if (managerRepository != null) {
            this.managerRepository = managerRepository;
        }
        this.managersObservable = subscribeToLocalData(this.managerEntity);
    }

    public LiveData<List<ManagerEntity>> getManager(ManagerEntity managerEntity) {
        this.managerEntity = managerEntity;
        return subscribeToLocalData(managerEntity);
    }

    public LiveData<List<ManagerEntity>> addMgr(final ManagerEntity managerEntity, final int chk) {
        return Transformations.switchMap(DatabaseCreator.getInstance().isDatabaseCreated(), new Function<Boolean, LiveData<List<ManagerEntity>>>() {
            public LiveData<List<ManagerEntity>> apply(Boolean isDbCreated) {
                if (!Boolean.TRUE.equals(isDbCreated)) {
                    return null;
                }
                ManagerViewModel.this.onDatabaseCreated();
                return ManagerViewModel.this.addUpdateManager(managerEntity, chk);
            }
        });
    }

    public LiveData<List<ManagerEntity>> updateMgrStatus(ManagerEntity managerEntity) {
        onDatabaseCreated();
        return updtMgrsStatus(managerEntity);
    }

    public LiveData<List<ManagerEntity>> deleteMgrById(ManagerEntity managerEntity) {
        onDatabaseCreated();
        return deletemgrsByID(managerEntity);
    }

    private LiveData<List<ManagerEntity>> subscribeToLocalData(final ManagerEntity managerEntity) {
        return Transformations.switchMap(DatabaseCreator.getInstance().isDatabaseCreated(), new Function<Boolean, LiveData<List<ManagerEntity>>>() {
            public LiveData<List<ManagerEntity>> apply(Boolean isDbCreated) {
                if (!Boolean.TRUE.equals(isDbCreated)) {
                    return null;
                }
                ManagerViewModel.this.onDatabaseCreated();
                return ManagerViewModel.this.subscribeManagerObservable(managerEntity);
            }
        });
    }

    private LiveData<List<ManagerEntity>> subscribeManagerObservable(ManagerEntity managerEntity) {
        return this.managerRepository.loadMgrsList(managerEntity);
    }

    private LiveData<List<ManagerEntity>> addUpdateManager(ManagerEntity managerEntity, int chk) {
        return this.managerRepository.addUpdateMgr(managerEntity, chk);
    }

    private LiveData<List<ManagerEntity>> updtMgrsStatus(ManagerEntity managerEntity) {
        return this.managerRepository.updateMgrStatus(managerEntity);
    }

    private LiveData<List<ManagerEntity>> deletemgrsByID(ManagerEntity c) {
        return this.managerRepository.deleteMgrByID(c);
    }

    private void onDatabaseCreated() {
        AppDatabase appDatabase = DatabaseCreator.getInstance().getDatabase();
        try {
            if (this.managerRepository == null) {
                this.managerRepository = ManagerRepository.getInstance(RemoteRepository.getInstance(), appDatabase.managerDao());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
