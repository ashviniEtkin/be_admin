package com.orangebyte.bahetiadmin.modules.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

import com.google.gson.annotations.SerializedName;
import com.orangebyte.bahetiadmin.modules.models.OfferModel;

import java.io.Serializable;

@Entity(tableName = "offers")
public class OfferEntity extends IdentifiableEntity implements OfferModel, Serializable {
    @SerializedName("brand_id")
    @ColumnInfo(name = "brand_id")
    String brand_id;
    @SerializedName("cat_id")
    @ColumnInfo(name = "cat_id")
    String cat_id;
    @SerializedName("discount")
    @ColumnInfo(name = "discount")
    String discount;
    @SerializedName("fb_cat")
    @ColumnInfo(name = "fb_cat")
    String fb_cat;
    @SerializedName("fb_product_id")
    @ColumnInfo(name = "fb_product_id")
    String fb_product_id;
    @SerializedName("fb_sub_cat")
    @ColumnInfo(name = "fb_sub_cat")
    String fb_sub_cat;
    @SerializedName("offer_image")
    @ColumnInfo(name = "offer_image")
    String offer_image;
    @SerializedName("offer_type")
    @ColumnInfo(name = "offer_type")
    String offer_type;
    @SerializedName("product_id")
    @ColumnInfo(name = "product_id")
    String product_id;
    @SerializedName("start_date")
    @ColumnInfo(name = "start_date")
    String start_date;
    @SerializedName("status")
    @ColumnInfo(name = "status")
    String status;
    @SerializedName("sub_cat_id")
    @ColumnInfo(name = "sub_cat_id")
    String sub_cat_id;
    @SerializedName("title")
    @ColumnInfo(name = "title")
    String title;
    @SerializedName("to_date")
    @ColumnInfo(name = "to_date")
    String to_date;

    @SerializedName("product_title")
    @ColumnInfo(name = "product_title")
    String product_title;


    @SerializedName("model_no")
    @ColumnInfo(name = "model_no")
    String model_no;

    public /* bridge */ /* synthetic */ long getId() {
        return super.getId();
    }

    public /* bridge */ /* synthetic */ void setId(long j) {
        super.setId(j);
    }

    @Ignore
    public OfferEntity(long id, String title, String cat_id, String sub_cat_id, String brand_id, String product_id, String start_date, String to_date, String offer_type, String discount, String status, String offer_image) {
        this.id = id;
        this.title = title;
        this.cat_id = cat_id;
        this.sub_cat_id = sub_cat_id;
        this.brand_id = brand_id;
        this.product_id = product_id;
        this.start_date = start_date;
        this.to_date = to_date;
        this.offer_type = offer_type;
        this.discount = discount;
        this.status = status;
        this.offer_image = offer_image;
    }

    public OfferEntity() {
    }


    public String getModel_no() {
        return model_no;
    }

    public void setModel_no(String model_no) {
        this.model_no = model_no;
    }

    public String getProduct_title() {
        return product_title;
    }

    public void setProduct_title(String product_title) {
        this.product_title = product_title;
    }

    public String getFb_cat() {
        return this.fb_cat;
    }

    public void setFb_cat(String fb_cat) {
        this.fb_cat = fb_cat;
    }

    public String getFb_sub_cat() {
        return this.fb_sub_cat;
    }

    public void setFb_sub_cat(String fb_sub_cat) {
        this.fb_sub_cat = fb_sub_cat;
    }

    public String getFb_product_id() {
        return this.fb_product_id;
    }

    public void setFb_product_id(String fb_product_id) {
        this.fb_product_id = fb_product_id;
    }

    public String getOffer_image() {
        return this.offer_image;
    }

    public void setOffer_image(String offer_image) {
        this.offer_image = offer_image;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public void setSub_cat_id(String sub_cat_id) {
        this.sub_cat_id = sub_cat_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public void setOffer_type(String offer_type) {
        this.offer_type = offer_type;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getTitle() {
        return this.title;
    }

    public String getCat_id() {
        return this.cat_id;
    }

    public String getSub_cat_id() {
        return this.sub_cat_id;
    }

    public String getBrand_id() {
        return this.brand_id;
    }

    public String getProduct_id() {
        return this.product_id;
    }

    public String getStart_date() {
        return this.start_date;
    }

    public String getTo_date() {
        return this.to_date;
    }

    public String getOffer_type() {
        return this.offer_type;
    }

    public String getDiscount() {
        return this.discount;
    }
}
