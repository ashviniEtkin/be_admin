package com.orangebyte.bahetiadmin.modules.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

import com.google.gson.annotations.SerializedName;
import com.orangebyte.bahetiadmin.modules.models.EmployeeModel;

import java.io.Serializable;

@Entity(tableName = "employee")
public class EmployeeEntity extends IdentifiableEntity implements EmployeeModel, Serializable {
    @SerializedName("assigned_mgr_type")
    @ColumnInfo(name = "assigned_mgr_type")
    private String assigned_mgr_type;
    @SerializedName("emp_nm")
    @ColumnInfo(name = "emp_nm")
    private String emp_nm;
    @SerializedName("mgr_id")
    @ColumnInfo(name = "mgr_id")
    private String mgr_id;
    @SerializedName("mob_no")
    @ColumnInfo(name = "mob_no")
    private String mob_no;
    @SerializedName("status")
    @ColumnInfo(name = "status")
    private String status;


    @SerializedName("password")
    @ColumnInfo(name = "password")
    private String password;


    //incentive

    @SerializedName("incentive")
    @ColumnInfo(name = "incentive")
    private String incentive;

    public EmployeeEntity() {
    }

    public /* bridge */ /* synthetic */ long getId() {
        return super.getId();
    }

    public /* bridge */ /* synthetic */ void setId(long j) {
        super.setId(j);
    }

    @Ignore
    public EmployeeEntity(long id, String emp_nm, String mob_no, String mgr_id, String status, String assigned_mgr_type) {
        this.id = id;
        this.emp_nm = emp_nm;
        this.mob_no = mob_no;
        this.mgr_id = mgr_id;
        this.status = status;
        this.assigned_mgr_type = assigned_mgr_type;
    }

    @Ignore
    public EmployeeEntity(EmployeeModel em) {
        this.id = em.getId();
        this.emp_nm = em.getEmp_nm();
        this.mob_no = em.getMob_no();
        this.mgr_id = em.getMgr_id();
        this.status = em.getStatus();
        this.assigned_mgr_type = em.getAssigned_mgr_type();
    }

    public String getIncentive() {
        return incentive;
    }

    public void setIncentive(String incentive) {
        this.incentive = incentive;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmp_nm() {
        return this.emp_nm;
    }

    public void setEmp_nm(String emp_nm) {
        this.emp_nm = emp_nm;
    }

    public String getMob_no() {
        return this.mob_no;
    }

    public void setMob_no(String mob_no) {
        this.mob_no = mob_no;
    }

    public String getMgr_id() {
        return this.mgr_id;
    }

    public void setMgr_id(String mgr_id) {
        this.mgr_id = mgr_id;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAssigned_mgr_type() {
        return this.assigned_mgr_type;
    }

    public void setAssigned_mgr_type(String assigned_mgr_type) {
        this.assigned_mgr_type = assigned_mgr_type;
    }

    public String getStatus() {
        return this.status;
    }
}
