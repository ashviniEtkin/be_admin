package com.orangebyte.bahetiadmin.modules.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.db.daos.ProductDAO;
import com.orangebyte.bahetiadmin.modules.entities.ProductEntity;

import java.util.List;

public class ProductRepository {
    private static ProductRepository sInstance;
    private final ProductDAO productDAO;
    private final RemoteRepository remoteRepository;

    interface RemoteProductListener {
        void onCheckResult(boolean z);

        void onRemotReceived(ProductEntity productEntity);

        void onRemoteReceived(List<ProductEntity> list);
    }

    interface TaskListener {
        void onTaskFinished();
    }

    public static ProductRepository getInstance(RemoteRepository remoteRepository, ProductDAO productDAO) {
        if (sInstance == null) {
            sInstance = new ProductRepository(remoteRepository, productDAO);
        }
        return sInstance;
    }

    private ProductRepository(RemoteRepository remoteRepository, ProductDAO productDAO) {
        this.remoteRepository = remoteRepository;
        this.productDAO = productDAO;
    }

    public LiveData<List<ProductEntity>> loadProductList(ProductEntity productEntity) {
        final MutableLiveData<List<ProductEntity>> productEntityMutableLiveData = new MutableLiveData();
        fetchProductList(new RemoteProductListener() {
            public void onRemotReceived(ProductEntity cmEntity) {
            }

            public void onRemoteReceived(List<ProductEntity> scmEntities) {
                if (scmEntities != null) {
                    try {
                        ProductRepository.this.insertAllTask(scmEntities);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    productEntityMutableLiveData.setValue(scmEntities);
                }
            }

            public void onCheckResult(boolean b) {
            }
        }, productEntity);
        return productEntityMutableLiveData;
    }

    private void insertAllTask(List<ProductEntity> productEntity) {
        insertProducts(productEntity, null);
    }

    private void insertProducts(final List<ProductEntity> productEntity, @Nullable final TaskListener listener) {
        if (VERSION.SDK_INT >= 3) {
            new AsyncTask<Context, Void, Void>() {
                protected Void doInBackground(Context... params) {
                    ProductRepository.this.productDAO.deleteAll();
                    ProductRepository.this.productDAO.insertAll(productEntity);
                    return null;
                }

                protected void onPostExecute(Void aVoid) {
                    if (VERSION.SDK_INT >= 3) {
                        super.onPostExecute(aVoid);
                    }
                    if (listener != null) {
                        listener.onTaskFinished();
                    }
                }
            }.execute(new Context[]{BahetiEnterprises.getInstance()});
        }
    }

    private void insertMgrs(ProductEntity productEntity) {
        insertProduct(productEntity, null);
    }

    private void insertProduct(final ProductEntity productEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                ProductRepository.this.productDAO.insert(productEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteProd(ProductEntity productEntity) {
        deletProduct(productEntity, null);
    }

    private void deletProduct(final ProductEntity productEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                ProductRepository.this.productDAO.delete(productEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteAllProduct() {
        deleteAllProduct(null);
    }

    private void deleteAllProduct(@Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                ProductRepository.this.productDAO.deleteAll();
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void fetchProductList(@NonNull final RemoteProductListener listener, ProductEntity productEntity) {
        this.remoteRepository.getProduct(productEntity, new RemoteCallback<List<ProductEntity>>() {
            public void onSuccess(List<ProductEntity> response) {
                listener.onRemoteReceived(response);
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }
}
