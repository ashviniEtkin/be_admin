package com.orangebyte.bahetiadmin.modules.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "installationndemo")
public class CallLogInstallationDemoEntity extends IdentifiableEntity implements Serializable {
    @SerializedName("actn_tkn_st")
    @ColumnInfo(name = "actn_tkn_st")
    String actn_tkn_st;
    @SerializedName("address")
    @ColumnInfo(name = "address")
    private String address;
    @SerializedName("assigned_mgr_type")
    @ColumnInfo(name = "assigned_mgr_type")
    private String assigned_mgr_type;
    @SerializedName("cat_id")
    @ColumnInfo(name = "cat_id")
    private String cat_id;
    @SerializedName("cat_nm")
    @ColumnInfo(name = "cat_nm")
    String cat_nm;
    @SerializedName("demodate")
    @ColumnInfo(name = "demodate")
    private String demodate;
    @SerializedName("descrip")
    @ColumnInfo(name = "descrip")
    private String descrip;
    @SerializedName("emp_id")
    @ColumnInfo(name = "emp_id")
    private String emp_id;
    @SerializedName("emp_nm")
    @ColumnInfo(name = "emp_nm")
    private String emp_nm;
    @SerializedName("emp_status")
    @ColumnInfo(name = "emp_status")
    private String emp_status;
    private boolean isSelected;
    @SerializedName("last_atnd_on")
    @ColumnInfo(name = "last_atnd_on")
    String last_atnd_on;
    @SerializedName("mgr_id")
    @ColumnInfo(name = "mgr_id")
    private String mgr_id;
    @SerializedName("mob_no")
    @ColumnInfo(name = "mob_no")
    private String mob_no;
    @SerializedName("mobile_no")
    @ColumnInfo(name = "mobile_no")
    private String mobile_no;
    @SerializedName("model_no")
    @ColumnInfo(name = "model_no")
    String model_no;
    @SerializedName("paid_amt")
    @ColumnInfo(name = "paid_amt")
    String paid_amt;
    @SerializedName("product_id")
    @ColumnInfo(name = "product_id")
    private String product_id;
    @SerializedName("resolved_date")
    @ColumnInfo(name = "resolved_date")
    String resolved_date;
    @SerializedName("status")
    @ColumnInfo(name = "status")
    private String status;
    @SerializedName("sub_cat_name")
    @ColumnInfo(name = "sub_cat_name")
    String sub_cat_name;
    @SerializedName("subcat_id")
    @ColumnInfo(name = "subcat_id")
    private String subcat_id;
    @SerializedName("title")
    @ColumnInfo(name = "title")
    String title;
    @SerializedName("type")
    @ColumnInfo(name = "type")
    private String type;
    @SerializedName("user_id")
    @ColumnInfo(name = "user_id")
    private String user_id;
    @SerializedName("usernm")
    @ColumnInfo(name = "usernm")
    private String usernm;


    @SerializedName("purchase_from")
    @ColumnInfo(name = "purchase_from")
    String purchase_from;


    @SerializedName("invoice_url")
    @ColumnInfo(name = "invoice_url")
    String invoice_url;


    @SerializedName("signature_url")
    @ColumnInfo(name = "signature_url")
    String signature_url;


    @SerializedName("paid_type")
    @ColumnInfo(name = "paid_type")
    String paid_type;

    @SerializedName("com_no")
    @ColumnInfo(name = "com_no")
    private String com_no;


    @SerializedName("priority")
    @ColumnInfo(name = "priority")
    String priority;


    @SerializedName("extra_charges_desc")
    @ColumnInfo(name = "extra_charges_desc")
    String extra_charges_desc;

    @SerializedName("extra_charges")
    @ColumnInfo(name = "extra_charges")
    String extra_charges;


    @SerializedName("proof_url")
    @ColumnInfo(name = "proof_url")
    String proof_url;


    @SerializedName("product_in")
    @ColumnInfo(name = "product_in")
    String product_in;


    @SerializedName("incentive")
    @ColumnInfo(name = "incentive")
    private String incentive;


    public String getIncentive() {
        return incentive;
    }

    public void setIncentive(String incentive) {
        this.incentive = incentive;
    }

    public String getProduct_in() {
        return product_in;
    }

    public void setProduct_in(String product_in) {
        this.product_in = product_in;
    }

    public String getProof_url() {
        return proof_url;
    }

    public void setProof_url(String proof_url) {
        this.proof_url = proof_url;
    }


    public String getExtra_charges_desc() {
        return extra_charges_desc;
    }

    public void setExtra_charges_desc(String extra_charges_desc) {
        this.extra_charges_desc = extra_charges_desc;
    }

    public String getExtra_charges() {
        return extra_charges;
    }

    public void setExtra_charges(String extra_charges) {
        this.extra_charges = extra_charges;
    }


    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }


    public String getCom_no() {
        return com_no;
    }

    public void setCom_no(String com_no) {
        this.com_no = com_no;
    }

    public String getPurchase_from() {
        return purchase_from;
    }

    public void setPurchase_from(String purchase_from) {
        this.purchase_from = purchase_from;
    }

    public String getInvoice_url() {
        return invoice_url;
    }

    public void setInvoice_url(String invoice_url) {
        this.invoice_url = invoice_url;
    }

    public String getSignature_url() {
        return signature_url;
    }

    public String getPaid_type() {
        return paid_type;
    }

    public void setPaid_type(String paid_type) {
        this.paid_type = paid_type;
    }

    public void setSignature_url(String signature_url) {
        this.signature_url = signature_url;
    }

    public /* bridge */ /* synthetic */ long getId() {
        return super.getId();
    }

    public /* bridge */ /* synthetic */ void setId(long j) {
        super.setId(j);
    }

    public String getLast_atnd_on() {
        return this.last_atnd_on;
    }

    public void setLast_atnd_on(String last_atnd_on) {
        this.last_atnd_on = last_atnd_on;
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }

    public String getActn_tkn_st() {
        return this.actn_tkn_st;
    }

    public void setActn_tkn_st(String actn_tkn_st) {
        this.actn_tkn_st = actn_tkn_st;
    }

    @Ignore
    public CallLogInstallationDemoEntity(long id, String product_id, String user_id, String descrip, String demodate, String status, String cat_id, String subcat_id, String type) {
        this.id = id;
        this.product_id = product_id;
        this.user_id = user_id;
        this.descrip = descrip;
        this.demodate = demodate;
        this.status = status;
        this.cat_id = cat_id;
        this.subcat_id = subcat_id;
        this.type = type;
    }

    public CallLogInstallationDemoEntity() {
    }

    public String getPaid_amt() {
        return this.paid_amt;
    }

    public void setPaid_amt(String paid_amt) {
        this.paid_amt = paid_amt;
    }

    public String getResolved_date() {
        return this.resolved_date;
    }

    public void setResolved_date(String resolved_date) {
        this.resolved_date = resolved_date;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getModel_no() {
        return this.model_no;
    }

    public void setModel_no(String model_no) {
        this.model_no = model_no;
    }

    public String getCat_nm() {
        return this.cat_nm;
    }

    public void setCat_nm(String cat_nm) {
        this.cat_nm = cat_nm;
    }

    public String getSub_cat_name() {
        return this.sub_cat_name;
    }

    public void setSub_cat_name(String sub_cat_name) {
        this.sub_cat_name = sub_cat_name;
    }

    public String getEmp_id() {
        return this.emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getEmp_nm() {
        return this.emp_nm;
    }

    public void setEmp_nm(String emp_nm) {
        this.emp_nm = emp_nm;
    }

    public String getMob_no() {
        return this.mob_no;
    }

    public void setMob_no(String mob_no) {
        this.mob_no = mob_no;
    }

    public String getEmp_status() {
        return this.emp_status;
    }

    public void setEmp_status(String emp_status) {
        this.emp_status = emp_status;
    }

    public String getMgr_id() {
        return this.mgr_id;
    }

    public void setMgr_id(String mgr_id) {
        this.mgr_id = mgr_id;
    }

    public String getAssigned_mgr_type() {
        return this.assigned_mgr_type;
    }

    public void setAssigned_mgr_type(String assigned_mgr_type) {
        this.assigned_mgr_type = assigned_mgr_type;
    }

    public String getUsernm() {
        return this.usernm;
    }

    public void setUsernm(String usernm) {
        this.usernm = usernm;
    }

    public String getMobile_no() {
        return this.mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCat_id() {
        return this.cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getSubcat_id() {
        return this.subcat_id;
    }

    public void setSubcat_id(String subcat_id) {
        this.subcat_id = subcat_id;
    }

    public String getProduct_id() {
        return this.product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getUser_id() {
        return this.user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDescrip() {
        return this.descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public String getDemodate() {
        return this.demodate;
    }

    public void setDemodate(String demodate) {
        this.demodate = demodate;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
