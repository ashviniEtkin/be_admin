package com.orangebyte.bahetiadmin.modules.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashvini on 4/15/2018.
 */

public class TypeAdapter extends RecyclerView.Adapter<TypeAdapter.TypeHolder> {


    List<String> arraylist = new ArrayList<>();

    public TypeAdapter(List<String> arraylist) {
        this.arraylist = arraylist;
    }

    @Override
    public TypeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TypeHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.status_check, parent, false));

    }

    @Override
    public void onBindViewHolder(TypeHolder holder, int position) {
        holder.status_txt.setText(arraylist.get(position));
    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public class TypeHolder extends RecyclerView.ViewHolder {

        TextView status_txt;

        public TypeHolder(View itemView) {
            super(itemView);
            status_txt = (TextView) itemView.findViewById(R.id.status_txt);

        }
    }

}
