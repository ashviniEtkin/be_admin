package com.orangebyte.bahetiadmin.modules.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.admin.AddCategory;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CategoryAdapter extends Adapter<CategoryAdapter.CategoryHolder> {
    Context ctx;
    List<CategoryEntity> dataSet = new ArrayList();
    ClickListener listener;

    public class CategoryHolder extends ViewHolder {
        TextView category_nm;
        ImageButton chng_status;
        ImageButton del;
        ImageButton edt;
        TextView srl_no;

        public CategoryHolder(View itemView) {
            super(itemView);
            this.category_nm = (TextView) itemView.findViewById(R.id.category_nm);
            this.edt = (ImageButton) itemView.findViewById(R.id.edt);
            this.del = (ImageButton) itemView.findViewById(R.id.del);
            this.chng_status = (ImageButton) itemView.findViewById(R.id.chng_status);
            this.srl_no = (TextView) itemView.findViewById(R.id.srl_no);
        }
    }

    public interface ClickListener {
        void onItemClicked(CategoryEntity categoryEntity);
    }

    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.category_row, parent, false));
    }

    public void onBindViewHolder(CategoryHolder holder, final int position) {
        holder.srl_no.setText((position + 1) + ".");
        holder.category_nm.setText(((CategoryEntity) this.dataSet.get(position)).getName());
        if (((CategoryEntity) this.dataSet.get(position)).getStatus().equalsIgnoreCase("1")) {
            holder.chng_status.setImageResource(R.drawable.ic_unhide);
            holder.chng_status.setColorFilter(this.ctx.getResources().getColor(R.color.app_color));
        } else {
            holder.chng_status.setImageResource(R.drawable.ic_hide);
            holder.chng_status.setColorFilter(this.ctx.getResources().getColor(R.color.black));
        }
        holder.edt.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(CategoryAdapter.this.ctx, AddCategory.class);
                i.putExtra(BahetiEnterprises.CHK_ADD_UPDATE, "1");
                i.putExtra(BahetiEnterprises.CATEGORY, (Serializable) CategoryAdapter.this.dataSet.get(position));
                CategoryAdapter.this.ctx.startActivity(i);
            }
        });
        holder.del.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
            }
        });
    }

    public void setItems(List<CategoryEntity> itemsList) {
        this.dataSet.clear();
        this.dataSet.addAll(itemsList);
        notifyDataSetChanged();
    }

    public CategoryAdapter(ClickListener listener, Context ctx) {
        setHasStableIds(true);
        this.listener = listener;
        this.ctx = ctx;
    }

    public long getItemId(int position) {
        return this.dataSet.size() >= position ? ((CategoryEntity) this.dataSet.get(position)).getId() : -1;
    }

    public int getItemCount() {
        return this.dataSet.size();
    }

    public void logout() {
        Builder dlg = new Builder(this.ctx);
        dlg.setTitle(com.orangebyte.pdflibrary.R.string.app_name);
        dlg.setMessage("Are You Sure You Want To Delete ?");
        dlg.setIcon(R.drawable.logo);
        dlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        dlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        dlg.create().show();
    }
}
