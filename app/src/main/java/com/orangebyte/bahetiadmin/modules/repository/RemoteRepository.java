package com.orangebyte.bahetiadmin.modules.repository;

import android.net.Uri;

import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.applications.PathUtil;
import com.orangebyte.bahetiadmin.modules.entities.BrandEntity;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.entities.EnquiryEntity;
import com.orangebyte.bahetiadmin.modules.entities.FeedbackEntity;
import com.orangebyte.bahetiadmin.modules.entities.ManagerEntity;
import com.orangebyte.bahetiadmin.modules.entities.NotificationEntity;
import com.orangebyte.bahetiadmin.modules.entities.OfferEntity;
import com.orangebyte.bahetiadmin.modules.entities.ProductEntity;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.UserEntity;
import com.orangebyte.bahetiadmin.modules.entities.WorkSheetEntity;
import com.orangebyte.bahetiadmin.modules.models.Cat;
import com.orangebyte.bahetiadmin.modules.models.ServicesResponse;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import im.delight.android.webview.BuildConfig;
import okhttp3.MediaType;
import okhttp3.MultipartBody.Part;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RemoteRepository {
    private static RemoteRepository sInstance;

    public static RemoteRepository getInstance() {
        if (sInstance == null) {
            sInstance = new RemoteRepository();
        }
        return sInstance;
    }

    public void getCategoryList(RemoteCallback<List<CategoryEntity>> listener) {
        getAllCategory(listener);
    }

    public boolean addCat(Cat c, int chl, RemoteCallback<Boolean> callback) {
        if (chl == 1) {
            return updateCategory(c, callback);
        }
        return addCategory(c, callback);
    }

    public void updateCatStatus(Cat c, RemoteCallback<Boolean> callback) {
        updateCatStatus1(c, callback);
    }

    public void deleteCategoryByID(Cat cat, RemoteCallback<Boolean> callback) {
        deletCategoryById(cat, callback);
    }

    private void getAllCategory(final RemoteCallback<List<CategoryEntity>> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/allCat/").getCategory().enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(((ServicesResponse) response.body()).getCategory());
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean addCategory(Cat cat, final RemoteCallback<Boolean> listener) {
        final boolean[] res = new boolean[]{false};
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/addCat/").addCategory(cat).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    boolean result = ((ServicesResponse) response.body()).isSuccess();
                    res[0] = result;
                    listener.onSuccess(Boolean.valueOf(result));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    res[0] = false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res[0];
    }

    private boolean updateCategory(Cat cat, final RemoteCallback<Boolean> listener) {
        final boolean[] res = new boolean[]{false};
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/updateCatDetails/").updateCategory(cat).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    boolean result = ((ServicesResponse) response.body()).isSuccess();
                    res[0] = result;
                    listener.onSuccess(Boolean.valueOf(result));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    res[0] = false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res[0];
    }

    private void updateCatStatus1(Cat cat, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/changeCatStatus/").updateCatStatus(cat).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deletCategoryById(Cat actObj, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/delCat/").deletCategoryById(actObj).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getSubCategoryList(final RemoteCallback<List<SubCategoryEntity>> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/allSubCat/").getSubCategory().enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(((ServicesResponse) response.body()).getSubcategory());
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addSubCat(SubCategoryEntity subcat, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/addSubCat/").addSubCategory(subcat).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateSubCategory(SubCategoryEntity subcat, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/updateSubCatDetails/").updateSubCategory(subcat).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateSubCatStatus(SubCategoryEntity subcat, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/changeSubCatStatus/").updateSubCatStatus(subcat).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteSubCategoryByID(SubCategoryEntity actObj, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/delSubCat/").deletSubCategoryById(actObj).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getBrandyList(final RemoteCallback<List<BrandEntity>> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/allBrand/").getBrand().enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(((ServicesResponse) response.body()).getBrandEntities());
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addBrand(BrandEntity brandEntity, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/addBrand/").addBrand(brandEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateBrand(BrandEntity brandEntity, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/updateBrandDetails/").updateBrand(brandEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateBrandStatus(BrandEntity brandEntity, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/changeBrandStatus/").updateBrandStatus(brandEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteBrandByID(BrandEntity actObj, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/delBrand/").deleteBrandById(actObj).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getManagerList(final RemoteCallback<List<ManagerEntity>> listener, ManagerEntity managerEntity) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/allMgr/").getMgrs(managerEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(((ServicesResponse) response.body()).getManagerEntityList());
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addManager(ManagerEntity managerEntity, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/addMgr/").addMgr(managerEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateManager(ManagerEntity managerEntity, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/updateManagerDetails/").updateMgr(managerEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateManagerStatus(ManagerEntity managerEntity, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/changeManagerStatus/").updateMgrStatus(managerEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteManagerByID(ManagerEntity actObj, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/delManager/").deleteMgrById(actObj).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getEmployeeList(EmployeeEntity em, final RemoteCallback<List<EmployeeEntity>> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/allEmp/")
                    .getEmployee(em)
                    .enqueue(new Callback<ServicesResponse>() {
                        public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                            listener.onSuccess(((ServicesResponse) response.body()).getEmployeeEntityList());
                        }

                        public void onFailure(Call<ServicesResponse> call, Throwable t) {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addEmployee(EmployeeEntity employeeEntity, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/addEmp/").addEmployee(employeeEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateEmployee(EmployeeEntity employeeEntity, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/updateEmpDetails/").updateEmployee(employeeEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateEmployeeStatus(EmployeeEntity employeeEntity, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/changeEmpStatus/").updateEmployeeStatus(employeeEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteEmployeeByID(EmployeeEntity actObj, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/delEmp/").deleteEmployeeById(actObj).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getOffers(OfferEntity offerEntity, final RemoteCallback<List<OfferEntity>> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/activeOffers/")
                    .getOffers(offerEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(((ServicesResponse) response.body()).getOfferEntityList());
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addOffer(int chk, OfferEntity offerEntity, Uri fileUri, RemoteCallback<Boolean> listener) {
        File file = null;
        RequestBody requestBody = null;
        Part fileToUpload = null;
        try {
            file = new File(PathUtil.getPath(BahetiEnterprises.getInstance(), fileUri));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            fileToUpload = Part.createFormData("uploaded_file", file.getName(), requestBody);
        } catch (Exception e22) {
            e22.printStackTrace();
        }
        ApiServices myApiServices = ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/addOffer/");
        try {
            Call<ServicesResponse> call;
            HashMap<String, RequestBody> map = new HashMap();
            map.put("title", BahetiEnterprises.retRequestBody(offerEntity.getTitle()));
            map.put("cat_id", BahetiEnterprises.retRequestBody(offerEntity.getCat_id()));
            map.put("sub_cat_id", BahetiEnterprises.retRequestBody(offerEntity.getSub_cat_id()));
            map.put("brand_id", BahetiEnterprises.retRequestBody(offerEntity.getBrand_id()));
            map.put("product_id", BahetiEnterprises.retRequestBody(offerEntity.getProduct_id()));
            map.put(Param.START_DATE, BahetiEnterprises.retRequestBody(offerEntity.getStart_date()));
            map.put("to_date", BahetiEnterprises.retRequestBody(offerEntity.getTo_date()));
            map.put("offer_type", BahetiEnterprises.retRequestBody(offerEntity.getOffer_type()));
            map.put("discount", BahetiEnterprises.retRequestBody(offerEntity.getDiscount()));
            map.put("status", BahetiEnterprises.retRequestBody(offerEntity.getStatus()));
            map.put("offer_image", BahetiEnterprises.retRequestBody(offerEntity.getOffer_image()));
            map.put("id", BahetiEnterprises.retRequestBody(offerEntity.getId() + BuildConfig.VERSION_NAME));
            map.put("fb_cat", BahetiEnterprises.retRequestBody(offerEntity.getFb_cat()));
            map.put("fb_sub_cat", BahetiEnterprises.retRequestBody(offerEntity.getFb_sub_cat()));
            map.put("fb_product_id", BahetiEnterprises.retRequestBody(offerEntity.getFb_product_id()));
            if (chk == 1) {
                call = myApiServices.addOffer(fileToUpload, map);
            } else {
                call = myApiServices.editOffer(fileToUpload, map);
            }
            final RemoteCallback<Boolean> remoteCallback = listener;
            call.enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    try {
                        remoteCallback.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                    } catch (Exception e) {
                        e.printStackTrace();
                        remoteCallback.onSuccess(Boolean.valueOf(true));
                    }
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    t.printStackTrace();
                    remoteCallback.onSuccess(Boolean.valueOf(true));
                }
            });
        } catch (Exception e222) {
            e222.printStackTrace();
        }
    }


    public void addBrand(int chk, BrandEntity brandEntity, Uri fileUri, RemoteCallback<Boolean> listener) {
        File file = null;
        RequestBody requestBody = null;
        Part fileToUpload = null;
        try {
            file = new File(PathUtil.getPath(BahetiEnterprises.getInstance(), fileUri));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            fileToUpload = Part.createFormData("uploaded_file", file.getName(), requestBody);
        } catch (Exception e22) {
            e22.printStackTrace();
        }
        ApiServices myApiServices = ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/addOffer/");
        try {
            Call<ServicesResponse> call;
            HashMap<String, RequestBody> map = new HashMap();
            map.put("brand_nm", BahetiEnterprises.retRequestBody(brandEntity.getBrand_nm()));
            map.put("cat_id", BahetiEnterprises.retRequestBody(brandEntity.getCat_id()));
            map.put("status", BahetiEnterprises.retRequestBody(brandEntity.getStatus()));
            map.put("toll_free_no", BahetiEnterprises.retRequestBody(brandEntity.getToll_free_no()));
            map.put("img", BahetiEnterprises.retRequestBody(brandEntity.getImg()));
            map.put("id", BahetiEnterprises.retRequestBody(brandEntity.getId() + ""));
            map.put("address", BahetiEnterprises.retRequestBody(brandEntity.getAddress()));
            if (chk == 1) {
                call = myApiServices.editBrand(fileToUpload, map);
            } else {
                call = myApiServices.addBrand(fileToUpload, map);
            }
            final RemoteCallback<Boolean> remoteCallback = listener;
            call.enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    try {
                        remoteCallback.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                    } catch (Exception e) {
                        e.printStackTrace();
                        remoteCallback.onSuccess(Boolean.valueOf(true));
                    }
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    t.printStackTrace();
                    remoteCallback.onSuccess(Boolean.valueOf(true));
                }
            });
        } catch (Exception e222) {
            e222.printStackTrace();
        }
    }


    public void updateOffer(OfferEntity offerEntity, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/updateOffStatus/").updateOffer(offerEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getProduct(ProductEntity productEntity, final RemoteCallback<List<ProductEntity>> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/allProd/").getProduct(productEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(((ServicesResponse) response.body()).getProductEntityList());
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getComplaints(ComplaintEntity complaintEntity, final RemoteCallback<List<ComplaintEntity>> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/getComplaintServices/")
                    .getComplaint(complaintEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(((ServicesResponse) response.body()).getComplaintEntityList());
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void assigEMp(int chk, ComplaintEntity complaintEntity, final RemoteCallback<Boolean> listener) {
        Call<ServicesResponse> call = null;
        ApiServices myApiServices = ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/assignComplaint/");
        if (chk == 1) {
            try {
                call = myApiServices.assignEmpTOComplaint(complaintEntity);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        } else if (chk == 2) {
            //assignEmpToMultipleComplaint
            try {
                call = myApiServices.assignEmpToMultipleComplaint(complaintEntity);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        } else if (chk == 3) {
            try {
                call = myApiServices.lodgeComplaint(complaintEntity);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                call = myApiServices.actionTaken(complaintEntity);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        call.enqueue(new Callback<ServicesResponse>() {
            public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
            }

            public void onFailure(Call<ServicesResponse> call, Throwable t) {
                listener.onSuccess(true);

                t.printStackTrace();
            }
        });
    }

    public void enquiryActionTaken(EnquiryEntity enquiryEntity, int chk, final RemoteCallback<Boolean> listener) {


        Call<ServicesResponse> call = null;
        ApiServices myApiServices = ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/updtJobStatusCompl/");

        if (chk == 1) {
            // for updating status
            call = myApiServices.actionTaken(enquiryEntity);

        } else if (chk == 2) {
            // for multiple employees
            call = myApiServices.assignEmpToEnquiry(enquiryEntity);

        } else if (chk == 3) {
            call = myApiServices.assignSingleEnquiry(enquiryEntity);
        } else if (chk == 4) {
            call = myApiServices.updateEnqStatus(enquiryEntity);

        }
        try {
            call.enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    try {
                        listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void getCallLogNInstallation(CallLogInstallationDemoEntity callLogInstallationDemoEntity, final RemoteCallback<List<CallLogInstallationDemoEntity>> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/getCallLogsNInstallationLog/")
                    .getCallInsatll(callLogInstallationDemoEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(((ServicesResponse) response.body()).getCallLogInstallationDemoEntityList());
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addCallLogNInstallation(int chk, CallLogInstallationDemoEntity callLogInstallationDemoEntity, final RemoteCallback<Boolean> listener) {
        ApiServices myApiServices = ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/addInstallationNdemo/");
        Call<ServicesResponse> call = null;
        if (chk == 1) {
            try {
                call = myApiServices.addCallInsatllDemo(callLogInstallationDemoEntity);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        } else if (chk == 2) {
            call = myApiServices.actionTaken(callLogInstallationDemoEntity);
        } else if (chk == 3) {
            call = myApiServices.assignEmp(callLogInstallationDemoEntity);
        } else {
            call = myApiServices.assignCallLog(callLogInstallationDemoEntity);
        }
        call.enqueue(new Callback<ServicesResponse>() {
            public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                try {
                    listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(Call<ServicesResponse> call, Throwable t) {
            }
        });
    }

    public void getEnquiry(EnquiryEntity enquiryEntity, final RemoteCallback<List<EnquiryEntity>> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/getEnquiry/").getEnquiry(enquiryEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(((ServicesResponse) response.body()).getEnquiryEntityList());
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getFeedback(FeedbackEntity feedbackEntity, final RemoteCallback<List<FeedbackEntity>> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/getFeedback/").getFeedback(feedbackEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(((ServicesResponse) response.body()).getFeedbackEntityList());
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getEmpWorksheet(WorkSheetEntity workSheetEntity, final RemoteCallback<List<WorkSheetEntity>> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/getWorksheet/").getEmployeeWorksheet(workSheetEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(((ServicesResponse) response.body()).getWorkSheetEntityList());
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addNotification(NotificationEntity notificationEntity, final RemoteCallback<Boolean> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/addNotifn/").addNoti(notificationEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    try {
                        listener.onSuccess(Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getUserByname(UserEntity userEntity, final RemoteCallback<List<UserEntity>> listener) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/getUsersByName/").getUsersByName(userEntity).enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    listener.onSuccess(((ServicesResponse) response.body()).getUserEntityList());
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addUser(final UserEntity userEntity, final RemoteCallback<Boolean> listener) {

        ApiServices myApiServices = ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/addUser/");
        try {
            Call<ServicesResponse> call = myApiServices.addUser(userEntity);
            call.enqueue(new Callback<ServicesResponse>() {
                @Override
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    boolean success = response.body().isSuccess();
                    listener.onSuccess(success);
                }

                @Override
                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    t.printStackTrace();
                }


            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
