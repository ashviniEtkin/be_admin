package com.orangebyte.bahetiadmin.modules.employee;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.expandableview.ExpandableRelativeLayout;
import com.orangebyte.bahetiadmin.modules.adapters.CallLogInstallAdapter;
import com.orangebyte.bahetiadmin.modules.admin.AddInstalltionDemo;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener.OnItemClickListener;
import com.orangebyte.bahetiadmin.modules.viewmodels.CallLogInstallViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.CallLogInstallViewModel.CallLogInstallFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels.Factory;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel.EmployeeFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel.SubCategoryFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import im.delight.android.webview.BuildConfig;

public class EmployeeCallLog extends AppCompatActivity implements LifecycleRegistryOwner {
    public static CheckBox all_cb;
    CallLogInstallAdapter adapter;
    List<SubCategoryEntity> allSubCat = new ArrayList();
    Button btn_assign;
    Button btn_new;
    Button btn_resolved;
    List<CallLogInstallationDemoEntity> callLogInstallationDemoEntities = new ArrayList();
    RecyclerView calllog_install_recycler;
    LinearLayout calllog_oper_contnr;
    List<CategoryEntity> categoryEntityList = new ArrayList();
    Spinner category_spinner;
    String chk = BuildConfig.VERSION_NAME;
    ExpandableRelativeLayout date_expandble_view;
    List<EmployeeEntity> employeeEntityList = new ArrayList();
    CallLogInstallFactory factory;
    TextView in_progress;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    LinearLayout list_container;
    int mDay;
    int mMonth;
    private OnDateSetListener mSeleteDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            EmployeeCallLog.this.select_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    private OnDateSetListener mStartDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            EmployeeCallLog.this.start_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    private OnDateSetListener mToDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            EmployeeCallLog.this.to_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    int mYear;
    TextView no_result_fount;
    TextView on_hold;
    TextView pending;
    String post = BuildConfig.VERSION_NAME;
    ImageButton reset;
    TextView resolved_closed;
    TextView select_date;
    SharedPreferences sh;
    Button show;
    TextView start_date;
    Spinner status_spinner;
    List<SubCategoryEntity> subCategoryEntityList = new ArrayList();
    TableRow sub_cat_container;
    Spinner subcategory_spinner;
    TextView to_date;
    TextView total;
    Spinner type_spinner;
    String uid = BuildConfig.VERSION_NAME;
    CallLogInstallViewModel viewModel;
    CallLogInstallationDemoEntity callLogInstallationDemoEntity = null;// new CallLogInstallationDemoEntity();
    EditText etd_cust_mobile_no;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_call_log);
        BahetiEnterprises.setActBar(getResources().getString(R.string.call_log_n_demo), this, false);
        this.chk = getIntent().getStringExtra(BahetiEnterprises.INTNT);
        init();
        setCatgoryToSpinner();
        this.category_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (EmployeeCallLog.this.allSubCat.isEmpty()) {
                    EmployeeCallLog.this.setSubCatgoryToSpinner();
                } else if (i > 0) {
                    EmployeeCallLog.this.setToSubCatSpinner(BahetiEnterprises.getIdFromCatgory(EmployeeCallLog.this.categoryEntityList, EmployeeCallLog.this.category_spinner.getSelectedItem().toString()) + BuildConfig.VERSION_NAME);
                    EmployeeCallLog.this.sub_cat_container.setVisibility(0);
                } else {
                    EmployeeCallLog.this.sub_cat_container.setVisibility(8);
                }
                if (i == 0) {
                    EmployeeCallLog.this.sub_cat_container.setVisibility(8);
                } else {
                    EmployeeCallLog.this.sub_cat_container.setVisibility(0);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.show.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                callLogInstallationDemoEntity = new CallLogInstallationDemoEntity();
                if (EmployeeCallLog.this.status_spinner.getSelectedItemPosition() == 0) {
                    callLogInstallationDemoEntity.setStatus("null");
                } else {
                    callLogInstallationDemoEntity.setStatus(EmployeeCallLog.this.status_spinner.getSelectedItem().toString());
                }
                if (EmployeeCallLog.this.category_spinner.getSelectedItemPosition() == 0) {
                    callLogInstallationDemoEntity.setCat_id("null");
                } else {
                    try {
                        if (BahetiEnterprises.checkSpinner(EmployeeCallLog.this.category_spinner)) {
                            callLogInstallationDemoEntity.setCat_id("null");
                        } else {
                            callLogInstallationDemoEntity.setCat_id(((CategoryEntity) EmployeeCallLog.this.categoryEntityList.get(EmployeeCallLog.this.category_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        callLogInstallationDemoEntity.setCat_id("null");
                    }
                }
                if (EmployeeCallLog.this.subcategory_spinner.getSelectedItemPosition() == 0) {
                    callLogInstallationDemoEntity.setSubcat_id("null");
                } else {
                    try {
                        if (BahetiEnterprises.checkSpinner(EmployeeCallLog.this.subcategory_spinner)) {
                            callLogInstallationDemoEntity.setSubcat_id("null");
                        } else {
                            callLogInstallationDemoEntity.setSubcat_id(((SubCategoryEntity) EmployeeCallLog.this.subCategoryEntityList.get(EmployeeCallLog.this.subcategory_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        callLogInstallationDemoEntity.setSubcat_id("null");
                    }
                }

                if (TextUtils.isEmpty(etd_cust_mobile_no.getText().toString())) {
                    callLogInstallationDemoEntity.setUser_id("null");
                } else {
                    callLogInstallationDemoEntity.setUser_id(etd_cust_mobile_no.getText().toString());
                }


                if (EmployeeCallLog.this.type_spinner.getSelectedItemPosition() == 0) {
                    callLogInstallationDemoEntity.setType("null");
                } else {
                    callLogInstallationDemoEntity.setType(EmployeeCallLog.this.type_spinner.getSelectedItem().toString());
                }
                if (!TextUtils.isEmpty(EmployeeCallLog.this.start_date.getText().toString()) && !TextUtils.isEmpty(EmployeeCallLog.this.to_date.getText().toString())) {
                    callLogInstallationDemoEntity.setDemodate(BahetiEnterprises.tStamp(EmployeeCallLog.this.start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    callLogInstallationDemoEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(EmployeeCallLog.this.to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                } else if (!TextUtils.isEmpty(EmployeeCallLog.this.start_date.getText().toString()) && TextUtils.isEmpty(EmployeeCallLog.this.to_date.getText().toString())) {
                    callLogInstallationDemoEntity.setDemodate(BahetiEnterprises.tStamp(EmployeeCallLog.this.start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    callLogInstallationDemoEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(EmployeeCallLog.this.start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                } else if (TextUtils.isEmpty(EmployeeCallLog.this.start_date.getText().toString()) && !TextUtils.isEmpty(EmployeeCallLog.this.to_date.getText().toString())) {
                    callLogInstallationDemoEntity.setDemodate(BahetiEnterprises.tStamp(EmployeeCallLog.this.to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    callLogInstallationDemoEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(EmployeeCallLog.this.to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                } else if (TextUtils.isEmpty(EmployeeCallLog.this.select_date.getText().toString())) {
                    callLogInstallationDemoEntity.setDemodate("null");
                    callLogInstallationDemoEntity.setLast_atnd_on("null");
                } else {
                    callLogInstallationDemoEntity.setDemodate(BahetiEnterprises.tStamp(EmployeeCallLog.this.select_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    callLogInstallationDemoEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(EmployeeCallLog.this.select_date.getText().toString()) + BuildConfig.VERSION_NAME);
                }
                callLogInstallationDemoEntity.setEmp_id(EmployeeCallLog.this.uid);
                EmployeeCallLog.this.initViewModel(callLogInstallationDemoEntity);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        all_cb.setChecked(false);
        if (callLogInstallationDemoEntity != null) {
            initViewModel(callLogInstallationDemoEntity);
        }
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    public void init() {
        etd_cust_mobile_no = (EditText) findViewById(R.id.etd_cust_mobile_no);
        this.no_result_fount = (TextView) findViewById(R.id.no_result_fount);
        this.show = (Button) findViewById(R.id.show);
        this.category_spinner = (Spinner) findViewById(R.id.category_spinner);
        this.subcategory_spinner = (Spinner) findViewById(R.id.subcategory_spinner);
        this.status_spinner = (Spinner) findViewById(R.id.status_spinner);
        this.type_spinner = (Spinner) findViewById(R.id.type_spinner);
        this.calllog_install_recycler = (RecyclerView) findViewById(R.id.calllog_install_recycler);
        this.calllog_oper_contnr = (LinearLayout) findViewById(R.id.calllog_oper_contnr);
        all_cb = (CheckBox) findViewById(R.id.all_cb);
        this.btn_new = (Button) findViewById(R.id.btn_new);
        this.btn_assign = (Button) findViewById(R.id.btn_assign);
        this.btn_resolved = (Button) findViewById(R.id.btn_resolved);
        this.calllog_oper_contnr.setVisibility(8);
        this.sub_cat_container = (TableRow) findViewById(R.id.sub_cat_container);
        this.sub_cat_container.setVisibility(8);
        this.sh = getSharedPreferences("log", 0);
        this.post = this.sh.getString("post", BuildConfig.VERSION_NAME);
        this.uid = this.sh.getString("uid", BuildConfig.VERSION_NAME);
        this.select_date = (TextView) findViewById(R.id.select_date);
        this.start_date = (TextView) findViewById(R.id.start_date);
        this.to_date = (TextView) findViewById(R.id.to_date);
        this.total = (TextView) findViewById(R.id.total);
        this.pending = (TextView) findViewById(R.id.pending);
        this.on_hold = (TextView) findViewById(R.id.on_hold);
        this.in_progress = (TextView) findViewById(R.id.in_progress);
        this.resolved_closed = (TextView) findViewById(R.id.resolved_closed);
        this.no_result_fount = (TextView) findViewById(R.id.no_result_fount);
        this.list_container = (LinearLayout) findViewById(R.id.list_container);
        list_container.setVisibility(View.GONE);

        this.reset = (ImageButton) findViewById(R.id.reset);
        this.factory = new CallLogInstallFactory();
        this.viewModel = (CallLogInstallViewModel) ViewModelProviders.of(this, this.factory).get(CallLogInstallViewModel.class);
        Calendar c = Calendar.getInstance();
        this.mYear = c.get(1);
        this.mMonth = c.get(2);
        this.mDay = c.get(5);

        this.reset.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                EmployeeCallLog.this.start_date.setText(BuildConfig.VERSION_NAME);
                EmployeeCallLog.this.select_date.setText(BuildConfig.VERSION_NAME);
                EmployeeCallLog.this.to_date.setText(BuildConfig.VERSION_NAME);
            }
        });
        this.btn_new.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                EmployeeCallLog.this.startActivity(new Intent(EmployeeCallLog.this, AddInstalltionDemo.class));
            }
        });
        this.btn_assign.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (EmployeeCallLog.this.checkSelection()) {
                    final Dialog d = new Dialog(EmployeeCallLog.this, R.style.Custom_Dialog);
                    d.getWindow().requestFeature(1);
                    d.setContentView(R.layout.assign_emp_complaint);
                    final Spinner empl_spinner = (Spinner) d.findViewById(R.id.employee_spinner);
                    Spinner priority_spinner = (Spinner) d.findViewById(R.id.priority_spinner);
                    ((TableRow) d.findViewById(R.id.priority_cont)).setVisibility(8);
                    EmployeeCallLog.this.setToSpinner(EmployeeCallLog.this.employeeEntityList, empl_spinner);
                    Button btn_assign_transfer1 = (Button) d.findViewById(R.id.btn_assign_transfer);
                    ((Button) d.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            d.dismiss();
                        }
                    });
                    btn_assign_transfer1.setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            if (empl_spinner.getSelectedItemPosition() == 0) {
                                BahetiEnterprises.retShowAlertDialod("Please Select Employee", EmployeeCallLog.this);
                                return;
                            }
                            String allSelectedIds = EmployeeCallLog.this.getSelectedIds(EmployeeCallLog.this.getSelectedCallLog());
                            CallLogInstallationDemoEntity ce = new CallLogInstallationDemoEntity();
                            ce.setUser_id(allSelectedIds);
                            ce.setEmp_id(((EmployeeEntity) EmployeeCallLog.this.employeeEntityList.get(empl_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);
                            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(EmployeeCallLog.this, 5);
                            sweetAlertDialog.setTitle("Loading");
                            EmployeeCallLog.this.viewModel.changeCallLog(ce, 3).observe(EmployeeCallLog.this, new Observer<List<Boolean>>() {
                                public void onChanged(@Nullable List<Boolean> booleen) {
                                    if (!booleen.isEmpty()) {
                                        d.dismiss();
                                        sweetAlertDialog.dismiss();
                                        EmployeeCallLog.this.onBackPressed();
                                    } else if (booleen.size() == 0) {
                                        d.dismiss();
                                        sweetAlertDialog.dismiss();
                                    } else {
                                        sweetAlertDialog.show();
                                    }
                                }
                            });
                        }
                    });
                    d.show();
                    return;
                }
                BahetiEnterprises.retShowAlertDialod("Please Select", EmployeeCallLog.this);
            }
        });
        this.btn_resolved.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (EmployeeCallLog.this.checkSelection()) {
                    String allSelectedIds = EmployeeCallLog.this.getSelectedIds(EmployeeCallLog.this.getSelectedCallLog());
                    CallLogInstallationDemoEntity ce = new CallLogInstallationDemoEntity();
                    ce.setUser_id(allSelectedIds);
                    ce.setStatus(EmployeeCallLog.this.getResources().getString(R.string.Resolved_Closed));
                    EmployeeCallLog.this.resolvedOrAssigned(ce, 2);
                    return;
                }
                BahetiEnterprises.retShowAlertDialod("Please Select", EmployeeCallLog.this);
            }
        });
    }

    public void resolvedOrAssigned(CallLogInstallationDemoEntity callLogInstallationDemoEntity, int chk) {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, 5);
        sweetAlertDialog.setTitle("Loading");
        this.viewModel.changeCallLog(callLogInstallationDemoEntity, chk).observe(this, new Observer<List<Boolean>>() {
            public void onChanged(@Nullable List<Boolean> booleen) {
                if (!booleen.isEmpty()) {
                    sweetAlertDialog.dismiss();
                    EmployeeCallLog.this.onBackPressed();
                } else if (booleen.size() == 0) {
                    sweetAlertDialog.dismiss();
                } else {
                    sweetAlertDialog.show();
                }
            }
        });
    }

    public boolean checkSelection() {
        List<CallLogInstallationDemoEntity> installationDemoEntityList = this.adapter.getCallLogInstallationDemoEntities();
        boolean chk = false;
        int i = 0;
        while (i < installationDemoEntityList.size()) {
            if (((CallLogInstallationDemoEntity) installationDemoEntityList.get(i)).isSelected()) {
                chk = true;
                i = installationDemoEntityList.size();
            } else {
                chk = false;
                i++;
            }
        }
        return chk;
    }

    public List<CallLogInstallationDemoEntity> getSelectedCallLog() {
        List<CallLogInstallationDemoEntity> installationDemoEntityList = this.adapter.getCallLogInstallationDemoEntities();
        List<CallLogInstallationDemoEntity> newList = new ArrayList();
        for (int i = 0; i < installationDemoEntityList.size(); i++) {
            if (((CallLogInstallationDemoEntity) installationDemoEntityList.get(i)).isSelected()) {
                newList.add(installationDemoEntityList.get(i));
            }
        }
        return newList;
    }

    public String getSelectedIds(List<CallLogInstallationDemoEntity> newList) {
        String ids = BuildConfig.VERSION_NAME;
        for (int i = 0; i < newList.size(); i++) {
            ids = ids + ((CallLogInstallationDemoEntity) newList.get(i)).getId() + ",";
        }
        return ids.substring(0, ids.length() - 1);
    }

    public void listeners() {
        this.calllog_install_recycler.addOnItemTouchListener(new RecyclerItemClickListener(this, new OnItemClickListener() {
            public void onItemClick(View view, int position) {
                ((CheckBox) view.findViewById(R.id.checkb)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (!EmployeeCallLog.this.adapter.checkAll()) {
                        }
                    }
                });
            }
        }));
    }

    public void setEmployeeToSpinner() {
        try {
            EmployeeEntity ee = new EmployeeEntity();
            ee.setAssigned_mgr_type(this.post);
            ((EmployeeViewModel) ViewModelProviders.of(this, new EmployeeFactory()).get(EmployeeViewModel.class)).getEmps(ee).observe(this, new Observer<List<EmployeeEntity>>() {
                public void onChanged(@Nullable List<EmployeeEntity> employeeEntities) {
                    if (!employeeEntities.isEmpty()) {
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setToSpinner(List<EmployeeEntity> employeeEntities, Spinner employee_spinner1) {
        this.employeeEntityList = employeeEntities;
        List<String> catList = new ArrayList();
        catList.add("All");
        try {
            for (EmployeeEntity employeeEntity : employeeEntities) {
                catList.add(employeeEntity.getEmp_nm());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        employee_spinner1.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
        setCatgoryToSpinner();
    }

    private void setCatDataToSpinner(List<CategoryEntity> categoryEntities) {
        this.categoryEntityList = categoryEntities;
        List<String> catList = new ArrayList();
        catList.add("All");
        try {
            for (CategoryEntity categoryEntity : categoryEntities) {
                if (this.post.equalsIgnoreCase(getResources().getString(R.string.service_Manager))) {
                    if (categoryEntity.getAssign_to().equalsIgnoreCase("01") || categoryEntity.getAssign_to().equalsIgnoreCase("11")) {
                        catList.add(categoryEntity.getName());
                    }
                } else if (!this.post.equalsIgnoreCase(getResources().getString(R.string.Sales_Manager))) {
                    catList.add(categoryEntity.getName());
                } else if (categoryEntity.getAssign_to().equalsIgnoreCase("10") || categoryEntity.getAssign_to().equalsIgnoreCase("11")) {
                    catList.add(categoryEntity.getName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.category_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
        setSubCatgoryToSpinner();
    }

    private void setToSubCatSpinner(String catId) {
        subCategoryEntityList.clear();

        List<String> catList = new ArrayList();
        catList.add("All");
        try {
            for (SubCategoryEntity subCategoryEntity : this.allSubCat) {
                if (subCategoryEntity.getCat_id().equalsIgnoreCase(catId)) {
                    catList.add(subCategoryEntity.getSub_cat_name());
                    this.subCategoryEntityList.add(subCategoryEntity);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.subcategory_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    public void setSubCatgoryToSpinner() {
        try {
            ((SubCategoryViewModel) ViewModelProviders.of(this, new SubCategoryFactory()).get(SubCategoryViewModel.class)).getSubCats().observe(this, new Observer<List<SubCategoryEntity>>() {
                public void onChanged(@Nullable List<SubCategoryEntity> subCategoryEntities) {
                    if (!subCategoryEntities.isEmpty()) {
                        EmployeeCallLog.this.allSubCat = subCategoryEntities;
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setCatgoryToSpinner() {
        try {
            ((CategoryViewMdels) ViewModelProviders.of(this, new Factory()).get(CategoryViewMdels.class)).getCats().observe(this, new Observer<List<CategoryEntity>>() {
                public void onChanged(@Nullable List<CategoryEntity> categoryEntities) {
                    if (!categoryEntities.isEmpty()) {
                        EmployeeCallLog.this.setCatDataToSpinner(categoryEntities);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void initViewModel(CallLogInstallationDemoEntity callLogInstallationDemoEntity) {
        subscribeToDataStreams((CallLogInstallViewModel) ViewModelProviders.of(this, new CallLogInstallFactory()).get(CallLogInstallViewModel.class), callLogInstallationDemoEntity);
    }

    private void subscribeToDataStreams(CallLogInstallViewModel viewModel, CallLogInstallationDemoEntity callLogInstallationDemoEntity) {
        try {
            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, 5);
            sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
            sweetAlertDialog.setTitleText("Loading");
            sweetAlertDialog.setCancelable(false);
            sweetAlertDialog.show();
            viewModel.getCallLogInstallation(callLogInstallationDemoEntity).observe(this, new Observer<List<CallLogInstallationDemoEntity>>() {
                public void onChanged(@Nullable List<CallLogInstallationDemoEntity> offerEntityList) {
                    if (!offerEntityList.isEmpty()) {
                        sweetAlertDialog.dismiss();
                        EmployeeCallLog.this.showCompListInUit(offerEntityList);
                        if (EmployeeCallLog.this.chk.equalsIgnoreCase("1")) {
                            EmployeeCallLog.this.calllog_oper_contnr.setVisibility(0);
                        } else {
                            EmployeeCallLog.this.calllog_oper_contnr.setVisibility(8);
                        }
                        EmployeeCallLog.this.no_result_fount.setVisibility(8);
                        EmployeeCallLog.this.calllog_install_recycler.setVisibility(0);
                        EmployeeCallLog.this.list_container.setVisibility(0);
                    } else if (offerEntityList.size() == 0) {
                        EmployeeCallLog.this.calllog_oper_contnr.setVisibility(8);
                        sweetAlertDialog.dismiss();
                        EmployeeCallLog.this.showCompListInUit(new ArrayList());
                        EmployeeCallLog.this.no_result_fount.setVisibility(0);
                        EmployeeCallLog.this.calllog_install_recycler.setVisibility(8);
                        EmployeeCallLog.this.list_container.setVisibility(8);
                    } else {
                        sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public void showDateView(View view) {
        this.date_expandble_view = (ExpandableRelativeLayout) findViewById(R.id.date_expandble_view);
        this.date_expandble_view.toggle();
    }

    public void showSelectDate(View view) {
        new DatePickerDialog(this, this.mSeleteDate, this.mYear, this.mMonth, this.mDay).show();
    }

    public void showStartDate(View view) {
        new DatePickerDialog(this, this.mStartDate, this.mYear, this.mMonth, this.mDay).show();
    }

    public void showToDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this.mToDate, this.mYear, this.mMonth, this.mDay);
        if (TextUtils.isEmpty(this.start_date.getText().toString())) {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        } else {
            datePickerDialog.getDatePicker().setMinDate(BahetiEnterprises.tStamp(this.start_date.getText().toString()));
        }
        datePickerDialog.show();
    }

    public int getCont(List<CallLogInstallationDemoEntity> callLogInstallationDemoEntityList2, String status) {
        int cnt = 0;
        try {
            for (CallLogInstallationDemoEntity ce : callLogInstallationDemoEntityList2) {
                if (ce.getStatus().equalsIgnoreCase(status)) {
                    cnt++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cnt;
    }

    private void showCompListInUit(List<CallLogInstallationDemoEntity> callLogInstallationDemoEntityList) {
        this.callLogInstallationDemoEntities = callLogInstallationDemoEntityList;
        int pend_cnt = getCont(callLogInstallationDemoEntityList, getResources().getString(R.string.Pending));
        int on_hold_cnt = getCont(callLogInstallationDemoEntityList, getResources().getString(R.string.On_Hold));
        int resolved_closed_cnt = getCont(callLogInstallationDemoEntityList, getResources().getString(R.string.Resolved_Closed));
        int in_progress_cnt = getCont(callLogInstallationDemoEntityList, getResources().getString(R.string.In_Progress));
        int all = ((pend_cnt + on_hold_cnt) + resolved_closed_cnt) + in_progress_cnt;
        this.pending.setText(getResources().getString(R.string.Pending) + " : " + pend_cnt);
        this.on_hold.setText(getResources().getString(R.string.On_Hold) + " : " + on_hold_cnt);
        this.resolved_closed.setText(getResources().getString(R.string.Resolved_Closed) + " : " + resolved_closed_cnt);
        this.in_progress.setText(getResources().getString(R.string.In_Progress) + " : " + in_progress_cnt);
        this.total.setText(getResources().getString(R.string.total) + " : " + all);
        if (this.chk.equalsIgnoreCase("1")) {
            this.adapter = new CallLogInstallAdapter(callLogInstallationDemoEntityList, this, 1, all_cb);
        } else if (this.chk.equalsIgnoreCase("2")) {
            this.adapter = new CallLogInstallAdapter(callLogInstallationDemoEntityList, this, 2, all_cb);
        } else {
            this.adapter = new CallLogInstallAdapter(callLogInstallationDemoEntityList, this, 0, all_cb);
        }
        this.calllog_install_recycler.setLayoutManager(new LinearLayoutManager(this));
        this.calllog_install_recycler.setAdapter(this.adapter);
        this.adapter.notifyDataSetChanged();
    }
}
