package com.orangebyte.bahetiadmin.modules.admin;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.adapters.ManagerAdapter;
import com.orangebyte.bahetiadmin.modules.adapters.ManagerAdapter.ClickListener;
import com.orangebyte.bahetiadmin.modules.entities.ManagerEntity;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener.OnItemClickListener;
import com.orangebyte.bahetiadmin.modules.viewmodels.ManagerViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.ManagerViewModel.ManagerFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog.OnSweetClickListener;
import com.orangebyte.bahetiadmin.views.CircleImageView;

import java.util.ArrayList;
import java.util.List;

public class ManagerActivity extends AppCompatActivity implements LifecycleRegistryOwner, ClickListener {
    ManagerAdapter adapter;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    List<ManagerEntity> managerEntityList = new ArrayList();
    ManagerViewModel managerViewModel;
    RecyclerView manager_list;
    TextView no_result_fount;
    SweetAlertDialog sweetAlertDialog;
    private Toolbar toolbarView;
    Spinner type_spinner;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager);
        BahetiEnterprises.setActBar(getResources().getString(R.string.manage_manager), this, false);
        initViews();
        listeners();
    }

    public void listeners() {
        this.manager_list.addOnItemTouchListener(new RecyclerItemClickListener(this, new OnItemClickListener() {
            public void onItemClick(View view, final int position) {


                ((ImageButton) view.findViewById(R.id.mgr_edit)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(ManagerActivity.this, AddManagerActivity.class);
                        i.putExtra(BahetiEnterprises.CHK_ADD_UPDATE, "1");
                        i.putExtra(BahetiEnterprises.FROM, managerEntityList.get(position));
                        startActivity(i);
                    }
                });


                ((ImageButton) view.findViewById(R.id.chng_status)).setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        ManagerActivity.this.sweetAlertDialog.changeAlertType(3);
                        if (((ManagerEntity) ManagerActivity.this.managerEntityList.get(position)).getStatus().equalsIgnoreCase("1")) {
                            ManagerActivity.this.sweetAlertDialog.setTitleText("Are You sure you want to hide?\n");
                        } else {
                            ManagerActivity.this.sweetAlertDialog.setTitleText("Are You sure you want to unhide?\n");
                        }
                        ManagerActivity.this.sweetAlertDialog.setCancelText("NO").setConfirmText("YES").setContentText("\n");
                        ManagerActivity.this.sweetAlertDialog.setConfirmClickListener(new OnSweetClickListener() {
                            public void onClick(final SweetAlertDialog sweetAlertDialog) {
                                ManagerEntity sce1 = (ManagerEntity) ManagerActivity.this.managerEntityList.get(position);
                                if (((ManagerEntity) ManagerActivity.this.managerEntityList.get(position)).getStatus().equalsIgnoreCase("1")) {
                                    sce1.setStatus("0");
                                    ManagerActivity.this.managerViewModel.updateMgrStatus(sce1).observe(ManagerActivity.this, new Observer<List<ManagerEntity>>() {
                                        public void onChanged(@Nullable List<ManagerEntity> managerEntity) {
                                            if (managerEntity.isEmpty()) {
                                                sweetAlertDialog.show();
                                                return;
                                            }
                                            sweetAlertDialog.dismiss();
                                            ManagerActivity.this.showManagerListInUit(managerEntity);
                                        }
                                    });
                                    return;
                                }
                                sce1.setStatus("1");
                                ManagerActivity.this.managerViewModel.updateMgrStatus(sce1).observe(ManagerActivity.this, new Observer<List<ManagerEntity>>() {
                                    public void onChanged(@Nullable List<ManagerEntity> managerEntity) {
                                        if (managerEntity.isEmpty()) {
                                            sweetAlertDialog.show();
                                            return;
                                        }
                                        sweetAlertDialog.dismiss();
                                        ManagerActivity.this.showManagerListInUit(managerEntity);
                                    }
                                });
                            }
                        }).show();
                        ManagerActivity.this.onResume();
                    }
                });
            }
        }));
    }

    protected void onResume() {
        super.onResume();
        ManagerEntity managerEntity = new ManagerEntity();

        if (this.type_spinner.getSelectedItemPosition() != 0) {
            managerEntity.setMgr_type(this.type_spinner.getSelectedItem().toString());
            initViewModel(managerEntity);

        } else {
            managerEntity = new ManagerEntity();
            managerEntity.setMgr_type("null");
            initViewModel(managerEntity);
        }
    }

    private void initViews() {
        this.no_result_fount = (TextView) findViewById(R.id.no_result_fount);
        this.type_spinner = (Spinner) findViewById(R.id.type_spinner);
        this.toolbarView = (Toolbar) findViewById(R.id.toolbar);
        this.sweetAlertDialog = new SweetAlertDialog(this);
        this.sweetAlertDialog.setCancelable(false);
        this.manager_list = (RecyclerView) findViewById(R.id.manager_list);
        if (VERSION.SDK_INT >= 9) {
            this.manager_list.setHasFixedSize(true);
        }
        if (VERSION.SDK_INT >= 9) {
            this.manager_list.setLayoutManager(new LinearLayoutManager(this));
        }
        this.adapter = new ManagerAdapter(this, this, new ArrayList());
        this.manager_list.setHasFixedSize(true);
        this.manager_list.setAdapter(this.adapter);
        this.type_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ManagerActivity.this.onResume();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        ((CircleImageView) findViewById(R.id.fab)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(ManagerActivity.this, AddManagerActivity.class);
                i.putExtra(BahetiEnterprises.CHK_ADD_UPDATE, "0");
                ManagerActivity.this.startActivity(i);
            }
        });
    }

    private void initViewModel(ManagerEntity managerEntity) {
        this.managerViewModel = (ManagerViewModel) ViewModelProviders.of(this, new ManagerFactory()).get(ManagerViewModel.class);
        subscribeToDataStreams(this.managerViewModel, managerEntity);
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    public void onItemClicked(ManagerEntity managerEntity) {
    }

    private void subscribeToDataStreams(ManagerViewModel viewModel, ManagerEntity managerEntity) {
        try {
            this.sweetAlertDialog = new SweetAlertDialog(this, 5);
            this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
            this.sweetAlertDialog.setTitleText("Loading");
            this.sweetAlertDialog.setCancelable(false);
            viewModel.getManager(managerEntity).observe(this, new Observer<List<ManagerEntity>>() {
                public void onChanged(@Nullable List<ManagerEntity> managerEntityList) {
                    if (!managerEntityList.isEmpty()) {
                        ManagerActivity.this.sweetAlertDialog.dismiss();
                        ManagerActivity.this.showManagerListInUit(managerEntityList);
                        ManagerActivity.this.no_result_fount.setVisibility(8);
                        ManagerActivity.this.manager_list.setVisibility(0);
                    } else if (managerEntityList.size() == 0) {
                        ManagerActivity.this.sweetAlertDialog.dismiss();
                        ManagerActivity.this.showManagerListInUit(new ArrayList());
                        ManagerActivity.this.no_result_fount.setVisibility(0);
                        ManagerActivity.this.manager_list.setVisibility(8);
                    } else {
                        ManagerActivity.this.sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showManagerListInUit(List<ManagerEntity> managerEntityList) {
        this.managerEntityList = managerEntityList;
        this.adapter = new ManagerAdapter(this, this, managerEntityList);
        this.manager_list.setHasFixedSize(true);
        this.manager_list.setAdapter(this.adapter);
    }
}
