package com.orangebyte.bahetiadmin.modules.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.employee.ComplaitAction;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;
import com.squareup.picasso.Picasso;

import im.delight.android.webview.BuildConfig;

public class ComplaintDetails extends AppCompatActivity {
    TextView action_taken_of_st;
    TextView assigned_to;
    Button btn_ok;
    TextView comp_desc;
    TextView comp_last_attened_on;
    TextView comp_status;
    ComplaintEntity complaintEntity;
    TextView cust_address;
    TextView cust_comp_dt;
    TextView cust_mob_no;
    TextView cust_nm;
    String from1 = BuildConfig.VERSION_NAME;
    TextView priority_txt;
    TextView prod;
    TextView prod_category;
    TextView prod_model;
    TextView prod_subcat, paid_type, paid_amt, extra_charges, product_in, additional_desc,comp_no;
    public static int chek_backpress = 0;
    ImageView signature_url, proof_url;
    LinearLayout prof_container;
    LinearLayout extra_charges_container;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_details);
        BahetiEnterprises.setActBar(getResources().getString(R.string.complaint_details), this, false);
        this.from1 = getIntent().getStringExtra(BahetiEnterprises.FROM);
        initView();
        this.complaintEntity = (ComplaintEntity) getIntent().getSerializableExtra(BahetiEnterprises.COMPLAINT_DETAILS);
        updateData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (chek_backpress == 1) {
            onBackPressed();
            chek_backpress = 0;
        }
    }

    public void initView() {

        extra_charges = (TextView) findViewById(R.id.extra_charges);
        product_in = (TextView) findViewById(R.id.product_in);
        additional_desc = (TextView) findViewById(R.id.additional_desc);
        comp_no = (TextView) findViewById(R.id.comp_no);

        extra_charges_container = (LinearLayout) findViewById(R.id.extra_charges_container);
        prof_container = (LinearLayout) findViewById(R.id.prof_container);
        proof_url = (ImageView) findViewById(R.id.proof_url);
        paid_type = (TextView) findViewById(R.id.paid_type);
        this.cust_nm = (TextView) findViewById(R.id.cust_nm);
        this.cust_mob_no = (TextView) findViewById(R.id.cust_mob_no);
        this.cust_address = (TextView) findViewById(R.id.cust_address);
        this.cust_comp_dt = (TextView) findViewById(R.id.cust_comp_dt);
        this.prod_category = (TextView) findViewById(R.id.prod_category);
        this.prod_subcat = (TextView) findViewById(R.id.prod_subcat);
        this.prod = (TextView) findViewById(R.id.prod);
        this.prod_model = (TextView) findViewById(R.id.prod_model);
        this.comp_desc = (TextView) findViewById(R.id.comp_desc);
        this.comp_status = (TextView) findViewById(R.id.comp_status);
        this.assigned_to = (TextView) findViewById(R.id.assigned_to);
        this.comp_last_attened_on = (TextView) findViewById(R.id.comp_last_attened_on);
        this.action_taken_of_st = (TextView) findViewById(R.id.action_taken_of_st);
        this.priority_txt = (TextView) findViewById(R.id.priority_txt);
        this.btn_ok = (Button) findViewById(R.id.btn_ok);
        this.comp_desc.setMovementMethod(new ScrollingMovementMethod());
        if (this.from1.equalsIgnoreCase("1")) {
            this.btn_ok.setText(getResources().getString(R.string.dialog_ok));
        } else {
            this.btn_ok.setText(getResources().getString(R.string.done));
        }
        paid_amt = (TextView) findViewById(R.id.paid_amt);
        signature_url = (ImageView) findViewById(R.id.signature_url);
        this.btn_ok.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (ComplaintDetails.this.from1.equalsIgnoreCase("1")) {
                    ComplaintDetails.this.onBackPressed();
                    return;
                }
                if (!complaintEntity.getStatus().equalsIgnoreCase(getResources().getString(R.string.Resolved_Closed))) {
                    Intent comDetails = new Intent(ComplaintDetails.this, ComplaitAction.class);
                    comDetails.putExtra(BahetiEnterprises.FROM, ComplaintDetails.this.complaintEntity);
                    ComplaintDetails.this.startActivity(comDetails);
                }
            }
        });
    }

    public void updateData() {
        BahetiEnterprises.setData(this.complaintEntity.getCom_no(), this.comp_no);
        BahetiEnterprises.setData(this.complaintEntity.getUsernm(), this.cust_nm);
        BahetiEnterprises.setData(this.complaintEntity.getMobile_no(), this.cust_mob_no);
        BahetiEnterprises.setData(this.complaintEntity.getAddress(), this.cust_address);
        BahetiEnterprises.setData(BahetiEnterprises.retDate(this.complaintEntity.getComp_date()), this.cust_comp_dt);
        BahetiEnterprises.setData(this.complaintEntity.getDesc(), this.comp_desc);
        BahetiEnterprises.setData(this.complaintEntity.getStatus(), this.comp_status);
        BahetiEnterprises.setData(this.complaintEntity.getEmp_nm(), this.assigned_to);
        BahetiEnterprises.setData(BahetiEnterprises.retDate(this.complaintEntity.getLast_atnd_on()), this.comp_last_attened_on);
        BahetiEnterprises.setData(this.complaintEntity.getActn_tkn_st(), this.action_taken_of_st);
        BahetiEnterprises.setData(this.complaintEntity.getTitle(), this.prod);
        BahetiEnterprises.setData(this.complaintEntity.getModel_no(), this.prod_model);
        BahetiEnterprises.setData(this.complaintEntity.getSub_cat_name(), this.prod_subcat);
        BahetiEnterprises.setData(this.complaintEntity.getCat_nm(), this.prod_category);
        BahetiEnterprises.setData(this.complaintEntity.getPriority(), this.priority_txt);
        BahetiEnterprises.setData(complaintEntity.getPaid_type(), paid_type);
        BahetiEnterprises.setData(complaintEntity.getPaid_amt(), paid_amt);
        BahetiEnterprises.setData(complaintEntity.getExtra_charges(), extra_charges);
        BahetiEnterprises.setData(complaintEntity.getExtra_charges_desc(), additional_desc);

        try {
            if (complaintEntity.getProduct_in().equalsIgnoreCase("1")) {
                BahetiEnterprises.setData(getString(R.string.warranty), product_in);

            } else {
                BahetiEnterprises.setData(getString(R.string.out_of_warranty), product_in);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (complaintEntity.getStatus().equalsIgnoreCase(getString(R.string.Resolved_Closed))) {
            ((LinearLayout) findViewById(R.id.sign_container)).setVisibility(View.VISIBLE);
            Picasso.with(ComplaintDetails.this).load(BahetiEnterprises.SIGNATURE_URL + complaintEntity.getSignature_url()).into(signature_url);

            ((LinearLayout) findViewById(R.id.prof_container)).setVisibility(View.VISIBLE);
            Picasso.with(ComplaintDetails.this).load(BahetiEnterprises.PROOF_URL + complaintEntity.getProof_url()).into(proof_url);


        } else {
            ((LinearLayout) findViewById(R.id.sign_container)).setVisibility(View.GONE);
            ((LinearLayout) findViewById(R.id.prof_container)).setVisibility(View.GONE);

        }
    }
}
