package com.orangebyte.bahetiadmin.modules.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.Nullable;


import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.db.daos.NotiDAO;
import com.orangebyte.bahetiadmin.modules.entities.NotificationCenter;

import java.util.List;

/**
 * Created by Ashvini on 3/29/2018.
 */
/*
public class NotiRepository {
}
*/

/**
 * Created by Ashvini on 1/8/2018.
 */

public class NotiRepository {


    private static NotiRepository sInstance;

    private final NotiDAO notificationDAO;


    public static NotiRepository getInstance(NotiDAO notificationDAO) {
        if (sInstance == null) {
            sInstance = new NotiRepository(notificationDAO);
        }
        return sInstance;
    }


    public NotiRepository(NotiDAO notificationDAO) {

        this.notificationDAO = notificationDAO;

    }

    public LiveData<List<NotificationCenter>> getAllNotification() {

        return notificationDAO.loadAll();
    }

    public void addNoti(NotificationCenter notificationCenter) {
        insertTask(notificationCenter);
    }


    private void insertTask(final NotificationCenter notificationEntity) {
        insertTask(notificationEntity, null);
    }

    private void insertTask(final NotificationCenter notificationEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            @Override
            protected Void doInBackground(Context... params) {
                notificationDAO.insert(notificationEntity);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(BahetiEnterprises.getInstance());
    }


    interface TaskListener {
        void onTaskFinished();
    }


    interface RemoteNotificationListener {
        void onRemotReceived(NotificationCenter notificationEntity);

        void onRemoteReceived(List<NotificationCenter> notificationEntities);

        void onCheckResult(boolean b);

    }
}
