package com.orangebyte.bahetiadmin.modules.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;

import java.util.List;

/**
 * Created by W10 on 3/20/2018.
 */

public class TollFreeAdapter extends RecyclerView.Adapter<TollFreeAdapter.TollFreeHolder> {

    Context ctx;
    List<String> toll_free_noList;

    public TollFreeAdapter(Context ctx, List<String> toll_free_noList) {
        this.ctx = ctx;
        this.toll_free_noList = toll_free_noList;
    }

    @Override
    public TollFreeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TollFreeHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.tollfree_row, parent, false));

    }

    @Override
    public void onBindViewHolder(TollFreeHolder holder, int position) {
        holder.toll_free_no.setText(toll_free_noList.get(position));
    }

    @Override
    public int getItemCount() {
        return toll_free_noList.size();
    }

    public class TollFreeHolder extends RecyclerView.ViewHolder {

        TextView toll_free_no;

        public TollFreeHolder(View itemView) {
            super(itemView);
            toll_free_no = (TextView) itemView.findViewById(R.id.toll_free_no);

        }
    }
}
