package com.orangebyte.bahetiadmin.modules.repository;

import com.orangebyte.bahetiadmin.modules.entities.AdminEntity;
import com.orangebyte.bahetiadmin.modules.entities.BrandEntity;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.entities.EnquiryEntity;
import com.orangebyte.bahetiadmin.modules.entities.FeedbackEntity;
import com.orangebyte.bahetiadmin.modules.entities.ManagerEntity;
import com.orangebyte.bahetiadmin.modules.entities.NotificationEntity;
import com.orangebyte.bahetiadmin.modules.entities.OfferEntity;
import com.orangebyte.bahetiadmin.modules.entities.ProductEntity;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.UserEntity;
import com.orangebyte.bahetiadmin.modules.entities.WorkSheetEntity;
import com.orangebyte.bahetiadmin.modules.models.Cat;
import com.orangebyte.bahetiadmin.modules.models.ServicesResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface ApiServices {

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updtJobStatusCallLog/")
    Call<ServicesResponse> actionTaken(@Body CallLogInstallationDemoEntity callLogInstallationDemoEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updtJobStatusCompl/")
    Call<ServicesResponse> actionTaken(@Body ComplaintEntity complaintEntity);


    @POST("http://be.orangebytetech.com/mobile_app/webservice/updateResolvedCompl/")
    @Multipart
    Call<ServicesResponse> actionTakenComplaint(@Part MultipartBody.Part part, @Part MultipartBody.Part part2, @PartMap Map<String, RequestBody> map);


    @POST("http://be.orangebytetech.com/mobile_app/webservice/updateResolvedCallLog/")
    @Multipart
    Call<ServicesResponse> updateResolvedCallLog(@Part MultipartBody.Part part, @Part MultipartBody.Part part2, @PartMap Map<String, RequestBody> map);


    //editProduct


    @POST("http://be.orangebytetech.com/mobile_app/webservice/editProduct/")
    @Multipart
    Call<ServicesResponse> editProduct(@Part MultipartBody.Part part, @PartMap Map<String, RequestBody> map);


    @POST("http://be.orangebytetech.com/mobile_app/webservice/updtJobStatusEnquiry/")
    Call<ServicesResponse> actionTaken(@Body EnquiryEntity enquiryEntity);


    //updateJobStatusEnquiry
    @POST("http://be.orangebytetech.com/mobile_app/webservice/updateJobStatusEnquiry/")
    Call<ServicesResponse> updateEnqStatus(@Body EnquiryEntity enquiryEntity);


    @POST("http://be.orangebytetech.com/mobile_app/webservice/updateProfile/")
    Call<ServicesResponse> updateProfile(@Body AdminEntity adminEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updatePassword/")
    Call<ServicesResponse> updatePassword(@Body AdminEntity adminEntity);


    @POST("http://be.orangebytetech.com/mobile_app/webservice/addBrand/")
    Call<ServicesResponse> addBrand(@Body BrandEntity brandEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/addInstallationNdemo/")
    Call<ServicesResponse> addCallInsatllDemo(@Body CallLogInstallationDemoEntity callLogInstallationDemoEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/addCat/")
    Call<ServicesResponse> addCategory(@Body Cat cat);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/addEmp/")
    Call<ServicesResponse> addEmployee(@Body EmployeeEntity employeeEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/addMgr/")
    Call<ServicesResponse> addMgr(@Body ManagerEntity managerEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/addNotifn/")
    Call<ServicesResponse> addNoti(@Body NotificationEntity notificationEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/addOffer/")
    @Multipart
    Call<ServicesResponse> addOffer(@Part MultipartBody.Part part, @PartMap Map<String, RequestBody> map);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/addBrnd/")
    @Multipart
    Call<ServicesResponse> addBrand(@Part MultipartBody.Part part, @PartMap Map<String, RequestBody> map);


    @POST("http://be.orangebytetech.com/mobile_app/webservice/editBrnd/")
    @Multipart
    Call<ServicesResponse> editBrand(@Part MultipartBody.Part part, @PartMap Map<String, RequestBody> map);


    @POST("http://be.orangebytetech.com/mobile_app/webservice/addSubCat/")
    Call<ServicesResponse> addSubCategory(@Body SubCategoryEntity subCategoryEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/adminLogin/")
    Call<ServicesResponse> adminLogin(@Body AdminEntity adminEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/assignEmpToDemo/")
    Call<ServicesResponse> assignEmp(@Body CallLogInstallationDemoEntity callLogInstallationDemoEntity);

    //assignEmpToComplaint

    //assignCallLog

    @POST("http://be.orangebytetech.com/mobile_app/webservice/assignCallLog/")
    Call<ServicesResponse> assignCallLog(@Body CallLogInstallationDemoEntity callLogInstallationDemoEntity);


    //assignEmpToEnquiry

    @POST("http://be.orangebytetech.com/mobile_app/webservice/assignEmpToEnquiry/")
    Call<ServicesResponse> assignEmpToEnquiry(@Body EnquiryEntity enquiryEntity);


    @POST("http://be.orangebytetech.com/mobile_app/webservice/assignEmpToEnquiry/")
    Call<ServicesResponse> assignSingleEnquiry(@Body EnquiryEntity enquiryEntity);


    @POST("http://be.orangebytetech.com/mobile_app/webservice/assignEmpToComplaint/")
    Call<ServicesResponse> assignEmpToMultipleComplaint(@Body ComplaintEntity complaintEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/lodgeComplaint/")
    Call<ServicesResponse> lodgeComplaint(@Body ComplaintEntity complaintEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/addEnqByManager/")
    Call<ServicesResponse> addEnquiry(@Body EnquiryEntity enquiryEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/assignComplaint/")
    Call<ServicesResponse> assignEmpTOComplaint(@Body ComplaintEntity complaintEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/changeStatusInstallDemo/")
    Call<ServicesResponse> changeStatus(@Body CallLogInstallationDemoEntity callLogInstallationDemoEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/delCat/")
    Call<ServicesResponse> deletCategoryById(@Body Cat cat);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/delSubCat/")
    Call<ServicesResponse> deletSubCategoryById(@Body SubCategoryEntity subCategoryEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/delBrand/")
    Call<ServicesResponse> deleteBrandById(@Body BrandEntity brandEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/delEmp/")
    Call<ServicesResponse> deleteEmployeeById(@Body EmployeeEntity employeeEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/delManager/")
    Call<ServicesResponse> deleteMgrById(@Body ManagerEntity managerEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/editOffer/")
    @Multipart
    Call<ServicesResponse> editOffer(@Part MultipartBody.Part part, @PartMap Map<String, RequestBody> map);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/editOffer/")
    Call<ServicesResponse> editOffer2(@PartMap Map<String, RequestBody> map);

    @GET("http://be.orangebytetech.com/mobile_app/webservice/allBrand/")
    Call<ServicesResponse> getBrand();

    @POST("http://be.orangebytetech.com/mobile_app/webservice/getCallLogsNInstallationLog/")
    Call<ServicesResponse> getCallInsatll(@Body CallLogInstallationDemoEntity callLogInstallationDemoEntity);

    @GET("http://be.orangebytetech.com/mobile_app/webservice/allCat/")
    Call<ServicesResponse> getCategory();

    @POST("http://be.orangebytetech.com/mobile_app/webservice/getComplaintServices/")
    Call<ServicesResponse> getComplaint(@Body ComplaintEntity complaintEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/allEmp/")
    Call<ServicesResponse> getEmployee(@Body EmployeeEntity employeeEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/getWorksheet/")
    Call<ServicesResponse> getEmployeeWorksheet(@Body WorkSheetEntity workSheetEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/getEnquiry/")
    Call<ServicesResponse> getEnquiry(@Body EnquiryEntity enquiryEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/getFeedback/")
    Call<ServicesResponse> getFeedback(@Body FeedbackEntity feedbackEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/allMgr/")
    Call<ServicesResponse> getMgrs(@Body ManagerEntity managerEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/activeOffers/")
    Call<ServicesResponse> getOffers(@Body OfferEntity offerEntity);


    //getCallLogsInstallEmployeeIncetive

    @POST("http://be.orangebytetech.com/mobile_app/webservice/getCallLogsInstallEmployeeIncetive/")
    Call<ServicesResponse> getCallLogsInstallEmployeeIncetive(@Body CallLogInstallationDemoEntity callLogInstallationDemoEntity);


    //getCallLogsInstallEmployeeIncetive

    @POST("http://be.orangebytetech.com/mobile_app/webservice/getEMployeeComplaintIncentive/")
    Call<ServicesResponse> getEMployeeComplaintIncentive(@Body ComplaintEntity complaintEntity);


    @POST("http://be.orangebytetech.com/mobile_app/webservice/allProd/")
    Call<ServicesResponse> getProduct(@Body ProductEntity productEntity);

    @GET("http://be.orangebytetech.com/mobile_app/webservice/allSubCat/")
    Call<ServicesResponse> getSubCategory();

    @POST("http://be.orangebytetech.com/mobile_app/webservice/getUsersByName/")
    Call<ServicesResponse> getUsersByName(@Body UserEntity userEntity);

    //forgetPassword
    @POST("http://be.orangebytetech.com/mobile_app/webservice/forgetPassword/")
    Call<ServicesResponse> forgetPassword(@Body AdminEntity adminEntity);

    ///addUser
    @POST("http://be.orangebytetech.com/mobile_app/webservice/addUser/")
    Call<ServicesResponse> addUser(@Body UserEntity userEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updateBrandDetails/")
    Call<ServicesResponse> updateBrand(@Body BrandEntity brandEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/changeBrandStatus/")
    Call<ServicesResponse> updateBrandStatus(@Body BrandEntity brandEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/changeCatStatus/")
    Call<ServicesResponse> updateCatStatus(@Body Cat cat);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updateCatDetails/")
    Call<ServicesResponse> updateCategory(@Body Cat cat);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updateEmpDetails/")
    Call<ServicesResponse> updateEmployee(@Body EmployeeEntity employeeEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/changeEmpStatus/")
    Call<ServicesResponse> updateEmployeeStatus(@Body EmployeeEntity employeeEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updateManagerDetails/")
    Call<ServicesResponse> updateMgr(@Body ManagerEntity managerEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/changeManagerStatus/")
    Call<ServicesResponse> updateMgrStatus(@Body ManagerEntity managerEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updateOffStatus/")
    Call<ServicesResponse> updateOffer(@Body OfferEntity offerEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/changeSubCatStatus/")
    Call<ServicesResponse> updateSubCatStatus(@Body SubCategoryEntity subCategoryEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updateSubCatDetails/")
    Call<ServicesResponse> updateSubCategory(@Body SubCategoryEntity subCategoryEntity);




  /*  @POST("http://be.orangebytetech.com/mobile_app/webservice/updtJobStatusCallLog/")
    Call<ServicesResponse> actionTaken(@Body CallLogInstallationDemoEntity callLogInstallationDemoEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updtJobStatusCompl/")
    Call<ServicesResponse> actionTaken(@Body ComplaintEntity complaintEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updtJobStatusEnquiry/")
    Call<ServicesResponse> actionTaken(@Body EnquiryEntity enquiryEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/addBrand/")
    Call<ServicesResponse> addBrand(@Body BrandEntity brandEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/addInstallationNdemo/")
    Call<ServicesResponse> addCallInsatllDemo(@Body CallLogInstallationDemoEntity callLogInstallationDemoEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/addCat/")
    Call<ServicesResponse> addCategory(@Body Cat cat);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/addEmp/")
    Call<ServicesResponse> addEmployee(@Body EmployeeEntity employeeEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/addMgr/")
    Call<ServicesResponse> addMgr(@Body ManagerEntity managerEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/addNotifn/")
    Call<ServicesResponse> addNoti(@Body NotificationEntity notificationEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/addOffer/")
    @Multipart
    Call<ServicesResponse> addOffer(@Part MultipartBody.Part part, @PartMap Map<String, RequestBody> map);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/addSubCat/")
    Call<ServicesResponse> addSubCategory(@Body SubCategoryEntity subCategoryEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/adminLogin/")
    Call<ServicesResponse> adminLogin(@Body AdminEntity adminEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/assignEmpToDemo/")
    Call<ServicesResponse> assignEmp(@Body CallLogInstallationDemoEntity callLogInstallationDemoEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/assignComplaint/")
    Call<ServicesResponse> assignEmpTOComplaint(@Body ComplaintEntity complaintEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/changeStatusInstallDemo/")
    Call<ServicesResponse> changeStatus(@Body CallLogInstallationDemoEntity callLogInstallationDemoEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/delCat/")
    Call<ServicesResponse> deletCategoryById(@Body Cat cat);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/delSubCat/")
    Call<ServicesResponse> deletSubCategoryById(@Body SubCategoryEntity subCategoryEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/delBrand/")
    Call<ServicesResponse> deleteBrandById(@Body BrandEntity brandEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/delEmp/")
    Call<ServicesResponse> deleteEmployeeById(@Body EmployeeEntity employeeEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/delManager/")
    Call<ServicesResponse> deleteMgrById(@Body ManagerEntity managerEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/editOffer/")
    @Multipart
    Call<ServicesResponse> editOffer(@Part MultipartBody.Part part, @PartMap Map<String, RequestBody> map);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/editOffer/")
    Call<ServicesResponse> editOffer2(@PartMap Map<String, RequestBody> map);

    @GET("http://be.orangebytetech.com/mobile_app/webservice/allBrand/")
    Call<ServicesResponse> getBrand();

    @POST("http://be.orangebytetech.com/mobile_app/webservice/getCallLogsNInstallationLog/")
    Call<ServicesResponse> getCallInsatll(@Body CallLogInstallationDemoEntity callLogInstallationDemoEntity);

    @GET("http://be.orangebytetech.com/mobile_app/webservice/allCat/")
    Call<ServicesResponse> getCategory();

    @POST("http://be.orangebytetech.com/mobile_app/webservice/getComplaintServices/")
    Call<ServicesResponse> getComplaint(@Body ComplaintEntity complaintEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/allEmp/")
    Call<ServicesResponse> getEmployee(@Body EmployeeEntity employeeEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/getWorksheet/")
    Call<ServicesResponse> getEmployeeWorksheet(@Body WorkSheetEntity workSheetEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/getEnquiry/")
    Call<ServicesResponse> getEnquiry(@Body EnquiryEntity enquiryEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/getFeedback/")
    Call<ServicesResponse> getFeedback(@Body FeedbackEntity feedbackEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/allMgr/")
    Call<ServicesResponse> getMgrs(@Body ManagerEntity managerEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/activeOffers/")
    Call<ServicesResponse> getOffers(@Body OfferEntity offerEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/allProd/")
    Call<ServicesResponse> getProduct(@Body ProductEntity productEntity);

    @GET("http://be.orangebytetech.com/mobile_app/webservice/allSubCat/")
    Call<ServicesResponse> getSubCategory();

    @POST("http://be.orangebytetech.com/mobile_app/webservice/getUsersByName/")
    Call<ServicesResponse> getUsersByName(@Body UserEntity userEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updateBrandDetails/")
    Call<ServicesResponse> updateBrand(@Body BrandEntity brandEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/changeBrandStatus/")
    Call<ServicesResponse> updateBrandStatus(@Body BrandEntity brandEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/changeCatStatus/")
    Call<ServicesResponse> updateCatStatus(@Body Cat cat);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updateCatDetails/")
    Call<ServicesResponse> updateCategory(@Body Cat cat);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updateEmpDetails/")
    Call<ServicesResponse> updateEmployee(@Body EmployeeEntity employeeEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/changeEmpStatus/")
    Call<ServicesResponse> updateEmployeeStatus(@Body EmployeeEntity employeeEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updateManagerDetails/")
    Call<ServicesResponse> updateMgr(@Body ManagerEntity managerEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/changeManagerStatus/")
    Call<ServicesResponse> updateMgrStatus(@Body ManagerEntity managerEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updateOffStatus/")
    Call<ServicesResponse> updateOffer(@Body OfferEntity offerEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/changeSubCatStatus/")
    Call<ServicesResponse> updateSubCatStatus(@Body SubCategoryEntity subCategoryEntity);

    @POST("http://be.orangebytetech.com/mobile_app/webservice/updateSubCatDetails/")
    Call<ServicesResponse> updateSubCategory(@Body SubCategoryEntity subCategoryEntity);
*/
}
