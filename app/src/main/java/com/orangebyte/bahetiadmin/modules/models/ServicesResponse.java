package com.orangebyte.bahetiadmin.modules.models;

import com.google.gson.annotations.SerializedName;
import com.orangebyte.bahetiadmin.modules.entities.BrandEntity;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.entities.EnquiryEntity;
import com.orangebyte.bahetiadmin.modules.entities.FeedbackEntity;
import com.orangebyte.bahetiadmin.modules.entities.ForgetEntity;
import com.orangebyte.bahetiadmin.modules.entities.LoginEntity;
import com.orangebyte.bahetiadmin.modules.entities.ManagerEntity;
import com.orangebyte.bahetiadmin.modules.entities.OfferEntity;
import com.orangebyte.bahetiadmin.modules.entities.ProductEntity;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.UserEntity;
import com.orangebyte.bahetiadmin.modules.entities.WorkSheetEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ServicesResponse {

    @SerializedName("otp")
    private List<ForgetEntity> otp;




    @SerializedName("brand")
    private List<BrandEntity> brandEntities;
    @SerializedName("installationndemo")
    private List<CallLogInstallationDemoEntity> callLogInstallationDemoEntityList;
    @SerializedName("category")
    private List<CategoryEntity> category;
    @SerializedName("complaints")
    private List<ComplaintEntity> complaintEntityList;
    @SerializedName("employee")
    private List<EmployeeEntity> employeeEntityList;
    @SerializedName("enquiry")
    private List<EnquiryEntity> enquiryEntityList;
    @SerializedName("feedback")
    private List<FeedbackEntity> feedbackEntityList;
    @SerializedName("login")
    private List<LoginEntity> loginEntityList;
    @SerializedName("managers")
    private List<ManagerEntity> managerEntityList;
    @SerializedName("offers")
    private List<OfferEntity> offerEntityList;
    @SerializedName("products")
    private List<ProductEntity> productEntityList;
    @SerializedName("sub_category")
    private List<SubCategoryEntity> subcategory;
    @SerializedName("success")
    private boolean success;
    @SerializedName("users")
    private List<UserEntity> userEntityList;
    @SerializedName("emp_worksheet")
    private List<WorkSheetEntity> workSheetEntityList;


    public List<ForgetEntity> getOtp() {
        return otp;
    }

    public void setOtp(List<ForgetEntity> otp) {
        this.otp = otp;
    }

    public List<LoginEntity> getLoginEntityList() {
        return this.loginEntityList;
    }

    public void setLoginEntityList(List<LoginEntity> loginEntityList) {
        this.loginEntityList = loginEntityList;
    }

    public List<WorkSheetEntity> getWorkSheetEntityList() {
        return this.workSheetEntityList;
    }

    public void setWorkSheetEntityList(List<WorkSheetEntity> workSheetEntityList) {
        this.workSheetEntityList = workSheetEntityList;
    }

    public List<FeedbackEntity> getFeedbackEntityList() {
        return this.feedbackEntityList;
    }

    public void setFeedbackEntityList(List<FeedbackEntity> feedbackEntityList) {
        this.feedbackEntityList = feedbackEntityList;
    }

    public List<EnquiryEntity> getEnquiryEntityList() {
        return this.enquiryEntityList;
    }

    public void setEnquiryEntityList(List<EnquiryEntity> enquiryEntityList) {
        this.enquiryEntityList = enquiryEntityList;
    }

    public List<CallLogInstallationDemoEntity> getCallLogInstallationDemoEntityList() {
        return this.callLogInstallationDemoEntityList;
    }

    public void setCallLogInstallationDemoEntityList(List<CallLogInstallationDemoEntity> callLogInstallationDemoEntityList) {
        this.callLogInstallationDemoEntityList = callLogInstallationDemoEntityList;
    }

    public List<UserEntity> getUserEntityList() {
        return this.userEntityList;
    }

    public void setUserEntityList(List<UserEntity> userEntityList) {
        this.userEntityList = userEntityList;
    }

    public List<ComplaintEntity> getComplaintEntityList() {
        return this.complaintEntityList;
    }

    public void setComplaintEntityList(List<ComplaintEntity> complaintEntityList) {
        this.complaintEntityList = complaintEntityList;
    }

    public List<ProductEntity> getProductEntityList() {
        return this.productEntityList;
    }

    public void setProductEntityList(List<ProductEntity> productEntityList) {
        this.productEntityList = productEntityList;
    }

    public List<OfferEntity> getOfferEntityList() {
        return this.offerEntityList;
    }

    public void setOfferEntityList(List<OfferEntity> offerEntityList) {
        this.offerEntityList = offerEntityList;
    }

    public List<ManagerEntity> getManagerEntityList() {
        return this.managerEntityList;
    }

    public void setManagerEntityList(List<ManagerEntity> managerEntityList) {
        this.managerEntityList = managerEntityList;
    }

    public List<EmployeeEntity> getEmployeeEntityList() {
        return this.employeeEntityList;
    }

    public void setEmployeeEntityList(List<EmployeeEntity> employeeEntityList) {
        this.employeeEntityList = employeeEntityList;
    }

    public List<SubCategoryEntity> getSubcategory() {
        return this.subcategory;
    }

    public List<BrandEntity> getBrandEntities() {
        return this.brandEntities;
    }

    public void setBrandEntities(List<BrandEntity> brandEntities) {
        this.brandEntities = brandEntities;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<CategoryEntity> getCategory() {
        return this.category;
    }

    public List<SubCategoryEntity> getSubCategory() {
        return this.subcategory;
    }

    public void setSubcategory(List<SubCategoryEntity> subcategory) {
        this.subcategory = subcategory;
    }

    public void setCategory(List<CategoryEntity> category) {
        this.category = category;
    }

    public void ComplaintUserProduct(JSONArray response) throws JSONException {
        for (int i = 0; i < response.length(); i++) {
            JSONObject jsonObject = response.getJSONObject(i);
            ComplaintEntity ce = new ComplaintEntity();
            ce.setId(jsonObject.getLong("id"));
            ce.setUser_id(jsonObject.getString("user_id"));
            ce.setService_type(jsonObject.getString("service_type"));
            ce.setAssigned_empid(jsonObject.getString("assigned_empid"));
            ce.setCom_no(jsonObject.getString("com_no"));
            ce.setDesc(jsonObject.getString("desc"));
            ce.setProduct_id(jsonObject.getString("product_id"));
            ce.setStatus(jsonObject.getString("status"));
            ce.setComp_date(jsonObject.getString("comp_date"));
            UserEntity ue = new UserEntity();
            ue.setId(jsonObject.getLong("userid"));
            ue.setAddress(jsonObject.getString("address"));
            ue.setMobile_no(jsonObject.getString("mobile_no"));
            ue.setUsernm(jsonObject.getString("usernm"));
            ue.setPassword(jsonObject.getString("password"));
            EmployeeEntity ee = new EmployeeEntity();
            ee.setId(jsonObject.getLong("empid"));
            ee.setAssigned_mgr_type(jsonObject.getString("assigned_mgr_type"));
            ee.setMgr_id(jsonObject.getString("mgr_id"));
            ee.setEmp_nm(jsonObject.getString("emp_nm"));
            ee.setMob_no(jsonObject.getString("mob_no"));
        }
    }

    public static List<ComplaintEntity> getComplaintList(JSONArray response) throws JSONException {
        List<ComplaintEntity> complaintEntityArrayList = new ArrayList();
        for (int i = 0; i < response.length(); i++) {
            JSONObject jsonObject = response.getJSONObject(i);
            ComplaintEntity ce = new ComplaintEntity();
            ce.setId(jsonObject.getLong("id"));
            ce.setUser_id(jsonObject.getString("user_id"));
            ce.setService_type(jsonObject.getString("service_type"));
            ce.setAssigned_empid(jsonObject.getString("assigned_empid"));
            ce.setCom_no(jsonObject.getString("com_no"));
            ce.setDesc(jsonObject.getString("desc"));
            ce.setProduct_id(jsonObject.getString("product_id"));
            ce.setStatus(jsonObject.getString("status"));
            ce.setComp_date(jsonObject.getString("comp_date"));
            complaintEntityArrayList.add(ce);
        }
        return complaintEntityArrayList;
    }

    public List<UserEntity> getUserList(JSONArray response) throws JSONException {
        List<UserEntity> userEntityList = new ArrayList();
        for (int i = 0; i < response.length(); i++) {
            JSONObject jsonObject = response.getJSONObject(i);
            UserEntity ue = new UserEntity();
            ue.setId(jsonObject.getLong("userid"));
            ue.setAddress(jsonObject.getString("address"));
            ue.setMobile_no(jsonObject.getString("mobile_no"));
            ue.setUsernm(jsonObject.getString("usernm"));
            ue.setPassword(jsonObject.getString("password"));
            userEntityList.add(ue);
        }
        return userEntityList;
    }

    public static UserEntity getUser(JSONObject jsonObject) throws JSONException {
        UserEntity ue = new UserEntity();
        ue.setId(jsonObject.getLong("userid"));
        ue.setAddress(jsonObject.getString("address"));
        ue.setMobile_no(jsonObject.getString("mobile_no"));
        ue.setUsernm(jsonObject.getString("usernm"));
        ue.setPassword(jsonObject.getString("password"));
        return ue;
    }

    public List<EmployeeEntity> getEmployeeList(JSONArray response) throws JSONException {
        List<EmployeeEntity> employeeEntityList = new ArrayList();
        for (int i = 0; i < response.length(); i++) {
            JSONObject jsonObject = response.getJSONObject(i);
            EmployeeEntity ee = new EmployeeEntity();
            ee.setId(jsonObject.getLong("empid"));
            ee.setAssigned_mgr_type(jsonObject.getString("assigned_mgr_type"));
            ee.setMgr_id(jsonObject.getString("mgr_id"));
            ee.setEmp_nm(jsonObject.getString("emp_nm"));
            ee.setMob_no(jsonObject.getString("mob_no"));
            employeeEntityList.add(ee);
        }
        return employeeEntityList;
    }

    public static EmployeeEntity getEmployee(JSONObject jsonObject) throws JSONException {
        EmployeeEntity ee = new EmployeeEntity();
        ee.setId(jsonObject.getLong("empid"));
        ee.setAssigned_mgr_type(jsonObject.getString("assigned_mgr_type"));
        ee.setMgr_id(jsonObject.getString("mgr_id"));
        ee.setEmp_nm(jsonObject.getString("emp_nm"));
        ee.setMob_no(jsonObject.getString("mob_no"));
        return ee;
    }
}
