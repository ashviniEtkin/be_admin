package com.orangebyte.bahetiadmin.modules.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.db.daos.EmployeeDAO;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;

import java.util.List;

public class EmployeeRepository {
    private static EmployeeRepository sInstance;
    private final EmployeeDAO employeeDAO;
    private final RemoteRepository remoteRepository;

    interface RemoteEmployeeListener {
        void onCheckResult(boolean z);

        void onRemotReceived(EmployeeEntity employeeEntity);

        void onRemoteReceived(List<EmployeeEntity> list);
    }

    interface TaskListener {
        void onTaskFinished();
    }

    public static EmployeeRepository getInstance(RemoteRepository remoteRepository, EmployeeDAO employeeDAO) {
        if (sInstance == null) {
            sInstance = new EmployeeRepository(remoteRepository, employeeDAO);
        }
        return sInstance;
    }

    private EmployeeRepository(RemoteRepository remoteRepository, EmployeeDAO employeeDAO) {
        this.remoteRepository = remoteRepository;
        this.employeeDAO = employeeDAO;
    }

    public LiveData<List<EmployeeEntity>> loadEMpList(EmployeeEntity employeeEntity) {
        fetchEMpList(employeeEntity, new RemoteEmployeeListener() {
            public void onRemotReceived(EmployeeEntity cmEntity) {
            }

            public void onRemoteReceived(List<EmployeeEntity> scmEntities) {
                if (scmEntities != null) {
                    EmployeeRepository.this.insertAllTask(scmEntities);
                }
            }

            public void onCheckResult(boolean b) {
            }
        });
        return this.employeeDAO.loadAll();
    }

    public LiveData<List<EmployeeEntity>> addUpdateEMp(final EmployeeEntity employeeEntity, final int chk) {
        addUpdateEMp(employeeEntity, chk, new RemoteEmployeeListener() {
            public void onRemotReceived(EmployeeEntity cmEntity) {
            }

            public void onRemoteReceived(List<EmployeeEntity> list) {
            }

            public void onCheckResult(boolean b) {
                EmployeeEntity ee = new EmployeeEntity();
                ee.setEmp_nm(employeeEntity.getEmp_nm());
                ee.setMgr_id(employeeEntity.getMgr_id());
                ee.setAssigned_mgr_type(employeeEntity.getAssigned_mgr_type());
                ee.setPassword(employeeEntity.getPassword());
                ee.setIncentive(employeeEntity.getIncentive());
                ee.setStatus(employeeEntity.getStatus());
                if (b && chk == 0) {
                    EmployeeRepository.this.insertEmp(ee);
                } else if (b && chk == 1) {
                    ee.setId(employeeEntity.getId());
                    EmployeeRepository.this.updateEmp(ee);
                }
            }
        });
        return this.employeeDAO.loadAll();
    }

    public LiveData<List<EmployeeEntity>> updateEMpStatus(final EmployeeEntity ee) {
        updateEMpStatus(ee, new RemoteEmployeeListener() {
            public void onRemotReceived(EmployeeEntity cmEntity) {
            }

            public void onRemoteReceived(List<EmployeeEntity> list) {
            }

            public void onCheckResult(boolean b) {
                if (b) {
                    EmployeeEntity employeeEntity = new EmployeeEntity();
                    employeeEntity.setId(ee.getId());
                    employeeEntity.setEmp_nm(ee.getEmp_nm());
                    employeeEntity.setMgr_id(ee.getMgr_id());
                    employeeEntity.setAssigned_mgr_type(ee.getAssigned_mgr_type());
                    employeeEntity.setPassword(ee.getPassword());
                    employeeEntity.setIncentive(ee.getIncentive());
                    employeeEntity.setStatus(ee.getStatus());
                    EmployeeRepository.this.updateEmp(employeeEntity);
                }
            }
        });
        return this.employeeDAO.loadAll();
    }

    public LiveData<List<EmployeeEntity>> deleteEMpByID(final EmployeeEntity employeeEntity) {
        deleteEMpByID(employeeEntity, new RemoteEmployeeListener() {
            public void onRemotReceived(EmployeeEntity employeeEntity) {
            }

            public void onRemoteReceived(List<EmployeeEntity> list) {
            }

            public void onCheckResult(boolean b) {
                if (b) {
                    EmployeeEntity ee = new EmployeeEntity();
                    ee.setId(employeeEntity.getId());
                    ee.setEmp_nm(employeeEntity.getEmp_nm());
                    ee.setMgr_id(employeeEntity.getMgr_id());
                    ee.setAssigned_mgr_type(employeeEntity.getAssigned_mgr_type());
                    ee.setPassword(employeeEntity.getPassword());
                    ee.setStatus(employeeEntity.getStatus());
                    ee.setIncentive(employeeEntity.getIncentive());
                    EmployeeRepository.this.deleteMngr(ee);
                }
            }
        });
        return this.employeeDAO.loadAll();
    }

    private void insertAllTask(List<EmployeeEntity> employeeEntity) {
        insertEmps(employeeEntity, null);
    }

    private void insertEmps(final List<EmployeeEntity> employeeEntity, @Nullable final TaskListener listener) {
        if (VERSION.SDK_INT >= 3) {
            new AsyncTask<Context, Void, Void>() {
                protected Void doInBackground(Context... params) {
                    EmployeeRepository.this.employeeDAO.deleteAll();
                    EmployeeRepository.this.employeeDAO.insertAll(employeeEntity);
                    return null;
                }

                protected void onPostExecute(Void aVoid) {
                    if (VERSION.SDK_INT >= 3) {
                        super.onPostExecute(aVoid);
                    }
                    if (listener != null) {
                        listener.onTaskFinished();
                    }
                }
            }.execute(new Context[]{BahetiEnterprises.getInstance()});
        }
    }

    private void insertEmp(EmployeeEntity employeeEntity) {
        insertEMp(employeeEntity, null);
    }

    private void insertEMp(final EmployeeEntity employeeEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                EmployeeRepository.this.employeeDAO.insert(employeeEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteMngr(EmployeeEntity employeeEntity) {
        deleteEmp(employeeEntity, null);
    }

    private void deleteEmp(final EmployeeEntity employeeEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                EmployeeRepository.this.employeeDAO.delete(employeeEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void updateEmp(EmployeeEntity employeeEntity) {
        updateEMp(employeeEntity, null);
    }

    private void updateEMp(final EmployeeEntity employeeEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                EmployeeRepository.this.employeeDAO.update(employeeEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteAllSubCategories() {
        deleteAllEmp(null);
    }

    private void deleteAllEmp(@Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                EmployeeRepository.this.employeeDAO.deleteAll();
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void fetchEMpList(EmployeeEntity employeeEntity, @NonNull final RemoteEmployeeListener listener) {
        this.remoteRepository.getEmployeeList(employeeEntity, new RemoteCallback<List<EmployeeEntity>>() {
            public void onSuccess(List<EmployeeEntity> response) {
                listener.onRemoteReceived(response);
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }

    private void addUpdateEMp(@NonNull EmployeeEntity employeeEntity, @NonNull int chk, @NonNull final RemoteEmployeeListener listener) {
        if (chk == 1) {
            this.remoteRepository.updateEmployee(employeeEntity, new RemoteCallback<Boolean>() {
                public void onSuccess(Boolean response) {
                    listener.onCheckResult(response.booleanValue());
                }

                public void onUnauthorized() {
                }

                public void onFailed(Throwable throwable) {
                }
            });
        } else {
            this.remoteRepository.addEmployee(employeeEntity, new RemoteCallback<Boolean>() {
                public void onSuccess(Boolean response) {
                    listener.onCheckResult(response.booleanValue());
                }

                public void onUnauthorized() {
                }

                public void onFailed(Throwable throwable) {
                }
            });
        }
    }

    private void updateEMpStatus(@NonNull EmployeeEntity employeeEntity, @NonNull final RemoteEmployeeListener listener) {
        this.remoteRepository.updateEmployeeStatus(employeeEntity, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }

    private void deleteEMpByID(@NonNull EmployeeEntity employeeEntity, @NonNull final RemoteEmployeeListener listener) {
        this.remoteRepository.deleteEmployeeByID(employeeEntity, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }
}
