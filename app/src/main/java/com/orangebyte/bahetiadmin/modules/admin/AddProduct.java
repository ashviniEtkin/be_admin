package com.orangebyte.bahetiadmin.modules.admin;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.adapters.ProductAdapter;
import com.orangebyte.bahetiadmin.modules.entities.BrandEntity;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.ProductEntity;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.viewmodels.BrandViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.BrandViewModel.BrandFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels.Factory;
import com.orangebyte.bahetiadmin.modules.viewmodels.ProductViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.ProductViewModel.ProductFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel.SubCategoryFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;

import im.delight.android.webview.BuildConfig;

import java.util.ArrayList;
import java.util.List;

public class AddProduct extends AppCompatActivity implements LifecycleRegistryOwner {
    ProductAdapter adapter;
    List<SubCategoryEntity> allSubCat = new ArrayList();
    List<BrandEntity> brandEntityList = new ArrayList();
    Spinner brand_spinner;
    List<CategoryEntity> categoryEntityList = new ArrayList();
    Spinner category_spinner;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    List<ProductEntity> productEntities = new ArrayList();
    RecyclerView product_list;
    Button btn_reset;
    Button search;
    List<SubCategoryEntity> subCategoryEntityList = new ArrayList();
    Spinner subcategory_spinner;
    ProductEntity pe = null;//new ProductEntity();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        BahetiEnterprises.setActBar(getResources().getString(R.string.Products), this, false);
        init();
        setCatgoryToSpinner();
        this.search.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                AddProduct.this.initModelView();
            }
        });
        this.category_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (AddProduct.this.allSubCat.isEmpty()) {
                    AddProduct.this.setSubCatgoryToSpinner(((CategoryEntity) AddProduct.this.categoryEntityList.get(i)).getId() + BuildConfig.VERSION_NAME);
                } else {
                    AddProduct.this.setToSubCatSpinner(AddProduct.this.allSubCat, ((CategoryEntity) AddProduct.this.categoryEntityList.get(i)).getId() + BuildConfig.VERSION_NAME);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.subcategory_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (AddProduct.this.brandEntityList.isEmpty()) {
                    AddProduct.this.setBrandToSpinner(((CategoryEntity) AddProduct.this.categoryEntityList.get(AddProduct.this.category_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
                } else {
                    AddProduct.this.setToBrandSpinner(AddProduct.this.brandEntityList, ((CategoryEntity) AddProduct.this.categoryEntityList.get(AddProduct.this.category_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //   initModelView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (pe != null) {
            initModelView();
        }
    }

    public void initModelView() {
        pe = new ProductEntity();
        if (BahetiEnterprises.checkSpinner(this.category_spinner)) {
            pe.setCat_id("null");
        } else {
            pe.setCat_id(((CategoryEntity) this.categoryEntityList.get(this.category_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
        }
        if (BahetiEnterprises.checkSpinner(this.subcategory_spinner)) {
            pe.setSub_cat_id("null");
        } else {
            pe.setSub_cat_id(((SubCategoryEntity) this.subCategoryEntityList.get(this.subcategory_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
        }
        try {
            if (this.brand_spinner.getSelectedItemPosition() == 0) {
                pe.setBrand_id("null");
            } else if (BahetiEnterprises.checkSpinner(this.brand_spinner)) {
                pe.setBrand_id("null");
            } else {
                pe.setBrand_id(((BrandEntity) this.brandEntityList.get(this.brand_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);
            }
        } catch (Exception e) {
            e.printStackTrace();
            pe.setBrand_id("null");
        }
        getProducts((ProductViewModel) ViewModelProviders.of(this, new ProductFactory()).get(ProductViewModel.class), pe);
    }

    private void showProductListInUit(List<ProductEntity> productEntities) {
        this.productEntities = productEntities;
        this.adapter = new ProductAdapter(this, productEntities);
        this.product_list.setAdapter(this.adapter);
        this.adapter.notifyDataSetChanged();
    }

    private void getProducts(ProductViewModel viewModel, ProductEntity c) {
        try {
            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, 5);
            sweetAlertDialog.setTitle("Loading");
            sweetAlertDialog.setTitleText("");
            sweetAlertDialog.setContentText("");
            sweetAlertDialog.show();
            viewModel.getProduct(c).observe(this, new Observer<List<ProductEntity>>() {
                public void onChanged(@Nullable List<ProductEntity> productEntities) {
                    if (!productEntities.isEmpty()) {
                        sweetAlertDialog.dismiss();
                        AddProduct.this.showProductListInUit(productEntities);
                    } else if (productEntities.size() == 0) {
                        sweetAlertDialog.dismiss();
                    } else {
                        sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init() {
        this.category_spinner = (Spinner) findViewById(R.id.category_spinner);
        this.subcategory_spinner = (Spinner) findViewById(R.id.subcategory_spinner);
        this.brand_spinner = (Spinner) findViewById(R.id.brand_spinner);
        this.search = (Button) findViewById(R.id.search);
        this.product_list = (RecyclerView) findViewById(R.id.product_list);
        this.adapter = new ProductAdapter(this, new ArrayList());
        this.product_list.setLayoutManager(new LinearLayoutManager(this));
        this.product_list.setAdapter(this.adapter);

        btn_reset = (Button) findViewById(R.id.btn_reset);

        btn_reset.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setCatgoryToSpinner();

            }
        });
    }

    public void setCatgoryToSpinner() {
        try {
            ((CategoryViewMdels) ViewModelProviders.of(this, new Factory()).get(CategoryViewMdels.class)).getCats().observe(this, new Observer<List<CategoryEntity>>() {
                public void onChanged(@Nullable List<CategoryEntity> categoryEntities) {
                    if (!categoryEntities.isEmpty()) {
                        AddProduct.this.setToSpinner(categoryEntities);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setSubCatgoryToSpinner(final String catId) {
        try {
            ((SubCategoryViewModel) ViewModelProviders.of(this, new SubCategoryFactory()).get(SubCategoryViewModel.class)).getSubCats().observe(this, new Observer<List<SubCategoryEntity>>() {
                public void onChanged(@Nullable List<SubCategoryEntity> subCategoryEntities) {
                    if (!subCategoryEntities.isEmpty()) {
                        AddProduct.this.allSubCat = subCategoryEntities;
                        AddProduct.this.setToSubCatSpinner(subCategoryEntities, catId);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setBrandToSpinner(final String catId) {
        try {
            ((BrandViewModel) ViewModelProviders.of(this, new BrandFactory()).get(BrandViewModel.class)).getBrands().observe(this, new Observer<List<BrandEntity>>() {
                public void onChanged(@Nullable List<BrandEntity> brandEntityList1) {
                    if (!brandEntityList1.isEmpty()) {
                        AddProduct.this.brandEntityList = brandEntityList1;
                        AddProduct.this.setToBrandSpinner(brandEntityList1, catId);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setToSpinner(List<CategoryEntity> categoryEntities) {
        this.categoryEntityList = categoryEntities;
        List<String> catList = new ArrayList();
        try {
            for (CategoryEntity categoryEntity : categoryEntities) {
                catList.add(categoryEntity.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.category_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    private void setToSubCatSpinner(List<SubCategoryEntity> subCategoryEntities, String catId) {
        this.subCategoryEntityList.clear();
        try {
            List<String> catList = new ArrayList();
            try {
                for (SubCategoryEntity subCategoryEntity : subCategoryEntities) {
                    if (subCategoryEntity.getCat_id().equalsIgnoreCase(catId)) {
                        catList.add(subCategoryEntity.getSub_cat_name());
                        this.subCategoryEntityList.add(subCategoryEntity);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.subcategory_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void setToBrandSpinner(List<BrandEntity> brandEntities, String catId) {
        List<String> catList = new ArrayList();
        catList.add("Select");
        try {
            for (BrandEntity subCategoryEntity : brandEntities) {
               // if (subCategoryEntity.getCat_id().equalsIgnoreCase(catId)) {
                    catList.add(subCategoryEntity.getBrand_nm());
                //}
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.brand_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }
}
