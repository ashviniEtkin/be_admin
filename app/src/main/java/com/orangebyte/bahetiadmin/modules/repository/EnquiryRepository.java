package com.orangebyte.bahetiadmin.modules.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.db.daos.EnquiryDAO;
import com.orangebyte.bahetiadmin.modules.entities.EnquiryEntity;

import java.util.ArrayList;
import java.util.List;

public class EnquiryRepository {
    private static EnquiryRepository sInstance;
    private final EnquiryDAO enquiryDAO;
    private final RemoteRepository remoteRepository;

    interface RemoteEnquiryListener {
        void onCheckResult(boolean z);

        void onRemotReceived(EnquiryEntity enquiryEntity);

        void onRemoteReceived(List<EnquiryEntity> list);
    }

    interface TaskListener {
        void onTaskFinished();
    }

    public static EnquiryRepository getInstance(RemoteRepository remoteRepository, EnquiryDAO enquiryDAO) {
        if (sInstance == null) {
            sInstance = new EnquiryRepository(remoteRepository, enquiryDAO);
        }
        return sInstance;
    }

    private EnquiryRepository(RemoteRepository remoteRepository, EnquiryDAO enquiryDAO) {
        this.remoteRepository = remoteRepository;
        this.enquiryDAO = enquiryDAO;
    }

    public LiveData<List<Boolean>> actionEnquiry(EnquiryEntity enquiryEntity , int chk) {
        final MutableLiveData<List<Boolean>> data = new MutableLiveData();
        actionEnquiry(new RemoteEnquiryListener() {
            public void onRemotReceived(EnquiryEntity enquiryEntity) {
            }

            public void onRemoteReceived(List<EnquiryEntity> list) {
            }

            public void onCheckResult(boolean b) {
                List<Boolean> list = new ArrayList();
                if (b) {
                    list.add(Boolean.valueOf(b));
                    data.setValue(list);
                }
            }
        }, enquiryEntity,chk);
        return data;
    }

    public LiveData<List<EnquiryEntity>> loadEnquiryList(EnquiryEntity enquiryEntity) {
        fetchEnquiryList(new RemoteEnquiryListener() {
            public void onRemotReceived(EnquiryEntity cmEntity) {
            }

            public void onRemoteReceived(List<EnquiryEntity> scmEntities) {
                if (scmEntities != null) {
                    EnquiryRepository.this.insertAllTask(scmEntities);
                }
            }

            public void onCheckResult(boolean b) {
            }
        }, enquiryEntity);
        return this.enquiryDAO.loadAll();
    }

    private void insertAllTask(List<EnquiryEntity> enquiryEntity) {
        insertEnquiries(enquiryEntity, null);
    }

    private void insertEnquiries(final List<EnquiryEntity> enquiryEntity, @Nullable final TaskListener listener) {
        if (VERSION.SDK_INT >= 3) {
            new AsyncTask<Context, Void, Void>() {
                protected Void doInBackground(Context... params) {
                    EnquiryRepository.this.enquiryDAO.deleteAll();
                    EnquiryRepository.this.enquiryDAO.insertAll(enquiryEntity);
                    return null;
                }

                protected void onPostExecute(Void aVoid) {
                    if (VERSION.SDK_INT >= 3) {
                        super.onPostExecute(aVoid);
                    }
                    if (listener != null) {
                        listener.onTaskFinished();
                    }
                }
            }.execute(new Context[]{BahetiEnterprises.getInstance()});
        }
    }

    private void insertEnqu(EnquiryEntity enquiryEntity) {
        insertEnquiry(enquiryEntity, null);
    }

    private void insertEnquiry(final EnquiryEntity enquiryEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                EnquiryRepository.this.enquiryDAO.insert(enquiryEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteEnq(EnquiryEntity enquiryEntity) {
        deletEnquiry(enquiryEntity, null);
    }

    private void deletEnquiry(final EnquiryEntity enquiryEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                EnquiryRepository.this.enquiryDAO.delete(enquiryEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteAllEnquiry() {
        deleteAllEnquiry(null);
    }

    private void deleteAllEnquiry(@Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                EnquiryRepository.this.enquiryDAO.deleteAll();
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void fetchEnquiryList(@NonNull final RemoteEnquiryListener listener, EnquiryEntity enquiryEntity) {
        this.remoteRepository.getEnquiry(enquiryEntity, new RemoteCallback<List<EnquiryEntity>>() {
            public void onSuccess(List<EnquiryEntity> response) {
                listener.onRemoteReceived(response);
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }

    private void actionEnquiry(@NonNull final RemoteEnquiryListener listener, EnquiryEntity enquiryEntity, int chk) {
        this.remoteRepository.enquiryActionTaken(enquiryEntity, chk, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }
}
