package com.orangebyte.bahetiadmin.modules.models;

public interface ManagerModel extends IdentifiableModel {
    String getMgr_type();

    String getMobile_no();

    String getName();

    String getStatus();
}
