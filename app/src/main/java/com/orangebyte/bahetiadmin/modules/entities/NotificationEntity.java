package com.orangebyte.bahetiadmin.modules.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "notifications")
public class NotificationEntity extends IdentifiableEntity implements Serializable {
    @SerializedName("cat_id")
    @ColumnInfo(name = "cat_id")
    String cat_id;
    @SerializedName("msg")
    @ColumnInfo(name = "msg")
    String msg;
    @SerializedName("ondate")
    @ColumnInfo(name = "ondate")
    String ondate;
    @SerializedName("sent_to")
    @ColumnInfo(name = "sent_to")
    String sent_to;
    @SerializedName("subcat_id")
    @ColumnInfo(name = "subcat_id")
    String subcat_id;

    @SerializedName("type")
    @ColumnInfo(name = "type")
    String type;

    public NotificationEntity() {
    }

    public /* bridge */ /* synthetic */ long getId() {
        return super.getId();
    }

    public /* bridge */ /* synthetic */ void setId(long j) {
        super.setId(j);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCat_id() {
        return this.cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getSubcat_id() {
        return this.subcat_id;
    }

    public void setSubcat_id(String subcat_id) {
        this.subcat_id = subcat_id;
    }

    public String getSent_to() {
        return this.sent_to;
    }

    public void setSent_to(String sent_to) {
        this.sent_to = sent_to;
    }

    public String getOndate() {
        return this.ondate;
    }

    public void setOndate(String ondate) {
        this.ondate = ondate;
    }
}
