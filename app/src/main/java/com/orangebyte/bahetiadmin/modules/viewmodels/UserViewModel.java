package com.orangebyte.bahetiadmin.modules.viewmodels;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider.NewInstanceFactory;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.modules.db.local.DatabaseCreator;
import com.orangebyte.bahetiadmin.modules.entities.UserEntity;
import com.orangebyte.bahetiadmin.modules.repository.RemoteRepository;
import com.orangebyte.bahetiadmin.modules.repository.UserRepository;
import com.orangebyte.bahetiadmin.modules.ui.AllContract.UserDataStreams;

import java.util.List;

public class UserViewModel extends ViewModel implements UserDataStreams {
    private UserRepository userRepository;

    public static class UserFactory extends NewInstanceFactory {
        UserRepository uUserRepository;

        public UserFactory() {
            this(null);
        }

        public UserFactory(@Nullable UserRepository uUserRepository) {
            this.uUserRepository = uUserRepository;
        }

        public <T extends ViewModel> T create(Class<T> cls) {
            return (T) new UserViewModel(this.uUserRepository);
        }
    }

    public UserViewModel() {
        this(null);
    }

    public UserViewModel(@Nullable UserRepository userRepository) {
        if (userRepository != null) {
            this.userRepository = userRepository;
        }
    }
    @Override
    public LiveData<List<Boolean>> addUser(UserEntity userEntity) {
        onInstanceCreated();
        return addU(userEntity);
    }

    private LiveData<List<UserEntity>> usersObservable(UserEntity userEntity) {
        return this.userRepository.worksheetList(userEntity);
    }

    private void onInstanceCreated() {
        try {
            if (this.userRepository == null) {
                this.userRepository = UserRepository.getInstance(RemoteRepository.getInstance());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private LiveData<List<Boolean>> addU(final UserEntity userEntity) {
        // Observe if/when database is created.
        return Transformations.switchMap(DatabaseCreator.getInstance().isDatabaseCreated(),
                new Function<Boolean, LiveData<List<Boolean>>>() {
                    @Override
                    public LiveData<List<Boolean>> apply(Boolean isDbCreated) {
                        if (Boolean.TRUE.equals(isDbCreated)) {
                            onInstanceCreated();
                            return userRepository.addUsr(userEntity);
                        }
                        return null;
                    }
                });
    }


    public LiveData<List<UserEntity>> getUsersByName(UserEntity userEntity) {
        onInstanceCreated();
        return usersObservable(userEntity);
    }
}
