package com.orangebyte.bahetiadmin.modules.viewmodels;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider.NewInstanceFactory;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.modules.db.local.AppDatabase;
import com.orangebyte.bahetiadmin.modules.db.local.DatabaseCreator;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.models.Cat;
import com.orangebyte.bahetiadmin.modules.repository.CategoryRepository;
import com.orangebyte.bahetiadmin.modules.repository.RemoteRepository;
import com.orangebyte.bahetiadmin.modules.ui.AllContract.DataStreams;

import java.util.List;

import im.delight.android.webview.BuildConfig;

public class CategoryViewMdels extends ViewModel implements DataStreams {
    private LiveData<List<CategoryEntity>> catObservable;
    private CategoryRepository catRepository;

    public static class Factory extends NewInstanceFactory {
        CategoryRepository mCatRepository;

        public Factory() {
            this(null, null);
        }

        public Factory(AppDatabase appDatabase) {
            this(CategoryRepository.getInstance(RemoteRepository.getInstance(), appDatabase.catDao()), BuildConfig.VERSION_NAME);
        }

        public Factory(@Nullable CategoryRepository mCatRepository, @Nullable String abc) {
            this.mCatRepository = mCatRepository;
        }

        public <T extends ViewModel> T create(Class<T> cls) {
             return (T) new CategoryViewMdels(this.mCatRepository);
        }
    }

    public CategoryViewMdels() {
        this(null);
    }

    public CategoryViewMdels(@Nullable CategoryRepository catRepository) {
        if (catRepository != null) {
            this.catRepository = catRepository;
        }
        this.catObservable = subscribeToLocalData();
    }

    public LiveData<List<CategoryEntity>> getCats() {
        return this.catObservable;
    }

    public LiveData<List<CategoryEntity>> addCategory(Cat c, int chk) {
        onDatabaseCreated();
        return addCat(c, chk);
    }

    public LiveData<List<CategoryEntity>> updateCatStatus(Cat c) {
        onDatabaseCreated();
        return updtCatStatus(c);
    }

    public LiveData<List<CategoryEntity>> deleteCategoryById(Cat cat) {
        onDatabaseCreated();
        return deleteCatById(cat);
    }

    private LiveData<List<CategoryEntity>> subscribeToLocalData() {
        return Transformations.switchMap(DatabaseCreator.getInstance().isDatabaseCreated(), new Function<Boolean, LiveData<List<CategoryEntity>>>() {
            public LiveData<List<CategoryEntity>> apply(Boolean isDbCreated) {
                if (!Boolean.TRUE.equals(isDbCreated)) {
                    return null;
                }
                CategoryViewMdels.this.onDatabaseCreated();
                return CategoryViewMdels.this.subscribeCatObservable();
            }
        });
    }

    private LiveData<List<CategoryEntity>> subscribeCatObservable() {
        return this.catRepository.loadList();
    }

    private LiveData<List<CategoryEntity>> addCat(Cat cat, int chk) {
        return this.catRepository.addCatgory(cat, chk);
    }

    private LiveData<List<CategoryEntity>> updtCatStatus(Cat cat) {
        return this.catRepository.updateCatStatus(cat);
    }

    private LiveData<List<CategoryEntity>> deleteCatById(Cat c) {
        return this.catRepository.deleteCategoryByID(c);
    }

    private void onDatabaseCreated() {
        AppDatabase appDatabase = DatabaseCreator.getInstance().getDatabase();
        if (this.catRepository == null) {
            this.catRepository = CategoryRepository.getInstance(RemoteRepository.getInstance(), appDatabase.catDao());
        }
    }
}
