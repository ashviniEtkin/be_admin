package com.orangebyte.bahetiadmin.modules.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.admin.AddSUbCategory;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SubCategoryAdapter extends Adapter<SubCategoryAdapter.SubCategoryHolder> {
    Context ctx;
    List<SubCategoryEntity> dataSet = new ArrayList();
    ClickListener listener;

    public interface ClickListener {
        void onItemClicked(SubCategoryEntity subCategoryEntity);
    }

    public class SubCategoryHolder extends ViewHolder {
        ImageButton chng_status;
        ImageButton del;
        ImageButton edt;
        TextView srl_no;
        TextView sub_category_nm;

        public SubCategoryHolder(View itemView) {
            super(itemView);
            this.sub_category_nm = (TextView) itemView.findViewById(R.id.category_nm);
            this.edt = (ImageButton) itemView.findViewById(R.id.edt);
            this.del = (ImageButton) itemView.findViewById(R.id.del);
            this.chng_status = (ImageButton) itemView.findViewById(R.id.chng_status);
            this.srl_no = (TextView) itemView.findViewById(R.id.srl_no);
        }
    }

    public SubCategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SubCategoryHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.category_row, parent, false));
    }

    public void onBindViewHolder(SubCategoryHolder holder, final int position) {
        holder.srl_no.setText((position + 1) + ".");
        holder.sub_category_nm.setText(((SubCategoryEntity) this.dataSet.get(position)).getSubCatName());
        if (((SubCategoryEntity) this.dataSet.get(position)).getStatus().equalsIgnoreCase("1")) {
            holder.chng_status.setImageResource(R.drawable.ic_unhide);
            holder.chng_status.setColorFilter(this.ctx.getResources().getColor(R.color.app_color));
        } else {
            holder.chng_status.setImageResource(R.drawable.ic_hide);
            holder.chng_status.setColorFilter(this.ctx.getResources().getColor(R.color.black));
        }
        holder.edt.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(SubCategoryAdapter.this.ctx, AddSUbCategory.class);
                i.putExtra(BahetiEnterprises.CHK_ADD_UPDATE, "1");
                i.putExtra(BahetiEnterprises.SUBCATEGORY, (Serializable) SubCategoryAdapter.this.dataSet.get(position));
                SubCategoryAdapter.this.ctx.startActivity(i);
            }
        });
        holder.del.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
            }
        });
    }

    public void setItems(List<SubCategoryEntity> itemsList) {
        this.dataSet.clear();
        this.dataSet.addAll(itemsList);
        notifyDataSetChanged();
    }

    public SubCategoryAdapter(ClickListener listener, Context ctx) {
        setHasStableIds(true);
        this.listener = listener;
        this.ctx = ctx;
    }

    public long getItemId(int position) {
        return this.dataSet.size() >= position ? ((SubCategoryEntity) this.dataSet.get(position)).getId() : -1;
    }

    public int getItemCount() {
        return this.dataSet.size();
    }
}
