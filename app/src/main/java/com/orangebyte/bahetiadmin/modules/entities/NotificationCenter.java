package com.orangebyte.bahetiadmin.modules.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;

/**
 * Created by Ashvini on 3/29/2018.
 */
@Entity(tableName = "notification_center")
public class NotificationCenter extends IdentifiableEntity{

    @ColumnInfo(name = "msg")
    String msg;

    @ColumnInfo(name = "dt")
    String dt;

    @ColumnInfo(name = "title")
    String title;


    @ColumnInfo(name = "type")
    String type;

    @ColumnInfo(name = "from")
    String from;

    @ColumnInfo(name = "read")
    int read;


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }
}
