package com.orangebyte.bahetiadmin.modules.admin;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableRow;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.NotificationEntity;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels.Factory;
import com.orangebyte.bahetiadmin.modules.viewmodels.NotificationViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.NotificationViewModel.NotificationFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel.SubCategoryFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;

import im.delight.android.webview.BuildConfig;

import java.util.ArrayList;
import java.util.List;

public class Notification extends AppCompatActivity implements LifecycleRegistryOwner {
    List<SubCategoryEntity> allSubCat = new ArrayList();
    List<CategoryEntity> categoryEntityList = new ArrayList();
    Spinner category_spinner;
    CheckBox cb_noti;
    CheckBox cb_sms;
    RadioGroup desi_rd_grp;
    AutoCompleteTextView edt_cell_phone;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    EditText msg;
    RadioButton rdo_all;
    RadioButton rdo_single;
    Button send;
    List<SubCategoryEntity> subCategoryEntityList = new ArrayList();
    TableRow sub_cat_container;
    Spinner subcategory_spinner;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        BahetiEnterprises.setActBar(getResources().getString(R.string.Notification), this, false);
        initView();
        setCatgoryToSpinner();
        this.send.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Notification.this.initModelView();
            }
        });
        this.category_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (Notification.this.allSubCat.isEmpty()) {
                    Notification.this.setSubCatgoryToSpinner();
                } else if (i > 0) {
                    Notification.this.setToSubCatSpinner(((CategoryEntity) Notification.this.categoryEntityList.get(i - 1)).getId() + BuildConfig.VERSION_NAME);
                }
                if (i == 0) {
                    Notification.this.sub_cat_container.setVisibility(8);
                } else {
                    Notification.this.sub_cat_container.setVisibility(0);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.rdo_single.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    Notification.this.edt_cell_phone.setVisibility(0);
                } else {
                    Notification.this.edt_cell_phone.setVisibility(8);
                }
            }
        });
    }

    public void initModelView() {
        NotificationViewModel viewModel = (NotificationViewModel) ViewModelProviders.of(this, new NotificationFactory()).get(NotificationViewModel.class);
        NotificationEntity ne = new NotificationEntity();
        if (this.category_spinner.getSelectedItemPosition() == 0) {
            ne.setCat_id("all");
        } else {
            try {
                if (BahetiEnterprises.checkSpinner(this.category_spinner)) {
                    ne.setCat_id("all");
                } else {
                    ne.setCat_id(((CategoryEntity) this.categoryEntityList.get(this.category_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);
                }
            } catch (Exception e) {
                e.printStackTrace();
                ne.setCat_id("all");
            }
        }
        if (this.subcategory_spinner.getSelectedItemPosition() == 0) {
            ne.setSubcat_id("all");
        } else {
            try {
                if (BahetiEnterprises.checkSpinner(this.subcategory_spinner)) {
                    ne.setSubcat_id("all");
                } else {
                    ne.setSubcat_id(((SubCategoryEntity) this.subCategoryEntityList.get(this.subcategory_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                ne.setSubcat_id("all");
            }
        }
        if (this.rdo_all.isChecked()) {
            ne.setSent_to("all");
        } else {
            ne.setSent_to(this.edt_cell_phone.getText().toString());
        }


        if ((cb_sms.isChecked() == true && cb_noti.isChecked() == true)) {

            ne.setType("both");
        } else if (cb_sms.isChecked() == true && cb_noti.isChecked() == false) {
            ne.setType("sms");
        } else {
            ne.setType("notification");
        }

        ne.setMsg(this.msg.getText().toString());
        ne.setOndate(System.currentTimeMillis() + BuildConfig.VERSION_NAME);
        if (TextUtils.isEmpty(this.msg.getText().toString())) {
            BahetiEnterprises.retShowAlertDialod("Please Enter Message", this);
        } else if (this.rdo_single.isChecked() && TextUtils.isEmpty(this.edt_cell_phone.getText().toString())) {
            BahetiEnterprises.retShowAlertDialod("Please Enter Mobile", this);
        } else {
            addNotification(viewModel, ne);
        }
    }

    private void addNotification(NotificationViewModel viewModel, NotificationEntity ne) {
        try {
            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, 5);
            sweetAlertDialog.setTitle("Loading");
            sweetAlertDialog.show();
            viewModel.addNotifcation(ne).observe(this, new Observer<List<NotificationEntity>>() {
                public void onChanged(@Nullable List<NotificationEntity> notificationEntities) {
                    if (!notificationEntities.isEmpty()) {
                        sweetAlertDialog.dismiss();
                        Notification.this.onBackPressed();
                    } else if (notificationEntities.size() == 0) {
                        sweetAlertDialog.dismiss();
                        Notification.this.clear();
                        sweetAlertDialog.changeAlertType(2);
                        sweetAlertDialog.setTitle("DONE");
                        sweetAlertDialog.setTitleText("Notification sent Successfully");
                        sweetAlertDialog.show();
                    } else {
                        sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clear() {
        this.msg.setText(BuildConfig.VERSION_NAME);
        this.edt_cell_phone.setText(BuildConfig.VERSION_NAME);
        this.cb_noti.setChecked(false);
        this.cb_sms.setChecked(false);
    }

    public void initView() {
        this.rdo_single = (RadioButton) findViewById(R.id.rdo_single);
        this.rdo_all = (RadioButton) findViewById(R.id.rdo_all);
        this.category_spinner = (Spinner) findViewById(R.id.category_spinner);
        this.subcategory_spinner = (Spinner) findViewById(R.id.subcategory_spinner);
        this.cb_noti = (CheckBox) findViewById(R.id.cb_noti);
        this.cb_sms = (CheckBox) findViewById(R.id.cb_sms);
        this.desi_rd_grp = (RadioGroup) findViewById(R.id.desi_rd_grp);
        this.edt_cell_phone = (AutoCompleteTextView) findViewById(R.id.edt_cell_phone);
        this.msg = (EditText) findViewById(R.id.msg);
        this.send = (Button) findViewById(R.id.send);
        this.sub_cat_container = (TableRow) findViewById(R.id.sub_cat_container);
    }

    public void setCatgoryToSpinner() {
        try {
            ((CategoryViewMdels) ViewModelProviders.of(this, new Factory()).get(CategoryViewMdels.class)).getCats().observe(this, new Observer<List<CategoryEntity>>() {
                public void onChanged(@Nullable List<CategoryEntity> categoryEntities) {
                    if (!categoryEntities.isEmpty()) {
                        Notification.this.setToSpinner(categoryEntities);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setSubCatgoryToSpinner() {
        try {
            ((SubCategoryViewModel) ViewModelProviders.of(this, new SubCategoryFactory()).get(SubCategoryViewModel.class)).getSubCats().observe(this, new Observer<List<SubCategoryEntity>>() {
                public void onChanged(@Nullable List<SubCategoryEntity> subCategoryEntities) {
                    if (!subCategoryEntities.isEmpty()) {
                        Notification.this.allSubCat = subCategoryEntities;
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setToSpinner(List<CategoryEntity> categoryEntities) {
        this.categoryEntityList = categoryEntities;
        List<String> catList = new ArrayList();
        catList.add("All");
        try {
            for (CategoryEntity categoryEntity : categoryEntities) {
                catList.add(categoryEntity.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.category_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    private void setToSubCatSpinner(String catId) {
        subCategoryEntityList.clear();

        List<String> catList = new ArrayList();
        catList.add("All");
        try {
            for (SubCategoryEntity subCategoryEntity : this.allSubCat) {
                if (subCategoryEntity.getCat_id().equalsIgnoreCase(catId)) {
                    catList.add(subCategoryEntity.getSub_cat_name());
                    this.subCategoryEntityList.add(subCategoryEntity);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.subcategory_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }
}
