package com.orangebyte.bahetiadmin.modules.db.local;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;

import java.util.concurrent.atomic.AtomicBoolean;

public class DatabaseCreator {
    private static final Object LOCK = new Object();
    private static DatabaseCreator sInstance;
    private AppDatabase mDb;
    private final AtomicBoolean mInitializing = new AtomicBoolean(true);
    private final MutableLiveData<Boolean> mIsDatabaseCreated = new MutableLiveData();

    public static synchronized DatabaseCreator getInstance() {
        DatabaseCreator instance;
        synchronized (DatabaseCreator.class) {
            instance = getInstance(BahetiEnterprises.getInstance());
        }
        return instance;
    }

    public static synchronized DatabaseCreator getInstance(Application application) {
        DatabaseCreator databaseCreator;
        synchronized (DatabaseCreator.class) {
            if (sInstance == null) {
                synchronized (LOCK) {
                    if (sInstance == null) {
                        sInstance = new DatabaseCreator(application);
                    }
                }
            }
            databaseCreator = sInstance;
        }
        return databaseCreator;
    }

    public DatabaseCreator(Application application) {
        createDb(application);
    }

    public LiveData<Boolean> isDatabaseCreated() {
        return this.mIsDatabaseCreated;
    }

    @Nullable
    public AppDatabase getDatabase() {
        return this.mDb;
    }

    private void createDb(Context context) {
        Log.d("DatabaseCreator", "Creating DB from " + Thread.currentThread().getName());
        if (this.mInitializing.compareAndSet(true, false)) {
            this.mIsDatabaseCreated.setValue(Boolean.valueOf(false));
            new AsyncTask<Context, Void, AppDatabase>() {
                protected AppDatabase doInBackground(Context... params) {
                    Log.d("DatabaseCreator", "Starting bg job " + Thread.currentThread().getName());
                    return (AppDatabase) Room.databaseBuilder(params[0].getApplicationContext().getApplicationContext(), AppDatabase.class, AppDatabase.DATABASE_NAME).build();
                }

                protected void onPostExecute(AppDatabase appDatabase) {
                    super.onPostExecute(appDatabase);
                    DatabaseCreator.this.mDb = appDatabase;
                    DatabaseCreator.this.mIsDatabaseCreated.setValue(Boolean.valueOf(true));
                }
            }.execute(new Context[]{context.getApplicationContext()});
        }
    }
}
