package com.orangebyte.bahetiadmin.modules.db.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;

import java.util.List;

@Dao
public interface ComplaintDAO {
    @Delete
    void delete(ComplaintEntity... complaintEntityArr);

    @Query("DELETE FROM complaints ")
    void deleteAll();

    @Delete
    void deleteAll(List<ComplaintEntity> list);

    @Insert(onConflict = 1)
    void insert(ComplaintEntity... complaintEntityArr);

    @Insert(onConflict = 1)
    void insertAll(List<ComplaintEntity> list);

    @Query("SELECT * FROM complaints ")
    LiveData<List<ComplaintEntity>> loadAll();

    @Update
    void update(ComplaintEntity... complaintEntityArr);

    @Update
    void updateAll(List<ComplaintEntity> list);
}
