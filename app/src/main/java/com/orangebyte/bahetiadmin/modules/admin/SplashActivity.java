package com.orangebyte.bahetiadmin.modules.admin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.MainActivity;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.modules.employee.EmployeeDashboard;
import com.orangebyte.bahetiadmin.modules.manager.ManagerDashboard;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 4500;
    Boolean log;
    String post = "";
    Thread splashTread;
    SharedPreferences sh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        sh = getSharedPreferences("log", MODE_PRIVATE);
        log = sh.getBoolean("login", false);
        StartAnimations();

    }

    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        LinearLayout l = (LinearLayout) findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);

        Animation anim2 = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim2.reset();

        TextView txt = (TextView) findViewById(R.id.txt);
        txt.clearAnimation();
        txt.startAnimation(anim2);
        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < 4500) {
                        sleep(100);
                        waited += 100;
                    }
                    if (log) {
                        post = sh.getString("post", "");
                        if (post.equalsIgnoreCase("admin")) {
                            startActivity(new Intent(SplashActivity.this, AdminDashboard.class));
                            finish();
                        } else if (post.equalsIgnoreCase("employee")) {
                            startActivity(new Intent(SplashActivity.this, EmployeeDashboard.class));
                            finish();
                        } else {
                            startActivity(new Intent(SplashActivity.this, ManagerDashboard.class));
                            finish();
                        }

                    } else {

                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                        finish();

                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                    // do nothing
                } finally {
                    SplashActivity.this.finish();
                }

            }
        };
        splashTread.start();

    }

}










/*
package com.orangebyte.bahetiadmin.modules.admin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.firebase.analytics.FirebaseAnalytics.Event;
import com.orangebyte.bahetiadmin.MainActivity;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.modules.employee.EmployeeDashboard;
import com.orangebyte.bahetiadmin.modules.manager.ManagerDashboard;
import im.delight.android.webview.BuildConfig;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 4500;
    Boolean log;
    String post = BuildConfig.VERSION_NAME;
    SharedPreferences sh;
    Thread splashTread;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().setFlags(1024, 1024);
        this.sh = getSharedPreferences("log", 0);
        this.log = Boolean.valueOf(this.sh.getBoolean(Event.LOGIN, false));
        StartAnimations();
    }

    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        LinearLayout l = (LinearLayout) findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);
        Animation anim2 = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim2.reset();
        TextView txt = (TextView) findViewById(R.id.txt);
        txt.clearAnimation();
        txt.startAnimation(anim2);
        this.splashTread = new Thread() {
            public void run() {
                int waited = 0;
                while (waited < 4500) {
                    try {
                        sleep(100);
                        waited += 100;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        return;
                    } finally {
                        SplashActivity.this.finish();
                    }
                }
                if (SplashActivity.this.log.booleanValue()) {
                    SplashActivity.this.post = SplashActivity.this.sh.getString("post", BuildConfig.VERSION_NAME);
                    if (SplashActivity.this.post.equalsIgnoreCase("admin")) {
                        SplashActivity.this.startActivity(new Intent(SplashActivity.this, AdminDashboard.class));
                        SplashActivity.this.finish();
                    } else if (SplashActivity.this.post.equalsIgnoreCase("employee")) {
                        SplashActivity.this.startActivity(new Intent(SplashActivity.this, EmployeeDashboard.class));
                        SplashActivity.this.finish();
                    } else {
                        SplashActivity.this.startActivity(new Intent(SplashActivity.this, ManagerDashboard.class));
                        SplashActivity.this.finish();
                    }
                } else {
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    SplashActivity.this.finish();
                }
                SplashActivity.this.finish();
            }
        };
        this.splashTread.start();
    }
}
*/
