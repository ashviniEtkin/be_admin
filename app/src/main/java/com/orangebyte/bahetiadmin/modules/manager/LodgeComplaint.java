package com.orangebyte.bahetiadmin.modules.manager;

import android.app.ProgressDialog;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.adapters.UserAutoCompleteAdapter;
import com.orangebyte.bahetiadmin.modules.admin.AddCustomer;
import com.orangebyte.bahetiadmin.modules.admin.AddInstalltionDemo;
import com.orangebyte.bahetiadmin.modules.entities.BrandEntity;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.entities.ProductEntity;
import com.orangebyte.bahetiadmin.modules.entities.ServiceType;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.UserEntity;
import com.orangebyte.bahetiadmin.modules.models.ServicesResponse;
import com.orangebyte.bahetiadmin.modules.repository.RemoteCallback;
import com.orangebyte.bahetiadmin.modules.repository.ServiceFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.BrandViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels;
import com.orangebyte.bahetiadmin.modules.viewmodels.ComplaintsViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.ProductViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.UserViewModel;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import com.orangebyte.bahetiadmin.views.DelayAutoCompleteTextView;
import com.orangebyte.bahetiadmin.views.MultiSelectionSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class LodgeComplaint extends AppCompatActivity implements LifecycleRegistryOwner {

    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    Spinner product_spinner, category_spinner, subcategory_spinner, brand_spinner, employee_spinner;
    MultiSelectionSpinner service_type;
    EditText desc, edt_amount;
    List<CategoryEntity> categoryEntityList = new ArrayList<>();
    List<SubCategoryEntity> subCategoryEntityList = new ArrayList<>();
    List<ProductEntity> productEntityList = new ArrayList<>();
    Button submit, add_customer;
    DelayAutoCompleteTextView search_user;
    List<BrandEntity> brandEntityList = new ArrayList();
    List<SubCategoryEntity> allSubCat = new ArrayList<>();
    ArrayList<ServiceType> serviceTypeArrayList = new ArrayList<>();
    List<EmployeeEntity> employeeEntityList = new ArrayList<>();
    UserEntity userEntity1;
    Spinner paid_spinner;
    LinearLayout Amount_to_pay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lodge_complaint);
        BahetiEnterprises.setActBar(getString(R.string.lodge_complaint), LodgeComplaint.this, false);
        init();

        setCatgoryToSpinner();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    initComplaintView();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        add_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LodgeComplaint.this, AddCustomer.class));
            }
        });

        category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {
                // setSubCatgoryToSpinner(categoryEntityList.get(i).getId() + "");
                if (allSubCat.isEmpty()) {
                    setSubCatgoryToSpinner(((CategoryEntity) categoryEntityList.get(category_spinner.getSelectedItemPosition())).getId() + "");
                } else {
                    setToSubCatSpinner(allSubCat, ((CategoryEntity) categoryEntityList.get(category_spinner.getSelectedItemPosition())).getId() + "");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        subcategory_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // setProductToSpinner();
                if (brandEntityList.size() == 0) {
                    setBrandToSpinner(categoryEntityList.get(category_spinner.getSelectedItemPosition()).getId() + "");
                } else {
                    setProductToSpinner();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        brand_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setProductToSpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        product_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //  model_nm.setText(productEntityList.get(i).getModel_no());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }


    public void addComplaint(JSONObject js) {


        final ProgressDialog pDialog = new ProgressDialog(LodgeComplaint.this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = BahetiEnterprises.BASE_URL + "/lodgeComplaint/";//AppController.Api_Url + "addMerProd";// "getProductBySuperCategory"; //getProd


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, js, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    boolean t1 = response.getBoolean("success");
                    //     Toast.makeText(LodgeComplaint.this, t1 + "", Toast.LENGTH_LONG).show();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                onBackPressed();

            }
        }, new com.android.volley.Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                //  Toast.makeText(LodgeComplaint.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        BahetiEnterprises.getInstance().addToRequestQueue(req, "lodgeComplaint");

    }


    public void setEmployeeToSpinner() {
        try {
            EmployeeEntity ee = new EmployeeEntity();
            ee.setAssigned_mgr_type("null");
            ((EmployeeViewModel) ViewModelProviders.of(this, new EmployeeViewModel.EmployeeFactory()).get(EmployeeViewModel.class)).getEmps(ee).observe(this, new Observer<List<EmployeeEntity>>() {
                public void onChanged(@Nullable List<EmployeeEntity> employeeEntities) {
                    if (!employeeEntities.isEmpty()) {
                        setEmpToSpinner(employeeEntities, employee_spinner);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setEmpToSpinner(List<EmployeeEntity> employeeEntities, Spinner employee_spinner1) {
        this.employeeEntityList = employeeEntities;
        List<String> catList = new ArrayList();
        try {
            for (EmployeeEntity employeeEntity : employeeEntities) {
                catList.add(employeeEntity.getEmp_nm());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        employee_spinner1.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    public void init() {
        UserViewModel viewModel2 = (UserViewModel) ViewModelProviders.of(this, new UserViewModel.UserFactory()).get(UserViewModel.class);
        product_spinner = (Spinner) findViewById(R.id.product_spinner);
        paid_spinner = (Spinner) findViewById(R.id.paid_spinner);
        // service_type = (Spinner) findViewById(R.id.service_spinner);
        this.employee_spinner = (Spinner) findViewById(R.id.employee_spinner);
        service_type = (MultiSelectionSpinner) findViewById(R.id.service_spinner);
        edt_amount = (EditText) findViewById(R.id.edt_amount);
        desc = (EditText) findViewById(R.id.desc);
        category_spinner = (Spinner) findViewById(R.id.category_spinner);
        subcategory_spinner = (Spinner) findViewById(R.id.subcategory_spinner);
        brand_spinner = (Spinner) findViewById(R.id.brand_spinner);
        submit = (Button) findViewById(R.id.submit);
        add_customer = (Button) findViewById(R.id.add_customer);
        Amount_to_pay = (LinearLayout) findViewById(R.id.Amount_to_pay);

        this.search_user = (DelayAutoCompleteTextView) findViewById(R.id.search_user);
        this.search_user.setThreshold(2);
        this.search_user.setAdapter(new UserAutoCompleteAdapter(this, (ProgressBar) findViewById(R.id.pb_loading_indicator), viewModel2));
        this.search_user.setLoadingIndicator((ProgressBar) findViewById(R.id.pb_loading_indicator));
        this.search_user.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                userEntity1 = (UserEntity) adapterView.getItemAtPosition(position);
                search_user.setText(userEntity1.getUsernm());
            }
        });

        List<String> serviceTypeList = Arrays.asList(getResources().getStringArray(R.array.service_type));


        for (int i = 0; i < serviceTypeList.size(); i++) {
            ServiceType serviceType = new ServiceType();
            serviceType.setTypenm(serviceTypeList.get(i));
            serviceType.setSelected(false);
            serviceTypeArrayList.add(serviceType);
        }

        service_type.setItems(serviceTypeList);
        //    service_type.setAdapter(new ServiceAdapter(getApplicationContext(), R.layout.lodge_spinner_layout, serviceTypeArrayList));

        paid_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 1) {
                    Amount_to_pay.setVisibility(View.VISIBLE);
                } else {
                    Amount_to_pay.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public String getServiceType() {
        String typs = "";
      /*  for (int i = 0; i < serviceTypeArrayList.size(); i++) {
            if (serviceTypeArrayList.get(i).isSelected()) {
                typs = typs + serviceTypeArrayList.get(i).getTypenm() + ",";
            }
        }*/
        typs = service_type.getSelectedItemsAsString();
        return typs;
    }

    public void initComplaintView() throws JSONException {
        ComplaintsViewModel.ComplaintFactory factory = new ComplaintsViewModel.ComplaintFactory();
        final ComplaintsViewModel viewModel = ViewModelProviders.of(LodgeComplaint.this, factory).get(ComplaintsViewModel.class);
        final ComplaintEntity ce = new ComplaintEntity();
        String serviceTypes = getServiceType();

        if (BahetiEnterprises.checkSpinner(product_spinner) == true) {
            BahetiEnterprises.retShowAlertDialod("Please Select Product", LodgeComplaint.this);
        } else if (BahetiEnterprises.checkSpinner(employee_spinner) == true) {
            BahetiEnterprises.retShowAlertDialod("Please Select Employee", LodgeComplaint.this);
        } else if (TextUtils.isEmpty(desc.getText().toString())) {
            BahetiEnterprises.retShowAlertDialod("Please Enter Description", LodgeComplaint.this);
        } /*else if (service_type.getSelectedItemPosition() == 0) {
            BahetiEnterprises.retShowAlertDialod("Please Select Service Type", LodgeComplaint.this);
        }*/ else if (TextUtils.isEmpty(serviceTypes)) {
            BahetiEnterprises.retShowAlertDialod("Please Select Service Type", LodgeComplaint.this);
        }
        if (this.userEntity1 == null) {
            BahetiEnterprises.retShowAlertDialod("Please Select User", this);
        } else {
            ce.setProduct_id(productEntityList.get(product_spinner.getSelectedItemPosition()).getId() + "");
            ce.setDesc(desc.getText().toString());
            ce.setComp_date(System.currentTimeMillis() + "");
            //  ce.setService_type(service_type.getSelectedItem().toString());
            ce.setUser_id(userEntity1.getId() + "");
            ce.setService_type(serviceTypes);
            ce.setStatus("Pending");
            ce.setAssigned_empid(employeeEntityList.get(employee_spinner.getSelectedItemPosition()).getId() + "");
            //lComplaint(ce);

            ce.setPaid_type(paid_spinner.getSelectedItem().toString());
            if (TextUtils.isEmpty(edt_amount.getText().toString())) {
                ce.setPaid_amt("0");
            } else {
                ce.setPaid_amt(edt_amount.getText().toString());
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("product_id", ce.getProduct_id());
            jsonObject.put("desc", ce.getDesc());
            jsonObject.put("comp_date", ce.getComp_date());
            jsonObject.put("user_id", ce.getUser_id());
            jsonObject.put("service_type", ce.getService_type());
            jsonObject.put("status", ce.getStatus());
            jsonObject.put("assigned_empid", ce.getAssigned_empid());
            jsonObject.put("paid_amt", ce.getPaid_amt());
            jsonObject.put("paid_type", ce.getPaid_type());
            addComplaint(jsonObject);
        }


    }


    public void lComplaint(ComplaintEntity complaintEntity) {
        try {
            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/ldgeComplaint/")
                    .lodgeComplaint(complaintEntity)
                    .enqueue(new Callback<ServicesResponse>() {
                        public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                            try {
                                ((ServicesResponse) response.body()).isSuccess();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        public void onFailure(Call<ServicesResponse> call, Throwable t) {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void lodgeComplaint(ComplaintsViewModel viewModel, ComplaintEntity c) {
        try {

            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            sweetAlertDialog.setTitle("Loading");
            sweetAlertDialog.setTitleText("Please wait while lodging complaint");

            //  sweetAlertDialog.show();
            viewModel.assignedEmp(c, 3).observe(this, new Observer<List<Boolean>>() {
                @Override
                public void onChanged(@Nullable List<Boolean> complaintEntities) {
                    if (!complaintEntities.isEmpty()) {
                        sweetAlertDialog.dismiss();
                        onBackPressed();
                    } else if (complaintEntities.size() == 0) {
                        sweetAlertDialog.dismiss();
                        //   onBackPressed();
                    } else {
                        sweetAlertDialog.show();
                    }


                }


            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void setCatgoryToSpinner() {
        try {

            CategoryViewMdels.Factory factory = new CategoryViewMdels.Factory();
            CategoryViewMdels viewModel2 = ViewModelProviders.of(this, factory).get(CategoryViewMdels.class);
            viewModel2.getCats().observe(this, new Observer<List<CategoryEntity>>() {
                @Override
                public void onChanged(@Nullable List<CategoryEntity> categoryEntities) {
                    if (!categoryEntities.isEmpty()) {

                        setToSpinner(categoryEntities);

                    } else {

                    }


                }


            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public void setSubCatgoryToSpinner(final String catId) {
        try {

            SubCategoryViewModel.SubCategoryFactory factory = new SubCategoryViewModel.SubCategoryFactory();
            SubCategoryViewModel viewModel2 = ViewModelProviders.of(this, factory).get(SubCategoryViewModel.class);
            viewModel2.getSubCats().observe(this, new Observer<List<SubCategoryEntity>>() {
                @Override
                public void onChanged(@Nullable List<SubCategoryEntity> subCategoryEntities) {
                    if (!subCategoryEntities.isEmpty()) {
                        allSubCat = subCategoryEntities;
                        setToSubCatSpinner(subCategoryEntities, catId);

                    } else {

                    }


                }


            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setBrandToSpinner(final String catId) {
        try {
            ((BrandViewModel) ViewModelProviders.of(this, new BrandViewModel.BrandFactory()).get(BrandViewModel.class)).getBrands().observe(this, new Observer<List<BrandEntity>>() {
                public void onChanged(@Nullable List<BrandEntity> brandEntityList) {
                    if (!brandEntityList.isEmpty()) {
                        setToBrandSpinner(brandEntityList, catId);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setToBrandSpinner(List<BrandEntity> brandEntities, String catId) {
        this.brandEntityList = brandEntities;
        List<String> catList = new ArrayList();
        catList.add("Select");
        try {
            for (BrandEntity subCategoryEntity : brandEntities) {
                //  if (subCategoryEntity.getCat_id().equalsIgnoreCase(catId)) {
                catList.add(subCategoryEntity.getBrand_nm());
                //  }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        brand_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }


    public void setProductToSpinner() {
        try {
            ProductViewModel.ProductFactory factory = new ProductViewModel.ProductFactory();
            ProductViewModel viewModel2 = ViewModelProviders.of(this, factory).get(ProductViewModel.class);

            ProductEntity pe = new ProductEntity();
            pe.setCat_id(categoryEntityList.get(category_spinner.getSelectedItemPosition()).getId() + "");

            try {

                pe.setSub_cat_id(subCategoryEntityList.get(subcategory_spinner.getSelectedItemPosition()).getId() + "");


            } catch (Exception e) {
                e.printStackTrace();
                pe.setSub_cat_id("null");
            }

            if (brand_spinner.getSelectedItemPosition() == 0) {
                pe.setBrand_id("null");
            } else {
                pe.setBrand_id(brandEntityList.get(brand_spinner.getSelectedItemPosition() - 1).getId() + "");
            }
            viewModel2.getProduct(pe).observe(this, new Observer<List<ProductEntity>>() {
                @Override
                public void onChanged(@Nullable List<ProductEntity> productEntityList) {
                    if (!productEntityList.isEmpty()) {
                        setProductDataToSpinner(productEntityList);
                    } else {
                        setProductDataToSpinner(new ArrayList<ProductEntity>());
                    }

                    setEmployeeToSpinner();

                }


            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private void setToSpinner(List<CategoryEntity> categoryEntities) {
        this.categoryEntityList = categoryEntities;
        List<String> catList = new ArrayList<>();
        for (CategoryEntity categoryEntity : categoryEntities) {
            catList.add(categoryEntity.getName());
        }

        ArrayAdapter<String> adapter_cat = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_layout, catList);
        category_spinner.setAdapter(adapter_cat);


    }


    private void setProductDataToSpinner(List<ProductEntity> productEntities) {
        this.productEntityList = productEntities;
        List<String> prodLIst = new ArrayList<>();
        for (ProductEntity productEntity : productEntities) {
            prodLIst.add(productEntity.getTitle());
        }

        ArrayAdapter<String> adapter_prod = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_layout, prodLIst);
        product_spinner.setAdapter(adapter_prod);


    }


    private void setToSubCatSpinner(List<SubCategoryEntity> subCategoryEntities, String catId) {
        //  this.subCategoryEntityList = subCategoryEntities;
        subCategoryEntityList.clear();

        List<String> catList = new ArrayList<>();
        for (SubCategoryEntity subCategoryEntity : subCategoryEntities) {
            if (subCategoryEntity.getCat_id().equalsIgnoreCase(catId)) {
                catList.add(subCategoryEntity.getSub_cat_name());
                subCategoryEntityList.add(subCategoryEntity);
            }
        }


        ArrayAdapter<String> adapter_cat = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_layout, catList);
        subcategory_spinner.setAdapter(adapter_cat);


    }


    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }
}
