/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.orangebyte.bahetiadmin.notifications;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.orangebyte.bahetiadmin.MainActivity;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.modules.admin.SplashActivity;
import com.orangebyte.bahetiadmin.modules.db.local.AppDatabase;
import com.orangebyte.bahetiadmin.modules.db.local.DatabaseCreator;
import com.orangebyte.bahetiadmin.modules.entities.NotificationCenter;
import com.orangebyte.bahetiadmin.modules.repository.NotiRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/*
import com.google.android.gms.gcm.GcmListenerService;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;*/

public class MyGcmPushReceiver extends FirebaseMessagingService {

    private static final String TAG = MyGcmPushReceiver.class.getSimpleName();

    NotiRepository notificationRepository;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        //Log.e(TAG, "From: " + remoteMessage.getFrom());
        String from1 = remoteMessage.getFrom();
        String title = remoteMessage.getData().get("title");//("title");
        String message = remoteMessage.getData().get("message");

        //  String name1 = remoteMessage.getData().get("name");

        sendNotification(message, title);




        //  String name1 = remoteMessage.getData().get("name");

       // sendNotification(message, title);

        try {
            String type = remoteMessage.getData().get("type");
            String from = remoteMessage.getData().get("frrm");
            NotificationCenter notificationCenter = new NotificationCenter();
            notificationCenter.setDt(System.currentTimeMillis() + "");
            notificationCenter.setFrom(from);
            notificationCenter.setMsg(message);
            notificationCenter.setRead(0);
            notificationCenter.setTitle(title);
            notificationCenter.setType(type);

       /* NotiViewModel.NotiFactory factory1 = new NotiViewModel.NotiFactory();
        Context activity = (Context) BahetiEnterprises.getInstance();
        NotiViewModel viewModel1 = ViewModelProviders.of( , factory1).get(NotiViewModel.class);

        viewModel1.addNotifcation(notificationCenter);*/


            try {
                AppDatabase appDatabase = DatabaseCreator.getInstance().getDatabase();
                try {
                    if (notificationRepository == null) {
                        notificationRepository = new NotiRepository(appDatabase.notiDAO());
                        notificationRepository.addNoti(notificationCenter);// addNotifn(notificationEntity);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * Showing notification with text and image
     */


    private void sendNotification(String messageBody, String message) {


        int notify_no = 1;

        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, notify_no /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        if (notify_no < 9) {
            notify_no = notify_no + 1;
        } else {
            notify_no = 0;
        }

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(messageBody)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notify_no + 2 /* ID of notification */, notificationBuilder.build());
    }


}

