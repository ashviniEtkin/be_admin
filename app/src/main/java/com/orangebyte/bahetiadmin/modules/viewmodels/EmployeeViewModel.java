package com.orangebyte.bahetiadmin.modules.viewmodels;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProvider.NewInstanceFactory;
import android.support.annotation.Nullable;
import com.orangebyte.bahetiadmin.modules.db.local.AppDatabase;
import com.orangebyte.bahetiadmin.modules.db.local.DatabaseCreator;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.repository.EmployeeRepository;
import com.orangebyte.bahetiadmin.modules.repository.RemoteRepository;
import com.orangebyte.bahetiadmin.modules.ui.AllContract.EmployeeDataStreams;
import im.delight.android.webview.BuildConfig;
import java.util.List;

public class EmployeeViewModel extends ViewModel implements EmployeeDataStreams {
    private EmployeeEntity employeeEntity;
    private LiveData<List<EmployeeEntity>> employeeObservable;
    private EmployeeRepository managerRepository;

    public static class EmployeeFactory extends  ViewModelProvider.NewInstanceFactory {
        EmployeeRepository mEmployeeRepository;

        public EmployeeFactory() {
            this(null, null);
        }

        public EmployeeFactory(AppDatabase appDatabase) {
            this(EmployeeRepository.getInstance(RemoteRepository.getInstance(), appDatabase.employeeDao()), BuildConfig.VERSION_NAME);
        }

        public EmployeeFactory(@Nullable EmployeeRepository mEmployeeRepository, @Nullable String abc) {
            this.mEmployeeRepository = mEmployeeRepository;
        }

        public <T extends ViewModel> T create(Class<T> cls) {
            return (T) new EmployeeViewModel(this.mEmployeeRepository);
        }
    }

    public EmployeeViewModel() {
        this(null);
    }

    public EmployeeViewModel(@Nullable EmployeeRepository managerRepository) {
        if (managerRepository != null) {
            this.managerRepository = managerRepository;
        }
    }

    public LiveData<List<EmployeeEntity>> getEmps(EmployeeEntity employeeEntity) {
        this.employeeEntity = employeeEntity;
        return subscribeToLocalData(employeeEntity);
    }

    public LiveData<List<EmployeeEntity>> addEmp(EmployeeEntity employeeEntity, int chk) {
        onDatabaseCreated();
        return addUpdateEmp(employeeEntity, chk);
    }

    public LiveData<List<EmployeeEntity>> updateEmpStatus(EmployeeEntity employeeEntity) {
        onDatabaseCreated();
        return updtEMpsStatus(employeeEntity);
    }

    public LiveData<List<EmployeeEntity>> deleteEMpById(EmployeeEntity employeeEntity) {
        onDatabaseCreated();
        return deleteEMpsByID(employeeEntity);
    }

    private LiveData<List<EmployeeEntity>> subscribeToLocalData(final EmployeeEntity employeeEntity) {
        return Transformations.switchMap(DatabaseCreator.getInstance().isDatabaseCreated(), new Function<Boolean, LiveData<List<EmployeeEntity>>>() {
            public LiveData<List<EmployeeEntity>> apply(Boolean isDbCreated) {
                if (!Boolean.TRUE.equals(isDbCreated)) {
                    return null;
                }
                EmployeeViewModel.this.onDatabaseCreated();
                return EmployeeViewModel.this.subscribeEMmpObservable(employeeEntity);
            }
        });
    }

    private LiveData<List<EmployeeEntity>> subscribeEMmpObservable(EmployeeEntity employeeEntity) {
        return this.managerRepository.loadEMpList(employeeEntity);
    }

    private LiveData<List<EmployeeEntity>> addUpdateEmp(EmployeeEntity employeeEntity, int chk) {
        return this.managerRepository.addUpdateEMp(employeeEntity, chk);
    }

    private LiveData<List<EmployeeEntity>> updtEMpsStatus(EmployeeEntity employeeEntity) {
        return this.managerRepository.updateEMpStatus(employeeEntity);
    }

    private LiveData<List<EmployeeEntity>> deleteEMpsByID(EmployeeEntity c) {
        return this.managerRepository.deleteEMpByID(c);
    }

    private void onDatabaseCreated() {
        AppDatabase appDatabase = DatabaseCreator.getInstance().getDatabase();
        if (this.managerRepository == null) {
            this.managerRepository = EmployeeRepository.getInstance(RemoteRepository.getInstance(), appDatabase.employeeDao());
        }
    }
}
