package com.orangebyte.bahetiadmin.modules.employee;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.adapters.GridAdapters;
import com.orangebyte.bahetiadmin.modules.admin.ChangePassword;
import com.orangebyte.bahetiadmin.modules.admin.NotiCenter;
import com.orangebyte.bahetiadmin.modules.admin.ShowEnquiry;
import com.orangebyte.bahetiadmin.modules.admin.UpdateProfile;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener.OnItemClickListener;
import com.pdfjet.Table;
import com.pdfjet.TextAlign;

import im.delight.android.webview.BuildConfig;

public class EmployeeDashboard extends AppCompatActivity {
    RecyclerView categories;
    int[] saleMgrDesign = new int[]{R.string.Installation_Demo, R.string.enquiry, R.string.noti_center};
    int[] serviceMgrDesign = new int[]{R.string.Complaints, R.string.Installation_Demo, R.string.noti_center};
    GridAdapters adapters;

    SharedPreferences sh;
    String post = "";
    String emp_type = "";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_dashboard);
        init();
        listeners();
    }

    public void init() {

        this.sh = getSharedPreferences("log", 0);
        this.post = this.sh.getString("post", BuildConfig.VERSION_NAME);
        emp_type = sh.getString("assignedMgrType", "");

        ((TextView) findViewById(R.id.screen_nm)).setText(Html.fromHtml(getResources().getString(R.string.baheti_enter)));


        this.categories = (RecyclerView) findViewById(R.id.categories);

        if (emp_type.equalsIgnoreCase("In Shop Demonstrator")) {
            adapters = new GridAdapters(this, this.saleMgrDesign);
        } else {
            adapters = new GridAdapters(this, this.serviceMgrDesign);
        }
        this.categories.setLayoutManager(new LinearLayoutManager(this));
        this.categories.setAdapter(adapters);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
    }

    public void listeners() {
        this.categories.addOnItemTouchListener(new RecyclerItemClickListener(this, new OnItemClickListener() {
            public void onItemClick(View view, int position) {
                EmployeeDashboard.this.intents(position);
            }
        }));
    }

    public void intents(int pos) {
        if (emp_type.equalsIgnoreCase("In Shop Demonstrator")) {

            switch (pos) {

                case Table.DATA_HAS_0_HEADER_ROWS /*0*/:
                    Intent ii = new Intent(this, EmployeeCallLog.class);
                    ii.putExtra(BahetiEnterprises.INTNT, "2");
                    startActivity(ii);
                    return;
                case TextAlign.BOTTOM /*1*/:
                    Intent call_intent = new Intent(this, EmployeeEnquiry.class);
                    call_intent.putExtra(BahetiEnterprises.FROM, "2");
                    startActivity(call_intent);
                    return;
                case 2 /*2*/:

                    Intent noti_intent = new Intent(this, NotiCenter.class);
                    noti_intent.putExtra(BahetiEnterprises.FROM, "2");
                    startActivity(noti_intent);
                    return;
                default:
                    return;
            }

        } else {
            switch (pos) {

                case Table.DATA_HAS_0_HEADER_ROWS /*0*/:
                    Intent ii = new Intent(this, EmployeeComplaintService.class);
                    ii.putExtra(BahetiEnterprises.FROM, "2");
                    startActivity(ii);
                    return;
                case TextAlign.BOTTOM /*1*/:
                    Intent call_intent = new Intent(this, EmployeeCallLog.class);
                    call_intent.putExtra(BahetiEnterprises.INTNT, "2");
                    startActivity(call_intent);
                    return;
                case 2 /*2*/:
                    Intent noti_intent = new Intent(this, NotiCenter.class);
                    noti_intent.putExtra(BahetiEnterprises.FROM, "2");
                    startActivity(noti_intent);
                default:
                    return;
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.admin_items, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout /*2131624289*/:
                BahetiEnterprises.logout(this);
                return true;
            case R.id.updt_profile /*2131624289*/:
                // BahetiEnterprises.logout(this);
                startActivity(new Intent(EmployeeDashboard.this, UpdateProfile.class));
                return true;
            case R.id.chang_pswd /*2131624289*/:
                // BahetiEnterprises.logout(this);
                startActivity(new Intent(EmployeeDashboard.this, ChangePassword.class));

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
