package com.orangebyte.bahetiadmin.modules.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "users")
public class UserEntity extends IdentifiableEntity implements Serializable {
    @SerializedName("address")
    @ColumnInfo(name = "address")
    String address;
    @SerializedName("mobile_no")
    @ColumnInfo(name = "mobile_no")
    String mobile_no;
    @SerializedName("password")
    @ColumnInfo(name = "password")
    String password;
    @SerializedName("usernm")
    @ColumnInfo(name = "usernm")
    String usernm;

    @SerializedName("email")
    @ColumnInfo(name = "email")
    String email;


    @SerializedName("gender")
    @ColumnInfo(name = "gender")
    String gender;

    @SerializedName("regId")
    @ColumnInfo(name = "regId")
    String regId;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public UserEntity() {
    }

    public /* bridge */ /* synthetic */ long getId() {
        return super.getId();
    }

    public /* bridge */ /* synthetic */ void setId(long j) {
        super.setId(j);
    }

    @Ignore
    public UserEntity(long id, String usernm, String password, String mobile_no, String address) {
        this.id = id;
        this.usernm = usernm;
        this.password = password;
        this.mobile_no = mobile_no;
        this.address = address;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsernm() {
        return this.usernm;
    }

    public void setUsernm(String usernm) {
        this.usernm = usernm;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile_no() {
        return this.mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }
}
