package com.orangebyte.bahetiadmin.modules.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.modules.db.local.AppDatabase;
import com.orangebyte.bahetiadmin.modules.db.local.DatabaseCreator;
import com.orangebyte.bahetiadmin.modules.entities.NotificationCenter;
import com.orangebyte.bahetiadmin.modules.repository.NotiRepository;
import com.orangebyte.bahetiadmin.modules.ui.AllContract;

import java.util.List;


/**
 * Created by Ashvini on 3/29/2018.
 */


/**
 * Created by Ashvini on 1/8/2018.
 */

public class NotiViewModel extends ViewModel implements AllContract.NotiDataStreams {

    private NotiRepository notificationRepository;


    public NotiViewModel() {
        this(null);
    }

    public NotiViewModel(@Nullable NotiRepository notificationRepository) {
        if (notificationRepository != null) {
            this.notificationRepository = notificationRepository;
        }


    }

    @Override
    public void addNotifcation(NotificationCenter notificationEntity) {
        onDatabaseCreated();
        notificationRepository.addNoti(notificationEntity);// addNotifn(notificationEntity);

        //  return null;
    }

    @Override
    public LiveData<List<NotificationCenter>> getAll() {
        try {
            onDatabaseCreated();
            return notificationRepository.getAllNotification();
        } catch (Exception e) {
            onDatabaseCreated();
            e.printStackTrace();
        }

        return null;
    }


    private void onDatabaseCreated() {
        AppDatabase appDatabase = DatabaseCreator.getInstance().getDatabase();
        try {
            if (notificationRepository == null) {
                notificationRepository = new NotiRepository(appDatabase.notiDAO());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static class NotiFactory extends ViewModelProvider.NewInstanceFactory {

        NotiRepository mNotificationRepository;


        public NotiFactory() {
            this(null,null);
        }

        public NotiFactory(AppDatabase appDatabase) {

            NotiRepository.getInstance(appDatabase.notiDAO());

        }

        public NotiFactory(@Nullable NotiRepository mNotificationRepository, @Nullable String abc) {
            this.mNotificationRepository = mNotificationRepository;

        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new NotiViewModel(mNotificationRepository);
        }
    }
}

