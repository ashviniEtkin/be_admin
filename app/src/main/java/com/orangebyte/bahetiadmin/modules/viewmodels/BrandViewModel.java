package com.orangebyte.bahetiadmin.modules.viewmodels;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider.NewInstanceFactory;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.modules.db.local.AppDatabase;
import com.orangebyte.bahetiadmin.modules.db.local.DatabaseCreator;
import com.orangebyte.bahetiadmin.modules.entities.BrandEntity;
import com.orangebyte.bahetiadmin.modules.repository.BrandRepository;
import com.orangebyte.bahetiadmin.modules.repository.RemoteRepository;
import com.orangebyte.bahetiadmin.modules.ui.AllContract.BrandDataStreams;

import java.util.List;

import im.delight.android.webview.BuildConfig;

public class BrandViewModel extends ViewModel implements BrandDataStreams {
    private LiveData<List<BrandEntity>> brandObservable;
    private BrandRepository brandRepository;

    public static class BrandFactory extends NewInstanceFactory {
        BrandRepository mBrandRepository;

        public BrandFactory() {
            this(null, null);
        }

        public BrandFactory(AppDatabase appDatabase) {
            this(BrandRepository.getInstance(RemoteRepository.getInstance(), appDatabase.brandDao()), BuildConfig.VERSION_NAME);
        }

        public BrandFactory(@Nullable BrandRepository mBrandRepository, @Nullable String abc) {
            this.mBrandRepository = mBrandRepository;
        }

        public <T extends ViewModel> T create(Class<T> cls) {
            return (T) new BrandViewModel(this.mBrandRepository);
        }
    }

    public BrandViewModel() {
        this(null);
    }

    public BrandViewModel(@Nullable BrandRepository brandRepository) {
        if (brandRepository != null) {
            this.brandRepository = brandRepository;
        }
        this.brandObservable = subscribeToLocalData();
    }

    public LiveData<List<BrandEntity>> getBrands() {
        return this.brandObservable;
    }

    public LiveData<List<BrandEntity>> addBrand(BrandEntity brandEntity, int chk, Uri uril) {
        onDatabaseCreated();
        return addUpdateBrand(brandEntity, chk, uril);
    }

    public LiveData<List<BrandEntity>> updateBrandStatus(BrandEntity brandEntity) {
        onDatabaseCreated();
        return updtSubCatStatus(brandEntity);
    }

    public LiveData<List<BrandEntity>> deleteBrandById(BrandEntity brandEntity) {
        onDatabaseCreated();
        return deleteSubCatById(brandEntity);
    }

    private LiveData<List<BrandEntity>> subscribeToLocalData() {
        return Transformations.switchMap(DatabaseCreator.getInstance().isDatabaseCreated(), new Function<Boolean, LiveData<List<BrandEntity>>>() {
            public LiveData<List<BrandEntity>> apply(Boolean isDbCreated) {
                if (!Boolean.TRUE.equals(isDbCreated)) {
                    return null;
                }
                BrandViewModel.this.onDatabaseCreated();
                return BrandViewModel.this.subscribeSubCatObservable();
            }
        });
    }

    private LiveData<List<BrandEntity>> subscribeSubCatObservable() {
        return this.brandRepository.loadBrandList();
    }

    private LiveData<List<BrandEntity>> addUpdateBrand(BrandEntity brandEntity, int chk, Uri file) {
        return this.brandRepository.addEditBrnd(brandEntity, chk, file);
    }

    private LiveData<List<BrandEntity>> updtSubCatStatus(BrandEntity brandEntity) {
        return this.brandRepository.updateBrandStatus(brandEntity);
    }

    private LiveData<List<BrandEntity>> deleteSubCatById(BrandEntity c) {
        return this.brandRepository.deleteBrandByID(c);
    }

    private void onDatabaseCreated() {
        AppDatabase appDatabase = DatabaseCreator.getInstance().getDatabase();
        if (this.brandRepository == null) {
            this.brandRepository = BrandRepository.getInstance(RemoteRepository.getInstance(), appDatabase.brandDao());
        }
    }
}
