package com.orangebyte.bahetiadmin.modules.admin;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.expandableview.ExpandableRelativeLayout;
import com.orangebyte.bahetiadmin.modules.adapters.FeedbackAdapter;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.FeedbackEntity;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels.Factory;
import com.orangebyte.bahetiadmin.modules.viewmodels.FeedbackViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.FeedbackViewModel.FeedbackFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel.SubCategoryFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import im.delight.android.webview.BuildConfig;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Feedback extends AppCompatActivity implements LifecycleRegistryOwner {
    List<SubCategoryEntity> allSubCat = new ArrayList();
    List<CategoryEntity> categoryEntityList = new ArrayList();
    Spinner category_spinner;
    Spinner complaint_type_spinner;
    ExpandableRelativeLayout date_expandble_view;
    TextView fdb_compalint;
    TextView fdb_installation;
    TextView fdb_purchase;
    List<FeedbackEntity> feedbackEntityList = new ArrayList();
    RecyclerView feedback_recycler;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    LinearLayout list_container;
    int mDay;
    int mMonth;
    private OnDateSetListener mSeleteDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            Feedback.this.select_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    private OnDateSetListener mStartDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            Feedback.this.start_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    private OnDateSetListener mToDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            Feedback.this.to_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    int mYear;
    TextView no_result_fount;
    ImageButton reset;
    TextView select_date;
    Button show,btn_reset;
    TextView start_date;
    List<SubCategoryEntity> subCategoryEntityList = new ArrayList();
    TableRow sub_cat_container;
    Spinner subcategory_spinner;
    TextView to_date;
    TextView total;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        BahetiEnterprises.setActBar(getResources().getString(R.string.Feedback), this, false);
        init();
        setCatgoryToSpinner();
        this.category_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (Feedback.this.allSubCat.isEmpty()) {
                    Feedback.this.setSubCatgoryToSpinner();
                } else if (i > 0) {
                    Feedback.this.setToSubCatSpinner(((CategoryEntity) Feedback.this.categoryEntityList.get(i - 1)).getId() + BuildConfig.VERSION_NAME);
                    Feedback.this.sub_cat_container.setVisibility(0);
                } else {
                    Feedback.this.sub_cat_container.setVisibility(8);
                }
                if (i == 0) {
                    Feedback.this.sub_cat_container.setVisibility(8);
                } else {
                    Feedback.this.sub_cat_container.setVisibility(0);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.show.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                FeedbackEntity feedbackEntity = new FeedbackEntity();
                if (Feedback.this.complaint_type_spinner.getSelectedItemPosition() == 0) {
                    feedbackEntity.setType("null");
                } else {
                    feedbackEntity.setType(Feedback.this.complaint_type_spinner.getSelectedItem().toString());
                }
                if (Feedback.this.category_spinner.getSelectedItemPosition() == 0) {
                    feedbackEntity.setCat_id("null");
                } else if (BahetiEnterprises.checkSpinner(Feedback.this.category_spinner)) {
                    feedbackEntity.setCat_id("null");
                } else {
                    feedbackEntity.setCat_id(((CategoryEntity) Feedback.this.categoryEntityList.get(Feedback.this.category_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);
                }
                if (Feedback.this.subcategory_spinner.getSelectedItemPosition() == 0) {
                    feedbackEntity.setSubcat_id("null");
                } else {
                    try {
                        if (BahetiEnterprises.checkSpinner(Feedback.this.subcategory_spinner)) {
                            feedbackEntity.setSubcat_id("null");
                        } else {
                            feedbackEntity.setSubcat_id(((SubCategoryEntity) Feedback.this.subCategoryEntityList.get(Feedback.this.subcategory_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        feedbackEntity.setSubcat_id("null");
                    }
                }
                if (!TextUtils.isEmpty(Feedback.this.start_date.getText().toString()) && !TextUtils.isEmpty(Feedback.this.to_date.getText().toString())) {
                    feedbackEntity.setOndate(BahetiEnterprises.tStamp(Feedback.this.start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    feedbackEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(Feedback.this.to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                } else if (!TextUtils.isEmpty(Feedback.this.start_date.getText().toString()) && TextUtils.isEmpty(Feedback.this.to_date.getText().toString())) {
                    feedbackEntity.setOndate(BahetiEnterprises.tStamp(Feedback.this.start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    feedbackEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(Feedback.this.start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                } else if (TextUtils.isEmpty(Feedback.this.start_date.getText().toString()) && !TextUtils.isEmpty(Feedback.this.to_date.getText().toString())) {
                    feedbackEntity.setOndate(BahetiEnterprises.tStamp(Feedback.this.to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    feedbackEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(Feedback.this.to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                } else if (TextUtils.isEmpty(Feedback.this.select_date.getText().toString())) {
                    feedbackEntity.setOndate("null");
                    feedbackEntity.setLast_atnd_on("null");
                } else {
                    feedbackEntity.setOndate(BahetiEnterprises.tStamp(Feedback.this.select_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    feedbackEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(Feedback.this.select_date.getText().toString()) + BuildConfig.VERSION_NAME);
                }
                Feedback.this.initViewModel(feedbackEntity);
            }
        });
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    public void showDateView(View view) {
        this.date_expandble_view = (ExpandableRelativeLayout) findViewById(R.id.date_expandble_view);
        this.date_expandble_view.toggle();
    }

    public void showSelectDate(View view) {
        new DatePickerDialog(this, this.mSeleteDate, this.mYear, this.mMonth, this.mDay).show();
    }

    public void showStartDate(View view) {
        new DatePickerDialog(this, this.mStartDate, this.mYear, this.mMonth, this.mDay).show();
    }

    public void showToDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this.mToDate, this.mYear, this.mMonth, this.mDay);
        if (TextUtils.isEmpty(this.start_date.getText().toString())) {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        } else {
            datePickerDialog.getDatePicker().setMinDate(BahetiEnterprises.tStamp(this.start_date.getText().toString()));
        }
        datePickerDialog.show();
    }

    private void initViewModel(FeedbackEntity feedbackEntity) {
        subscribeToDataStreams((FeedbackViewModel) ViewModelProviders.of(this, new FeedbackFactory()).get(FeedbackViewModel.class), feedbackEntity);
    }

    private void subscribeToDataStreams(FeedbackViewModel viewModel, FeedbackEntity feedbackEntity) {
        try {
            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, 5);
            sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
            sweetAlertDialog.setTitleText("Loading");
            sweetAlertDialog.setCancelable(false);
            sweetAlertDialog.show();
            viewModel.getFeedback(feedbackEntity).observe(this, new Observer<List<FeedbackEntity>>() {
                public void onChanged(@Nullable List<FeedbackEntity> offerEntityList) {
                    if (!offerEntityList.isEmpty()) {
                        sweetAlertDialog.dismiss();
                        Feedback.this.showFeedbackListInUit(offerEntityList);
                        Feedback.this.feedback_recycler.setVisibility(0);
                        Feedback.this.no_result_fount.setVisibility(8);
                        Feedback.this.list_container.setVisibility(0);
                    } else if (offerEntityList.size() == 0) {
                        sweetAlertDialog.dismiss();
                        Feedback.this.showFeedbackListInUit(new ArrayList());
                        Feedback.this.feedback_recycler.setVisibility(8);
                        Feedback.this.no_result_fount.setVisibility(0);
                        Feedback.this.list_container.setVisibility(8);
                    } else {
                        sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showFeedbackListInUit(List<FeedbackEntity> feedbackEntityList) {
        this.feedbackEntityList = feedbackEntityList;
        int fb_complaint = getCont(feedbackEntityList, getResources().getString(R.string.fdb_Complaint));
        int fd_install = getCont(feedbackEntityList, getResources().getString(R.string.fdb_Installation));
        int fd_purchase = getCont(feedbackEntityList, getResources().getString(R.string.fdb_Purchase));
        int all = (fb_complaint + fd_install) + fd_purchase;
        this.fdb_compalint.setText(getResources().getString(R.string.fdb_Complaint) + " : " + fb_complaint);
        this.fdb_installation.setText(getResources().getString(R.string.fdb_Installation) + " : " + fd_install);
        this.fdb_purchase.setText(getResources().getString(R.string.fdb_Purchase) + " : " + fd_purchase);
        this.total.setText(getResources().getString(R.string.total) + " : " + all);
        FeedbackAdapter adapter = new FeedbackAdapter(feedbackEntityList, this);
        this.feedback_recycler.setLayoutManager(new LinearLayoutManager(this));
        this.feedback_recycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void init() {
        this.no_result_fount = (TextView) findViewById(R.id.no_result_fount);
        this.show = (Button) findViewById(R.id.show);
        this.category_spinner = (Spinner) findViewById(R.id.category_spinner);
        this.subcategory_spinner = (Spinner) findViewById(R.id.subcategory_spinner);
        this.complaint_type_spinner = (Spinner) findViewById(R.id.complaint_type_spinner);
        this.feedback_recycler = (RecyclerView) findViewById(R.id.feedback_recycler);
        this.sub_cat_container = (TableRow) findViewById(R.id.sub_cat_container);
        this.sub_cat_container.setVisibility(8);
        this.select_date = (TextView) findViewById(R.id.select_date);
        this.start_date = (TextView) findViewById(R.id.start_date);
        this.to_date = (TextView) findViewById(R.id.to_date);
        this.total = (TextView) findViewById(R.id.total);
        this.fdb_compalint = (TextView) findViewById(R.id.fdb_complaint);
        this.fdb_installation = (TextView) findViewById(R.id.fdb_installation);
        this.fdb_purchase = (TextView) findViewById(R.id.fdb_purchase);
        this.no_result_fount = (TextView) findViewById(R.id.no_result_fount);
        this.list_container = (LinearLayout) findViewById(R.id.list_container);
        this.reset = (ImageButton) findViewById(R.id.reset);
        Calendar c = Calendar.getInstance();
        this.mYear = c.get(1);
        this.mMonth = c.get(2);
        this.mDay = c.get(5);
        this.reset.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Feedback.this.start_date.setText(BuildConfig.VERSION_NAME);
                Feedback.this.select_date.setText(BuildConfig.VERSION_NAME);
                Feedback.this.to_date.setText(BuildConfig.VERSION_NAME);
            }
        });

        btn_reset = (Button) findViewById(R.id.btn_reset);

        btn_reset.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setCatgoryToSpinner();
                complaint_type_spinner.setSelection(0);
                select_date.setText("");
                start_date.setText("");
                to_date.setText("");

            }
        });
    }

    public int getCont(List<FeedbackEntity> feedbackEntities, String status) {
        int cnt = 0;
        try {
            for (FeedbackEntity ce : feedbackEntities) {
                if (ce.getType().equalsIgnoreCase(status)) {
                    cnt++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cnt;
    }

    private void setCatDataToSpinner(List<CategoryEntity> categoryEntities) {
        this.categoryEntityList = categoryEntities;
        List<String> catList = new ArrayList();
        catList.add("All");
        try {
            for (CategoryEntity categoryEntity : categoryEntities) {
                catList.add(categoryEntity.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.category_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
        setSubCatgoryToSpinner();
    }

    private void setToSubCatSpinner(String catId) {
        subCategoryEntityList.clear();

        List<String> catList = new ArrayList();
        catList.add("All");
        try {
            for (SubCategoryEntity subCategoryEntity : this.allSubCat) {
                if (subCategoryEntity.getCat_id().equalsIgnoreCase(catId)) {
                    catList.add(subCategoryEntity.getSub_cat_name());
                    this.subCategoryEntityList.add(subCategoryEntity);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.subcategory_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    public void setSubCatgoryToSpinner() {
        try {
            ((SubCategoryViewModel) ViewModelProviders.of(this, new SubCategoryFactory()).get(SubCategoryViewModel.class)).getSubCats().observe(this, new Observer<List<SubCategoryEntity>>() {
                public void onChanged(@Nullable List<SubCategoryEntity> subCategoryEntities) {
                    if (!subCategoryEntities.isEmpty()) {
                        Feedback.this.allSubCat = subCategoryEntities;
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setCatgoryToSpinner() {
        try {
            ((CategoryViewMdels) ViewModelProviders.of(this, new Factory()).get(CategoryViewMdels.class)).getCats().observe(this, new Observer<List<CategoryEntity>>() {
                public void onChanged(@Nullable List<CategoryEntity> categoryEntities) {
                    if (!categoryEntities.isEmpty()) {
                        Feedback.this.setCatDataToSpinner(categoryEntities);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
