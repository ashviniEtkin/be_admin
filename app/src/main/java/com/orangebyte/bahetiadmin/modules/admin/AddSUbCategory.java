package com.orangebyte.bahetiadmin.modules.admin;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels.Factory;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel.SubCategoryFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import im.delight.android.webview.BuildConfig;
import java.util.ArrayList;
import java.util.List;

public class AddSUbCategory extends AppCompatActivity implements LifecycleRegistryOwner {
    Button add;
    List<CategoryEntity> categoryEntityList = new ArrayList();
    Spinner category_spinner;
    String chk_add_update = BuildConfig.VERSION_NAME;
    EditText edt_subcat;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    SubCategoryEntity subCategoryEntity;
    SweetAlertDialog sweetAlertDialog;
    SubCategoryViewModel viewModel;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sub_category);
        initUi();
        this.chk_add_update = getIntent().getStringExtra(BahetiEnterprises.CHK_ADD_UPDATE);
        if (this.chk_add_update.equalsIgnoreCase("1")) {
            this.add.setText("Update");
            BahetiEnterprises.setActBar(getResources().getString(R.string.Update_SubCategory), this, false);
            this.subCategoryEntity = (SubCategoryEntity) getIntent().getSerializableExtra(BahetiEnterprises.SUBCATEGORY);
            updateData();
            return;
        }
        this.add.setText("Add");
        BahetiEnterprises.setActBar(getResources().getString(R.string.Add_SubCategory), this, false);
    }

    public void updateData() {
        this.edt_subcat.setText(this.subCategoryEntity.getSubCatName());
    }

    public void initUi() {
        this.category_spinner = (Spinner) findViewById(R.id.category_spinner);
        this.edt_subcat = (EditText) findViewById(R.id.edt_subcat);
        this.add = (Button) findViewById(com.orangebyte.pdflibrary.R.id.add);
        this.sweetAlertDialog = new SweetAlertDialog(this, 5);
        this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
        this.sweetAlertDialog.setTitleText("Loading");
        this.sweetAlertDialog.setCancelable(false);
        try {
            ((CategoryViewMdels) ViewModelProviders.of(this, new Factory()).get(CategoryViewMdels.class)).getCats().observe(this, new Observer<List<CategoryEntity>>() {
                public void onChanged(@Nullable List<CategoryEntity> categoryEntities) {
                    if (categoryEntities.isEmpty()) {
                        AddSUbCategory.this.sweetAlertDialog.show();
                        return;
                    }
                    AddSUbCategory.this.sweetAlertDialog.dismiss();
                    AddSUbCategory.this.setToSpinner(categoryEntities);
                    if (AddSUbCategory.this.chk_add_update.equalsIgnoreCase("1")) {
                        AddSUbCategory.this.setCategoryToSpinner(AddSUbCategory.this.subCategoryEntity.getCatId(), categoryEntities);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        this.add.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                AddSUbCategory.this.initModelView();
            }
        });
    }

    private void setToSpinner(List<CategoryEntity> categoryEntities) {
        this.categoryEntityList = categoryEntities;
        List<String> catList = new ArrayList();
        try {
            for (CategoryEntity categoryEntity : categoryEntities) {
                catList.add(categoryEntity.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.category_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    public void initModelView() {
        this.viewModel = (SubCategoryViewModel) ViewModelProviders.of(this, new SubCategoryFactory()).get(SubCategoryViewModel.class);
        long catId = ((CategoryEntity) this.categoryEntityList.get(this.category_spinner.getSelectedItemPosition())).getId();
        if (this.chk_add_update.equalsIgnoreCase("1")) {
            addCategory(this.viewModel, new SubCategoryEntity(this.subCategoryEntity.getId(), this.edt_subcat.getText().toString(), catId + BuildConfig.VERSION_NAME, this.subCategoryEntity.getStatus() + BuildConfig.VERSION_NAME), 1);
            return;
        }
        addCategory(this.viewModel, new SubCategoryEntity(0, this.edt_subcat.getText().toString(), catId + BuildConfig.VERSION_NAME, "1"), 0);
    }

    private void addCategory(SubCategoryViewModel viewModel, SubCategoryEntity c, int chk) {
        try {
            this.sweetAlertDialog.show();
            viewModel.addSubCategory(c, chk).observe(this, new Observer<List<SubCategoryEntity>>() {
                public void onChanged(@Nullable List<SubCategoryEntity> categoryEntities) {
                    if (categoryEntities.isEmpty()) {
                        AddSUbCategory.this.sweetAlertDialog.show();
                        return;
                    }
                    AddSUbCategory.this.sweetAlertDialog.dismiss();
                    AddSUbCategory.this.onBackPressed();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    public void setCategoryToSpinner(String catId, List<CategoryEntity> categoryEntities) {
        int pos = 0;
        List<String> catList = new ArrayList();
        try {
            for (CategoryEntity categoryEntity : categoryEntities) {
                catList.add(String.valueOf(categoryEntity.getId()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        int i = 0;
        while (i < catList.size()) {
            if (((String) catList.get(i)).equalsIgnoreCase(catId)) {
                pos = i;
                i = catList.size();
            }
            i++;
        }
        this.category_spinner.setSelection(pos);
    }
}
