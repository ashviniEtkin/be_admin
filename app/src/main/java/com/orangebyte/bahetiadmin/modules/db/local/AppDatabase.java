package com.orangebyte.bahetiadmin.modules.db.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.orangebyte.bahetiadmin.modules.db.daos.BrandDAO;
import com.orangebyte.bahetiadmin.modules.db.daos.CallLogNInstallDAO;
import com.orangebyte.bahetiadmin.modules.db.daos.CategoryDAO;
import com.orangebyte.bahetiadmin.modules.db.daos.ComplaintDAO;
import com.orangebyte.bahetiadmin.modules.db.daos.EmployeeDAO;
import com.orangebyte.bahetiadmin.modules.db.daos.EnquiryDAO;
import com.orangebyte.bahetiadmin.modules.db.daos.FeedbackDAO;
import com.orangebyte.bahetiadmin.modules.db.daos.ManagerDAO;
import com.orangebyte.bahetiadmin.modules.db.daos.NotiDAO;
import com.orangebyte.bahetiadmin.modules.db.daos.NotificationDAO;
import com.orangebyte.bahetiadmin.modules.db.daos.OfferDAO;
import com.orangebyte.bahetiadmin.modules.db.daos.ProductDAO;
import com.orangebyte.bahetiadmin.modules.db.daos.SubCategoryDAO;
import com.orangebyte.bahetiadmin.modules.entities.BrandEntity;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.entities.EnquiryEntity;
import com.orangebyte.bahetiadmin.modules.entities.FeedbackEntity;
import com.orangebyte.bahetiadmin.modules.entities.ManagerEntity;
import com.orangebyte.bahetiadmin.modules.entities.NotificationCenter;
import com.orangebyte.bahetiadmin.modules.entities.NotificationEntity;
import com.orangebyte.bahetiadmin.modules.entities.OfferEntity;
import com.orangebyte.bahetiadmin.modules.entities.ProductEntity;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;

@Database(entities = {CategoryEntity.class, SubCategoryEntity.class, BrandEntity.class,
        ManagerEntity.class, EmployeeEntity.class, OfferEntity.class,
        ProductEntity.class, ComplaintEntity.class, CallLogInstallationDemoEntity.class,
        EnquiryEntity.class, FeedbackEntity.class, NotificationEntity.class, NotificationCenter.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public static final String DATABASE_NAME = "be-db";
    private static AppDatabase sInstance;

    public abstract BrandDAO brandDao();

    public abstract CallLogNInstallDAO callLogNInstallDAO();

    public abstract CategoryDAO catDao();

    public abstract ComplaintDAO complaintDAO();

    public abstract EmployeeDAO employeeDao();

    public abstract EnquiryDAO enquiryDAO();

    public abstract FeedbackDAO feedbackDAO();

    public abstract ManagerDAO managerDao();

    public abstract NotificationDAO notificationDAO();

    public abstract OfferDAO offerDAO();

    public abstract ProductDAO productDAO();

    public abstract SubCategoryDAO subCatDao();


    public abstract NotiDAO notiDAO();
}
