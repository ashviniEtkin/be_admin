package com.orangebyte.bahetiadmin.modules.models;

public interface BrandModel extends IdentifiableModel {
    String getBrand_nm();

    String getCat_id();

    String getStatus();
}
