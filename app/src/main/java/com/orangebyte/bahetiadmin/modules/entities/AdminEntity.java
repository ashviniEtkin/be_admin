package com.orangebyte.bahetiadmin.modules.entities;

import com.google.gson.annotations.SerializedName;

public class AdminEntity extends IdentifiableEntity {
    @SerializedName("password")
    String password;
    @SerializedName("post")
    String post;
    @SerializedName("rid")
    String rid;
    @SerializedName("username")
    String username;

    @SerializedName("mobile_no")
    private String mobile_no;

   /* @SerializedName("id")
    private String id;

    public void setId(String id) {
        this.id = id;
    }*/

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public /* bridge */ /* synthetic */ long getId() {
        return super.getId();
    }

    public /* bridge */ /* synthetic */ void setId(long j) {
        super.setId(j);
    }

    public String getRid() {
        return this.rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPost() {
        return this.post;
    }

    public void setPost(String post) {
        this.post = post;
    }
}
