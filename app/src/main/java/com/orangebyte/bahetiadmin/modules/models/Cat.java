package com.orangebyte.bahetiadmin.modules.models;

import com.google.gson.annotations.SerializedName;

public class Cat {
    @SerializedName("assign_to")
    private String assign_to;
    @SerializedName("cat_nm")
    private String cat_nm;
    @SerializedName("id")
    private String id;
    @SerializedName("status")
    private String status;

    public Cat(String id, String cat_nm, String assign_to, String status) {
        this.id = id;
        this.cat_nm = cat_nm;
        this.assign_to = assign_to;
        this.status = status;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCat_nm() {
        return this.cat_nm;
    }

    public void setCat_nm(String cat_nm) {
        this.cat_nm = cat_nm;
    }

    public String getAssign_to() {
        return this.assign_to;
    }

    public void setAssign_to(String assign_to) {
        this.assign_to = assign_to;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
