package com.orangebyte.bahetiadmin.modules.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ashvini on 4/10/2018.
 */

public class ForgetEntity implements Serializable {

    @SerializedName("post")
    String post;
    @SerializedName("id")
    String id;
    @SerializedName("otp")
    String otp;

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}

