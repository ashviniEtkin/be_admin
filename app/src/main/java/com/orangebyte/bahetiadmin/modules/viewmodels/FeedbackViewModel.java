package com.orangebyte.bahetiadmin.modules.viewmodels;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider.NewInstanceFactory;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.modules.db.local.AppDatabase;
import com.orangebyte.bahetiadmin.modules.db.local.DatabaseCreator;
import com.orangebyte.bahetiadmin.modules.entities.FeedbackEntity;
import com.orangebyte.bahetiadmin.modules.repository.FeedbackRepository;
import com.orangebyte.bahetiadmin.modules.repository.RemoteRepository;
import com.orangebyte.bahetiadmin.modules.ui.AllContract.FeedbackStreams;

import java.util.List;

import im.delight.android.webview.BuildConfig;

public class FeedbackViewModel extends ViewModel implements FeedbackStreams {
    private FeedbackRepository feedbackRepository;

    public static class FeedbackFactory extends NewInstanceFactory {
        FeedbackRepository pProductRepository;

        public FeedbackFactory() {
            this(null, null);
        }

        public FeedbackFactory(AppDatabase appDatabase) {
            this(FeedbackRepository.getInstance(RemoteRepository.getInstance(), appDatabase.feedbackDAO()), BuildConfig.VERSION_NAME);
        }

        public FeedbackFactory(@Nullable FeedbackRepository pProductRepository, @Nullable String abc) {
            this.pProductRepository = pProductRepository;
        }

        public <T extends ViewModel> T create(Class<T> cls) {
            return (T) new FeedbackViewModel(this.pProductRepository);
        }
    }

    public FeedbackViewModel() {
        this(null);
    }

    public FeedbackViewModel(@Nullable FeedbackRepository feedbackRepository) {
        if (feedbackRepository != null) {
            this.feedbackRepository = feedbackRepository;
        }
    }

    private LiveData<List<FeedbackEntity>> subscribeToLocalData(final FeedbackEntity feedbackEntity) {
        return Transformations.switchMap(DatabaseCreator.getInstance().isDatabaseCreated(), new Function<Boolean, LiveData<List<FeedbackEntity>>>() {
            public LiveData<List<FeedbackEntity>> apply(Boolean isDbCreated) {
                if (!Boolean.TRUE.equals(isDbCreated)) {
                    return null;
                }
                FeedbackViewModel.this.onDatabaseCreated();
                return FeedbackViewModel.this.subscribeProductObservable(feedbackEntity);
            }
        });
    }

    private LiveData<List<FeedbackEntity>> subscribeProductObservable(FeedbackEntity feedbackEntity) {
        return this.feedbackRepository.loadFeedbackList(feedbackEntity);
    }

    private void onDatabaseCreated() {
        AppDatabase appDatabase = DatabaseCreator.getInstance().getDatabase();
        try {
            if (this.feedbackRepository == null) {
                this.feedbackRepository = FeedbackRepository.getInstance(RemoteRepository.getInstance(), appDatabase.feedbackDAO());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public LiveData<List<FeedbackEntity>> getFeedback(FeedbackEntity feedbackEntity) {
        return subscribeToLocalData(feedbackEntity);
    }
}
