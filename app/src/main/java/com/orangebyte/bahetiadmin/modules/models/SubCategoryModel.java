package com.orangebyte.bahetiadmin.modules.models;

public interface SubCategoryModel extends IdentifiableModel {
    String getCatId();

    String getStatus();

    String getSubCatName();
}
