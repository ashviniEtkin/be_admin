package com.orangebyte.bahetiadmin.modules.admin;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.entities.BrandEntity;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.viewmodels.BrandViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.BrandViewModel.BrandFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels.Factory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import com.orangebyte.bahetiadmin.views.CircleImageView;
import com.pdfjet.Single;
import com.squareup.picasso.Picasso;

import im.delight.android.webview.BuildConfig;

import java.util.ArrayList;
import java.util.List;

public class AddBrandActivity extends AppCompatActivity implements LifecycleRegistryOwner {
    Button add;
    BrandEntity brandEntity;
    List<CategoryEntity> categoryEntityList = new ArrayList();
    Spinner category_spinner;
    String chk_add_update = "";
    EditText edt_brand, edt_tollfree, edt_address;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    SweetAlertDialog sweetAlertDialog;
    BrandViewModel viewModel;
    CircleImageView brand_icon;
    int RESULT_LOAD_IMG = 360;
    Uri selectedImageUri = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_brand);
        initUi();
        this.chk_add_update = getIntent().getStringExtra(BahetiEnterprises.CHK_ADD_UPDATE);
        if (this.chk_add_update.equalsIgnoreCase("1")) {
            this.add.setText("Update");
            BahetiEnterprises.setActBar(getResources().getString(R.string.Update_brand), this, false);
            this.brandEntity = (BrandEntity) getIntent().getSerializableExtra(BahetiEnterprises.SUBCATEGORY);
            updateData();
            return;
        }
        this.add.setText("Add");
        BahetiEnterprises.setActBar(getResources().getString(R.string.Add_brand), this, false);
    }

    public void updateData() {
        this.edt_brand.setText(this.brandEntity.getBrand_nm());
        edt_tollfree.setText(brandEntity.getToll_free_no());
        edt_address.setText(brandEntity.getAddress());
        Picasso.with(AddBrandActivity.this).load(BahetiEnterprises.Brand_URL + brandEntity.getImg()).placeholder(R.drawable.logo).into(brand_icon);
    }

    public void chooseImage(View view) {
        if (ActivityCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, com.orangebyte.pdflibrary.R.styleable.AppCompatTheme_ratingBarStyleIndicator);
            return;
        }
        Intent intent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"), this.RESULT_LOAD_IMG);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == this.RESULT_LOAD_IMG && resultCode == -1 && data != null) {
            this.selectedImageUri = data.getData();
            Cursor cursor = managedQuery(this.selectedImageUri, new String[]{"_data"}, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow("_data");
            cursor.moveToFirst();
            String selectedImagePath = cursor.getString(column_index);
            String filePath = BahetiEnterprises.getRealPathFromURI(data.getDataString());
            BitmapFactory.Options options = new BitmapFactory.Options();
          //  options.inSampleSize = 8;
         //   this.brand_icon.setImageBitmap(BitmapFactory.decodeFile(filePath, options));
            // this.brand_icon.setScaleType(ImageView.ScaleType.FIT_XY);
            Picasso.with(AddBrandActivity.this).load(selectedImageUri).into(brand_icon);
          //  this.brand_icon.setScaleType(ImageView.ScaleType.FIT_XY);
            return;
        }
        Toast.makeText(getApplicationContext(), "you haven't select image", Toast.LENGTH_LONG).show();
    }

    public void initUi() {
        this.category_spinner = (Spinner) findViewById(R.id.category_spinner);
        this.edt_brand = (EditText) findViewById(R.id.edt_brand);
        edt_tollfree = (EditText) findViewById(R.id.edt_tollfree);
        edt_address = (EditText) findViewById(R.id.edt_address);
        brand_icon = (CircleImageView) findViewById(R.id.brand_icon);
        this.add = (Button) findViewById(com.orangebyte.pdflibrary.R.id.add);
        this.sweetAlertDialog = new SweetAlertDialog(this, 5);
        this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
        this.sweetAlertDialog.setTitleText("Loading");
        this.sweetAlertDialog.setCancelable(false);
        try {
            ((CategoryViewMdels) ViewModelProviders.of(this, new Factory()).get(CategoryViewMdels.class)).getCats().observe(this, new Observer<List<CategoryEntity>>() {
                public void onChanged(@Nullable List<CategoryEntity> categoryEntities) {
                    if (categoryEntities.isEmpty()) {
                        AddBrandActivity.this.sweetAlertDialog.show();
                        return;
                    }
                    AddBrandActivity.this.sweetAlertDialog.dismiss();
                    AddBrandActivity.this.setToSpinner(categoryEntities);
                    if (AddBrandActivity.this.chk_add_update.equalsIgnoreCase("1")) {
                        AddBrandActivity.this.setCategoryToSpinner(AddBrandActivity.this.brandEntity.getCat_id(), categoryEntities);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        this.add.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                AddBrandActivity.this.initModelView();
            }
        });
    }

    private void setToSpinner(List<CategoryEntity> categoryEntities) {
        this.categoryEntityList = categoryEntities;
        List<String> catList = new ArrayList();
        try {
            for (CategoryEntity categoryEntity : categoryEntities) {
                catList.add(categoryEntity.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.category_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    public void initModelView() {
        this.viewModel = (BrandViewModel) ViewModelProviders.of(this, new BrandFactory()).get(BrandViewModel.class);
        long catId = ((CategoryEntity) this.categoryEntityList.get(this.category_spinner.getSelectedItemPosition())).getId();
        String brnd_img = edt_brand.getText().toString().replace(Single.space, "_") + System.currentTimeMillis() + ".jpg";


        if (TextUtils.isEmpty(edt_brand.getText().toString())) {
            edt_brand.setError(getString(R.string.bbrand_name_error));
        } else if (TextUtils.isEmpty(edt_tollfree.getText().toString())) {
            edt_tollfree.setError(getString(R.string.toll_free_error));
        } else if (TextUtils.isEmpty(edt_address.getText().toString())) {
            edt_address.setError(getString(R.string.address_error));
        } else {

            if (this.chk_add_update.equalsIgnoreCase("1")) {
                addUpdateBrand(this.viewModel, new BrandEntity(this.brandEntity.getId(), this.edt_brand.getText().toString(), catId + "", this.brandEntity.getStatus() + "", edt_tollfree.getText().toString(), brnd_img, edt_address.getText().toString()), 1);
                return;
            } else {
                if (selectedImageUri == null) {
                    BahetiEnterprises.retShowAlertDialod("Choose Image", AddBrandActivity.this);
                } else {
                    addUpdateBrand(this.viewModel, new BrandEntity(0, this.edt_brand.getText().toString(), catId + "", "1", edt_tollfree.getText().toString(), brnd_img, edt_address.getText().toString()), 0);
                }
            }
        }
    }

    private void addUpdateBrand(BrandViewModel viewModel, BrandEntity brandEntity, int chk) {
        try {
            this.sweetAlertDialog.show();

            viewModel.addBrand(brandEntity, chk, selectedImageUri).observe(this, new Observer<List<BrandEntity>>() {
                public void onChanged(@Nullable List<BrandEntity> categoryEntities) {
                    if (categoryEntities.isEmpty()) {
                        AddBrandActivity.this.sweetAlertDialog.show();
                        return;
                    }
                    AddBrandActivity.this.sweetAlertDialog.dismiss();
                    AddBrandActivity.this.onBackPressed();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    public void setCategoryToSpinner(String catId, List<CategoryEntity> categoryEntities) {
        int pos = 0;
        List<String> catList = new ArrayList();
        for (CategoryEntity categoryEntity : categoryEntities) {
            catList.add(String.valueOf(categoryEntity.getId()));
        }
        int i = 0;
        while (i < catList.size()) {
            if (((String) catList.get(i)).equalsIgnoreCase(catId)) {
                pos = i;
                i = catList.size();
            }
            i++;
        }
        this.category_spinner.setSelection(pos);
    }
}
