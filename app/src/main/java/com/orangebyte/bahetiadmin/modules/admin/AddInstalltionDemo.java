package com.orangebyte.bahetiadmin.modules.admin;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.adapters.UserAutoCompleteAdapter;
import com.orangebyte.bahetiadmin.modules.entities.BrandEntity;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.entities.ProductEntity;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.UserEntity;
import com.orangebyte.bahetiadmin.modules.viewmodels.BrandViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.CallLogInstallViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.CallLogInstallViewModel.CallLogInstallFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels.Factory;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel.EmployeeFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.ProductViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.ProductViewModel.ProductFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel.SubCategoryFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.UserViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.UserViewModel.UserFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import com.orangebyte.bahetiadmin.views.DelayAutoCompleteTextView;
import im.delight.android.webview.BuildConfig;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddInstalltionDemo extends AppCompatActivity implements LifecycleRegistryOwner {
    Button add,add_customer;
    List<CategoryEntity> categoryEntityList = new ArrayList();
    Spinner category_spinner;
    EditText descrp,edt_amount;
    List<EmployeeEntity> employeeEntityList = new ArrayList();
    Spinner employee_spinner;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    int mDay;
    int mMonth;
    List<BrandEntity> brandEntityList = new ArrayList();

    private OnDateSetListener mSeleteDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            select_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    int mYear;
    List<ProductEntity> productEntityList = new ArrayList();
    Spinner product_spinner;
    DelayAutoCompleteTextView search_user;
    TextView select_date;
    List<SubCategoryEntity> subCategoryEntityList = new ArrayList();
    Spinner subcategory_spinner, brand_spinner;
    Spinner type_spinner;
    UserEntity userEntity1;

    Spinner paid_spinner;
    LinearLayout Amount_to_pay;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_installtion_demo);
        BahetiEnterprises.setActBar(getResources().getString(R.string.New), this, false);
        initView();
        setCatgoryToSpinner();
        this.category_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setSubCatgoryToSpinner(((CategoryEntity) categoryEntityList.get(i)).getId() + BuildConfig.VERSION_NAME);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.subcategory_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (brandEntityList.size() == 0) {
                    setBrandToSpinner(categoryEntityList.get(category_spinner.getSelectedItemPosition()).getId() + "");
                } else {
                    setProductToSpinner();
                }
                

               // setProductToSpinner();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        brand_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setProductToSpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        this.add.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                initModelView();
            }
        });
    }

    public void initModelView() {
        CallLogInstallViewModel viewModel = (CallLogInstallViewModel) ViewModelProviders.of(this, new CallLogInstallFactory()).get(CallLogInstallViewModel.class);
        CallLogInstallationDemoEntity oe = new CallLogInstallationDemoEntity();
        if (BahetiEnterprises.checkSpinner(this.category_spinner)) {
            oe.setCat_id("null");
        } else {
            oe.setCat_id(((CategoryEntity) this.categoryEntityList.get(this.category_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
        }
        if (BahetiEnterprises.checkSpinner(this.subcategory_spinner)) {
            oe.setSubcat_id("null");
        } else {
            oe.setSubcat_id(((SubCategoryEntity) this.subCategoryEntityList.get(this.subcategory_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
        }
        if (BahetiEnterprises.checkSpinner(this.product_spinner)) {
            BahetiEnterprises.retShowAlertDialod("Please Select Product", this);
        } else {
            oe.setProduct_id(((ProductEntity) this.productEntityList.get(this.product_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
        }
        if (BahetiEnterprises.checkSpinner(this.employee_spinner)) {
            oe.setEmp_id("null");
        } else {
            oe.setEmp_id(((EmployeeEntity) this.employeeEntityList.get(this.employee_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
        }
        oe.setStatus("Pending");
        oe.setDescrip(this.descrp.getText().toString());
        oe.setType(this.type_spinner.getSelectedItem().toString());
        oe.setUser_id(this.userEntity1.getId() + BuildConfig.VERSION_NAME);
        oe.setDemodate(BahetiEnterprises.tStamp(this.select_date.getText().toString()) + BuildConfig.VERSION_NAME);
        if (this.userEntity1 == null) {
            BahetiEnterprises.retShowAlertDialod("Please Select User", this);
        } else if (TextUtils.isEmpty(this.descrp.getText().toString())) {
            BahetiEnterprises.retShowAlertDialod("Please Enter Description", this);
        } else if (this.type_spinner.getSelectedItemPosition() == 0) {
            BahetiEnterprises.retShowAlertDialod("Please Select Type", this);
        } else if (TextUtils.isEmpty(this.select_date.getText().toString())) {
            BahetiEnterprises.retShowAlertDialod("Please Select Date", this);
        } else {

            oe.setPaid_type(paid_spinner.getSelectedItem().toString());
            if (TextUtils.isEmpty(edt_amount.getText().toString())) {
                oe.setPaid_amt("0");
            } else {
                oe.setPaid_amt(edt_amount.getText().toString());
            }
            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, 5);
            sweetAlertDialog.setTitle("Loading");
            viewModel.changeCallLog(oe, 1).observe(this, new Observer<List<Boolean>>() {
                public void onChanged(@Nullable List<Boolean> booleen) {
                    if (!booleen.isEmpty()) {
                        sweetAlertDialog.dismiss();
                        onBackPressed();
                    } else if (booleen.size() == 0) {
                        sweetAlertDialog.dismiss();
                    } else {
                        sweetAlertDialog.show();
                    }
                }
            });
        }
    }

    public void setCatgoryToSpinner() {
        try {
            ((CategoryViewMdels) ViewModelProviders.of(this, new Factory()).get(CategoryViewMdels.class)).getCats().observe(this, new Observer<List<CategoryEntity>>() {
                public void onChanged(@Nullable List<CategoryEntity> categoryEntities) {
                    if (!categoryEntities.isEmpty()) {
                        setToSpinner(categoryEntities);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setSubCatgoryToSpinner(final String catId) {
        try {
            ((SubCategoryViewModel) ViewModelProviders.of(this, new SubCategoryFactory()).get(SubCategoryViewModel.class)).getSubCats().observe(this, new Observer<List<SubCategoryEntity>>() {
                public void onChanged(@Nullable List<SubCategoryEntity> subCategoryEntities) {
                    if (!subCategoryEntities.isEmpty()) {
                        setToSubCatSpinner(subCategoryEntities, catId);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setProductToSpinner() {
        try {
            ProductViewModel viewModel2 = (ProductViewModel) ViewModelProviders.of(this, new ProductFactory()).get(ProductViewModel.class);
            ProductEntity pe = new ProductEntity();
           /* pe.setSub_cat_id(((SubCategoryEntity) this.subCategoryEntityList.get(this.subcategory_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
            pe.setCat_id(((CategoryEntity) this.categoryEntityList.get(this.category_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
            pe.setBrand_id("null");*/

            pe.setCat_id(categoryEntityList.get(category_spinner.getSelectedItemPosition()).getId() + "");


            try {
                pe.setSub_cat_id(subCategoryEntityList.get(subcategory_spinner.getSelectedItemPosition()).getId() + "");

            } catch (Exception e) {
                e.printStackTrace();
                pe.setSub_cat_id("null");
            }

            if (brand_spinner.getSelectedItemPosition() == 0) {
                pe.setBrand_id("null");
            } else {
                pe.setBrand_id(brandEntityList.get(brand_spinner.getSelectedItemPosition() - 1).getId() + "");
            }



            viewModel2.getProduct(pe).observe(this, new Observer<List<ProductEntity>>() {
                public void onChanged(@Nullable List<ProductEntity> productEntityList) {
                    if (productEntityList.isEmpty()) {
                        setProductDataToSpinner(new ArrayList());
                    } else {
                        setProductDataToSpinner(productEntityList);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setToSpinner(List<CategoryEntity> categoryEntities) {
        this.categoryEntityList = categoryEntities;
        List<String> catList = new ArrayList();
        try {
            for (CategoryEntity categoryEntity : categoryEntities) {
                catList.add(categoryEntity.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.category_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    private void setProductDataToSpinner(List<ProductEntity> productEntities) {
        this.productEntityList = productEntities;
        List<String> prodLIst = new ArrayList();
        try {
            for (ProductEntity productEntity : productEntities) {
                prodLIst.add(productEntity.getTitle());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.product_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, prodLIst));
    }

    private void setToSubCatSpinner(List<SubCategoryEntity> subCategoryEntities, String catId) {
        subCategoryEntityList.clear();
        List<String> catList = new ArrayList();
        try {
            for (SubCategoryEntity subCategoryEntity : subCategoryEntities) {
                if (subCategoryEntity.getCat_id().equalsIgnoreCase(catId)) {
                    catList.add(subCategoryEntity.getSub_cat_name());
                    this.subCategoryEntityList.add(subCategoryEntity);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.subcategory_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    public void setEmployeeToSpinner() {
        try {
            EmployeeEntity ee = new EmployeeEntity();
            ee.setAssigned_mgr_type("null");
            ((EmployeeViewModel) ViewModelProviders.of(this, new EmployeeFactory()).get(EmployeeViewModel.class)).getEmps(ee).observe(this, new Observer<List<EmployeeEntity>>() {
                public void onChanged(@Nullable List<EmployeeEntity> employeeEntities) {
                    if (!employeeEntities.isEmpty()) {
                        setEmpToSpinner(employeeEntities, employee_spinner);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setEmpToSpinner(List<EmployeeEntity> employeeEntities, Spinner employee_spinner1) {
        this.employeeEntityList = employeeEntities;
        List<String> catList = new ArrayList();
        try {
            for (EmployeeEntity employeeEntity : employeeEntities) {
                catList.add(employeeEntity.getEmp_nm());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        employee_spinner1.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    public void showSelectDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this.mSeleteDate, this.mYear, this.mMonth, this.mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }


    public void setBrandToSpinner(final String catId) {
        try {
            ((BrandViewModel) ViewModelProviders.of(this, new BrandViewModel.BrandFactory()).get(BrandViewModel.class)).getBrands().observe(this, new Observer<List<BrandEntity>>() {
                public void onChanged(@Nullable List<BrandEntity> brandEntityList) {
                    if (!brandEntityList.isEmpty()) {
                        setToBrandSpinner(brandEntityList, catId);
                        setEmployeeToSpinner();
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setToBrandSpinner(List<BrandEntity> brandEntities, String catId) {
        this.brandEntityList = brandEntities;
        List<String> catList = new ArrayList();
        catList.add("Select");
        try {
            for (BrandEntity subCategoryEntity : brandEntities) {
              //  if (subCategoryEntity.getCat_id().equalsIgnoreCase(catId)) {
                    catList.add(subCategoryEntity.getBrand_nm());
               // }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        brand_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }



    public void initView() {
        UserViewModel viewModel2 = (UserViewModel) ViewModelProviders.of(this, new UserFactory()).get(UserViewModel.class);
        this.add = (Button) findViewById(com.orangebyte.pdflibrary.R.id.add);
        add_customer =(Button) findViewById(R.id.add_customer);
        this.category_spinner = (Spinner) findViewById(R.id.category_spinner);
        this.subcategory_spinner = (Spinner) findViewById(R.id.subcategory_spinner);
        this.product_spinner = (Spinner) findViewById(R.id.product_spinner);
        brand_spinner =(Spinner) findViewById(R.id.brand_spinner) ;
        this.employee_spinner = (Spinner) findViewById(R.id.employee_spinner);
        this.type_spinner = (Spinner) findViewById(R.id.type_spinner);
        this.select_date = (TextView) findViewById(R.id.select_date);
        Calendar c = Calendar.getInstance();
        this.mYear = c.get(1);
        this.mMonth = c.get(2);
        this.mDay = c.get(5);
        edt_amount = (EditText) findViewById(R.id.edt_amount);
        Amount_to_pay = (LinearLayout) findViewById(R.id.Amount_to_pay);
        paid_spinner = (Spinner) findViewById(R.id.paid_spinner);

        this.descrp = (EditText) findViewById(R.id.descrp);
        this.search_user = (DelayAutoCompleteTextView) findViewById(R.id.search_user);
        this.search_user.setThreshold(2);
        this.search_user.setAdapter(new UserAutoCompleteAdapter(this, (ProgressBar) findViewById(R.id.pb_loading_indicator), viewModel2));
        this.search_user.setLoadingIndicator((ProgressBar) findViewById(R.id.pb_loading_indicator));
        this.search_user.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                userEntity1 = (UserEntity) adapterView.getItemAtPosition(position);
                search_user.setText(userEntity1.getUsernm());
            }
        });


        add_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AddInstalltionDemo.this, AddCustomer.class));
            }
        });
        paid_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 1) {
                    Amount_to_pay.setVisibility(View.VISIBLE);
                } else {
                    Amount_to_pay.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }
}
