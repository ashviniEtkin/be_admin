package com.orangebyte.bahetiadmin.modules.admin;

import android.app.DatePickerDialog;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.expandableview.ExpandableRelativeLayout;
import com.orangebyte.bahetiadmin.modules.adapters.ComplaintIncentiveAdapter;
import com.orangebyte.bahetiadmin.modules.adapters.IncentiveCallLogAdapter;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.models.ServicesResponse;
import com.orangebyte.bahetiadmin.modules.repository.ServiceFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.ComplaintsViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import im.delight.android.webview.BuildConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MonitoringEmployee extends AppCompatActivity implements LifecycleRegistryOwner {
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);


    List<EmployeeEntity> employeeEntityList = new ArrayList();
    Spinner employee_spinner;
    TextView select_date;
    Spinner service_spinner;
    SharedPreferences sh;
    Button show, btn_reset;
    TextView start_date;
    int mDay;
    int mMonth;
    SweetAlertDialog sweetAlertDialog;
    TextView to_date, totl_incentive;
    RecyclerView incentiveList;
    ExpandableRelativeLayout date_expandble_view;

    private DatePickerDialog.OnDateSetListener mSeleteDate = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            select_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    private DatePickerDialog.OnDateSetListener mStartDate = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            start_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    private DatePickerDialog.OnDateSetListener mToDate = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            to_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    int mYear;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitoring_employee);

        BahetiEnterprises.setActBar(getString(R.string.monitoring_employee), MonitoringEmployee.this, false);
        initView();
        setEmployeeToSpinner();

    }

    public void initView() {

        employee_spinner = (Spinner) findViewById(R.id.employee_spinner);
        totl_incentive = (TextView) findViewById(R.id.totl_incentive);
        service_spinner = (Spinner) findViewById(R.id.status_spinner);

        show = (Button) findViewById(R.id.show);
        incentiveList = (RecyclerView) findViewById(R.id.complaint_recycler);
        select_date = (TextView) findViewById(R.id.select_date);
        start_date = (TextView) findViewById(R.id.start_date);
        to_date = (TextView) findViewById(R.id.to_date);

        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        sh = getSharedPreferences("log", 0);


        sweetAlertDialog = new SweetAlertDialog(this, 5);
        sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
        sweetAlertDialog.setTitleText("Loading");
        sweetAlertDialog.setCancelable(false);

        btn_reset = (Button) findViewById(R.id.btn_reset);

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEmployeeToSpinner();

                service_spinner.setSelection(0);
                select_date.setText("");
                start_date.setText("");
                to_date.setText("");


            }
        });

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (service_spinner.getSelectedItemPosition() == 0) {
                    BahetiEnterprises.retShowAlertDialod("Select Type", MonitoringEmployee.this);
                } else if (employee_spinner.getSelectedItemPosition() == 0) {
                    BahetiEnterprises.retShowAlertDialod("Select Employee", MonitoringEmployee.this);

                } else {
                    getEMployee();
                }
            }
        });

    }

    public void getEMployee() {
        if (service_spinner.getSelectedItemPosition() == 1) {
            CallLogInstallationDemoEntity callLogInstallationDemoEntity = new CallLogInstallationDemoEntity();
            callLogInstallationDemoEntity.setEmp_id(employeeEntityList.get(employee_spinner.getSelectedItemPosition() - 1).getId() + "");
            callLogInstallationDemoEntity.setStatus(getString(R.string.Resolved_Closed));
            if (!TextUtils.isEmpty(start_date.getText().toString()) && !TextUtils.isEmpty(to_date.getText().toString())) {
                callLogInstallationDemoEntity.setDemodate(BahetiEnterprises.tStamp(start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                callLogInstallationDemoEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(to_date.getText().toString()) + BuildConfig.VERSION_NAME);
            } else if (!TextUtils.isEmpty(start_date.getText().toString()) && TextUtils.isEmpty(to_date.getText().toString())) {
                callLogInstallationDemoEntity.setDemodate(BahetiEnterprises.tStamp(start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                callLogInstallationDemoEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(start_date.getText().toString()) + BuildConfig.VERSION_NAME);
            } else if (TextUtils.isEmpty(start_date.getText().toString()) && !TextUtils.isEmpty(to_date.getText().toString())) {
                callLogInstallationDemoEntity.setDemodate(BahetiEnterprises.tStamp(to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                callLogInstallationDemoEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(to_date.getText().toString()) + BuildConfig.VERSION_NAME);
            } else if (TextUtils.isEmpty(select_date.getText().toString())) {
                callLogInstallationDemoEntity.setDemodate("null");
                callLogInstallationDemoEntity.setLast_atnd_on("null");
            } else {
                callLogInstallationDemoEntity.setDemodate(BahetiEnterprises.tStamp(select_date.getText().toString()) + BuildConfig.VERSION_NAME);
                callLogInstallationDemoEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(select_date.getText().toString()) + BuildConfig.VERSION_NAME);
            }


            try {
                ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/activeOffers/")
                        .getCallLogsInstallEmployeeIncetive(callLogInstallationDemoEntity).enqueue(new Callback<ServicesResponse>() {
                    public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                        List<CallLogInstallationDemoEntity> callLogInstallationDemoEntityList = (((ServicesResponse) response.body()).getCallLogInstallationDemoEntityList());

                        setCallLogList(callLogInstallationDemoEntityList);
                    }

                    public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (service_spinner.getSelectedItemPosition() == 2) {

            ComplaintEntity complaintEntity = new ComplaintEntity();
            complaintEntity.setAssigned_empid(employeeEntityList.get(employee_spinner.getSelectedItemPosition() - 1).getId() + "");
            complaintEntity.setStatus(getString(R.string.Resolved_Closed));
            if (!TextUtils.isEmpty(start_date.getText().toString()) && !TextUtils.isEmpty(to_date.getText().toString())) {
                complaintEntity.setComp_date(BahetiEnterprises.tStamp(start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                complaintEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(to_date.getText().toString()) + BuildConfig.VERSION_NAME);
            } else if (!TextUtils.isEmpty(start_date.getText().toString()) && TextUtils.isEmpty(to_date.getText().toString())) {
                complaintEntity.setComp_date(BahetiEnterprises.tStamp(start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                complaintEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(start_date.getText().toString()) + BuildConfig.VERSION_NAME);
            } else if (TextUtils.isEmpty(start_date.getText().toString()) && !TextUtils.isEmpty(to_date.getText().toString())) {
                complaintEntity.setComp_date(BahetiEnterprises.tStamp(to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                complaintEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(to_date.getText().toString()) + BuildConfig.VERSION_NAME);
            } else if (TextUtils.isEmpty(select_date.getText().toString())) {
                complaintEntity.setComp_date("null");
                complaintEntity.setLast_atnd_on("null");
            } else {
                complaintEntity.setComp_date(BahetiEnterprises.tStamp(select_date.getText().toString()) + BuildConfig.VERSION_NAME);
                complaintEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(select_date.getText().toString()) + BuildConfig.VERSION_NAME);
            }

            try {
                ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/activeOffers/")
                        .getEMployeeComplaintIncentive(complaintEntity).enqueue(new Callback<ServicesResponse>() {
                    public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                        List<ComplaintEntity> complaintEntityList = (((ServicesResponse) response.body()).getComplaintEntityList());

                        setComplaintList(complaintEntityList);
                    }

                    public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    public void setComplaintList(List<ComplaintEntity> complaintEntityList) {

        int totl = Integer.parseInt(complaintEntityList.get(0).getIncentive()) * complaintEntityList.size();
        totl_incentive.setText("Total Incentive : Rs." + totl);
        ComplaintIncentiveAdapter adapter = new ComplaintIncentiveAdapter(complaintEntityList, MonitoringEmployee.this);
        incentiveList.setLayoutManager(new LinearLayoutManager(MonitoringEmployee.this));
        incentiveList.setAdapter(adapter);

    }


    public void setCallLogList(List<CallLogInstallationDemoEntity> callLogInstallationDemoEntityList) {

        int totl = Integer.parseInt(callLogInstallationDemoEntityList.get(0).getIncentive()) * callLogInstallationDemoEntityList.size();
        totl_incentive.setText("Total Incentive : Rs." + totl);
        IncentiveCallLogAdapter adapter = new IncentiveCallLogAdapter(callLogInstallationDemoEntityList, MonitoringEmployee.this);
        incentiveList.setLayoutManager(new LinearLayoutManager(MonitoringEmployee.this));
        incentiveList.setAdapter(adapter);
    }

    public void setEmployeeToSpinner() {
        try {
            EmployeeEntity ee = new EmployeeEntity();
            ee.setAssigned_mgr_type("null");
            ((EmployeeViewModel) ViewModelProviders.of(this, new EmployeeViewModel.EmployeeFactory()).get(EmployeeViewModel.class))
                    .getEmps(ee).observe(this, new Observer<List<EmployeeEntity>>() {
                public void onChanged(@Nullable List<EmployeeEntity> employeeEntities) {
                    if (!employeeEntities.isEmpty()) {
                        setToSpinner(employeeEntities, employee_spinner);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private void setToSpinner(List<EmployeeEntity> employeeEntities, Spinner employee_spinner1) {
        employeeEntityList = employeeEntities;
        List<String> catList = new ArrayList();
        catList.add("Select");
        try {
            for (EmployeeEntity employeeEntity : employeeEntities) {
                catList.add(employeeEntity.getEmp_nm());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        employee_spinner1.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    public void showDateView(View view) {
        date_expandble_view = (ExpandableRelativeLayout) findViewById(R.id.date_expandble_view);
        date_expandble_view.toggle();
    }

    public void showSelectDate(View view) {
        new DatePickerDialog(this, mSeleteDate, mYear, mMonth, mDay).show();
    }

    public void showStartDate(View view) {
        new DatePickerDialog(this, mStartDate, mYear, mMonth, mDay).show();
    }

    public void showToDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, mToDate, mYear, mMonth, mDay);
        if (TextUtils.isEmpty(start_date.getText().toString())) {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        } else {
            datePickerDialog.getDatePicker().setMinDate(BahetiEnterprises.tStamp(start_date.getText().toString()));
        }
        datePickerDialog.show();
    }


    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }
}
