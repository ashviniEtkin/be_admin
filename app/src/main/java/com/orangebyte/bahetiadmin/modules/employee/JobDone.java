package com.orangebyte.bahetiadmin.modules.employee;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;
import com.orangebyte.pdflibrary.PdfDocument;
import com.orangebyte.pdflibrary.PdfDocument.A4_MODE;
import com.orangebyte.pdflibrary.PdfDocument.Callback;
import com.orangebyte.pdflibrary.viewRenderer.AbstractViewRenderer;

import java.io.File;
import java.util.Calendar;

import im.delight.android.webview.BuildConfig;

public class JobDone extends AppCompatActivity {
    EditText amt_to_pay;
    Button btn_send;
    String intnt = BuildConfig.VERSION_NAME;
    TextView iv_address;
    TextView iv_descp;
    TextView iv_name;
    TextView iv_paied_amt;
    TextView iv_product_model_no;
    TextView iv_product_nm;
    TextView iv_resloved_date;
    TextView iv_status;
    int mDay;
    int mMonth;
    int mYear;
    TextView name;
    TextView select_date;
    TextView status;
    TextView service_no, rec_dt, recp_name, recp_amt, recp_rs_n_dt, rep_rs;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_done);
        BahetiEnterprises.setActBar(getResources().getString(R.string.Invoice), this, false);
        init();
        this.intnt = getIntent().getStringExtra(BahetiEnterprises.INTNT);
        if (this.intnt.equalsIgnoreCase("complaint")) {
            this.name.setText(((ComplaintEntity) getIntent().getSerializableExtra(BahetiEnterprises.FROM)).getUsernm());
        } else {
            this.name.setText(((CallLogInstallationDemoEntity) getIntent().getSerializableExtra(BahetiEnterprises.FROM)).getUsernm());
        }
        this.btn_send.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
            }
        });
    }

    public void init() {
        this.select_date = (TextView) findViewById(R.id.select_date);
        this.status = (TextView) findViewById(R.id.status);
        this.name = (TextView) findViewById(R.id.name);
        this.amt_to_pay = (EditText) findViewById(R.id.amt_to_pay);
        this.btn_send = (Button) findViewById(R.id.btn_send);
        Calendar c = Calendar.getInstance();
        this.mYear = c.get(1);
        this.mMonth = c.get(2);
        this.mDay = c.get(5);
        this.select_date.setText(this.mDay + "-" + (this.mMonth + 1) + "-" + this.mYear);
        this.status.setText(getResources().getString(R.string.resolved));
    }

    public void initPdfView(View pdfView) {


        this.service_no = (TextView) pdfView.findViewById(R.id.service_no);
        this.rec_dt = (TextView) pdfView.findViewById(R.id.rec_dt);
        this.recp_name = (TextView) pdfView.findViewById(R.id.recp_name);
        this.recp_amt = (TextView) pdfView.findViewById(R.id.recp_amt);
        this.recp_rs_n_dt = (TextView) pdfView.findViewById(R.id.recp_rs_n_dt);
        this.rep_rs = (TextView) pdfView.findViewById(R.id.rep_rs);


        this.iv_name = (TextView) pdfView.findViewById(R.id.name);
        this.iv_product_nm = (TextView) pdfView.findViewById(R.id.product_nm);
        this.iv_descp = (TextView) pdfView.findViewById(R.id.descp);
        this.iv_product_model_no = (TextView) pdfView.findViewById(R.id.product_model_no);
        this.iv_status = (TextView) pdfView.findViewById(R.id.status);
        this.iv_resloved_date = (TextView) pdfView.findViewById(R.id.resloved_date);
        this.iv_paied_amt = (TextView) pdfView.findViewById(R.id.paied_amt);
    }

    public void updateComplaintData(ComplaintEntity complaintEntity) {
      /*  this.iv_name.setText(complaintEntity.getUsernm());
        this.iv_product_nm.setText(complaintEntity.getTitle());
        this.iv_product_model_no.setText(complaintEntity.getModel_no());
        this.iv_status.setText(getResources().getString(R.string.resolved));
        this.iv_resloved_date.setText(this.mDay + "/" + this.mMonth + "/" + this.mYear);
        this.iv_paied_amt.setText(this.amt_to_pay.getText().toString());*/

        // TextView service_no, rec_dt, recp_name, recp_amt, recp_rs_n_dt, rep_rs;


        this.recp_name.setText(complaintEntity.getUsernm());
        this.service_no.setText(getString(R.string.service_request_no) + " " + complaintEntity.getCom_no());
        this.rec_dt.setText(getString(R.string.Date)+this.mDay + "/" + this.mMonth + "/" + this.mYear);
        this.recp_rs_n_dt.setText(" 388293 Date : " + this.mDay + "/" + this.mMonth + "/" + this.mYear);
        this.recp_amt.setText(this.amt_to_pay.getText().toString());
    }

    public void updateInstallationData(CallLogInstallationDemoEntity cdemo) {
      /*  this.iv_name.setText(cdemo.getUsernm());
        this.iv_product_nm.setText(cdemo.getTitle());
        this.iv_product_model_no.setText(cdemo.getModel_no());
        this.iv_status.setText(getResources().getString(R.string.resolved));
        this.iv_resloved_date.setText(this.mDay + "/" + this.mMonth + "/" + this.mYear);
        this.iv_paied_amt.setText(this.amt_to_pay.getText().toString());*/

        this.recp_name.setText(cdemo.getUsernm());
        this.service_no.setText(getString(R.string.service_request_no) + " " + cdemo.getCom_no());
        this.rec_dt.setText(getString(R.string.Date)+this.mDay + "/" + this.mMonth + "/" + this.mYear);
        this.recp_rs_n_dt.setText(" 388293 Date : " + this.mDay + "/" + this.mMonth + "/" + this.mYear);
        this.recp_amt.setText(this.amt_to_pay.getText().toString());
    }

    public void sendInvoice() {
        AbstractViewRenderer abstractViewRenderer = new AbstractViewRenderer(this, R.layout.invoice_after_job_complete) {
            protected void initView(View view) {
                JobDone.this.initPdfView(view);
                if (JobDone.this.intnt.equalsIgnoreCase("complaint")) {
                    JobDone.this.updateComplaintData((ComplaintEntity) JobDone.this.getIntent().getSerializableExtra(BahetiEnterprises.FROM));
                    return;
                }
                JobDone.this.updateInstallationData((CallLogInstallationDemoEntity) JobDone.this.getIntent().getSerializableExtra(BahetiEnterprises.FROM));
            }
        };
    }

    public void createMulPDf(Context ctx, AbstractViewRenderer pages, String excel_file_nm, String excel_attach_nm, final ProgressDialog progressDialog) {
        PdfDocument doc = new PdfDocument(ctx);
        String file_nm = "invoice_" + System.currentTimeMillis() + BuildConfig.VERSION_NAME;
        File f = new File(Environment.getExternalStorageDirectory() + "/.Orange/.InvoiceSummary/" + file_nm);
        if (f.exists() && !f.isDirectory()) {
            f.delete();
        }
        File folder = new File(Environment.getExternalStorageDirectory() + File.separator + ".Orange" + File.separator + ".InvoiceSummary");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        if (success) {
            Log.e("Dir", "Created");
        } else {
            Log.e("Dir", "Not Created");
        }
        doc.addPage(pages);
        doc.setOrientation(A4_MODE.PORTRAIT);
        doc.setProgressTitle(com.orangebyte.pdflibrary.R.string.app_name);
        doc.setProgressMessage(R.string.pdf_message);
        doc.setFileName(file_nm);
        doc.setSaveDirectory(folder);
        doc.setInflateOnMainThread(false);
        doc.setListener(new Callback() {
            public void onComplete(File file) {
                Log.i(PdfDocument.TAG_PDF_MY_XML, "Complete");
                new AsyncTask() {
                    protected void onPreExecute() {
                        super.onPreExecute();
                    }

                    protected Object doInBackground(Object[] objects) {
                        return null;
                    }

                    protected void onPostExecute(Object o) {
                        super.onPostExecute(o);
                        progressDialog.dismiss();
                    }
                }.execute(new Object[0]);
            }

            public void onError(Exception e) {
                e.printStackTrace();
                Log.i(PdfDocument.TAG_PDF_MY_XML, "Error");
            }
        });
        doc.createPdf(ctx);
    }
}
