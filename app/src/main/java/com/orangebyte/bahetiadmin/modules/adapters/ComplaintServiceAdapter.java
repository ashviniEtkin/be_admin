package com.orangebyte.bahetiadmin.modules.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.admin.ComplaintDetails;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;

import java.io.Serializable;
import java.util.List;

public class ComplaintServiceAdapter extends Adapter<ComplaintServiceAdapter.ComplaintServiceHolder> {
    int chk;
    List<ComplaintEntity> complaintServiceEntityList;
    Context ctx;
    CheckBox cb;

    public List<ComplaintEntity> getComplaintServiceEntityList() {
        return complaintServiceEntityList;
    }

    public void setComplaintServiceEntityList(List<ComplaintEntity> complaintServiceEntityList) {
        this.complaintServiceEntityList = complaintServiceEntityList;
    }

    public boolean checkAll() {
        boolean a = false;
        int k = 0;
        while (k < this.getComplaintServiceEntityList().size()) {
            if (((ComplaintEntity) this.getComplaintServiceEntityList().get(k)).isSelected()) {
                a = true;
                k++;
            } else {
                a = false;
                k = this.complaintServiceEntityList.size();
            }
        }
        return a;
    }

    public void setAllCheck(boolean a) {
        for (int i = 0; i < this.complaintServiceEntityList.size(); i++) {
            ((ComplaintEntity) this.complaintServiceEntityList.get(i)).setSelected(a);
        }
        notifyDataSetChanged();
    }

    public class ComplaintServiceHolder extends ViewHolder {
        TextView asign_to;
        Button btn_assign_transfer;
        Button btn_details;
        LinearLayout btn_layout;
        TextView comp_date;
        TextView comp_desc;
        TextView comp_status;
        CardView container_id;
        TextView cust_nm;
        TextView service_request_no;
        LinearLayout service_no_container;
        CheckBox checkb;

        public ComplaintServiceHolder(View itemView) {
            super(itemView);
            this.asign_to = (TextView) itemView.findViewById(R.id.asign_to);
            this.cust_nm = (TextView) itemView.findViewById(R.id.cust_nm);
            this.comp_desc = (TextView) itemView.findViewById(R.id.comp_desc);
            this.comp_date = (TextView) itemView.findViewById(R.id.comp_date);
            this.comp_status = (TextView) itemView.findViewById(R.id.comp_status);
            this.container_id = (CardView) itemView.findViewById(R.id.container_id);
            this.btn_layout = (LinearLayout) itemView.findViewById(R.id.btn_layout);
            this.btn_assign_transfer = (Button) itemView.findViewById(R.id.btn_assign_transfer);
            this.btn_details = (Button) itemView.findViewById(R.id.btn_details);
            this.checkb = (CheckBox) itemView.findViewById(R.id.checkb);
            service_no_container = (LinearLayout) itemView.findViewById(R.id.service_no_container);
            service_request_no = (TextView) itemView.findViewById(R.id.service_request_no);
            service_no_container.setVisibility(View.VISIBLE);
        }
    }

    public ComplaintServiceAdapter(List<ComplaintEntity> complaintServiceEntityList, Context ctx, int chk, CheckBox cb) {
        this.complaintServiceEntityList = complaintServiceEntityList;
        this.ctx = ctx;
        this.chk = chk;
        this.cb = cb;
    }

    public ComplaintServiceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ComplaintServiceHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.complaint_row, parent, false));
    }

    public void onBindViewHolder(ComplaintServiceHolder holder, final int position) {
        holder.cust_nm.setText(((ComplaintEntity) this.complaintServiceEntityList.get(position)).getUsernm());
        holder.comp_desc.setText(((ComplaintEntity) this.complaintServiceEntityList.get(position)).getDesc());
        holder.comp_date.setText(BahetiEnterprises.retDate(((ComplaintEntity) this.complaintServiceEntityList.get(position)).getComp_date()));
        holder.comp_status.setText(((ComplaintEntity) this.complaintServiceEntityList.get(position)).getStatus());
        holder.service_request_no.setText(complaintServiceEntityList.get(position).getCom_no());

        BahetiEnterprises.setData(((ComplaintEntity) this.complaintServiceEntityList.get(position)).getEmp_nm(), holder.asign_to);
        if (((ComplaintEntity) this.complaintServiceEntityList.get(position)).getStatus().equalsIgnoreCase("Pending")) {
            if (BahetiEnterprises.showInRed(Long.parseLong(((ComplaintEntity) this.complaintServiceEntityList.get(position)).getComp_date()))) {
                holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.red_900));
            } else {
                holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.red_100));
            }
            holder.btn_assign_transfer.setVisibility(View.VISIBLE);
        } else if (((ComplaintEntity) this.complaintServiceEntityList.get(position)).getStatus().equalsIgnoreCase("On Hold")) {
            holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.green_100));
            holder.btn_assign_transfer.setVisibility(View.VISIBLE);
        } else if (((ComplaintEntity) this.complaintServiceEntityList.get(position)).getStatus().equalsIgnoreCase("In Progress")) {
            holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.teal_100));
            holder.btn_assign_transfer.setVisibility(View.VISIBLE);
        } else {
            holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.amber_200));
            holder.btn_assign_transfer.setVisibility(View.GONE);
        }
        if (this.chk == 0 || this.chk == 2) {
            holder.btn_layout.setVisibility(View.GONE);
            holder.checkb.setVisibility(View.GONE);
        } else {
            holder.btn_layout.setVisibility(View.VISIBLE);
            holder.checkb.setVisibility(View.VISIBLE);

        }
        holder.container_id.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(ComplaintServiceAdapter.this.ctx, ComplaintDetails.class);
                if (ComplaintServiceAdapter.this.chk == 1) {
                    i.putExtra(BahetiEnterprises.FROM, "1");
                } else {
                    i.putExtra(BahetiEnterprises.FROM, "2");
                }
                i.putExtra(BahetiEnterprises.COMPLAINT_DETAILS, (Serializable) ComplaintServiceAdapter.this.complaintServiceEntityList.get(position));
                ComplaintServiceAdapter.this.ctx.startActivity(i);
            }
        });


        try {
            holder.checkb.setChecked(((ComplaintEntity) this.complaintServiceEntityList.get(position)).isSelected());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        this.cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                /*if (b = false && checkAll() == true) {
                    cb.setChecked(false);
                    setAllCheck(b);
                }*/

                boolean g = checkedAllSelected(complaintServiceEntityList);
                if (b == false && g == true) {
                    setAllCheck(b);
                } else if (b == false && g == false) {
                    // setAllCheck(b);
                } else if (b == true && g == false) {
                    setAllCheck(b);
                }

            }
        });


        holder.checkb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                complaintServiceEntityList.get(position).setSelected(b);

                boolean g = checkedAllSelected(complaintServiceEntityList);

                if (g) {
                    cb.setChecked(true);
                } else {
                    cb.setChecked(false);
                }
               /* if (checkAll() == false) {
                    cb.setChecked(false);
                }
*/
            }
        });
    }

    public boolean checkedAllSelected(List<ComplaintEntity> complaintEntityList) {
        boolean s = false;
        int f = 0;
        for (int y = 0; y < complaintEntityList.size(); y++) {
            if (complaintEntityList.get(y).isSelected()) {
                f++;
            }
        }

        if (f == complaintEntityList.size()) {
            return true;
        } else {
            return false;
        }

    }

    public int getItemCount() {
        return this.complaintServiceEntityList.size();
    }
}
