package com.orangebyte.bahetiadmin.modules.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.entities.OfferEntity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class OfferAdapter extends Adapter<OfferAdapter.OfferHolder> {
    Context ctx;
    List<OfferEntity> offerEntiityList;

    public class OfferHolder extends ViewHolder {
        ImageButton chng_status;
        TextView discount;
        TableRow discount_row;
        TextView model_no;
        ImageView offer_img;
        TextView offer_title;
        TextView offer_type;
        TextView prod_title;
        TextView start_date;
        TextView to_date, dis_nm;

        public OfferHolder(View itemView) {
            super(itemView);
            this.chng_status = (ImageButton) itemView.findViewById(R.id.chng_status);
            this.offer_img = (ImageView) itemView.findViewById(R.id.offer_img);
            this.offer_title = (TextView) itemView.findViewById(R.id.offer_title);
            this.prod_title = (TextView) itemView.findViewById(R.id.prod_title);
            this.model_no = (TextView) itemView.findViewById(R.id.model_no);
            this.start_date = (TextView) itemView.findViewById(R.id.start_date);
            this.to_date = (TextView) itemView.findViewById(R.id.to_date);
            this.offer_type = (TextView) itemView.findViewById(R.id.offer_type);
            this.discount = (TextView) itemView.findViewById(R.id.discount);
            this.dis_nm = (TextView) itemView.findViewById(R.id.dis_nm);
            this.discount_row = (TableRow) itemView.findViewById(R.id.discount_row);
        }
    }

    public OfferAdapter(List<OfferEntity> offerEntiityList, Context ctx) {
        this.ctx = ctx;
        this.offerEntiityList = offerEntiityList;
    }

    public OfferHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OfferHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_row, parent, false));
    }

    public void onBindViewHolder(OfferHolder holder, int position) {
        OfferEntity offerEntiity = (OfferEntity) this.offerEntiityList.get(position);
        holder.offer_title.setText(offerEntiity.getTitle());
        holder.offer_type.setText(offerEntiity.getOffer_type());
        if (offerEntiity.getOffer_type().equalsIgnoreCase("Freebie")) {
            //  holder.discount_row.setVisibility(View.GONE);
            holder.dis_nm.setText(ctx.getResources().getString(R.string.Product));
            holder.discount.setText(offerEntiity.getProduct_title() + "(" + offerEntiity.getModel_no() + ")");

        } else {
            // holder.discount_row.setVisibility(View.VISIBLE);
            holder.dis_nm.setText(ctx.getResources().getString(R.string.discount));
            // holder.discount.setText(offerEntiity.getProduct_title() + "(" + offerEntiity.getModel_no() + ")");
            holder.discount.setText(offerEntiity.getDiscount());
        }

        holder.start_date.setText(BahetiEnterprises.retDate(offerEntiity.getStart_date()));
        holder.to_date.setText(BahetiEnterprises.retDate(offerEntiity.getTo_date()));
        if (offerEntiity.getStatus().equalsIgnoreCase("1")) {
            holder.chng_status.setImageResource(R.drawable.ic_unhide);
            holder.chng_status.setColorFilter(this.ctx.getResources().getColor(R.color.app_color));
        } else {
            holder.chng_status.setImageResource(R.drawable.ic_hide);
            holder.chng_status.setColorFilter(this.ctx.getResources().getColor(R.color.black));
        }
        String imageUrl = BahetiEnterprises.IMAGE_URL + offerEntiity.getOffer_image();
        Log.e("Imagw", imageUrl);
        Picasso.with(this.ctx).load(Uri.parse(imageUrl)).placeholder((int) R.drawable.logo).into(holder.offer_img);
    }

    public int getItemCount() {
        return this.offerEntiityList.size();
    }
}
