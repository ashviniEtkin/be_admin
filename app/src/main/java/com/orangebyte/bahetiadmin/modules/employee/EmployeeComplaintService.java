package com.orangebyte.bahetiadmin.modules.employee;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.expandableview.ExpandableRelativeLayout;
import com.orangebyte.bahetiadmin.modules.adapters.ComplaintServiceAdapter;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener.OnItemClickListener;
import com.orangebyte.bahetiadmin.modules.viewmodels.ComplaintsViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.ComplaintsViewModel.ComplaintFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel.EmployeeFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import im.delight.android.webview.BuildConfig;

public class EmployeeComplaintService extends AppCompatActivity implements LifecycleRegistryOwner {
    ComplaintServiceAdapter adapter;
    ComplaintEntity ce;
    ComplaintsViewModel cnmplViewModel;
    List<ComplaintEntity> complaintEntityList = new ArrayList();
    RecyclerView complaint_recycler;
    ExpandableRelativeLayout date_expandble_view;
    List<EmployeeEntity> employeeEntityList = new ArrayList();
    EditText etd_cust_mobile_no;
    ComplaintFactory factory;
    String from = BuildConfig.VERSION_NAME;
    TextView in_progress;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    LinearLayout list_container;
    int mDay;
    int mMonth;
    private OnDateSetListener mSeleteDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            EmployeeComplaintService.this.select_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    private OnDateSetListener mStartDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            EmployeeComplaintService.this.start_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    private OnDateSetListener mToDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            EmployeeComplaintService.this.to_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    int mYear;
    TextView no_result_fount;
    TextView on_hold;
    TextView pending;
    String post = BuildConfig.VERSION_NAME;
    TextView resolved_closed;
    TextView select_date;
    Spinner service_spinner;
    SharedPreferences sh;
    Button show;
    TextView start_date;
    Spinner status_spinner;
    SweetAlertDialog sweetAlertDialog;
    TextView to_date;
    TextView total;
    String uid = BuildConfig.VERSION_NAME;
    CheckBox all_cb;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_complaint_service);
        BahetiEnterprises.setActBar(getResources().getString(R.string.Complaint_Service), this, false);
        this.from = getIntent().getStringExtra(BahetiEnterprises.FROM);
        initView();
        setEmployeeToSpinner();
        listeners();
        this.show.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (EmployeeComplaintService.this.status_spinner.getSelectedItemPosition() == 0) {
                    EmployeeComplaintService.this.ce.setStatus("null");
                } else {
                    EmployeeComplaintService.this.ce.setStatus(EmployeeComplaintService.this.status_spinner.getSelectedItem().toString());
                }
                try {
                    EmployeeComplaintService.this.ce.setAssigned_empid(EmployeeComplaintService.this.uid);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (EmployeeComplaintService.this.service_spinner.getSelectedItemPosition() == 0) {
                    EmployeeComplaintService.this.ce.setService_type("null");
                } else {
                    EmployeeComplaintService.this.ce.setService_type(EmployeeComplaintService.this.service_spinner.getSelectedItem().toString());
                }
                if (TextUtils.isEmpty(EmployeeComplaintService.this.etd_cust_mobile_no.getText().toString())) {
                    EmployeeComplaintService.this.ce.setUser_id("null");
                } else {
                    EmployeeComplaintService.this.ce.setUser_id(EmployeeComplaintService.this.etd_cust_mobile_no.getText().toString());
                }
                if (!TextUtils.isEmpty(EmployeeComplaintService.this.start_date.getText().toString()) && !TextUtils.isEmpty(EmployeeComplaintService.this.to_date.getText().toString())) {
                    EmployeeComplaintService.this.ce.setComp_date(BahetiEnterprises.tStamp(EmployeeComplaintService.this.start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    EmployeeComplaintService.this.ce.setLast_atnd_on(BahetiEnterprises.lasttStamp(EmployeeComplaintService.this.to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                } else if (!TextUtils.isEmpty(EmployeeComplaintService.this.start_date.getText().toString()) && TextUtils.isEmpty(EmployeeComplaintService.this.to_date.getText().toString())) {
                    EmployeeComplaintService.this.ce.setComp_date(BahetiEnterprises.tStamp(EmployeeComplaintService.this.start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    EmployeeComplaintService.this.ce.setLast_atnd_on(BahetiEnterprises.lasttStamp(EmployeeComplaintService.this.start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                } else if (TextUtils.isEmpty(EmployeeComplaintService.this.start_date.getText().toString()) && !TextUtils.isEmpty(EmployeeComplaintService.this.to_date.getText().toString())) {
                    EmployeeComplaintService.this.ce.setComp_date(BahetiEnterprises.tStamp(EmployeeComplaintService.this.to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    EmployeeComplaintService.this.ce.setLast_atnd_on(BahetiEnterprises.lasttStamp(EmployeeComplaintService.this.to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                } else if (TextUtils.isEmpty(EmployeeComplaintService.this.select_date.getText().toString())) {
                    EmployeeComplaintService.this.ce.setComp_date("null");
                    EmployeeComplaintService.this.ce.setLast_atnd_on("null");
                } else {
                    EmployeeComplaintService.this.ce.setComp_date(BahetiEnterprises.tStamp(EmployeeComplaintService.this.select_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    EmployeeComplaintService.this.ce.setLast_atnd_on(BahetiEnterprises.lasttStamp(EmployeeComplaintService.this.select_date.getText().toString()) + BuildConfig.VERSION_NAME);
                }
                EmployeeComplaintService.this.initViewModel(EmployeeComplaintService.this.ce);
            }
        });
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    private void initViewModel(ComplaintEntity complaintEntity) {
        subscribeToDataStreams(this.cnmplViewModel, complaintEntity);
    }

    private void subscribeToDataStreams(ComplaintsViewModel viewModel, ComplaintEntity complaintEntity) {
        try {
            this.sweetAlertDialog.show();
            viewModel.getComplaints(complaintEntity).observe(this, new Observer<List<ComplaintEntity>>() {
                public void onChanged(@Nullable List<ComplaintEntity> complaintEntitiesList) {
                    if (!complaintEntitiesList.isEmpty()) {
                        EmployeeComplaintService.this.sweetAlertDialog.dismiss();
                        EmployeeComplaintService.this.list_container.setVisibility(View.VISIBLE);
                        EmployeeComplaintService.this.no_result_fount.setVisibility(View.GONE);
                        EmployeeComplaintService.this.showCompListInUit(complaintEntitiesList);
                    } else if (complaintEntitiesList.size() == 0) {
                        EmployeeComplaintService.this.sweetAlertDialog.dismiss();
                        EmployeeComplaintService.this.list_container.setVisibility(View.GONE);
                        EmployeeComplaintService.this.no_result_fount.setVisibility(View.VISIBLE);
                    } else {
                        EmployeeComplaintService.this.sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        all_cb.setChecked(false);
        if (ce.getStatus() != null) {
            initViewModel(ce);
        }
    }

    private void showCompListInUit(List<ComplaintEntity> complaintEntityList) {
        this.complaintEntityList = complaintEntityList;
        int pend_cnt = getCont(complaintEntityList, getResources().getString(R.string.Pending));
        int on_hold_cnt = getCont(complaintEntityList, getResources().getString(R.string.On_Hold));
        int resolved_closed_cnt = getCont(complaintEntityList, getResources().getString(R.string.Resolved_Closed));
        int in_progress_cnt = getCont(complaintEntityList, getResources().getString(R.string.In_Progress));
        int all = ((pend_cnt + on_hold_cnt) + resolved_closed_cnt) + in_progress_cnt;
        this.pending.setText(getResources().getString(R.string.Pending) + " : " + pend_cnt);
        this.on_hold.setText(getResources().getString(R.string.On_Hold) + " : " + on_hold_cnt);
        this.resolved_closed.setText(getResources().getString(R.string.Resolved_Closed) + " : " + resolved_closed_cnt);
        this.in_progress.setText(getResources().getString(R.string.In_Progress) + " : " + in_progress_cnt);
        this.total.setText(getResources().getString(R.string.total) + " : " + all);
        if (this.from.equalsIgnoreCase("0")) {
            this.adapter = new ComplaintServiceAdapter(complaintEntityList, this, 0,all_cb);
        } else if (this.from.equalsIgnoreCase("1")) {
            this.adapter = new ComplaintServiceAdapter(complaintEntityList, this, 1,all_cb);
        } else {
            this.adapter = new ComplaintServiceAdapter(complaintEntityList, this, 2,all_cb);
        }
        this.complaint_recycler.setLayoutManager(new LinearLayoutManager(this));
        this.complaint_recycler.setAdapter(this.adapter);
        this.adapter.notifyDataSetChanged();
    }

    public void listeners() {
        this.complaint_recycler.addOnItemTouchListener(new RecyclerItemClickListener(this, new OnItemClickListener() {
            public void onItemClick(View view, final int position) {
                Button btn_details = (Button) view.findViewById(R.id.btn_details);
                ((Button) view.findViewById(R.id.btn_assign_transfer)).setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        final Dialog d = new Dialog(EmployeeComplaintService.this, R.style.Custom_Dialog);
                        d.getWindow().requestFeature(1);
                        d.setContentView(R.layout.assign_emp_complaint);
                        final Spinner empl_spinner = (Spinner) d.findViewById(R.id.employee_spinner);
                        final Spinner priority_spinner = (Spinner) d.findViewById(R.id.priority_spinner);


                        final Spinner paid_spinner = (Spinner) d.findViewById(R.id.paid_spinner);
                        final EditText edt_amount = (EditText) d.findViewById(R.id.edt_amount);
                        final TableRow amount_to_pay = (TableRow) d.findViewById(R.id.Amount_to_pay);

                        TableRow Paid_cont = (TableRow) d.findViewById(R.id.Paid_cont);
                        amount_to_pay.setVisibility(View.GONE);
                        Paid_cont.setVisibility(View.GONE);


                        EmployeeComplaintService.this.setToSpinner(EmployeeComplaintService.this.employeeEntityList, empl_spinner);
                        Button btn_assign_transfer1 = (Button) d.findViewById(R.id.btn_assign_transfer);
                        ((Button) d.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
                            public void onClick(View view) {
                                d.dismiss();
                            }
                        });
                        btn_assign_transfer1.setOnClickListener(new OnClickListener() {
                            public void onClick(View view) {
                                if (empl_spinner.getSelectedItemPosition() == 0) {
                                    BahetiEnterprises.retShowAlertDialod("Select Employee", EmployeeComplaintService.this);
                                    return;
                                }
                                if (paid_spinner.getSelectedItemPosition() == 0) {
                                    BahetiEnterprises.retShowAlertDialod("Select Paid Type", EmployeeComplaintService.this);
                                    return;
                                }

                                ComplaintEntity complaintEntity = new ComplaintEntity();
                                complaintEntity.setId(((ComplaintEntity) EmployeeComplaintService.this.complaintEntityList.get(position)).getId());
                                complaintEntity.setPriority(priority_spinner.getSelectedItem().toString());
                                complaintEntity.setAssigned_empid(((EmployeeEntity) EmployeeComplaintService.this.employeeEntityList.get(empl_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);

                                if (TextUtils.isEmpty(edt_amount.getText().toString())) {
                                    complaintEntity.setPaid_amt("0");
                                } else {
                                    complaintEntity.setPaid_amt(edt_amount.getText().toString());
                                }
                                EmployeeComplaintService.this.cnmplViewModel.assignedEmp(complaintEntity, 1).observe(EmployeeComplaintService.this, new Observer<List<Boolean>>() {
                                    public void onChanged(@Nullable List<Boolean> booleen) {
                                        if (!booleen.isEmpty()) {
                                            EmployeeComplaintService.this.sweetAlertDialog.dismiss();
                                            d.dismiss();
                                            EmployeeComplaintService.this.subscribeToDataStreams(EmployeeComplaintService.this.cnmplViewModel, EmployeeComplaintService.this.ce);
                                        } else if (booleen.size() == 0) {
                                            EmployeeComplaintService.this.sweetAlertDialog.dismiss();
                                            d.dismiss();
                                            EmployeeComplaintService.this.subscribeToDataStreams(EmployeeComplaintService.this.cnmplViewModel, EmployeeComplaintService.this.ce);
                                        } else {
                                            EmployeeComplaintService.this.sweetAlertDialog.show();
                                        }
                                    }
                                });
                            }
                        });
                        d.show();
                    }
                });
            }
        }));
    }

    public void initView() {
        this.ce = new ComplaintEntity();
        this.status_spinner = (Spinner) findViewById(R.id.status_spinner);
        this.service_spinner = (Spinner) findViewById(R.id.service_spinner);
        this.etd_cust_mobile_no = (EditText) findViewById(R.id.etd_cust_mobile_no);
        this.show = (Button) findViewById(R.id.show);
        this.complaint_recycler = (RecyclerView) findViewById(R.id.complaint_recycler);
        this.select_date = (TextView) findViewById(R.id.select_date);
        this.start_date = (TextView) findViewById(R.id.start_date);
        this.to_date = (TextView) findViewById(R.id.to_date);
        this.total = (TextView) findViewById(R.id.total);
        this.pending = (TextView) findViewById(R.id.pending);
        this.on_hold = (TextView) findViewById(R.id.on_hold);
        this.in_progress = (TextView) findViewById(R.id.in_progress);
        this.resolved_closed = (TextView) findViewById(R.id.resolved_closed);
        this.no_result_fount = (TextView) findViewById(R.id.no_result_fount);
        this.list_container = (LinearLayout) findViewById(R.id.list_container);
        all_cb = (CheckBox) findViewById(R.id.all_cb);
        this.list_container.setVisibility(View.GONE);
        Calendar c = Calendar.getInstance();
        this.mYear = c.get(Calendar.YEAR);
        this.mMonth = c.get(Calendar.MONTH);
        this.mDay = c.get(Calendar.DATE);
        this.sh = getSharedPreferences("log", 0);
        this.post = this.sh.getString("post", BuildConfig.VERSION_NAME);
        this.uid = this.sh.getString("uid", BuildConfig.VERSION_NAME);
        this.factory = new ComplaintFactory();
        this.cnmplViewModel = (ComplaintsViewModel) ViewModelProviders.of(this, this.factory).get(ComplaintsViewModel.class);
        this.sweetAlertDialog = new SweetAlertDialog(this, 5);
        this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
        this.sweetAlertDialog.setTitleText("Loading");
        this.sweetAlertDialog.setCancelable(false);
    }

    public void showDateView(View view) {
        this.date_expandble_view = (ExpandableRelativeLayout) findViewById(R.id.date_expandble_view);
        this.date_expandble_view.toggle();
    }

    public void showSelectDate(View view) {
        new DatePickerDialog(this, this.mSeleteDate, this.mYear, this.mMonth, this.mDay).show();
    }

    public void showStartDate(View view) {
        new DatePickerDialog(this, this.mStartDate, this.mYear, this.mMonth, this.mDay).show();
    }

    public void showToDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this.mToDate, this.mYear, this.mMonth, this.mDay);
        if (TextUtils.isEmpty(this.start_date.getText().toString())) {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        } else {
            datePickerDialog.getDatePicker().setMinDate(BahetiEnterprises.tStamp(this.start_date.getText().toString()));
        }
        datePickerDialog.show();
    }

    public void setEmployeeToSpinner() {
        try {
            EmployeeEntity ee = new EmployeeEntity();
            ee.setAssigned_mgr_type(this.post);
            ((EmployeeViewModel) ViewModelProviders.of(this, new EmployeeFactory()).get(EmployeeViewModel.class)).getEmps(ee).observe(this, new Observer<List<EmployeeEntity>>() {
                public void onChanged(@Nullable List<EmployeeEntity> employeeEntities) {
                    if (!employeeEntities.isEmpty()) {
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int getCont(List<ComplaintEntity> complaintEntityList, String status) {
        int cnt = 0;
        try {
            for (ComplaintEntity ce : complaintEntityList) {
                if (ce.getStatus().equalsIgnoreCase(status)) {
                    cnt++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cnt;
    }

    private void setToSpinner(List<EmployeeEntity> employeeEntities, Spinner employee_spinner1) {
        this.employeeEntityList = employeeEntities;
        List<String> catList = new ArrayList();
        catList.add("Select");
        try {
            for (EmployeeEntity employeeEntity : employeeEntities) {
                catList.add(employeeEntity.getEmp_nm());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        employee_spinner1.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }
}
