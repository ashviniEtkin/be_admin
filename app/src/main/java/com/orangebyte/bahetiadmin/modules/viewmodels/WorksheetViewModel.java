package com.orangebyte.bahetiadmin.modules.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.modules.entities.WorkSheetEntity;
import com.orangebyte.bahetiadmin.modules.repository.RemoteRepository;
import com.orangebyte.bahetiadmin.modules.repository.WorksheetRepository;
import com.orangebyte.bahetiadmin.modules.ui.AllContract;

import java.util.List;

/**
 * Created by Ashvini on 1/7/2018.
 */

public class WorksheetViewModel extends ViewModel implements AllContract.EmployeeWorksheetStream {


    private WorksheetRepository worksheetRepository;


    public WorksheetViewModel() {
        this(null);
    }

    public WorksheetViewModel(@Nullable WorksheetRepository worksheetRepository) {
        if (worksheetRepository != null) {
            this.worksheetRepository = worksheetRepository;
        }

    }


    @Override
    public 	LiveData<List<WorkSheetEntity>> getEmpWorksheet(WorkSheetEntity workSheetEntity) {
        onInstanceCreated();
        return worksheetObservable(workSheetEntity);
    }

    /* private LiveData<List<WorkSheetEntity>> subscribeToLocalData(final WorkSheetEntity workSheetEntity) {
         // Observe if/when database is created.
         return Transformations.switchMap(DatabaseCreator.getInstance().isDatabaseCreated(),
                 new Function<Boolean, LiveData<List<WorkSheetEntity>>>() {
                     @Override
                     public LiveData<List<WorkSheetEntity>> apply(Boolean isDbCreated) {
                         if (Boolean.TRUE.equals(isDbCreated)) {
                             onDatabaseCreated();
                             return subscribeProductObservable(workSheetEntity);
                         }
                         return null;
                     }
                 });

      // worksheetObservable(workSheetEntity)
     }
 */
    private LiveData<List<WorkSheetEntity>> worksheetObservable(WorkSheetEntity workSheetEntity) {
        return worksheetRepository.worksheetList(workSheetEntity);
    }


    private void onInstanceCreated() {

        try {
            if (worksheetRepository == null) {
                worksheetRepository = WorksheetRepository.getInstance(RemoteRepository.getInstance());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    /**
     * A creator is used to inject the product ID into the ViewModel
     * <p>
     * This creator is to showcase how to inject dependencies into ViewModels. It's not
     * actually necessary in this case, as the product ID can be passed in a public method.
     */
    public static class WorksheetFactory extends ViewModelProvider.NewInstanceFactory {

        WorksheetRepository wWorksheetRepository;


        public WorksheetFactory() {
            this(null);
        }


        public WorksheetFactory(@Nullable WorksheetRepository wWorksheetRepository) {
            this.wWorksheetRepository = wWorksheetRepository;

        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new WorksheetViewModel(wWorksheetRepository);
        }
    }


}
