package com.orangebyte.bahetiadmin.modules.admin;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telecom.Call;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.chrisbanes.photoview.PhotoView;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.expandableview.ExpandableRelativeLayout;
import com.orangebyte.bahetiadmin.modules.adapters.CallLogInstallAdapter;
import com.orangebyte.bahetiadmin.modules.employee.CallLogAction;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener.OnItemClickListener;
import com.orangebyte.bahetiadmin.modules.viewmodels.CallLogInstallViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.CallLogInstallViewModel.CallLogInstallFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels.Factory;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel.EmployeeFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel.SubCategoryFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import com.orangebyte.pdflibrary.PdfDocument;
import com.orangebyte.pdflibrary.viewRenderer.AbstractViewRenderer;
import com.squareup.picasso.Picasso;

import im.delight.android.webview.BuildConfig;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CallLogAndInstallationDemo extends AppCompatActivity implements LifecycleRegistryOwner {
    public static CheckBox all_cb;
    CallLogInstallAdapter adapter;
    List<SubCategoryEntity> allSubCat = new ArrayList();
    Button btn_assign;
    Button btn_new;
    Button btn_resolved;
    List<CallLogInstallationDemoEntity> callLogInstallationDemoEntities = new ArrayList();
    CallLogInstallationDemoEntity callLogInstallationDemoEntity = null;// new CallLogInstallationDemoEntity();
    RecyclerView calllog_install_recycler;
    LinearLayout calllog_oper_contnr;
    List<CategoryEntity> categoryEntityList = new ArrayList();
    Spinner category_spinner;
    String chk = "";
    ExpandableRelativeLayout date_expandble_view;
    List<EmployeeEntity> employeeEntityList = new ArrayList();
    Spinner employee_spinner;
    CallLogInstallFactory factory;
    TextView in_progress;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    LinearLayout list_container;
    int mDay;
    int mMonth;
    String nm = "";
    private OnDateSetListener mSeleteDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            select_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    private OnDateSetListener mStartDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            start_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    private OnDateSetListener mToDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            to_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    int mYear;
    TextView no_result_fount;
    TextView on_hold;
    TextView pending;
    String post = "";
    ImageButton reset;
    TextView resolved_closed;
    TextView select_date;
    SharedPreferences sh;
    Button show;
    TextView start_date;
    Spinner status_spinner;
    List<SubCategoryEntity> subCategoryEntityList = new ArrayList();
    TableRow sub_cat_container;
    Spinner subcategory_spinner;
    TextView to_date;
    TextView total;
    Spinner type_spinner;
    CallLogInstallViewModel viewModel;
    EditText etd_cust_mobile_no;
    Button btn_reset;

    RadioGroup assigned_grp;
    RadioButton assigned, unassigned;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_log_and_installation_demo);
        BahetiEnterprises.setActBar(getResources().getString(R.string.call_log_n_demo), this, false);
        this.chk = getIntent().getStringExtra(BahetiEnterprises.INTNT);
        init();
        setEmployeeToSpinner();
        this.category_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (allSubCat.isEmpty()) {
                    setSubCatgoryToSpinner();
                } else if (i > 0) {
                    setToSubCatSpinner(BahetiEnterprises.getIdFromCatgory(categoryEntityList, category_spinner.getSelectedItem().toString()) + "");
                    sub_cat_container.setVisibility(View.VISIBLE);
                } else {
                    sub_cat_container.setVisibility(View.GONE);
                }
                if (i == 0) {
                    sub_cat_container.setVisibility(View.GONE);
                } else {
                    sub_cat_container.setVisibility(View.VISIBLE);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.show.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                callLogInstallationDemoEntity = new CallLogInstallationDemoEntity();
                int selectedId = assigned_grp.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                RadioButton ass_uns = (RadioButton) findViewById(selectedId);

                if (status_spinner.getSelectedItemPosition() == 0) {
                    callLogInstallationDemoEntity.setStatus("null");
                } else {
                    callLogInstallationDemoEntity.setStatus(status_spinner.getSelectedItem().toString());
                }
                if (category_spinner.getSelectedItemPosition() == 0) {
                    callLogInstallationDemoEntity.setCat_id("null");
                } else {
                    try {
                        if (BahetiEnterprises.checkSpinner(category_spinner)) {
                            callLogInstallationDemoEntity.setCat_id("null");
                        } else {
                            callLogInstallationDemoEntity.setCat_id(((CategoryEntity) categoryEntityList.get(category_spinner.getSelectedItemPosition() - 1)).getId() + "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        callLogInstallationDemoEntity.setCat_id("null");
                    }
                }
                if (subcategory_spinner.getSelectedItemPosition() == 0) {
                    callLogInstallationDemoEntity.setSubcat_id("null");
                } else {
                    try {
                        if (BahetiEnterprises.checkSpinner(subcategory_spinner)) {
                            callLogInstallationDemoEntity.setSubcat_id("null");
                        } else {
                            callLogInstallationDemoEntity.setSubcat_id(((SubCategoryEntity) subCategoryEntityList.get(subcategory_spinner.getSelectedItemPosition() - 1)).getId() + "");
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        callLogInstallationDemoEntity.setSubcat_id("null");
                    }
                }
                if (type_spinner.getSelectedItemPosition() == 0) {
                    callLogInstallationDemoEntity.setType("null");
                } else {
                    callLogInstallationDemoEntity.setType(type_spinner.getSelectedItem().toString());
                }

                if (TextUtils.isEmpty(etd_cust_mobile_no.getText().toString())) {
                    callLogInstallationDemoEntity.setUser_id("null");
                } else {
                    callLogInstallationDemoEntity.setUser_id(etd_cust_mobile_no.getText().toString());
                }

                if (employee_spinner.getSelectedItemPosition() == 0) {
                    try {
                        if (ass_uns.getText().toString().equalsIgnoreCase(getString(R.string.assigned))) {
                            callLogInstallationDemoEntity.setEmp_id("yes");

                        } else if (ass_uns.getText().toString().equalsIgnoreCase(getString(R.string.unassigned))) {
                            callLogInstallationDemoEntity.setEmp_id("no");

                        } else {
                            callLogInstallationDemoEntity.setEmp_id("null");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        callLogInstallationDemoEntity.setEmp_id("null");
                    }
                } else {
                    try {
                        if (BahetiEnterprises.checkSpinner(employee_spinner)) {
                            callLogInstallationDemoEntity.setEmp_id("null");
                        } else {
                            callLogInstallationDemoEntity.setEmp_id(((EmployeeEntity) employeeEntityList.get(employee_spinner.getSelectedItemPosition() - 1)).getId() + "");
                        }
                    } catch (Exception e22) {
                        e22.printStackTrace();
                        callLogInstallationDemoEntity.setEmp_id("null");
                    }
                }
                if (!TextUtils.isEmpty(start_date.getText().toString()) && !TextUtils.isEmpty(to_date.getText().toString())) {
                    callLogInstallationDemoEntity.setDemodate(BahetiEnterprises.tStamp(start_date.getText().toString()) + "");
                    callLogInstallationDemoEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(to_date.getText().toString()) + "");
                } else if (!TextUtils.isEmpty(start_date.getText().toString()) && TextUtils.isEmpty(to_date.getText().toString())) {
                    callLogInstallationDemoEntity.setDemodate(BahetiEnterprises.tStamp(start_date.getText().toString()) + "");
                    callLogInstallationDemoEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(start_date.getText().toString()) + "");
                } else if (TextUtils.isEmpty(start_date.getText().toString()) && !TextUtils.isEmpty(to_date.getText().toString())) {
                    callLogInstallationDemoEntity.setDemodate(BahetiEnterprises.tStamp(to_date.getText().toString()) + "");
                    callLogInstallationDemoEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(to_date.getText().toString()) + "");
                } else if (TextUtils.isEmpty(select_date.getText().toString())) {
                    callLogInstallationDemoEntity.setDemodate("null");
                    callLogInstallationDemoEntity.setLast_atnd_on("null");
                } else {
                    callLogInstallationDemoEntity.setDemodate(BahetiEnterprises.tStamp(select_date.getText().toString()) + "");
                    callLogInstallationDemoEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(select_date.getText().toString()) + "");
                }
                initViewModel(callLogInstallationDemoEntity);
            }
        });

        listeners();
    }


    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    public void showDateView(View view) {
        this.date_expandble_view = (ExpandableRelativeLayout) findViewById(R.id.date_expandble_view);
        this.date_expandble_view.toggle();
    }

    public void showSelectDate(View view) {
        new DatePickerDialog(this, this.mSeleteDate, this.mYear, this.mMonth, this.mDay).show();
    }

    public void showStartDate(View view) {
        new DatePickerDialog(this, this.mStartDate, this.mYear, this.mMonth, this.mDay).show();
    }

    public void showToDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this.mToDate, this.mYear, this.mMonth, this.mDay);
        if (TextUtils.isEmpty(this.start_date.getText().toString())) {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        } else {
            datePickerDialog.getDatePicker().setMinDate(BahetiEnterprises.tStamp(this.start_date.getText().toString()));
        }
        datePickerDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ActivityCompat.checkSelfPermission(CallLogAndInstallationDemo.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(CallLogAndInstallationDemo.this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE
                    },
                    110);

        }


        all_cb.setChecked(false);
        if (callLogInstallationDemoEntity != null) {
            initViewModel(callLogInstallationDemoEntity);
        }
    }

    public void init() {

        assigned_grp = (RadioGroup) findViewById(R.id.assigned_grp);
        unassigned = (RadioButton) findViewById(R.id.unassigned);
        assigned = (RadioButton) findViewById(R.id.assigned);

        btn_reset = (Button) findViewById(R.id.btn_reset);

        btn_reset.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setEmployeeToSpinner();
                status_spinner.setSelection(0);
                type_spinner.setSelection(0);
                select_date.setText("");
                start_date.setText("");
                to_date.setText("");
                etd_cust_mobile_no.setText("");
                assigned.setChecked(false);
                unassigned.setChecked(false);

            }
        });

        this.etd_cust_mobile_no = (EditText) findViewById(R.id.etd_cust_mobile_no);

        this.no_result_fount = (TextView) findViewById(R.id.no_result_fount);
        this.show = (Button) findViewById(R.id.show);
        this.employee_spinner = (Spinner) findViewById(R.id.employee_spinner);
        this.category_spinner = (Spinner) findViewById(R.id.category_spinner);
        this.subcategory_spinner = (Spinner) findViewById(R.id.subcategory_spinner);
        this.status_spinner = (Spinner) findViewById(R.id.status_spinner);
        this.type_spinner = (Spinner) findViewById(R.id.type_spinner);
        this.calllog_install_recycler = (RecyclerView) findViewById(R.id.calllog_install_recycler);
        this.calllog_oper_contnr = (LinearLayout) findViewById(R.id.calllog_oper_contnr);
        all_cb = (CheckBox) findViewById(R.id.all_cb);
        this.btn_new = (Button) findViewById(R.id.btn_new);
        this.btn_assign = (Button) findViewById(R.id.btn_assign);
        this.btn_resolved = (Button) findViewById(R.id.btn_resolved);
        this.calllog_oper_contnr.setVisibility(View.GONE);
        this.sub_cat_container = (TableRow) findViewById(R.id.sub_cat_container);
        this.sub_cat_container.setVisibility(View.GONE);
        this.select_date = (TextView) findViewById(R.id.select_date);
        this.start_date = (TextView) findViewById(R.id.start_date);
        this.to_date = (TextView) findViewById(R.id.to_date);
        this.total = (TextView) findViewById(R.id.total);
        this.pending = (TextView) findViewById(R.id.pending);
        this.on_hold = (TextView) findViewById(R.id.on_hold);
        this.in_progress = (TextView) findViewById(R.id.in_progress);
        this.resolved_closed = (TextView) findViewById(R.id.resolved_closed);
        this.no_result_fount = (TextView) findViewById(R.id.no_result_fount);
        this.list_container = (LinearLayout) findViewById(R.id.list_container);
        this.reset = (ImageButton) findViewById(R.id.reset);
        this.sh = getSharedPreferences("log", 0);
        this.post = this.sh.getString("post", "");
        nm = sh.getString("unm", "");
        this.factory = new CallLogInstallFactory();
        this.viewModel = (CallLogInstallViewModel) ViewModelProviders.of(this, this.factory).get(CallLogInstallViewModel.class);
        Calendar c = Calendar.getInstance();
        this.mYear = c.get(Calendar.YEAR);
        this.mMonth = c.get(Calendar.MONTH);
        this.mDay = c.get(Calendar.DAY_OF_MONTH);
        this.reset.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                start_date.setText("");
                select_date.setText("");
                to_date.setText("");
            }
        });
        this.btn_new.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(CallLogAndInstallationDemo.this, AddInstalltionDemo.class));
            }
        });
        this.btn_assign.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (checkSelection()) {
                    final Dialog d = new Dialog(CallLogAndInstallationDemo.this, R.style.Custom_Dialog);
                    d.getWindow().requestFeature(1);
                    d.setContentView(R.layout.assign_emp_complaint);
                    final Spinner empl_spinner = (Spinner) d.findViewById(R.id.employee_spinner);
                    final Spinner priority_spinner = (Spinner) d.findViewById(R.id.priority_spinner);
                    Spinner paid_spinner = (Spinner) d.findViewById(R.id.paid_spinner);
                    final EditText edt_amount = (EditText) d.findViewById(R.id.edt_amount);
                    final TableRow amount_to_pay = (TableRow) d.findViewById(R.id.Amount_to_pay);
                    final TableRow paidCont = (TableRow) d.findViewById(R.id.Paid_cont);
                    paidCont.setVisibility(View.GONE);
                    amount_to_pay.setVisibility(View.GONE);
                    //((TableRow) d.findViewById(R.id.priority_cont)).setVisibility(View.GONE);
                    setToSpinner(employeeEntityList, empl_spinner);
                    Button btn_assign_transfer1 = (Button) d.findViewById(R.id.btn_assign_transfer);
                    ((Button) d.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            d.dismiss();
                        }
                    });

                   /* paid_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if (i == 1) {
                                amount_to_pay.setVisibility(View.VISIBLE);
                            } else {
                                amount_to_pay.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });*/
                    btn_assign_transfer1.setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            if (empl_spinner.getSelectedItemPosition() == 0) {
                                BahetiEnterprises.retShowAlertDialod("Please Select Employee", CallLogAndInstallationDemo.this);
                                return;
                            }

                            String allSelectedIds = getSelectedIds(getSelectedCallLog());
                            CallLogInstallationDemoEntity ce = new CallLogInstallationDemoEntity();
                            ce.setUser_id(allSelectedIds);
                            ce.setEmp_id(((EmployeeEntity) employeeEntityList.get(empl_spinner.getSelectedItemPosition() - 1)).getId() + "");
                            //ce.setPaid_amt(edt_amount.getText().toString());
                            ce.setPriority(priority_spinner.getSelectedItem().toString());
                            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(CallLogAndInstallationDemo.this, 5);
                            sweetAlertDialog.setTitle("Loading");
                            viewModel.changeCallLog(ce, 3).observe(CallLogAndInstallationDemo.this, new Observer<List<Boolean>>() {
                                public void onChanged(@Nullable List<Boolean> booleen) {
                                    if (!booleen.isEmpty()) {
                                        d.dismiss();
                                        sweetAlertDialog.dismiss();
                                        createJobCart(empl_spinner.getSelectedItem().toString());
                                        // initViewModel(callLogInstallationDemoEntity);
                                    } else if (booleen.size() == 0) {
                                        d.dismiss();
                                        sweetAlertDialog.dismiss();
                                    } else {
                                        sweetAlertDialog.show();
                                    }
                                }
                            });
                        }
                    });
                    d.show();
                    return;
                }
                BahetiEnterprises.retShowAlertDialod("Please Select", CallLogAndInstallationDemo.this);
            }
        });
        this.btn_resolved.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (checkSelection()) {
                    String allSelectedIds = getSelectedIds(getSelectedCallLog());
                    CallLogInstallationDemoEntity ce = new CallLogInstallationDemoEntity();
                    ce.setUser_id(allSelectedIds);
                    ce.setStatus(getResources().getString(R.string.Resolved_Closed));
                    resolvedOrAssigned(ce, 2);
                    return;
                }
                BahetiEnterprises.retShowAlertDialod("Please Select", CallLogAndInstallationDemo.this);
            }
        });
    }

    public void resolvedOrAssigned(CallLogInstallationDemoEntity callLogInstallationDemoEntity, int chk) {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, 5);
        sweetAlertDialog.setTitle("Loading");
        this.viewModel.changeCallLog(callLogInstallationDemoEntity, chk).observe(this, new Observer<List<Boolean>>() {
            public void onChanged(@Nullable List<Boolean> booleen) {
                if (!booleen.isEmpty()) {
                    sweetAlertDialog.dismiss();
                    onBackPressed();
                } else if (booleen.size() == 0) {
                    sweetAlertDialog.dismiss();
                } else {
                    sweetAlertDialog.show();
                }
            }
        });
    }

    public boolean checkSelection() {
        List<CallLogInstallationDemoEntity> installationDemoEntityList = this.adapter.getCallLogInstallationDemoEntities();
        boolean chk = false;
        int i = 0;
        while (i < installationDemoEntityList.size()) {
            if (((CallLogInstallationDemoEntity) installationDemoEntityList.get(i)).isSelected()) {
                chk = true;
                i = installationDemoEntityList.size();
            } else {
                chk = false;
                i++;
            }
        }
        return chk;
    }

    public List<CallLogInstallationDemoEntity> getSelectedCallLog() {
        List<CallLogInstallationDemoEntity> installationDemoEntityList = this.adapter.getCallLogInstallationDemoEntities();
        List<CallLogInstallationDemoEntity> newList = new ArrayList();
        for (int i = 0; i < installationDemoEntityList.size(); i++) {
            if (((CallLogInstallationDemoEntity) installationDemoEntityList.get(i)).isSelected()) {
                newList.add(installationDemoEntityList.get(i));
            }
        }
        return newList;
    }

    public String getSelectedIds(List<CallLogInstallationDemoEntity> newList) {
        String ids = "";
        for (int i = 0; i < newList.size(); i++) {
            ids = ids + ((CallLogInstallationDemoEntity) newList.get(i)).getId() + ",";
        }
        return ids.substring(0, ids.length() - 1);
    }

    public void listeners() {
        this.calllog_install_recycler.addOnItemTouchListener(new RecyclerItemClickListener(this, new OnItemClickListener() {
            public void onItemClick(View view, final int position) {
                ((Button) view.findViewById(R.id.btn_assign_transfer)).setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        final Dialog d = new Dialog(CallLogAndInstallationDemo.this, R.style.Custom_Dialog);
                        d.getWindow().requestFeature(1);
                        d.setContentView(R.layout.assign_emp_complaint);
                        final Spinner empl_spinner = (Spinner) d.findViewById(R.id.employee_spinner);
                        final Spinner priority_spinner = (Spinner) d.findViewById(R.id.priority_spinner);

                        // ((TableRow) d.findViewById(R.id.priority_cont)).setVisibility(View.GONE);

                        final Spinner paid_spinner = (Spinner) d.findViewById(R.id.paid_spinner);
                        final EditText edt_amount = (EditText) d.findViewById(R.id.edt_amount);
                        final TableRow amount_to_pay = (TableRow) d.findViewById(R.id.Amount_to_pay);
                        amount_to_pay.setVisibility(View.GONE);

                        TableRow Paid_cont = (TableRow) d.findViewById(R.id.Paid_cont);
                        Paid_cont.setVisibility(View.VISIBLE);

                        setToSpinner(employeeEntityList, empl_spinner);


                        try {
                            PhotoView invoice_url = (PhotoView) d.findViewById(R.id.invoice_url);
                            LinearLayout invoice_note_container = (LinearLayout) d.findViewById(R.id.invoice_note_container);
                            invoice_note_container.setVisibility(View.VISIBLE);
                            TextView invoice_note = (TextView) d.findViewById(R.id.invoice_note);
                            final ImageButton show_invoice = (ImageButton) d.findViewById(R.id.show_invoice);
                            final LinearLayout invoice_container = (LinearLayout) d.findViewById(R.id.invoice_container);

                            show_invoice.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (invoice_container.getVisibility() == View.VISIBLE) {
                                        show_invoice.setImageDrawable(getResources().getDrawable(R.drawable.ic_hide));
                                        invoice_container.setVisibility(View.GONE);
                                    } else {
                                        show_invoice.setImageDrawable(getResources().getDrawable(R.drawable.ic_unhide));
                                        invoice_container.setVisibility(View.VISIBLE);

                                    }
                                }
                            });

                            if (callLogInstallationDemoEntities.get(position).getPurchase_from().equalsIgnoreCase(getString(R.string.Baheti_Enterprises))) {
                                //   invoice_container.setVisibility(View.VISIBLE);
                                invoice_note.setText("This product Purchase From " + getString(R.string.Baheti_Enterprises));
                                show_invoice.setVisibility(View.VISIBLE);
                                Picasso.with(CallLogAndInstallationDemo.this).load(BahetiEnterprises.INVOICE_URL + callLogInstallationDemoEntities.get(position).getInvoice_url()).into(invoice_url);
                            } else {
                                //  invoice_container.setVisibility(View.GONE);
                                invoice_note.setText("This product Purchase From " + getString(R.string.Other));
                                show_invoice.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        paid_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i == 1) {
                                    amount_to_pay.setVisibility(View.VISIBLE);
                                } else {
                                    amount_to_pay.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                        Button btn_assign_transfer1 = (Button) d.findViewById(R.id.btn_assign_transfer);
                        ((Button) d.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
                            public void onClick(View view) {
                                d.dismiss();
                            }
                        });
                        btn_assign_transfer1.setOnClickListener(new OnClickListener() {
                            public void onClick(View view) {
                                if (empl_spinner.getSelectedItemPosition() == 0) {
                                    BahetiEnterprises.retShowAlertDialod("Select Employee", CallLogAndInstallationDemo.this);
                                    return;
                                }

                                if (paid_spinner.getSelectedItemPosition() == 0) {
                                    BahetiEnterprises.retShowAlertDialod("Select Paid Type", CallLogAndInstallationDemo.this);
                                    return;
                                }


                                CallLogInstallationDemoEntity callLogInstallationDemoEntity1 = new CallLogInstallationDemoEntity();
                                callLogInstallationDemoEntity1.setId(((CallLogInstallationDemoEntity) callLogInstallationDemoEntities.get(position)).getId());
                                callLogInstallationDemoEntity1.setPriority(priority_spinner.getSelectedItem().toString());
                                callLogInstallationDemoEntity1.setEmp_id(((EmployeeEntity) employeeEntityList.get(empl_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);
                                callLogInstallationDemoEntity1.setPaid_type(paid_spinner.getSelectedItem().toString());

                                if (TextUtils.isEmpty(edt_amount.getText().toString())) {
                                    callLogInstallationDemoEntity1.setPaid_amt("0");
                                } else {
                                    callLogInstallationDemoEntity1.setPaid_amt(edt_amount.getText().toString());
                                }


                                final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(CallLogAndInstallationDemo.this, 5);
                                sweetAlertDialog.setTitle("Loading");
                                viewModel.changeCallLog(callLogInstallationDemoEntity1, 4).observe(CallLogAndInstallationDemo.this, new Observer<List<Boolean>>() {
                                    public void onChanged(@Nullable List<Boolean> booleen) {
                                        if (!booleen.isEmpty()) {
                                            d.dismiss();
                                            sweetAlertDialog.dismiss();
                                            singleJobCart(callLogInstallationDemoEntities.get(position), empl_spinner.getSelectedItem().toString());
                                            initViewModel(callLogInstallationDemoEntity);
                                        } else if (booleen.size() == 0) {
                                            d.dismiss();
                                            sweetAlertDialog.dismiss();
                                        } else {
                                            sweetAlertDialog.show();
                                        }
                                    }
                                });
                            }
                        });
                        d.show();
                    }
                });
            }
        }));

    }


    public void setEmployeeToSpinner() {
        try {
            EmployeeEntity ee = new EmployeeEntity();
            ee.setAssigned_mgr_type(this.post);
            ((EmployeeViewModel) ViewModelProviders.of(this, new EmployeeFactory()).get(EmployeeViewModel.class)).getEmps(ee).observe(this, new Observer<List<EmployeeEntity>>() {
                public void onChanged(@Nullable List<EmployeeEntity> employeeEntities) {
                    if (!employeeEntities.isEmpty()) {
                        setToSpinner(employeeEntities, employee_spinner);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setToSpinner(List<EmployeeEntity> employeeEntities, Spinner employee_spinner1) {
        this.employeeEntityList = employeeEntities;
        List<String> catList = new ArrayList();
        catList.add("All");
        try {
            for (EmployeeEntity employeeEntity : employeeEntities) {
                catList.add(employeeEntity.getEmp_nm());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        employee_spinner1.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
        setCatgoryToSpinner();
    }

    private void setCatDataToSpinner(List<CategoryEntity> categoryEntities) {
        this.categoryEntityList = categoryEntities;
        List<String> catList = new ArrayList();
        catList.add("All");
        try {
            for (CategoryEntity categoryEntity : categoryEntities) {
                if (this.post.equalsIgnoreCase(getResources().getString(R.string.service_Manager))) {
                    if (categoryEntity.getAssign_to().equalsIgnoreCase("01") || categoryEntity.getAssign_to().equalsIgnoreCase("11")) {
                        catList.add(categoryEntity.getName());
                    }
                } else if (!this.post.equalsIgnoreCase(getResources().getString(R.string.Sales_Manager))) {
                    catList.add(categoryEntity.getName());
                } else if (categoryEntity.getAssign_to().equalsIgnoreCase("10") || categoryEntity.getAssign_to().equalsIgnoreCase("11")) {
                    catList.add(categoryEntity.getName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.category_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
        setSubCatgoryToSpinner();
    }

    private void setToSubCatSpinner(String catId) {
        subCategoryEntityList.clear();
        List<String> catList = new ArrayList();
        catList.add("All");
        try {
            for (SubCategoryEntity subCategoryEntity : this.allSubCat) {
                if (subCategoryEntity.getCat_id().equalsIgnoreCase(catId)) {
                    catList.add(subCategoryEntity.getSub_cat_name());
                    this.subCategoryEntityList.add(subCategoryEntity);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.subcategory_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    public void setSubCatgoryToSpinner() {
        try {
            ((SubCategoryViewModel) ViewModelProviders.of(this, new SubCategoryFactory()).get(SubCategoryViewModel.class)).getSubCats().observe(this, new Observer<List<SubCategoryEntity>>() {
                public void onChanged(@Nullable List<SubCategoryEntity> subCategoryEntities) {
                    if (!subCategoryEntities.isEmpty()) {
                        allSubCat = subCategoryEntities;
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setCatgoryToSpinner() {
        try {
            ((CategoryViewMdels) ViewModelProviders.of(this, new Factory()).get(CategoryViewMdels.class)).getCats().observe(this, new Observer<List<CategoryEntity>>() {
                public void onChanged(@Nullable List<CategoryEntity> categoryEntities) {
                    if (!categoryEntities.isEmpty()) {
                        setCatDataToSpinner(categoryEntities);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void initViewModel(CallLogInstallationDemoEntity callLogInstallationDemoEntity) {
        subscribeToDataStreams((CallLogInstallViewModel) ViewModelProviders.of(this, new CallLogInstallFactory()).get(CallLogInstallViewModel.class), callLogInstallationDemoEntity);
    }

    private void subscribeToDataStreams(CallLogInstallViewModel viewModel, CallLogInstallationDemoEntity callLogInstallationDemoEntity) {
        try {
            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, 5);
            sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
            sweetAlertDialog.setTitleText("Loading");
            sweetAlertDialog.setCancelable(false);
            sweetAlertDialog.show();
            viewModel.getCallLogInstallation(callLogInstallationDemoEntity).observe(this, new Observer<List<CallLogInstallationDemoEntity>>() {
                public void onChanged(@Nullable List<CallLogInstallationDemoEntity> offerEntityList) {
                    if (!offerEntityList.isEmpty()) {
                        sweetAlertDialog.dismiss();
                        showCompListInUit(offerEntityList);
                        if (chk.equalsIgnoreCase("1")) {
                            calllog_oper_contnr.setVisibility(View.VISIBLE);
                        } else {
                            calllog_oper_contnr.setVisibility(View.GONE);
                        }
                        list_container.setVisibility(View.VISIBLE);
                        no_result_fount.setVisibility(View.GONE);
                        calllog_install_recycler.setVisibility(View.VISIBLE);
                        list_container.setVisibility(View.VISIBLE);
                    } else if (offerEntityList.size() == 0) {
                        calllog_oper_contnr.setVisibility(View.GONE);
                        sweetAlertDialog.dismiss();
                        showCompListInUit(new ArrayList());
                        no_result_fount.setVisibility(View.VISIBLE);
                        list_container.setVisibility(View.GONE);
                        calllog_install_recycler.setVisibility(View.GONE);
                        list_container.setVisibility(View.GONE);
                    } else {
                        sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getCont(List<CallLogInstallationDemoEntity> callLogInstallationDemoEntityList2, String status) {
        int cnt = 0;
        try {
            for (CallLogInstallationDemoEntity ce : callLogInstallationDemoEntityList2) {
                if (ce.getStatus().equalsIgnoreCase(status)) {
                    cnt++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cnt;
    }

    private void showCompListInUit(List<CallLogInstallationDemoEntity> callLogInstallationDemoEntityList) {
        this.callLogInstallationDemoEntities = callLogInstallationDemoEntityList;
        int pend_cnt = getCont(callLogInstallationDemoEntityList, getResources().getString(R.string.Pending));
        int on_hold_cnt = getCont(callLogInstallationDemoEntityList, getResources().getString(R.string.On_Hold));
        int resolved_closed_cnt = getCont(callLogInstallationDemoEntityList, getResources().getString(R.string.Resolved_Closed));
        int in_progress_cnt = getCont(callLogInstallationDemoEntityList, getResources().getString(R.string.In_Progress));
        int all = ((pend_cnt + on_hold_cnt) + resolved_closed_cnt) + in_progress_cnt;
        this.pending.setText(getResources().getString(R.string.Pending) + " : " + pend_cnt);
        this.on_hold.setText(getResources().getString(R.string.On_Hold) + " : " + on_hold_cnt);
        this.resolved_closed.setText(getResources().getString(R.string.Resolved_Closed) + " : " + resolved_closed_cnt);
        this.in_progress.setText(getResources().getString(R.string.In_Progress) + " : " + in_progress_cnt);
        this.total.setText(getResources().getString(R.string.total) + " : " + all);
        if (this.chk.equalsIgnoreCase("1")) {
            this.adapter = new CallLogInstallAdapter(callLogInstallationDemoEntityList, this, 1, all_cb);
        } else if (this.chk.equalsIgnoreCase("2")) {
            this.adapter = new CallLogInstallAdapter(callLogInstallationDemoEntityList, this, 2, all_cb);
        } else {
            this.adapter = new CallLogInstallAdapter(callLogInstallationDemoEntityList, this, 0, all_cb);
        }
        this.calllog_install_recycler.setLayoutManager(new LinearLayoutManager(this));
        this.calllog_install_recycler.setAdapter(this.adapter);
        this.adapter.notifyDataSetChanged();
    }


    public void singleJobCart(CallLogInstallationDemoEntity callLogInstallationDemoEntity, String assigned_to) {


        AbstractViewRenderer abstractViewRenderer = BahetiEnterprises.getPdf(CallLogAndInstallationDemo.this, null
                , callLogInstallationDemoEntity, nm, assigned_to);


        try {

            long ts = System.currentTimeMillis();
            final String file_nm = callLogInstallationDemoEntity.getUsernm().replaceAll("\\s", "") + callLogInstallationDemoEntity.getMobile_no() + "_report_" + ts + "";
            final String filePathString = Environment.getExternalStorageDirectory() + "/BE/Reports/" + file_nm;

            final File f = new File(filePathString);
            if (f.exists() && !f.isDirectory()) {
                f.delete();
            }

            File folder = new File(Environment.getExternalStorageDirectory() +
                    File.separator + "BE" + File.separator + "Reports");
            boolean success = true;
            if (!folder.exists()) {
                success = folder.mkdirs();
            }

            createPDf(CallLogAndInstallationDemo.this, abstractViewRenderer, file_nm, folder);


        } catch (Exception exea) {
            exea.printStackTrace();
        }


    }

    public void createJobCart(String assigned_to) {
        //   List<AbstractViewRenderer> abstractViewRendererList = new ArrayList<>();
        final int[] i = {0};
        final List<CallLogInstallationDemoEntity> complaintEntityList = getSelectedCallLog();
        for (; i[0] < complaintEntityList.size();i[0]++ ) {
            AbstractViewRenderer abstractViewRenderer = BahetiEnterprises.getPdf(CallLogAndInstallationDemo.this, null
                    , complaintEntityList.get(i[0]), nm, assigned_to);
            //  abstractViewRendererList.add(abstractViewRenderer);


            try {

                long ts = System.currentTimeMillis();
                final String file_nm = complaintEntityList.get(i[0]).getUsernm().replaceAll("\\s", "") + complaintEntityList.get(i[0]).getMobile_no() + "_report_" + ts + "";
                final String filePathString = Environment.getExternalStorageDirectory() + "/BE/Reports/" + file_nm;

                final File f = new File(filePathString);
                if (f.exists() && !f.isDirectory()) {
                    f.delete();
                }

                File folder = new File(Environment.getExternalStorageDirectory() +
                        File.separator + "BE" + File.separator + "Reports");
                boolean success = true;
                if (!folder.exists()) {
                    success = folder.mkdirs();
                }

                //  createPDf(CallLogAndInstallationDemo.this, abstractViewRenderer, file_nm, folder);


                final PdfDocument doc = new PdfDocument(CallLogAndInstallationDemo.this);
                doc.addPage(abstractViewRenderer);


                doc.setOrientation(PdfDocument.A4_MODE.PORTRAIT);
                doc.setProgressTitle(R.string.app_name);
                doc.setProgressMessage(R.string.pdf_message);
                doc.setFileName(file_nm);
                doc.setSaveDirectory(folder);
                doc.setInflateOnMainThread(false);
                doc.setListener(new PdfDocument.Callback() {
                    @Override
                    public void onComplete(File file) {
                        Log.i(PdfDocument.TAG_PDF_MY_XML, "Complete");
                        doc.dismissPd();
                        if (i[0] == complaintEntityList.size()) {
                            onResume();
                        }
                        i[0]++;

                    }

                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        Log.i(PdfDocument.TAG_PDF_MY_XML, "Error");
                    }
                });

                doc.createPdf(CallLogAndInstallationDemo.this);


            } catch (Exception exea) {
                exea.printStackTrace();
            }

        }


    }


    public void createPDf(Context ctx, AbstractViewRenderer page, final String file_nm, final File folder) {
        PdfDocument doc = new PdfDocument(ctx);
        doc.addPage(page);


        doc.setOrientation(PdfDocument.A4_MODE.PORTRAIT);
        doc.setProgressTitle(R.string.app_name);
        doc.setProgressMessage(R.string.pdf_message);
        doc.setFileName(file_nm);
        doc.setSaveDirectory(folder);
        doc.setInflateOnMainThread(false);
        doc.setListener(new PdfDocument.Callback() {
            @Override
            public void onComplete(File file) {
                Log.i(PdfDocument.TAG_PDF_MY_XML, "Complete");


            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                Log.i(PdfDocument.TAG_PDF_MY_XML, "Error");
            }
        });

        doc.createPdf(ctx);


    }


}
