package com.orangebyte.bahetiadmin.modules.db.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.orangebyte.bahetiadmin.modules.entities.NotificationEntity;

import java.util.List;

@Dao
public interface NotificationDAO {
    @Delete
    void delete(NotificationEntity... notificationEntityArr);

    @Query("DELETE FROM notifications ")
    void deleteAll();

    @Delete
    void deleteAll(List<NotificationEntity> list);

    @Insert(onConflict = 1)
    void insert(NotificationEntity... notificationEntityArr);

    @Insert(onConflict = 1)
    void insertAll(List<NotificationEntity> list);

    @Query("SELECT * FROM notifications ")
    LiveData<List<NotificationEntity>> loadAll();

    @Update
    void update(NotificationEntity... notificationEntityArr);

    @Update
    void updateAll(List<NotificationEntity> list);
}
