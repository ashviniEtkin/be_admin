package com.orangebyte.bahetiadmin.modules.admin;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.adapters.OfferAdapter;
import com.orangebyte.bahetiadmin.modules.entities.OfferEntity;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener.OnItemClickListener;
import com.orangebyte.bahetiadmin.modules.viewmodels.OfferViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.OfferViewModel.OfferFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import com.orangebyte.bahetiadmin.views.CircleImageView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class OfferActivity extends AppCompatActivity implements LifecycleRegistryOwner {
    OfferAdapter adapter;
    boolean isTouched = false;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    TextView no_result_fount;
    List<OfferEntity> offerEntityList = new ArrayList();
    OfferViewModel offerViewModel;
    RecyclerView offers_list;
    SweetAlertDialog sweetAlertDialog;
    private Toolbar toolbarView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer);
        BahetiEnterprises.setActBar(getResources().getString(R.string.Manage_Offers), this, false);
        initViews();
        listeners();
    }

    public void listeners() {
        this.offers_list.addOnItemTouchListener(new RecyclerItemClickListener(this, new OnItemClickListener() {
            public void onItemClick(View view, final int position) {
                view.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        Intent offerIntent = new Intent(OfferActivity.this, EditOffer.class);
                        offerIntent.putExtra(BahetiEnterprises.FROM, (Serializable) offerEntityList.get(position));
                        startActivity(offerIntent);
                    }
                });
                ((ImageButton) view.findViewById(R.id.chng_status)).setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        OfferEntity offerEntity1 = (OfferEntity) offerEntityList.get(position);
                        if (((OfferEntity) offerEntityList.get(position)).getStatus().equalsIgnoreCase("1")) {
                            offerEntity1.setStatus("0");
                        } else {
                            offerEntity1.setStatus("1");
                        }
                        final SweetAlertDialog sweetAlertDialog2 = new SweetAlertDialog(OfferActivity.this);
                        sweetAlertDialog2.changeAlertType(5);
                        sweetAlertDialog2.setTitleText(getResources().getString(R.string.LOADING));
                        sweetAlertDialog2.show();
                        offerViewModel.updateOfferStatus(offerEntity1).observe(OfferActivity.this, new Observer<List<Boolean>>() {
                            public void onChanged(@Nullable List<Boolean> managerEntity) {
                                if (managerEntity.isEmpty()) {
                                    sweetAlertDialog2.show();
                                    return;
                                }
                                sweetAlertDialog2.dismiss();
                                if (((Boolean) managerEntity.get(0)).booleanValue()) {
                                    onResume();
                                } else {
                                    BahetiEnterprises.retShowAlertDialod("Only 6 Offer will be Visible", OfferActivity.this);
                                }
                            }
                        });
                        onResume();
                    }
                });
            }
        }));
    }

    protected void onResume() {
        super.onResume();
        OfferEntity of = new OfferEntity();
        of.setStatus("1");
        initViewModel(of);
    }

    private void initViews() {
        this.no_result_fount = (TextView) findViewById(R.id.no_result_fount);
        this.toolbarView = (Toolbar) findViewById(R.id.toolbar);
        this.sweetAlertDialog = new SweetAlertDialog(this);
        this.sweetAlertDialog.setCancelable(false);
        this.offers_list = (RecyclerView) findViewById(R.id.offers_list);
        if (VERSION.SDK_INT >= 9) {
            this.offers_list.setHasFixedSize(true);
        }
        if (VERSION.SDK_INT >= 9) {
            this.offers_list.setLayoutManager(new LinearLayoutManager(this));
        }
        this.adapter = new OfferAdapter(new ArrayList(), this);
        this.offers_list.setAdapter(this.adapter);
        ((CircleImageView) findViewById(R.id.fab)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(OfferActivity.this, AddOffer.class));
            }
        });
    }

    private void initViewModel(OfferEntity offerEntity) {
        this.offerViewModel = (OfferViewModel) ViewModelProviders.of(this, new OfferFactory()).get(OfferViewModel.class);
        subscribeToDataStreams(this.offerViewModel, offerEntity);
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    private void subscribeToDataStreams(OfferViewModel viewModel, OfferEntity offerEntity) {

        this.sweetAlertDialog = new SweetAlertDialog(this, 5);
        this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
        this.sweetAlertDialog.setTitleText("Loading");
        this.sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();
        try {

            viewModel.getOffer(offerEntity).observe(this, new Observer<List<OfferEntity>>() {
                public void onChanged(@Nullable List<OfferEntity> offerEntityList) {
                    if (!offerEntityList.isEmpty()) {
                        sweetAlertDialog.dismiss();
                        showOfferListInUit(offerEntityList);
                        offers_list.setVisibility(0);
                        no_result_fount.setVisibility(8);
                    } else if (offerEntityList.size() == 0) {
                        sweetAlertDialog.dismiss();
                        showOfferListInUit(new ArrayList());
                        offers_list.setVisibility(8);
                        no_result_fount.setVisibility(0);
                    } else {
                        sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception e) {
            sweetAlertDialog.dismiss();
            e.printStackTrace();
        }
    }

    private void showOfferListInUit(List<OfferEntity> offerEntityList) {
        this.offerEntityList = offerEntityList;
        this.adapter = new OfferAdapter(offerEntityList, this);
        this.offers_list.setAdapter(this.adapter);
        this.adapter.notifyDataSetChanged();
    }
}
