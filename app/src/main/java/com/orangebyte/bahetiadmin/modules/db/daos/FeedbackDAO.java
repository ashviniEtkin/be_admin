package com.orangebyte.bahetiadmin.modules.db.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.orangebyte.bahetiadmin.modules.entities.FeedbackEntity;

import java.util.List;

@Dao
public interface FeedbackDAO {
    @Delete
    void delete(FeedbackEntity... feedbackEntityArr);

    @Query("DELETE FROM feedback ")
    void deleteAll();

    @Delete
    void deleteAll(List<FeedbackEntity> list);

    @Insert(onConflict = 1)
    void insert(FeedbackEntity... feedbackEntityArr);

    @Insert(onConflict = 1)
    void insertAll(List<FeedbackEntity> list);

    @Query("SELECT * FROM feedback ")
    LiveData<List<FeedbackEntity>> loadAll();

    @Update
    void update(FeedbackEntity... feedbackEntityArr);

    @Update
    void updateAll(List<FeedbackEntity> list);
}
