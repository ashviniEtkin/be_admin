package com.orangebyte.bahetiadmin.modules.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.admin.ComplaintDetails;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ashvini on 4/15/2018.
 */


public class ComplaintIncentiveAdapter extends RecyclerView.Adapter<ComplaintIncentiveAdapter.ComplaintServiceHolder> {

    List<ComplaintEntity> complaintServiceEntityList;
    Context ctx;


    public List<ComplaintEntity> getComplaintServiceEntityList() {
        return complaintServiceEntityList;
    }

    public void setComplaintServiceEntityList(List<ComplaintEntity> complaintServiceEntityList) {
        this.complaintServiceEntityList = complaintServiceEntityList;
    }

    public boolean checkAll() {
        boolean a = false;
        int k = 0;
        while (k < this.getComplaintServiceEntityList().size()) {
            if (((ComplaintEntity) this.getComplaintServiceEntityList().get(k)).isSelected()) {
                a = true;
                k++;
            } else {
                a = false;
                k = this.complaintServiceEntityList.size();
            }
        }
        return a;
    }

    public void setAllCheck(boolean a) {
        for (int i = 0; i < this.complaintServiceEntityList.size(); i++) {
            ((ComplaintEntity) this.complaintServiceEntityList.get(i)).setSelected(a);
        }
        notifyDataSetChanged();
    }

    public class ComplaintServiceHolder extends RecyclerView.ViewHolder {
        TextView asign_to;
        Button btn_assign_transfer;
        Button btn_details;
        LinearLayout btn_layout;
        TextView comp_date;
        TextView comp_desc;
        TextView comp_status;
        CardView container_id;
        TextView cust_nm;
        TextView service_request_no, incentive;
        LinearLayout service_no_container, incentive_container;
        CheckBox checkb;

        public ComplaintServiceHolder(View itemView) {
            super(itemView);

            incentive_container = (LinearLayout) itemView.findViewById(R.id.incentive_container);
            incentive = (TextView) itemView.findViewById(R.id.incentive);
            incentive_container.setVisibility(View.VISIBLE);
            this.asign_to = (TextView) itemView.findViewById(R.id.asign_to);
            this.cust_nm = (TextView) itemView.findViewById(R.id.cust_nm);
            this.comp_desc = (TextView) itemView.findViewById(R.id.comp_desc);
            this.comp_date = (TextView) itemView.findViewById(R.id.comp_date);
            this.comp_status = (TextView) itemView.findViewById(R.id.comp_status);
            this.container_id = (CardView) itemView.findViewById(R.id.container_id);
            this.btn_layout = (LinearLayout) itemView.findViewById(R.id.btn_layout);
            this.btn_assign_transfer = (Button) itemView.findViewById(R.id.btn_assign_transfer);
            this.btn_details = (Button) itemView.findViewById(R.id.btn_details);
            this.checkb = (CheckBox) itemView.findViewById(R.id.checkb);
            service_no_container = (LinearLayout) itemView.findViewById(R.id.service_no_container);
            service_request_no = (TextView) itemView.findViewById(R.id.service_request_no);
            service_no_container.setVisibility(View.VISIBLE);
        }
    }

    public ComplaintIncentiveAdapter(List<ComplaintEntity> complaintServiceEntityList, Context ctx) {
        this.complaintServiceEntityList = complaintServiceEntityList;
        this.ctx = ctx;

    }

    public ComplaintServiceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ComplaintServiceHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.complaint_row, parent, false));
    }

    public void onBindViewHolder(ComplaintServiceHolder holder, final int position) {
        holder.incentive.setText(complaintServiceEntityList.get(position).getIncentive());
        holder.cust_nm.setText(((ComplaintEntity) this.complaintServiceEntityList.get(position)).getUsernm());
        holder.comp_desc.setText(((ComplaintEntity) this.complaintServiceEntityList.get(position)).getDesc());
        holder.comp_date.setText(BahetiEnterprises.retDate(((ComplaintEntity) this.complaintServiceEntityList.get(position)).getComp_date()));
        holder.comp_status.setText(((ComplaintEntity) this.complaintServiceEntityList.get(position)).getStatus());
        holder.service_request_no.setText(complaintServiceEntityList.get(position).getCom_no());

        BahetiEnterprises.setData(((ComplaintEntity) this.complaintServiceEntityList.get(position)).getEmp_nm(), holder.asign_to);
        if (((ComplaintEntity) this.complaintServiceEntityList.get(position)).getStatus().equalsIgnoreCase("Pending")) {
            if (BahetiEnterprises.showInRed(Long.parseLong(((ComplaintEntity) this.complaintServiceEntityList.get(position)).getComp_date()))) {
                holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.red_900));
            } else {
                holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.red_100));
            }
            holder.btn_assign_transfer.setVisibility(View.VISIBLE);
        } else if (((ComplaintEntity) this.complaintServiceEntityList.get(position)).getStatus().equalsIgnoreCase("On Hold")) {
            holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.green_100));
            holder.btn_assign_transfer.setVisibility(View.VISIBLE);
        } else if (((ComplaintEntity) this.complaintServiceEntityList.get(position)).getStatus().equalsIgnoreCase("In Progress")) {
            holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.teal_100));
            holder.btn_assign_transfer.setVisibility(View.VISIBLE);
        } else {
            holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.amber_200));
            holder.btn_assign_transfer.setVisibility(View.GONE);
        }

        holder.btn_layout.setVisibility(View.GONE);
        holder.checkb.setVisibility(View.GONE);


    }


    public int getItemCount() {
        return this.complaintServiceEntityList.size();
    }
}
