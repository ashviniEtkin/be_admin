package com.orangebyte.bahetiadmin.modules.viewmodels;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider.NewInstanceFactory;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.modules.db.local.AppDatabase;
import com.orangebyte.bahetiadmin.modules.db.local.DatabaseCreator;
import com.orangebyte.bahetiadmin.modules.entities.OfferEntity;
import com.orangebyte.bahetiadmin.modules.repository.OfferRepository;
import com.orangebyte.bahetiadmin.modules.repository.RemoteRepository;
import com.orangebyte.bahetiadmin.modules.ui.AllContract.OfferDataStreams;

import java.util.List;

import im.delight.android.webview.BuildConfig;

public class OfferViewModel extends ViewModel implements OfferDataStreams {
    private OfferRepository offerRepository;

    public static class OfferFactory extends NewInstanceFactory {
        OfferRepository mOfferRepository;

        public OfferFactory() {
            this(null, null);
        }

        public OfferFactory(AppDatabase appDatabase) {
            this(OfferRepository.getInstance(RemoteRepository.getInstance(), appDatabase.offerDAO()), BuildConfig.VERSION_NAME);
        }

        public OfferFactory(@Nullable OfferRepository mOfferRepository, @Nullable String abc) {
            this.mOfferRepository = mOfferRepository;
        }

        public <T extends ViewModel> T create(Class<T> cls) {
             return (T) new OfferViewModel(this.mOfferRepository);
        }
    }

    public OfferViewModel() {
        this(null);
    }

    public OfferViewModel(@Nullable OfferRepository offerRepository) {
        if (offerRepository != null) {
            this.offerRepository = offerRepository;
        }
    }

    public LiveData<List<OfferEntity>> getOffer(OfferEntity offerEntity) {
        return subscribeToLocalData(offerEntity);
    }

    public LiveData<List<OfferEntity>> addOffer(int chk, OfferEntity offerEntity, Uri fileUri) {
        onDatabaseCreated();
        return addOff(chk, offerEntity, fileUri);
    }

    public LiveData<List<Boolean>> updateOfferStatus(OfferEntity offerEntity) {
        onDatabaseCreated();
        return updtOffStatus(offerEntity);
    }

    private LiveData<List<OfferEntity>> subscribeToLocalData(final OfferEntity offerEntity) {
        return Transformations.switchMap(DatabaseCreator.getInstance().isDatabaseCreated(), new Function<Boolean, LiveData<List<OfferEntity>>>() {
            public LiveData<List<OfferEntity>> apply(Boolean isDbCreated) {
                if (!Boolean.TRUE.equals(isDbCreated)) {
                    return null;
                }
                OfferViewModel.this.onDatabaseCreated();
                return OfferViewModel.this.subscribeOfferObservable(offerEntity);
            }
        });
    }

    private LiveData<List<OfferEntity>> subscribeOfferObservable(OfferEntity offerEntity) {
        return this.offerRepository.loadOffersList(offerEntity);
    }

    private LiveData<List<OfferEntity>> addOff(int chk, OfferEntity offerEntity, Uri fileUri) {
        return this.offerRepository.addOffer(chk, offerEntity, fileUri);
    }

    private LiveData<List<Boolean>> updtOffStatus(OfferEntity offerEntity) {
        return this.offerRepository.updateOfferStatus(offerEntity);
    }

    private void onDatabaseCreated() {
        AppDatabase appDatabase = DatabaseCreator.getInstance().getDatabase();
        try {
            if (this.offerRepository == null) {
                this.offerRepository = OfferRepository.getInstance(RemoteRepository.getInstance(), appDatabase.offerDAO());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
