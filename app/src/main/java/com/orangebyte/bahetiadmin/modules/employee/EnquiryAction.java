package com.orangebyte.bahetiadmin.modules.employee;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.admin.EnquiryDetails;
import com.orangebyte.bahetiadmin.modules.entities.EnquiryEntity;
import com.orangebyte.bahetiadmin.modules.viewmodels.EnquiryViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.EnquiryViewModel.EnquiryFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import im.delight.android.webview.BuildConfig;

public class EnquiryAction extends AppCompatActivity implements LifecycleRegistryOwner {
    Spinner action_tk_txt;
    Button btn_send;
    EnquiryEntity ce;
    EnquiryViewModel cnmplViewModel;
    EnquiryFactory factory;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    int mDay;
    int mMonth;
    int mYear;
    TextView name;
    TextView select_date;
    Spinner status;
    SweetAlertDialog sweetAlertDialog;
    LinearLayout action_taken_container;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complait_action);
        BahetiEnterprises.setActBar(getResources().getString(R.string.change_status), this, false);

        init();
        this.ce = (EnquiryEntity) getIntent().getSerializableExtra(BahetiEnterprises.FROM);
        this.name.setText(this.ce.getUsernm());
        this.btn_send.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (EnquiryAction.this.status.getSelectedItemPosition() == 0) {
                    BahetiEnterprises.retShowAlertDialod("Please Status", EnquiryAction.this);
                } /*else if (TextUtils.isEmpty(EnquiryAction.this.action_tk_txt.getSelectedItem().toString())) {
                    BahetiEnterprises.retShowAlertDialod("Please Enter Action", EnquiryAction.this);
                }*/ else {
                    EnquiryAction.this.updateStatus();
                }
            }
        });

        status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 3) {

                    action_taken_container.setVisibility(View.VISIBLE);
                    List<String> actionTaken_resolved = Arrays.asList(getResources().getStringArray(R.array.action_taken_resolved));
                    ArrayAdapter adapter = new ArrayAdapter(EnquiryAction.this, R.layout.spinner_layout, actionTaken_resolved);
                    action_tk_txt.setAdapter(adapter);
                } else if (i == 1) {

                    action_taken_container.setVisibility(View.VISIBLE);
                    List<String> actionTaken_resolved = Arrays.asList(getResources().getStringArray(R.array.action_taken_on_hold));
                    ArrayAdapter adapter = new ArrayAdapter(EnquiryAction.this, R.layout.spinner_layout, actionTaken_resolved);
                    action_tk_txt.setAdapter(adapter);
                } else {
                    action_taken_container.setVisibility(View.GONE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void updateStatus() {
        EnquiryEntity complaintEntity = new EnquiryEntity();
        complaintEntity.setId(this.ce.getId());
        complaintEntity.setLast_atnd_on(BahetiEnterprises.tStamp(this.select_date.getText().toString()) + BuildConfig.VERSION_NAME);

        if (status.getSelectedItemPosition() == 2) {
            complaintEntity.setActn_tkn_st("-");

        } else {
            complaintEntity.setActn_tkn_st(this.action_tk_txt.getSelectedItem().toString());
        }
        complaintEntity.setStatus(this.status.getSelectedItem().toString());
        sweetAlertDialog.show();
        this.factory = new EnquiryFactory();
        this.cnmplViewModel = (EnquiryViewModel) ViewModelProviders.of(this, this.factory).get(EnquiryViewModel.class);
        this.cnmplViewModel.chngEnquiry(complaintEntity, 1).observe(this, new Observer<List<Boolean>>() {
            public void onChanged(@Nullable List<Boolean> booleen) {
                if (!booleen.isEmpty()) {
                    EnquiryAction.this.sweetAlertDialog.dismiss();
                    EnquiryAction.this.onBackPressed();
                } else if (booleen.size() == 0) {
                    EnquiryAction.this.sweetAlertDialog.dismiss();

                } else {
                    EnquiryAction.this.sweetAlertDialog.show();
                }
                onBackPressed();

                EnquiryDetails.chek_backpress = 1;
            }
        });
    }

    public void init() {
        this.select_date = (TextView) findViewById(R.id.select_date);
        this.status = (Spinner) findViewById(R.id.status);
        this.name = (TextView) findViewById(R.id.name);
        this.action_tk_txt = (Spinner) findViewById(R.id.action_tk_txt);
        this.btn_send = (Button) findViewById(R.id.btn_send);
        action_taken_container = (LinearLayout) findViewById(R.id.action_taken_container);

        Calendar c = Calendar.getInstance();
        this.mYear = c.get(1);
        this.mMonth = c.get(2);
        this.mDay = c.get(5);
        this.select_date.setText(this.mDay + "-" + (this.mMonth + 1) + "-" + this.mYear);
        this.factory = new EnquiryFactory();
        this.cnmplViewModel = (EnquiryViewModel) ViewModelProviders.of(this, this.factory).get(EnquiryViewModel.class);
        this.sweetAlertDialog = new SweetAlertDialog(this, 5);
        this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
        this.sweetAlertDialog.setTitleText("Loading");
        this.sweetAlertDialog.setCancelable(false);
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }
}
