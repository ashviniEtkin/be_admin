package com.orangebyte.bahetiadmin.modules.db.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import com.orangebyte.bahetiadmin.modules.entities.BrandEntity;
import java.util.List;

@Dao
public interface BrandDAO {
    @Delete
    void delete(BrandEntity... brandEntityArr);

    @Query("DELETE FROM brand ")
    void deleteAll();

    @Delete
    void deleteAll(List<BrandEntity> list);

    @Insert(onConflict = 1)
    void insert(BrandEntity... brandEntityArr);

    @Insert(onConflict = 1)
    void insertAll(List<BrandEntity> list);

    @Query("SELECT * FROM brand ")
    LiveData<List<BrandEntity>> loadAll();

    @Update
    void update(BrandEntity... brandEntityArr);

    @Update
    void updateAll(List<BrandEntity> list);
}
