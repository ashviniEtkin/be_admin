package com.orangebyte.bahetiadmin.modules.db.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.orangebyte.bahetiadmin.modules.entities.ProductEntity;

import java.util.List;

@Dao
public interface ProductDAO {
    @Delete
    void delete(ProductEntity... productEntityArr);

    @Query("DELETE FROM products ")
    void deleteAll();

    @Delete
    void deleteAll(List<ProductEntity> list);

    @Insert(onConflict = 1)
    void insert(ProductEntity... productEntityArr);

    @Insert(onConflict = 1)
    void insertAll(List<ProductEntity> list);

    @Query("SELECT * FROM products ")
    LiveData<List<ProductEntity>> loadAll();

    @Update
    void update(ProductEntity... productEntityArr);

    @Update
    void updateAll(List<ProductEntity> list);
}
