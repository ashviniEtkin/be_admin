package com.orangebyte.bahetiadmin.modules.models;

public interface OfferModel extends IdentifiableModel {
    String getBrand_id();

    String getCat_id();

    String getDiscount();

    String getOffer_type();

    String getProduct_id();

    String getStart_date();

    String getStatus();

    String getSub_cat_id();

    String getTitle();

    String getTo_date();
}
