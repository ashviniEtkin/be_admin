package com.orangebyte.bahetiadmin.modules.manager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.adapters.GridAdapters;
import com.orangebyte.bahetiadmin.modules.admin.AdminDashboard;
import com.orangebyte.bahetiadmin.modules.admin.CallLogAndInstallationDemo;
import com.orangebyte.bahetiadmin.modules.admin.ChangePassword;
import com.orangebyte.bahetiadmin.modules.admin.ComplaintServices;
import com.orangebyte.bahetiadmin.modules.admin.EMployeeActivity;
import com.orangebyte.bahetiadmin.modules.admin.NotiCenter;
import com.orangebyte.bahetiadmin.modules.admin.OfferActivity;
import com.orangebyte.bahetiadmin.modules.admin.ShowEnquiry;
import com.orangebyte.bahetiadmin.modules.admin.UpdateProfile;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener.OnItemClickListener;
import com.pdfjet.Table;
import com.pdfjet.TextAlign;

import im.delight.android.webview.BuildConfig;
import okhttp3.internal.platform.Platform;

public class ManagerDashboard extends AppCompatActivity {
    GridAdapters adapters;
    String can_edit = BuildConfig.VERSION_NAME;
    RecyclerView categories;
    String post = BuildConfig.VERSION_NAME;
    SharedPreferences sh;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_dashboard);
        init();
        listeners();
    }

    public void init() {
        this.categories = (RecyclerView) findViewById(R.id.categories);
        this.sh = getSharedPreferences("log", 0);
        this.post = this.sh.getString("post", BuildConfig.VERSION_NAME);
        this.can_edit = this.sh.getString("can_edit", BuildConfig.VERSION_NAME);
        ((TextView) findViewById(R.id.screen_nm)).setText(Html.fromHtml(getResources().getString(R.string.baheti_enter)));

        if (this.post.equalsIgnoreCase("Service Manager")) {
            if (this.can_edit.equalsIgnoreCase("1")) {
                this.adapters = new GridAdapters(this, new int[]{R.string.Employee, R.string.Complaints, R.string.Installation_Demo, R.string.noti_center, R.string.Offers,});
            } else {
                this.adapters = new GridAdapters(this, new int[]{R.string.Employee, R.string.Complaints, R.string.Installation_Demo, R.string.noti_center});
            }
        } else if (this.post.equalsIgnoreCase(getResources().getString(R.string.Store_Manager))) {
            if (this.can_edit.equalsIgnoreCase("1")) {
                this.adapters = new GridAdapters(this, new int[]{R.string.Employee, R.string.enquiry, R.string.Complaints, R.string.noti_center, R.string.Offers});
            } else {
                this.adapters = new GridAdapters(this, new int[]{R.string.Employee, R.string.enquiry, R.string.Complaints, R.string.noti_center});
            }
        } else {
            if (this.can_edit.equalsIgnoreCase("1")) {
                this.adapters = new GridAdapters(this, new int[]{R.string.Employee, R.string.Complaints, R.string.Installation_Demo, R.string.enquiry, R.string.noti_center, R.string.Offers});
            } else {
                this.adapters = new GridAdapters(this, new int[]{R.string.Employee, R.string.Complaints, R.string.Installation_Demo, R.string.enquiry, R.string.noti_center});
            }
        }
        this.categories.setLayoutManager(new LinearLayoutManager(this));
        this.categories.setAdapter(this.adapters);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
    }

    public void listeners() {
        this.categories.addOnItemTouchListener(new RecyclerItemClickListener(this, new OnItemClickListener() {
            public void onItemClick(View view, int position) {
                ManagerDashboard.this.intents(position);
            }
        }));
    }

    public void intents(int pos) {
        if (this.post.equalsIgnoreCase(getResources().getString(R.string.service_Manager))) {
            switch (pos) {
                case 0 /*0*/:
                    startActivity(new Intent(this, EMployeeActivity.class));
                    return;
               /* case 1 *//*1*//*:
                    startActivity(new Intent(this, CheckStatus.class));
                    return;*/
                case 1 /*2*/:
                    Intent ii = new Intent(this, ComplaintServices.class);
                    ii.putExtra(BahetiEnterprises.FROM, "1");
                    startActivity(ii);
                    return;
                case 2 /*3*/:
                    Intent call_intent = new Intent(this, CallLogAndInstallationDemo.class);
                    call_intent.putExtra(BahetiEnterprises.INTNT, "1");
                    startActivity(call_intent);
                    return;
                case 4 /*4*/:
                    startActivity(new Intent(this, OfferActivity.class));
                    return;

                case 3 /*5*/:
                    Intent noti_intent = new Intent(this, NotiCenter.class);
                    noti_intent.putExtra(BahetiEnterprises.FROM, "1");
                    startActivity(noti_intent);
                    return;
                default:
                    return;
            }
        } else if (this.post.equalsIgnoreCase(getResources().getString(R.string.Store_Manager))) {
            switch (pos) {
                case 0 /*0*/:
                    startActivity(new Intent(this, EMployeeActivity.class));
                    return;
               /* case 1 *//*1*//*:
                    startActivity(new Intent(this, CheckStatus.class));
                    return;*/
                case 1 /*2*/:
                    Intent ii_enq = new Intent(this, ShowEnquiry.class);
                    ii_enq.putExtra(BahetiEnterprises.FROM, "1");
                    startActivity(ii_enq);
                    return;
                case 2 /*3*/:
                    Intent ii = new Intent(this, ComplaintServices.class);
                    ii.putExtra(BahetiEnterprises.FROM, "1");
                    startActivity(ii);
                    return;
                case 4 /*4*/:
                    startActivity(new Intent(this, OfferActivity.class));
                    return;
                case 3 /*5*/:
                    Intent noti_intent = new Intent(this, NotiCenter.class);
                    noti_intent.putExtra(BahetiEnterprises.FROM, "1");
                    startActivity(noti_intent);
                    return;
                default:
                    return;
            }
        } else {

            switch (pos) {
                case 0 /*0*/:
                    startActivity(new Intent(this, EMployeeActivity.class));
                    return;
               /* case 1 *//*1*//*:
                    startActivity(new Intent(this, CheckStatus.class));
                    return;*/
                case 1 /*2*/:
                    Intent ii = new Intent(this, ComplaintServices.class);
                    ii.putExtra(BahetiEnterprises.FROM, "1");
                    startActivity(ii);
                    return;
                case 2 /*3*/:
                    Intent call_intent = new Intent(this, CallLogAndInstallationDemo.class);
                    call_intent.putExtra(BahetiEnterprises.INTNT, "1");
                    startActivity(call_intent);
                    return;
                case 3 /*3*/:
                    Intent ii_enq = new Intent(this, ShowEnquiry.class);
                    ii_enq.putExtra(BahetiEnterprises.FROM, "1");
                    startActivity(ii_enq);
                    return;
                case 5 /*4*/:
                    startActivity(new Intent(this, OfferActivity.class));
                    return;

                case 4 /*5*/:
                    Intent noti_intent = new Intent(this, NotiCenter.class);
                    noti_intent.putExtra(BahetiEnterprises.FROM, "1");
                    startActivity(noti_intent);
                    return;
                default:
                    return;
            }

        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        try {
            getWindow().getDecorView().setSystemUiVisibility(4);
        } catch (Exception e) {
            e.printStackTrace();
        }
        getMenuInflater().inflate(R.menu.admin_items, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout /*2131624289*/:
                BahetiEnterprises.logout(this);
                return true;
            case R.id.updt_profile /*2131624289*/:
                // BahetiEnterprises.logout(this);
                startActivity(new Intent(ManagerDashboard.this, UpdateProfile.class));
                return true;
            case R.id.chang_pswd /*2131624289*/:
                startActivity(new Intent(ManagerDashboard.this, ChangePassword.class));

                //  BahetiEnterprises.logout(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
