package com.orangebyte.bahetiadmin.modules.manager;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.expandableview.ExpandableRelativeLayout;
import com.orangebyte.bahetiadmin.modules.adapters.WorksheetAdapter;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.entities.WorkSheetEntity;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel.EmployeeFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.WorksheetViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.WorksheetViewModel.WorksheetFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import im.delight.android.webview.BuildConfig;

public class CheckStatus extends AppCompatActivity implements LifecycleRegistryOwner {
    WorksheetAdapter adapter;
    ExpandableRelativeLayout date_expandble_view;
    RecyclerView emp_worksheet_recycler;
    List<EmployeeEntity> employeeEntityList = new ArrayList();
    Spinner employee_spinner;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    int mDay;
    int mMonth;
    private OnDateSetListener mSeleteDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            CheckStatus.this.select_date.setText(new StringBuilder().append(day).append("-").append(month + 1)
                    .append("-").append(year));
        }
    };
    private OnDateSetListener mStartDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            CheckStatus.this.start_date.setText(new StringBuilder().append(day).append("-")
                    .append(month + 1).append("-").append(year));
        }
    };
    private OnDateSetListener mToDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            CheckStatus.this.to_date.setText(new StringBuilder().append(day).append("-").append(month + 1)
                    .append("-").append(year));
        }
    };
    int mYear;
    String post = BuildConfig.VERSION_NAME;
    TextView select_date;
    SharedPreferences sh;
    Button show;
    TextView start_date;
    TextView to_date;
    List<WorkSheetEntity> workSheetEntityList = new ArrayList();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_status);
        BahetiEnterprises.setActBar(getResources().getString(R.string.Employee_Working_Status), this, false);
        initView();
        setEmployeeToSpinner();
        this.show.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                WorkSheetEntity workSheetEntity = new WorkSheetEntity();
                if (!TextUtils.isEmpty(CheckStatus.this.start_date.getText().toString()) && !TextUtils.isEmpty(CheckStatus.this.to_date.getText().toString())) {
                    workSheetEntity.setOndate(BahetiEnterprises.tStamp(CheckStatus.this.start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    workSheetEntity.setResolved_date(BahetiEnterprises.lasttStamp(CheckStatus.this.to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                } else if (!TextUtils.isEmpty(CheckStatus.this.start_date.getText().toString()) && TextUtils.isEmpty(CheckStatus.this.to_date.getText().toString())) {
                    workSheetEntity.setOndate(BahetiEnterprises.tStamp(CheckStatus.this.start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    workSheetEntity.setResolved_date(BahetiEnterprises.lasttStamp(CheckStatus.this.start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                } else if (!TextUtils.isEmpty(CheckStatus.this.start_date.getText().toString()) || TextUtils.isEmpty(CheckStatus.this.to_date.getText().toString())) {
                    workSheetEntity.setOndate(BahetiEnterprises.tStamp(CheckStatus.this.select_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    workSheetEntity.setResolved_date(BahetiEnterprises.lasttStamp(CheckStatus.this.select_date.getText().toString()) + BuildConfig.VERSION_NAME);
                } else {
                    workSheetEntity.setOndate(BahetiEnterprises.tStamp(CheckStatus.this.to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    workSheetEntity.setResolved_date(BahetiEnterprises.lasttStamp(CheckStatus.this.to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                }
                if (BahetiEnterprises.checkSpinner(CheckStatus.this.employee_spinner)) {
                    workSheetEntity.setEmp_id("null");
                } else {
                    workSheetEntity.setEmp_id(((EmployeeEntity) CheckStatus.this.employeeEntityList.get(CheckStatus.this.employee_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
                }
                CheckStatus.this.initViewModel(workSheetEntity);
            }
        });
    }

    public void initView() {
        this.employee_spinner = (Spinner) findViewById(R.id.employee_spinner);
        this.show = (Button) findViewById(R.id.show);
        this.emp_worksheet_recycler = (RecyclerView) findViewById(R.id.emp_worksheet_recycler);
        this.select_date = (TextView) findViewById(R.id.select_date);
        this.start_date = (TextView) findViewById(R.id.start_date);
        this.to_date = (TextView) findViewById(R.id.to_date);
        this.adapter = new WorksheetAdapter(this);
        this.emp_worksheet_recycler.setLayoutManager(new LinearLayoutManager(this));
        this.emp_worksheet_recycler.setAdapter(this.adapter);
        Calendar c = Calendar.getInstance();
        this.mYear = c.get(1);
        this.mMonth = c.get(2);
        this.mDay = c.get(5);
        this.sh = getSharedPreferences("log", 0);
        this.post = this.sh.getString("post", BuildConfig.VERSION_NAME);
    }

    public void showDateView(View view) {
        this.date_expandble_view = (ExpandableRelativeLayout) findViewById(R.id.date_expandble_view);
        this.date_expandble_view.toggle();
    }

    public void showSelectDate(View view) {
        new DatePickerDialog(this, this.mSeleteDate, this.mYear, this.mMonth, this.mDay).show();
    }

    public void showStartDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this.mStartDate, this.mYear, this.mMonth, this.mDay);
     //   datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    public void showToDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this.mToDate, this.mYear, this.mMonth, this.mDay);
       // datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());

        if (TextUtils.isEmpty(this.start_date.getText().toString())) {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        } else {
            datePickerDialog.getDatePicker().setMinDate(BahetiEnterprises.tStamp(this.start_date.getText().toString()));
        }

        datePickerDialog.show();
    }

    private void initViewModel(WorkSheetEntity workSheetEntity) {
        getWsheet((WorksheetViewModel) ViewModelProviders.of(this, new WorksheetFactory()).get(WorksheetViewModel.class), workSheetEntity);
    }

    public void setEmployeeToSpinner() {
        try {
            EmployeeEntity ee = new EmployeeEntity();
            ee.setAssigned_mgr_type(this.post);
            ((EmployeeViewModel) ViewModelProviders.of(this, new EmployeeFactory()).get(EmployeeViewModel.class)).getEmps(ee).observe(this, new Observer<List<EmployeeEntity>>() {
                public void onChanged(@Nullable List<EmployeeEntity> workShetEntities) {
                    if (!workShetEntities.isEmpty()) {
                        CheckStatus.this.setToSpinner(workShetEntities);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setToSpinner(List<EmployeeEntity> workShetEntities) {
        this.employeeEntityList = workShetEntities;
        List<String> catList = new ArrayList();
        try {
            for (EmployeeEntity employeeEntity : workShetEntities) {
                catList.add(employeeEntity.getEmp_nm());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.employee_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    private void getWsheet(WorksheetViewModel viewModel, WorkSheetEntity workSheetEntity) {
        try {
            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, 5);
            sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
            sweetAlertDialog.setTitleText("Loading");
            sweetAlertDialog.setCancelable(false);
            viewModel.getEmpWorksheet(workSheetEntity).observe(this, new Observer<List<WorkSheetEntity>>() {
                public void onChanged(@Nullable List<WorkSheetEntity> workShetEntities) {
                    if (!workShetEntities.isEmpty()) {
                        CheckStatus.this.showWorkList(workShetEntities);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showWorkList(List<WorkSheetEntity> workSheetEntityList) {
        this.adapter.setItems(workSheetEntityList);
        this.adapter.notifyDataSetChanged();
    }
}
