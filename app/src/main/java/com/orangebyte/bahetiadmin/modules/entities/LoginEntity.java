package com.orangebyte.bahetiadmin.modules.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginEntity implements Serializable {
    @SerializedName("assigned_mgr_type")
    String assigned_mgr_type;
    @SerializedName("can_edit")
    String can_edit;
    @SerializedName("mgr_id")
    String mgr_id;
    @SerializedName("mgrtype")
    String mgrtype;
    @SerializedName("post")
    String post;
    @SerializedName("uid")
    String uid;
    @SerializedName("umobileno")
    String umobileno;
    @SerializedName("password")
    String password;
    @SerializedName("uname")
    String uname;
    @SerializedName("ustatus")
    String ustatus;


    public LoginEntity() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCan_edit() {
        return this.can_edit;
    }

    public void setCan_edit(String can_edit) {
        this.can_edit = can_edit;
    }

    public String getUid() {
        return this.uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUname() {
        return this.uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUmobileno() {
        return this.umobileno;
    }

    public void setUmobileno(String umobileno) {
        this.umobileno = umobileno;
    }

    public String getUstatus() {
        return this.ustatus;
    }

    public void setUstatus(String ustatus) {
        this.ustatus = ustatus;
    }

    public String getMgrtype() {
        return this.mgrtype;
    }

    public void setMgrtype(String mgrtype) {
        this.mgrtype = mgrtype;
    }

    public String getMgr_id() {
        return this.mgr_id;
    }

    public void setMgr_id(String mgr_id) {
        this.mgr_id = mgr_id;
    }

    public String getAssigned_mgr_type() {
        return this.assigned_mgr_type;
    }

    public void setAssigned_mgr_type(String assigned_mgr_type) {
        this.assigned_mgr_type = assigned_mgr_type;
    }

    public String getPost() {
        return this.post;
    }

    public void setPost(String post) {
        this.post = post;
    }
}
