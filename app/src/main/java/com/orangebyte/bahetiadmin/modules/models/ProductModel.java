package com.orangebyte.bahetiadmin.modules.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductModel implements Serializable {
    @SerializedName("brand_id")
    private String brand_id;
    @SerializedName("brand_nm")
    private String brand_nm;
    @SerializedName("cat_id")
    private String cat_id;
    @SerializedName("cat_nm")
    private String cat_nm;
    @SerializedName("features")
    private String features;
    @SerializedName("hide")
    private String hide;
    @SerializedName("id")
    private String id;
    @SerializedName("image1")
    private String image1;
    @SerializedName("image2")
    private String image2;
    @SerializedName("image3")
    private String image3;
    @SerializedName("image4")
    private String image4;
    @SerializedName("image5")
    private String image5;
    @SerializedName("model_no")
    private String model_no;
    @SerializedName("price")
    private String price;
    @SerializedName("status")
    private String status;
    @SerializedName("sub_cat_id")
    private String sub_cat_id;
    @SerializedName("sub_cat_name")
    private String sub_cat_name;
    @SerializedName("title")
    private String title;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCat_id() {
        return this.cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getSub_cat_id() {
        return this.sub_cat_id;
    }

    public void setSub_cat_id(String sub_cat_id) {
        this.sub_cat_id = sub_cat_id;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBrand_id() {
        return this.brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getModel_no() {
        return this.model_no;
    }

    public void setModel_no(String model_no) {
        this.model_no = model_no;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getFeatures() {
        return this.features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getImage1() {
        return this.image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return this.image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return this.image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return this.image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getImage5() {
        return this.image5;
    }

    public void setImage5(String image5) {
        this.image5 = image5;
    }

    public String getBrand_nm() {
        return this.brand_nm;
    }

    public void setBrand_nm(String brand_nm) {
        this.brand_nm = brand_nm;
    }

    public String getCat_nm() {
        return this.cat_nm;
    }

    public void setCat_nm(String cat_nm) {
        this.cat_nm = cat_nm;
    }

    public String getSub_cat_name() {
        return this.sub_cat_name;
    }

    public void setSub_cat_name(String sub_cat_name) {
        this.sub_cat_name = sub_cat_name;
    }

    public String getHide() {
        return this.hide;
    }

    public void setHide(String hide) {
        this.hide = hide;
    }
}
