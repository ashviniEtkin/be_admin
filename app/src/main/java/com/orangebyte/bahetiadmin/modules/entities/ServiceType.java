package com.orangebyte.bahetiadmin.modules.entities;

public class ServiceType {
    String typenm;
    boolean selected = false;


    public String getTypenm() {
        return typenm;
    }

    public void setTypenm(String typenm) {
        this.typenm = typenm;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}