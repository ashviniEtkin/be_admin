package com.orangebyte.bahetiadmin.modules.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "category")
public class CategoryEntity extends IdentifiableEntity implements Serializable {
    @SerializedName("assign_to")
    @ColumnInfo(name = "assign_to")
    private String assign_to;
    @SerializedName("cat_nm")
    @ColumnInfo(name = "cat_nm")
    private String name;
    @SerializedName("status")
    @ColumnInfo(name = "status")
    private String status;

    public /* bridge */ /* synthetic */ void setId(long j) {
        super.setId(j);
    }

    public String getAssign_to() {
        return this.assign_to;
    }

    public String getStatus() {
        return this.status;
    }

    public void setAssign_to(String assign_to) {
        this.assign_to = assign_to;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Ignore
    public CategoryEntity(long id, String name, String assign_to, String status) {
        this.id = id;
        this.name = name;
        this.assign_to = assign_to;
        this.status = status;
    }

    public CategoryEntity(){

    }
    public String getName() {
        return this.name;
    }

    public long getId() {
        return this.id;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CategoryEntity)) {
            return false;
        }
        CategoryEntity that = (CategoryEntity) o;
        if (this.name != null) {
            return this.name.equals(that.name);
        }
        if (that.name != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (int) (this.id ^ (this.id >>> 32));
    }
}
