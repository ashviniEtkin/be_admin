package com.orangebyte.bahetiadmin.modules.repository;

import com.pdfjet.Single;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class RemoteCallback<T> implements Callback<T> {
    public abstract void onFailed(Throwable th);

    public abstract void onSuccess(T t);

    public abstract void onUnauthorized();

    public final void onResponse(Call<T> call, Response<T> response) {
        switch (response.code()) {
            case 200:
            case 201:
            case 202:
            case 203:
                if (response.body() != null) {
                    onSuccess(response.body());
                    return;
                }
                return;
            case 401:
                onUnauthorized();
                return;
            default:
                onFailed(new Throwable("Default " + response.code() + Single.space + response.message()));
                return;
        }
    }

    public final void onFailure(Call<T> call, Throwable t) {
        onFailed(t);
    }
}
