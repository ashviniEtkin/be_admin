package com.orangebyte.bahetiadmin.modules.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

import com.google.gson.annotations.SerializedName;
import com.orangebyte.bahetiadmin.modules.models.SubCategoryModel;

import java.io.Serializable;

@Entity(tableName = "sub_category")
public class SubCategoryEntity extends IdentifiableEntity implements SubCategoryModel, Serializable {
    @SerializedName("cat_id")
    @ColumnInfo(name = "cat_id")
    private String cat_id;
    @SerializedName("status")
    @ColumnInfo(name = "status")
    private String status;
    @SerializedName("sub_cat_name")
    @ColumnInfo(name = "sub_cat_name")
    private String sub_cat_name;

    public /* bridge */ /* synthetic */ long getId() {
        return super.getId();
    }

    public /* bridge */ /* synthetic */ void setId(long j) {
        super.setId(j);
    }

    public String getSub_cat_name() {
        return this.sub_cat_name;
    }

    public String getCat_id() {
        return this.cat_id;
    }

    @Ignore
    public SubCategoryEntity(long id, String sub_cat_name, String cat_id, String status) {
        this.id = id;
        this.sub_cat_name = sub_cat_name;
        this.cat_id = cat_id;
        this.status = status;
    }

    @Ignore
    public SubCategoryEntity(SubCategoryModel scm) {
        this.id = scm.getId();
        this.sub_cat_name = scm.getSubCatName();
        this.cat_id = scm.getCatId();
        this.status = scm.getStatus();
    }

    public SubCategoryEntity() {
    }

    public void setSub_cat_name(String sub_cat_name) {
        this.sub_cat_name = sub_cat_name;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubCatName() {
        return this.sub_cat_name;
    }

    public String getCatId() {
        return this.cat_id;
    }

    public String getStatus() {
        return this.status;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CategoryEntity)) {
            return false;
        }
        SubCategoryEntity that = (SubCategoryEntity) o;
        if (this.sub_cat_name != null) {
            return this.sub_cat_name.equals(that.sub_cat_name);
        }
        if (that.sub_cat_name != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (int) (this.id ^ (this.id >>> 32));
    }
}
