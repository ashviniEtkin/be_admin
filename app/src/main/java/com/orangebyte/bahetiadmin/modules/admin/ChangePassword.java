package com.orangebyte.bahetiadmin.modules.admin;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.entities.AdminEntity;
import com.orangebyte.bahetiadmin.modules.models.ServicesResponse;
import com.orangebyte.bahetiadmin.modules.repository.ServiceFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePassword extends AppCompatActivity {

    TextView username, design, mobile_number;
    EditText password;
    Button update;
    SharedPreferences sh;
    String post = "";
    String uid = "";
    String mobile = "";
    String unm = "";
    String password1 = "";

    String old_pswd = "";

    LinearLayout password_container;
    EditText pswd, conf_pswd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        BahetiEnterprises.setActBar(getResources().getString(R.string.change_password), this, false);

        init();
    }

    public void init() {
        username = (TextView) findViewById(R.id.username);
        design = (TextView) findViewById(R.id.design);
        mobile_number = (TextView) findViewById(R.id.mobile_number);
        password = (EditText) findViewById(R.id.password);
        update = (Button) findViewById(R.id.update);
        pswd = (EditText) findViewById(R.id.pswd);
        conf_pswd = (EditText) findViewById(R.id.conf_pswd);
        password_container = (LinearLayout) findViewById(R.id.password_container);

        this.sh = getSharedPreferences("log", 0);
        this.post = this.sh.getString("post", "");
        mobile = sh.getString("mobile", "");
        uid = sh.getString("uid", "");
        unm = sh.getString("unm", "");
        password1 = sh.getString("password", "");
        try {
            mobile_number.setText(mobile);
            design.setText(post);
            username.setText(unm);
            old_pswd = password1;
        } catch (Exception e) {
            e.printStackTrace();
        }

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {


                if (password.getText().toString().length() == old_pswd.length()) {

                    if (password.getText().toString().equals(old_pswd)) {
                        password_container.setVisibility(View.VISIBLE);

                    } else {
                        password_container.setVisibility(View.GONE);
                    }

                } else {
                    password_container.setVisibility(View.GONE);
                   // password.setError(getString(R.string.current_password));

                }

            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!old_pswd.equals(password.getText().toString())) {
                    BahetiEnterprises.retShowAlertDialod(getString(R.string.current_password), ChangePassword.this);

                } else if (TextUtils.isEmpty(pswd.getText().toString())) {
                    BahetiEnterprises.retShowAlertDialod("Please Enter New password", ChangePassword.this);

                } else if (TextUtils.isEmpty(conf_pswd.getText().toString())) {
                    BahetiEnterprises.retShowAlertDialod("Please Enter Confirm Password", ChangePassword.this);

                } else if (!(pswd.getText().toString().equals(conf_pswd.getText().toString()))) {
                    BahetiEnterprises.retShowAlertDialod("Please Password Doesn't Match", ChangePassword.this);

                } else {

                    AdminEntity adminEntity = new AdminEntity();
                    adminEntity.setId(Long.parseLong(uid));
                    adminEntity.setMobile_no(mobile_number.getText().toString());
                    adminEntity.setPost(post);
                    adminEntity.setPassword(conf_pswd.getText().toString());
                    updatePassword(adminEntity);
                }
            }
        });


    }


    public void updatePassword(AdminEntity adminEntity) {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.getProgressHelper().setBarColor((getResources().getColor(R.color.app_color)));
        sweetAlertDialog.setTitleText("Please Wait");
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();
        try {
            //   RequestBody body = RequestBody.create(MediaType.parse("application/json"), adminEntity.toString());


            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/updateProfile/")
                    .updatePassword(adminEntity)
                    .enqueue(new Callback<ServicesResponse>() {
                        public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                            boolean t = (Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                            sh.edit().putString("password", mobile_number.getText().toString()).commit();
                            Toast.makeText(ChangePassword.this, "Chnaged Successfully", Toast.LENGTH_LONG).show();
                            init();
                            sweetAlertDialog.dismiss();
                        }

                        public void onFailure(Call<ServicesResponse> call, Throwable t) {
                            t.printStackTrace();
                            sweetAlertDialog.dismiss();
                        }
                    });
        } catch (Exception e) {
            sweetAlertDialog.dismiss();
            e.printStackTrace();
        }
    }
}
