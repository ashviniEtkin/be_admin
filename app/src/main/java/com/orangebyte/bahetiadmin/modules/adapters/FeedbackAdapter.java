package com.orangebyte.bahetiadmin.modules.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.admin.FeedbackDetails;
import com.orangebyte.bahetiadmin.modules.entities.FeedbackEntity;
import java.io.Serializable;
import java.util.List;

public class FeedbackAdapter extends Adapter<FeedbackAdapter.FeedbackHolder> {
    Context ctx;
    List<FeedbackEntity> feedbackEntity;

    public class FeedbackHolder extends ViewHolder {
        TextView comp_date;
        TextView comp_desc;
        LinearLayout container_id;
        TextView cust_nm;
        TextView emp_nm;
        TextView fdb_type;
        TextView rating;

        public FeedbackHolder(View itemView) {
            super(itemView);
            this.cust_nm = (TextView) itemView.findViewById(R.id.cust_nm);
            this.comp_desc = (TextView) itemView.findViewById(R.id.comp_desc);
            this.comp_date = (TextView) itemView.findViewById(R.id.comp_date);
            this.fdb_type = (TextView) itemView.findViewById(R.id.fdb_type);
            this.emp_nm = (TextView) itemView.findViewById(R.id.emp_nm);
            this.rating = (TextView) itemView.findViewById(R.id.rating);
            this.container_id = (LinearLayout) itemView.findViewById(R.id.container_id);
        }
    }

    public FeedbackAdapter(List<FeedbackEntity> feedbackEntity, Context ctx) {
        this.feedbackEntity = feedbackEntity;
        this.ctx = ctx;
    }

    public FeedbackHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FeedbackHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.feedback_row, parent, false));
    }

    public void onBindViewHolder(FeedbackHolder holder, final int position) {
        holder.cust_nm.setText(((FeedbackEntity) this.feedbackEntity.get(position)).getUsernm());
        holder.comp_desc.setText(((FeedbackEntity) this.feedbackEntity.get(position)).getDesp());
        holder.comp_date.setText(BahetiEnterprises.retDate(((FeedbackEntity) this.feedbackEntity.get(position)).getOndate()));
        holder.fdb_type.setText(((FeedbackEntity) this.feedbackEntity.get(position)).getType());
        holder.rating.setText(((FeedbackEntity) this.feedbackEntity.get(position)).getRating());
        if (((FeedbackEntity) this.feedbackEntity.get(position)).getEmp_nm() == null) {
            holder.emp_nm.setText("Not Assigned");
        } else {
            holder.emp_nm.setText(((FeedbackEntity) this.feedbackEntity.get(position)).getEmp_nm());
        }
        holder.container_id.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(FeedbackAdapter.this.ctx, FeedbackDetails.class);
                i.putExtra(BahetiEnterprises.COMPLAINT_DETAILS, (Serializable) FeedbackAdapter.this.feedbackEntity.get(position));
                FeedbackAdapter.this.ctx.startActivity(i);
            }
        });
    }

    public int getItemCount() {
        return this.feedbackEntity.size();
    }
}
