package com.orangebyte.bahetiadmin.modules.db.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;


import com.orangebyte.bahetiadmin.modules.entities.NotificationCenter;

import java.util.List;

/**
 * Created by Ashvini on 3/29/2018.
 */

@Dao
public interface NotiDAO {
    @Query("SELECT * FROM notification_center ")
    LiveData<List<NotificationCenter>> loadAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<NotificationCenter> notificationCenterList);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(NotificationCenter... notificationCenters);


    @Update
    void update(NotificationCenter... notificationCenters);

    @Update
    void updateAll(List<NotificationCenter> notificationCenters);

    @Delete
    void delete(NotificationCenter... notificationCenters);

    @Delete
    void deleteAll(List<NotificationCenter> notificationCenters);

    @Query("DELETE FROM notification_center ")
    void deleteAll();


}
