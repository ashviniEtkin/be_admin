package com.orangebyte.bahetiadmin.modules.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;

import java.util.ArrayList;
import java.util.List;

public class CallLogInstallationRepository {
    private static CallLogInstallationRepository sInstance;
    private final RemoteRepository remoteRepository;

    interface RemoteCallLogInstallListener {
        void onCheckResult(boolean z);

        void onRemotReceived(CallLogInstallationDemoEntity callLogInstallationDemoEntity);

        void onRemoteReceived(List<CallLogInstallationDemoEntity> list);
    }

    interface TaskListener {
        void onTaskFinished();
    }

    public static CallLogInstallationRepository getInstance(RemoteRepository remoteRepository) {
        if (sInstance == null) {
            sInstance = new CallLogInstallationRepository(remoteRepository);
        }
        return sInstance;
    }

    private CallLogInstallationRepository(RemoteRepository remoteRepository) {
        this.remoteRepository = remoteRepository;
    }

    public LiveData<List<CallLogInstallationDemoEntity>> loadCallLogInstallsList(CallLogInstallationDemoEntity callLogInstallationDemoEntity) {
        final MutableLiveData<List<CallLogInstallationDemoEntity>> dataList = new MutableLiveData();
        fetchCallLogInstallList(new RemoteCallLogInstallListener() {
            public void onRemotReceived(CallLogInstallationDemoEntity cmEntity) {
            }

            public void onRemoteReceived(List<CallLogInstallationDemoEntity> scmEntities) {
                if (scmEntities != null) {
                    dataList.setValue(scmEntities);
                }
            }

            public void onCheckResult(boolean b) {
            }
        }, callLogInstallationDemoEntity);
        return dataList;
    }

    public LiveData<List<Boolean>> changeCallLog(int chk, CallLogInstallationDemoEntity callLogInstallationDemoEntity) {
        final MutableLiveData<List<Boolean>> changeList = new MutableLiveData();
        changeCallLog(chk, new RemoteCallLogInstallListener() {
            public void onRemotReceived(CallLogInstallationDemoEntity callLogInstallationDemoEntity) {
            }

            public void onRemoteReceived(List<CallLogInstallationDemoEntity> list) {
            }

            public void onCheckResult(boolean b) {
                List<Boolean> data = new ArrayList();
                data.add(Boolean.valueOf(b));
                changeList.setValue(data);
            }
        }, callLogInstallationDemoEntity);
        return changeList;
    }

    private void fetchCallLogInstallList(@NonNull final RemoteCallLogInstallListener listener, CallLogInstallationDemoEntity callLogInstallationDemoEntity) {
        this.remoteRepository.getCallLogNInstallation(callLogInstallationDemoEntity, new RemoteCallback<List<CallLogInstallationDemoEntity>>() {
            public void onSuccess(List<CallLogInstallationDemoEntity> response) {
                listener.onRemoteReceived(response);
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }

    private void changeCallLog(@NonNull int chk, @NonNull final RemoteCallLogInstallListener listener, CallLogInstallationDemoEntity callLogInstallationDemoEntity) {
        this.remoteRepository.addCallLogNInstallation(chk, callLogInstallationDemoEntity, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }
}
