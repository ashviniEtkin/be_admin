package com.orangebyte.pdflibrary.interfaces;

public interface IId {

	void 		setId(String id);
	String 	getId();
}
