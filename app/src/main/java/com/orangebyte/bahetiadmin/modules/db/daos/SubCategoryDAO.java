package com.orangebyte.bahetiadmin.modules.db.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;

import java.util.List;

@Dao
public interface SubCategoryDAO {
    @Delete
    void delete(SubCategoryEntity... subCategoryEntityArr);

    @Query("DELETE FROM sub_category ")
    void deleteAll();

    @Delete
    void deleteAll(List<SubCategoryEntity> list);

    @Insert(onConflict = 1)
    void insert(SubCategoryEntity... subCategoryEntityArr);

    @Insert(onConflict = 1)
    void insertAll(List<SubCategoryEntity> list);

    @Query("SELECT * FROM sub_category ")
    LiveData<List<SubCategoryEntity>> loadAll();

    @Update
    void update(SubCategoryEntity... subCategoryEntityArr);

    @Update
    void updateAll(List<SubCategoryEntity> list);
}
