package com.orangebyte.bahetiadmin.modules.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "complaints")
public class ComplaintEntity extends IdentifiableEntity implements Serializable {
    @SerializedName("actn_tkn_st")
    @ColumnInfo(name = "actn_tkn_st")
    String actn_tkn_st;
    @SerializedName("address")
    @ColumnInfo(name = "address")
    String address;
    @SerializedName("assigned_empid")
    @ColumnInfo(name = "assigned_empid")
    private String assigned_empid;
    @SerializedName("assigned_mgr_type")
    @ColumnInfo(name = "assigned_mgr_type")
    String assigned_mgr_type;
    @SerializedName("cat_nm")
    @ColumnInfo(name = "cat_nm")
    String cat_nm;

    @SerializedName("com_no")
    @ColumnInfo(name = "com_no")
    private String com_no;

    @SerializedName("comp_date")
    @ColumnInfo(name = "comp_date")
    private String comp_date;
    @SerializedName("desc")
    @ColumnInfo(name = "desc")
    private String desc;
    @SerializedName("emp_nm")
    @ColumnInfo(name = "emp_nm")
    String emp_nm;
    @SerializedName("last_atnd_on")
    @ColumnInfo(name = "last_atnd_on")
    String last_atnd_on;
    @SerializedName("mgr_id")
    @ColumnInfo(name = "mgr_id")
    String mgr_id;
    @SerializedName("mob_no")
    @ColumnInfo(name = "mob_no")
    String mob_no;
    @SerializedName("mobile_no")
    @ColumnInfo(name = "mobile_no")
    String mobile_no;
    @SerializedName("model_no")
    @ColumnInfo(name = "model_no")
    String model_no;
    @SerializedName("priority")
    @ColumnInfo(name = "priority")
    String priority;
    @SerializedName("product_id")
    @ColumnInfo(name = "product_id")
    private String product_id;
    @SerializedName("service_type")
    @ColumnInfo(name = "service_type")
    private String service_type;
    @SerializedName("status")
    @ColumnInfo(name = "status")
    private String status;
    @SerializedName("sub_cat_name")
    @ColumnInfo(name = "sub_cat_name")
    String sub_cat_name;
    @SerializedName("title")
    @ColumnInfo(name = "title")
    String title;

    @SerializedName("user_id")
    @ColumnInfo(name = "user_id")
    private String user_id;


    @SerializedName("usernm")
    @ColumnInfo(name = "usernm")
    String usernm;


    @SerializedName("signature_url")
    @ColumnInfo(name = "signature_url")
    String signature_url;


    @SerializedName("purchase_from")
    @ColumnInfo(name = "purchase_from")
    String purchase_from;


    @SerializedName("invoice_url")
    @ColumnInfo(name = "invoice_url")
    String invoice_url;

    @SerializedName("paid_amt")
    @ColumnInfo(name = "paid_amt")
    String paid_amt;

    @SerializedName("paid_type")
    @ColumnInfo(name = "paid_type")
    String paid_type;


    @SerializedName("extra_charges_desc")
    @ColumnInfo(name = "extra_charges_desc")
    String extra_charges_desc;

    @SerializedName("extra_charges")
    @ColumnInfo(name = "extra_charges")
    String extra_charges;


    @SerializedName("proof_url")
    @ColumnInfo(name = "proof_url")
    String proof_url;


    @SerializedName("product_in")
    @ColumnInfo(name = "product_in")
    String product_in;


    @SerializedName("incentive")
    @ColumnInfo(name = "incentive")
    private String incentive;


    public String getIncentive() {
        return incentive;
    }

    public void setIncentive(String incentive) {
        this.incentive = incentive;
    }

    public String getProduct_in() {
        return product_in;
    }

    public void setProduct_in(String product_in) {
        this.product_in = product_in;
    }

    public String getProof_url() {
        return proof_url;
    }

    public void setProof_url(String proof_url) {
        this.proof_url = proof_url;
    }

    public String getExtra_charges_desc() {
        return extra_charges_desc;
    }

    public void setExtra_charges_desc(String extra_charges_desc) {
        this.extra_charges_desc = extra_charges_desc;
    }

    public String getExtra_charges() {
        return extra_charges;
    }

    public void setExtra_charges(String extra_charges) {
        this.extra_charges = extra_charges;
    }

    public String getPurchase_from() {
        return purchase_from;
    }

    public void setPurchase_from(String purchase_from) {
        this.purchase_from = purchase_from;
    }

    public String getInvoice_url() {
        return invoice_url;
    }

    public void setInvoice_url(String invoice_url) {
        this.invoice_url = invoice_url;
    }

    private boolean isSelected;

    public /* bridge */ /* synthetic */ long getId() {
        return super.getId();
    }

    public /* bridge */ /* synthetic */ void setId(long j) {
        super.setId(j);
    }

    public ComplaintEntity() {
    }

    public String getPaid_amt() {
        return paid_amt;
    }

    public String getPaid_type() {
        return paid_type;
    }

    public void setPaid_type(String paid_type) {
        this.paid_type = paid_type;
    }

    public void setPaid_amt(String paid_amt) {
        this.paid_amt = paid_amt;
    }

    @Ignore
    public ComplaintEntity(long id, String product_id, String service_type, String desc, String com_no, String comp_date, String status, String assigned_empid, String user_id) {
        this.id = id;
        this.product_id = product_id;
        this.service_type = service_type;
        this.desc = desc;
        this.com_no = com_no;
        this.comp_date = comp_date;
        this.status = status;
        this.assigned_empid = assigned_empid;
        this.user_id = user_id;
    }


    public String getSignature_url() {
        return signature_url;
    }

    public void setSignature_url(String signature_url) {
        this.signature_url = signature_url;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getPriority() {
        return this.priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getLast_atnd_on() {
        return this.last_atnd_on;
    }

    public void setLast_atnd_on(String last_atnd_on) {
        this.last_atnd_on = last_atnd_on;
    }

    public String getActn_tkn_st() {
        return this.actn_tkn_st;
    }

    public void setActn_tkn_st(String actn_tkn_st) {
        this.actn_tkn_st = actn_tkn_st;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getModel_no() {
        return this.model_no;
    }

    public void setModel_no(String model_no) {
        this.model_no = model_no;
    }

    public String getCat_nm() {
        return this.cat_nm;
    }

    public void setCat_nm(String cat_nm) {
        this.cat_nm = cat_nm;
    }

    public String getSub_cat_name() {
        return this.sub_cat_name;
    }

    public void setSub_cat_name(String sub_cat_name) {
        this.sub_cat_name = sub_cat_name;
    }

    public String getEmp_nm() {
        return this.emp_nm;
    }

    public void setEmp_nm(String emp_nm) {
        this.emp_nm = emp_nm;
    }

    public String getMob_no() {
        return this.mob_no;
    }

    public void setMob_no(String mob_no) {
        this.mob_no = mob_no;
    }

    public String getMgr_id() {
        return this.mgr_id;
    }

    public void setMgr_id(String mgr_id) {
        this.mgr_id = mgr_id;
    }

    public String getAssigned_mgr_type() {
        return this.assigned_mgr_type;
    }

    public void setAssigned_mgr_type(String assigned_mgr_type) {
        this.assigned_mgr_type = assigned_mgr_type;
    }

    public String getMobile_no() {
        return this.mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsernm() {
        return this.usernm;
    }

    public void setUsernm(String usernm) {
        this.usernm = usernm;
    }

    public String getProduct_id() {
        return this.product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getService_type() {
        return this.service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCom_no() {
        return this.com_no;
    }

    public void setCom_no(String com_no) {
        this.com_no = com_no;
    }

    public String getComp_date() {
        return this.comp_date;
    }

    public void setComp_date(String comp_date) {
        this.comp_date = comp_date;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAssigned_empid() {
        return this.assigned_empid;
    }

    public void setAssigned_empid(String assigned_empid) {
        this.assigned_empid = assigned_empid;
    }

    public String getUser_id() {
        return this.user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
