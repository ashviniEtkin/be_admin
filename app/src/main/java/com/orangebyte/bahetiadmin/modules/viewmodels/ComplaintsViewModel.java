package com.orangebyte.bahetiadmin.modules.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider.NewInstanceFactory;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;
import com.orangebyte.bahetiadmin.modules.repository.ComplaintRepository;
import com.orangebyte.bahetiadmin.modules.repository.RemoteRepository;
import com.orangebyte.bahetiadmin.modules.ui.AllContract.ComplaintsDataStreams;

import java.util.List;

public class ComplaintsViewModel extends ViewModel implements ComplaintsDataStreams {
    private ComplaintRepository productRepository;

    public static class ComplaintFactory extends NewInstanceFactory {
        ComplaintRepository pProductRepository;

        public ComplaintFactory() {
            this(null, null);
        }

        public ComplaintFactory(@Nullable ComplaintRepository pProductRepository, @Nullable String abc) {
            this.pProductRepository = pProductRepository;
        }

        public <T extends ViewModel> T create(Class<T> cls) {
            return (T) new ComplaintsViewModel(this.pProductRepository);
        }
    }

    public ComplaintsViewModel() {
        this(null);
    }

    public ComplaintsViewModel(@Nullable ComplaintRepository productRepository) {
        if (productRepository != null) {
            this.productRepository = productRepository;
        }
    }

    public LiveData<List<ComplaintEntity>> getComplaints(ComplaintEntity complaintEntity) {
        onInstanceCreated();
        return subscribeProductObservable(complaintEntity);
    }

    public LiveData<List<Boolean>> assignedEmp(ComplaintEntity complaintEntity, int chk) {
        onInstanceCreated();
        return this.productRepository.assingnedEmployee(complaintEntity, chk);
    }

    private LiveData<List<ComplaintEntity>> subscribeProductObservable(ComplaintEntity complaintEntity) {
        return this.productRepository.loadComplaintsList(complaintEntity);
    }

    private void onInstanceCreated() {
        try {
            if (this.productRepository == null) {
                this.productRepository = ComplaintRepository.getInstance(RemoteRepository.getInstance());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
