package com.orangebyte.bahetiadmin.modules.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.adapters.GridAdapters;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener.OnItemClickListener;
import com.pdfjet.Point;
import com.pdfjet.Table;
import com.pdfjet.TextAlign;

import okhttp3.internal.platform.Platform;

public class AdminDashboard extends AppCompatActivity {
    RecyclerView categories;
    int[] names = new int[]{R.string.Category, R.string.Sub_Categories, R.string.Brands, R.string.Products, R.string.Manager, R.string.Employee, R.string.Offers, R.string.enquiry, R.string.Complaints, R.string.Notification, R.string.Installation_Demo, R.string.Customer_Feedback_Review, R.string.noti_center, R.string.monitoring_employee};

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dashboard);
        init();
        listeners();
    }

    public void init() {
        this.categories = (RecyclerView) findViewById(R.id.categories);
        GridAdapters adapters = new GridAdapters(this, this.names);
        this.categories.setLayoutManager(new LinearLayoutManager(this));
        this.categories.setAdapter(adapters);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        ((TextView) findViewById(R.id.screen_nm)).setText(Html.fromHtml(getResources().getString(R.string.baheti_enter)));
    }

    public void listeners() {
        this.categories.addOnItemTouchListener(new RecyclerItemClickListener(this, new OnItemClickListener() {
            public void onItemClick(View view, int position) {
                AdminDashboard.this.intents(position);
            }
        }));
    }

    public void intents(int pos) {
        switch (pos) {
            case 0 /*0*/:
                startActivity(new Intent(this, CategoryActivity.class));
                return;
            case 1 /*1*/:
                startActivity(new Intent(this, SubCategoryActivity.class));
                return;
            case 2 /*2*/:
                startActivity(new Intent(this, BrandActivity.class));
                return;
            case 3 /*3*/:
                startActivity(new Intent(this, AddProduct.class));
                return;
            case 4 /*4*/:
                startActivity(new Intent(this, ManagerActivity.class));
                return;
            case 5/*5*/:
                startActivity(new Intent(this, EMployeeActivity.class));
                return;
            case 6 /*6*/:
                startActivity(new Intent(this, OfferActivity.class));
                return;
            case 7 /*7*/:
               /* startActivity(new Intent(this, ShowEnquiry.class));
                return;*/

                Intent ii_enq = new Intent(this, ShowEnquiry.class);
                ii_enq.putExtra(BahetiEnterprises.FROM, "0");
                startActivity(ii_enq);
                return;
            case 8 /*8*/:
                Intent ii = new Intent(this, ComplaintServices.class);
                ii.putExtra(BahetiEnterprises.FROM, "0");
                startActivity(ii);
                return;
            case 9 /*9*/:
                startActivity(new Intent(this, Notification.class));
                return;
            case 10 /*10*/:
                Intent call_intent = new Intent(this, CallLogAndInstallationDemo.class);
                call_intent.putExtra(BahetiEnterprises.INTNT, "0");
                startActivity(call_intent);
                return;
            case 11 /*11*/:
                startActivity(new Intent(this, Feedback.class));
                return;

            case 12 /*12*/:
                Intent noti_intent = new Intent(this, NotiCenter.class);
                noti_intent.putExtra(BahetiEnterprises.FROM, "1");
                startActivity(noti_intent);
                return;

            case 13 /*13*/:
                Intent mon_emp = new Intent(this, MonitoringEmployee.class);
                startActivity(mon_emp);
                return;
            default:
                return;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.admin_items, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout /*2131624289*/:
                BahetiEnterprises.logout(this);
                return true;

            case R.id.updt_profile /*2131624289*/:
                // BahetiEnterprises.logout(this);
                startActivity(new Intent(AdminDashboard.this, UpdateProfile.class));
                return true;
            case R.id.chang_pswd /*2131624289*/:
                startActivity(new Intent(AdminDashboard.this, ChangePassword.class));

                //  BahetiEnterprises.logout(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
