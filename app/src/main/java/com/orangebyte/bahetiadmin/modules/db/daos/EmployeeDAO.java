package com.orangebyte.bahetiadmin.modules.db.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;

import java.util.List;

@Dao
public interface EmployeeDAO {
    @Delete
    void delete(EmployeeEntity... employeeEntityArr);

    @Query("DELETE FROM employee ")
    void deleteAll();

    @Delete
    void deleteAll(List<EmployeeEntity> list);

    @Insert(onConflict = 1)
    void insert(EmployeeEntity... employeeEntityArr);

    @Insert(onConflict = 1)
    void insertAll(List<EmployeeEntity> list);

    @Query("SELECT * FROM employee ")
    LiveData<List<EmployeeEntity>> loadAll();

    @Update
    void update(EmployeeEntity... employeeEntityArr);

    @Update
    void updateAll(List<EmployeeEntity> list);
}
