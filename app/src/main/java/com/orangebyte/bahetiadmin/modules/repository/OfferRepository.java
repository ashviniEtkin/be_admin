package com.orangebyte.bahetiadmin.modules.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.db.daos.OfferDAO;
import com.orangebyte.bahetiadmin.modules.entities.OfferEntity;

import java.util.ArrayList;
import java.util.List;

public class OfferRepository {
    private static OfferRepository sInstance;
    private final OfferDAO offerDAO;
    private final RemoteRepository remoteRepository;

    interface RemoteOfferListener {
        void onCheckResult(boolean z);

        void onRemotReceived(OfferEntity offerEntity);

        void onRemoteReceived(List<OfferEntity> list);
    }

    interface TaskListener {
        void onTaskFinished();
    }

    public static OfferRepository getInstance(RemoteRepository remoteRepository, OfferDAO offerDAO) {
        if (sInstance == null) {
            sInstance = new OfferRepository(remoteRepository, offerDAO);
        }
        return sInstance;
    }

    private OfferRepository(RemoteRepository remoteRepository, OfferDAO offerDAO) {
        this.remoteRepository = remoteRepository;
        this.offerDAO = offerDAO;
    }

    public LiveData<List<OfferEntity>> loadOffersList(OfferEntity offerEntity) {

        final MutableLiveData<List<OfferEntity>> ofListMutableLiveData = new MutableLiveData<>();
        fetchOfferList(new RemoteOfferListener() {
            public void onRemotReceived(OfferEntity cmEntity) {
            }

            public void onRemoteReceived(List<OfferEntity> scmEntities) {
                if (scmEntities != null) {
                    ofListMutableLiveData.setValue(scmEntities);

                    OfferRepository.this.insertAllTask(scmEntities);


                }
            }

            public void onCheckResult(boolean b) {
            }
        }, offerEntity);
        // return this.offerDAO.loadAll();

        return ofListMutableLiveData;
    }

    public LiveData<List<OfferEntity>> addOffer(int chk, final OfferEntity offerEntity, Uri fileUri) {
        addOffer(chk, offerEntity, fileUri, new RemoteOfferListener() {
            public void onRemotReceived(OfferEntity cmEntity) {
            }

            public void onRemoteReceived(List<OfferEntity> list) {
            }

            public void onCheckResult(boolean b) {
                OfferEntity oe = new OfferEntity();
                oe.setStatus(offerEntity.getStatus());
                oe.setBrand_id(offerEntity.getBrand_id());
                oe.setCat_id(offerEntity.getCat_id());
                oe.setStatus(offerEntity.getStatus());
                oe.setSub_cat_id(offerEntity.getSub_cat_id());
                oe.setStart_date(offerEntity.getStart_date());
                oe.setTo_date(offerEntity.getTo_date());
                oe.setProduct_id(offerEntity.getProduct_id());
                oe.setDiscount(offerEntity.getDiscount());
                oe.setOffer_type(offerEntity.getOffer_type());
                oe.setTitle(offerEntity.getTitle());
                OfferRepository.this.insertOfffer(oe);
            }
        });
        return this.offerDAO.loadAll();
    }

    public LiveData<List<Boolean>> updateOfferStatus(final OfferEntity offerEntity) {
        final MutableLiveData<List<Boolean>> dataset = new MutableLiveData();
        updateOfferStatus(offerEntity, new RemoteOfferListener() {
            public void onRemotReceived(OfferEntity cmEntity) {
            }

            public void onRemoteReceived(List<OfferEntity> list) {
            }

            public void onCheckResult(boolean b) {
                List<Boolean> bolList = new ArrayList();
                bolList.add(Boolean.valueOf(b));
                dataset.setValue(bolList);
              /*  if (b) {
                    OfferEntity oe = new OfferEntity();
                    oe.setStatus(offerEntity.getStatus());
                    oe.setBrand_id(offerEntity.getBrand_id());
                    oe.setCat_id(offerEntity.getCat_id());
                    oe.setStatus(offerEntity.getStatus());
                    oe.setSub_cat_id(offerEntity.getSub_cat_id());
                    oe.setStart_date(offerEntity.getStart_date());
                    oe.setTo_date(offerEntity.getTo_date());
                    oe.setProduct_id(offerEntity.getProduct_id());
                    oe.setDiscount(offerEntity.getDiscount());
                    oe.setOffer_type(offerEntity.getOffer_type());
                    oe.setTitle(offerEntity.getTitle());
                    OfferRepository.this.updtOffers(oe);
                }*/
            }
        });
        return dataset;
    }

    private void insertAllTask(List<OfferEntity> offerEntity) {
        insertOffers(offerEntity, null);
    }

    private void insertOffers(final List<OfferEntity> offerEntity, @Nullable final TaskListener listener) {
        if (VERSION.SDK_INT >= 3) {
            new AsyncTask<Context, Void, Void>() {
                protected Void doInBackground(Context... params) {
                    OfferRepository.this.offerDAO.deleteAll();
                    OfferRepository.this.offerDAO.insertAll(offerEntity);
                    return null;
                }

                protected void onPostExecute(Void aVoid) {
                    if (VERSION.SDK_INT >= 3) {
                        super.onPostExecute(aVoid);
                    }
                    if (listener != null) {
                        listener.onTaskFinished();
                    }
                }
            }.execute(new Context[]{BahetiEnterprises.getInstance()});
        }
    }

    private void insertOfffer(OfferEntity offerEntity) {
        insertOffer(offerEntity, null);
    }

    private void insertOffer(final OfferEntity offerEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                OfferRepository.this.offerDAO.insert(offerEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteOfer(OfferEntity offerEntity) {
        deleteOffer(offerEntity, null);
    }

    private void deleteOffer(final OfferEntity offerEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                OfferRepository.this.offerDAO.delete(offerEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void updtOffers(OfferEntity offerEntity) {
        updateOffers(offerEntity, null);
    }

    private void updateOffers(final OfferEntity offerEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                OfferRepository.this.offerDAO.update(offerEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteAllOffers() {
        deleteAllMgr(null);
    }

    private void deleteAllMgr(@Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                OfferRepository.this.offerDAO.deleteAll();
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void fetchOfferList(@NonNull final RemoteOfferListener listener, OfferEntity offerEntity) {
        this.remoteRepository.getOffers(offerEntity, new RemoteCallback<List<OfferEntity>>() {
            public void onSuccess(List<OfferEntity> response) {
                listener.onRemoteReceived(response);
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }

    private void addOffer(@NonNull int chk, @NonNull OfferEntity offerEntity, @NonNull Uri fileUri, @NonNull final RemoteOfferListener listener) {
        this.remoteRepository.addOffer(chk, offerEntity, fileUri, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }

    private void updateOfferStatus(@NonNull OfferEntity offerEntity, @NonNull final RemoteOfferListener listener) {
        this.remoteRepository.updateOffer(offerEntity, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }
}
