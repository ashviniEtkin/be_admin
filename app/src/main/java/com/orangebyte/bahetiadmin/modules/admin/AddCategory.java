package com.orangebyte.bahetiadmin.modules.admin;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.models.Cat;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels.Factory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import im.delight.android.webview.BuildConfig;
import java.util.List;

public class AddCategory extends AppCompatActivity implements LifecycleRegistryOwner {
    Button add;
    String assigned = BuildConfig.VERSION_NAME;
    EditText cat_nm;
    CategoryEntity categoryEntity;
    String chk_add_update = BuildConfig.VERSION_NAME;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    CheckBox service_mgr;
    CheckBox store_mgr;
    SweetAlertDialog sweetAlertDialog;
    private CategoryViewMdels viewModel;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);
        initViews();
        this.chk_add_update = getIntent().getStringExtra(BahetiEnterprises.CHK_ADD_UPDATE);
        if (this.chk_add_update.equalsIgnoreCase("1")) {
            this.add.setText("Update");
            BahetiEnterprises.setActBar(getResources().getString(R.string.Update_Category), this, false);
            this.categoryEntity = (CategoryEntity) getIntent().getSerializableExtra(BahetiEnterprises.CATEGORY);
            updateData();
        } else {
            this.add.setText("Add");
            BahetiEnterprises.setActBar(getResources().getString(R.string.Add_Category), this, false);
        }
        this.add.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (AddCategory.this.store_mgr.isChecked() && AddCategory.this.service_mgr.isChecked()) {
                    AddCategory.this.assigned = "11";
                    AddCategory.this.initModelView();
                } else if (AddCategory.this.store_mgr.isChecked() && !AddCategory.this.service_mgr.isChecked()) {
                    AddCategory.this.assigned = "10";
                    AddCategory.this.initModelView();
                } else if (AddCategory.this.store_mgr.isChecked() || !AddCategory.this.service_mgr.isChecked()) {
                    Toast.makeText(BahetiEnterprises.getInstance(), "Please Select Assign Call", 1).show();
                } else {
                    AddCategory.this.assigned = "01";
                    AddCategory.this.initModelView();
                }
            }
        });
    }

    public void initModelView() {
        this.viewModel = (CategoryViewMdels) ViewModelProviders.of(this, new Factory()).get(CategoryViewMdels.class);
        if (this.chk_add_update.equalsIgnoreCase("1")) {
            addCategory(this.viewModel, new Cat(this.categoryEntity.getId() + BuildConfig.VERSION_NAME, this.cat_nm.getText().toString(), this.assigned, this.categoryEntity.getStatus() + BuildConfig.VERSION_NAME), 1);
            return;
        }
        addCategory(this.viewModel, new Cat(null, this.cat_nm.getText().toString(), this.assigned, "1"), 0);
    }

    public void initViews() {
        this.service_mgr = (CheckBox) findViewById(R.id.service_mgr);
        this.store_mgr = (CheckBox) findViewById(R.id.store_mgr);
        this.cat_nm = (EditText) findViewById(R.id.cat_nm);
        this.add = (Button) findViewById(com.orangebyte.pdflibrary.R.id.add);
        this.sweetAlertDialog = new SweetAlertDialog(this, 5);
        this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
        this.sweetAlertDialog.setTitleText("Loading");
        this.sweetAlertDialog.setCancelable(false);
    }

    public void updateData() {
        this.cat_nm.setText(this.categoryEntity.getName());
        if ((this.categoryEntity.getAssign_to() + BuildConfig.VERSION_NAME).equalsIgnoreCase("11")) {
            this.store_mgr.setChecked(true);
            this.service_mgr.setChecked(true);
        } else if ((this.categoryEntity.getAssign_to() + BuildConfig.VERSION_NAME).equalsIgnoreCase("01")) {
            this.store_mgr.setChecked(false);
            this.service_mgr.setChecked(true);
        } else if ((this.categoryEntity.getAssign_to() + BuildConfig.VERSION_NAME).equalsIgnoreCase("10")) {
            this.store_mgr.setChecked(true);
            this.service_mgr.setChecked(false);
        }
    }

    private void addCategory(CategoryViewMdels viewModel, Cat c, int chk) {
        try {
            this.sweetAlertDialog.show();
            viewModel.addCategory(c, chk).observe(this, new Observer<List<CategoryEntity>>() {
                public void onChanged(@Nullable List<CategoryEntity> categoryEntities) {
                    if (categoryEntities.isEmpty()) {
                        AddCategory.this.sweetAlertDialog.show();
                        return;
                    }
                    AddCategory.this.sweetAlertDialog.dismiss();
                    AddCategory.this.onBackPressed();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }
}
