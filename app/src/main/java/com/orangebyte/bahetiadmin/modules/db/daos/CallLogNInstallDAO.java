package com.orangebyte.bahetiadmin.modules.db.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import java.util.List;

@Dao
public interface CallLogNInstallDAO {
    @Delete
    void delete(CallLogInstallationDemoEntity... callLogInstallationDemoEntityArr);

    @Query("DELETE FROM installationndemo ")
    void deleteAll();

    @Delete
    void deleteAll(List<CallLogInstallationDemoEntity> list);

    @Insert(onConflict = 1)
    void insert(CallLogInstallationDemoEntity... callLogInstallationDemoEntityArr);

    @Insert(onConflict = 1)
    void insertAll(List<CallLogInstallationDemoEntity> list);

    @Query("SELECT * FROM installationndemo ")
    LiveData<List<CallLogInstallationDemoEntity>> loadAll();

    @Update
    void update(CallLogInstallationDemoEntity... callLogInstallationDemoEntityArr);

    @Update
    void updateAll(List<CallLogInstallationDemoEntity> list);
}
