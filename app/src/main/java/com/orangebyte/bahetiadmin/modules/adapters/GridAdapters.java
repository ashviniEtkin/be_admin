package com.orangebyte.bahetiadmin.modules.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.orangebyte.bahetiadmin.R;

public class GridAdapters extends Adapter<GridAdapters.GridHolder> {
    Context ctx;
    int[] names;

    public class GridHolder extends ViewHolder {
        Button cat_nm;

        public GridHolder(View itemView) {
            super(itemView);
            this.cat_nm = (Button) itemView.findViewById(R.id.cat_nm);
        }
    }

    public GridAdapters(Context ctx, int[] names) {
        this.ctx = ctx;
        this.names = names;
    }

    public GridHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GridHolder(LayoutInflater.from(this.ctx).inflate(R.layout.cat_grid, null));
    }

    public void onBindViewHolder(GridHolder holder, int position) {
        holder.cat_nm.setText(this.ctx.getResources().getString(this.names[position]));
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public int getItemCount() {
        return this.names.length;
    }
}
