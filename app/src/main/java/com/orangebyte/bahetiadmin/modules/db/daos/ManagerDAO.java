package com.orangebyte.bahetiadmin.modules.db.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.orangebyte.bahetiadmin.modules.entities.ManagerEntity;

import java.util.List;

@Dao
public interface ManagerDAO {
    @Delete
    void delete(ManagerEntity... managerEntityArr);

    @Query("DELETE FROM managers ")
    void deleteAll();

    @Delete
    void deleteAll(List<ManagerEntity> list);

    @Insert(onConflict = 1)
    void insert(ManagerEntity... managerEntityArr);

    @Insert(onConflict = 1)
    void insertAll(List<ManagerEntity> list);

    @Query("SELECT * FROM managers ")
    LiveData<List<ManagerEntity>> loadAll();

    // "WHERE Loan.user_id == :userId "

    @Query("SELECT * FROM managers WHERE managers.mgr_type == :mgr_type ")
    LiveData<List<ManagerEntity>> getManagetType(String mgr_type);

    @Update
    void update(ManagerEntity... managerEntityArr);

    @Update
    void updateAll(List<ManagerEntity> list);
}
