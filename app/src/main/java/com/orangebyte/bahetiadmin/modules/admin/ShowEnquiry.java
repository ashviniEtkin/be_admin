package com.orangebyte.bahetiadmin.modules.admin;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.expandableview.ExpandableRelativeLayout;
import com.orangebyte.bahetiadmin.modules.adapters.EnquiryAdapter;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.entities.EnquiryEntity;
import com.orangebyte.bahetiadmin.modules.entities.LoginEntity;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener;
import com.orangebyte.bahetiadmin.modules.manager.LodgeEnquiry;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels.Factory;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel.EmployeeFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.EnquiryViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.EnquiryViewModel.EnquiryFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel.SubCategoryFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;

import im.delight.android.webview.BuildConfig;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ShowEnquiry extends AppCompatActivity implements LifecycleRegistryOwner {
    List<SubCategoryEntity> allSubCatList = new ArrayList();
    List<CategoryEntity> categoryEntityList = new ArrayList();
    Spinner category_spinner;
    ExpandableRelativeLayout date_expandble_view;
    List<EmployeeEntity> employeeEntityList = new ArrayList();
    Spinner employee_spinner;
    List<EnquiryEntity> enquiryEntityList = new ArrayList();
    RecyclerView enquiry_recycler;
    TextView in_progress;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    LinearLayout list_container;
    int mDay;
    int mMonth;
    String from;
    EnquiryAdapter adapter;
    EnquiryEntity enquiryEntity = null;//new EnquiryEntity();


    EnquiryViewModel cnmplViewModel;
    EnquiryFactory factory;

    private OnDateSetListener mSeleteDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            select_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    private OnDateSetListener mStartDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            start_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    private OnDateSetListener mToDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            to_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    int mYear;
    TextView no_result_fount;
    TextView on_hold;
    TextView pending;
    String post = "";
    ImageButton reset;
    TextView resolved_closed;
    TextView select_date;
    SharedPreferences sh;
    Button show, btn_reset;
    TextView start_date;
    Spinner status_spinner;
    List<SubCategoryEntity> subCategoryEntityList = new ArrayList();
    TableRow sub_cat_container;
    Spinner subcategory_spinner;
    TextView to_date;
    TextView total;

    CheckBox all_cb;
    Button btn_assign;
    Button btn_new;
    Button btn_resolved;
    LinearLayout calllog_oper_contnr;
    SweetAlertDialog sweetAlertDialog;
    ;

    EditText etd_cust_mobile_no;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_enquiry);
        BahetiEnterprises.setActBar(getResources().getString(R.string.Show_Enquiry), this, false);

        from = getIntent().getStringExtra(BahetiEnterprises.FROM);
        init();
        setEmployeeToSpinner();

        listeners();
        this.category_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (allSubCatList.isEmpty()) {
                    setSubCatgoryToSpinner();
                } else if (i > 0) {
                    setToSubCatSpinner(((CategoryEntity) categoryEntityList.get(i - 1)).getId() + "");
                }
                if (i == 0) {
                    sub_cat_container.setVisibility(8);
                } else {
                    sub_cat_container.setVisibility(0);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.show.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                enquiryEntity = new EnquiryEntity();
                if (status_spinner.getSelectedItemPosition() == 0) {
                    enquiryEntity.setStatus("null");
                } else {
                    enquiryEntity.setStatus(status_spinner.getSelectedItem().toString());
                }
                if (category_spinner.getSelectedItemPosition() == 0) {
                    enquiryEntity.setCat_id("null");
                } else {
                    try {
                        if (BahetiEnterprises.checkSpinner(category_spinner)) {
                            enquiryEntity.setCat_id("null");
                        } else {
                            enquiryEntity.setCat_id(((CategoryEntity) categoryEntityList.get(category_spinner.getSelectedItemPosition() - 1)).getId() + "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        enquiryEntity.setCat_id("null");
                    }
                }
                if (subcategory_spinner.getSelectedItemPosition() == 0) {
                    enquiryEntity.setSubcat_id("null");
                } else {
                    try {
                        if (BahetiEnterprises.checkSpinner(subcategory_spinner)) {
                            enquiryEntity.setSubcat_id("null");
                        } else {
                            enquiryEntity.setSubcat_id(((SubCategoryEntity) subCategoryEntityList.get(subcategory_spinner.getSelectedItemPosition() - 1)).getId() + "");
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        enquiryEntity.setSubcat_id("null");
                    }
                }
                if (employee_spinner.getSelectedItemPosition() == 0) {
                    enquiryEntity.setAssigned_emp_id("null");
                } else {
                    try {
                        if (BahetiEnterprises.checkSpinner(employee_spinner)) {
                            enquiryEntity.setAssigned_emp_id("null");
                        } else {
                            enquiryEntity.setAssigned_emp_id(((EmployeeEntity) employeeEntityList.get(employee_spinner.getSelectedItemPosition() - 1)).getId() + "");
                        }
                    } catch (Exception e22) {
                        e22.printStackTrace();
                        enquiryEntity.setAssigned_emp_id("null");
                    }
                }


                if (TextUtils.isEmpty(etd_cust_mobile_no.getText().toString())) {
                    enquiryEntity.setUser_id("null");
                } else {
                    enquiryEntity.setUser_id(etd_cust_mobile_no.getText().toString());
                }

                if (!TextUtils.isEmpty(start_date.getText().toString()) && !TextUtils.isEmpty(to_date.getText().toString())) {
                    enquiryEntity.setOndate(BahetiEnterprises.tStamp(start_date.getText().toString()) + "");
                    enquiryEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(to_date.getText().toString()) + "");
                } else if (!TextUtils.isEmpty(start_date.getText().toString()) && TextUtils.isEmpty(to_date.getText().toString())) {
                    enquiryEntity.setOndate(BahetiEnterprises.tStamp(start_date.getText().toString()) + "");
                    enquiryEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(start_date.getText().toString()) + "");
                } else if (TextUtils.isEmpty(start_date.getText().toString()) && !TextUtils.isEmpty(to_date.getText().toString())) {
                    enquiryEntity.setOndate(BahetiEnterprises.tStamp(to_date.getText().toString()) + "");
                    enquiryEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(to_date.getText().toString()) + "");
                } else if (TextUtils.isEmpty(select_date.getText().toString())) {
                    enquiryEntity.setOndate("null");
                    enquiryEntity.setLast_atnd_on("null");
                } else {
                    enquiryEntity.setOndate(BahetiEnterprises.tStamp(select_date.getText().toString()) + "");
                    enquiryEntity.setLast_atnd_on(BahetiEnterprises.lasttStamp(select_date.getText().toString()) + "");
                }
                initViewModel(enquiryEntity);
            }
        });
    }

    private void initViewModel(EnquiryEntity enquiryEntity) {
        subscribeToDataStreams((EnquiryViewModel) ViewModelProviders.of(this, new EnquiryFactory()).get(EnquiryViewModel.class), enquiryEntity);
    }


    public void listeners() {
        enquiry_recycler.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            public void onItemClick(View view, final int position) {

                /*((CheckBox) view.findViewById(R.id.checkb)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                        if (adapter.checkAll()) {
                            all_cb.setChecked(true);
                        } else {
                            all_cb.setChecked(false);
                        }
                    }
                });*/

                Button btn_details = (Button) view.findViewById(R.id.btn_details);
                ((Button) view.findViewById(R.id.btn_assign_transfer)).setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        final Dialog d = new Dialog(ShowEnquiry.this, R.style.Custom_Dialog);
                        d.getWindow().requestFeature(1);
                        d.setContentView(R.layout.assign_emp_complaint);
                        final Spinner empl_spinner = (Spinner) d.findViewById(R.id.employee_spinner);
                        final Spinner priority_spinner = (Spinner) d.findViewById(R.id.priority_spinner);


                        final Spinner paid_spinner = (Spinner) d.findViewById(R.id.paid_spinner);
                        final EditText edt_amount = (EditText) d.findViewById(R.id.edt_amount);
                        final TableRow amount_to_pay = (TableRow) d.findViewById(R.id.Amount_to_pay);
                        amount_to_pay.setVisibility(View.GONE);

                        ((TableRow) d.findViewById(R.id.priority_cont)).setVisibility(View.GONE);
                        //
                        TableRow Paid_cont = (TableRow) d.findViewById(R.id.Paid_cont);
                        Paid_cont.setVisibility(View.GONE);

                        setToSpinner(employeeEntityList, empl_spinner);

                        paid_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i == 1) {
                                    amount_to_pay.setVisibility(View.VISIBLE);
                                } else {
                                    amount_to_pay.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                        Button btn_assign_transfer1 = (Button) d.findViewById(R.id.btn_assign_transfer);
                        ((Button) d.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
                            public void onClick(View view) {
                                d.dismiss();
                            }
                        });
                        btn_assign_transfer1.setOnClickListener(new OnClickListener() {
                            public void onClick(View view) {
                                if (empl_spinner.getSelectedItemPosition() == 0) {
                                    BahetiEnterprises.retShowAlertDialod("Select Employee", ShowEnquiry.this);
                                    return;
                                }
/*
                                if (paid_spinner.getSelectedItemPosition() == 0) {
                                    BahetiEnterprises.retShowAlertDialod("Select Paid Type", ShowEnquiry.this);
                                    return;
                                }*/


                                final EnquiryEntity enquiryEntity = new EnquiryEntity();
                                enquiryEntity.setUser_id(((EnquiryEntity) enquiryEntityList.get(position)).getId() + "");
                                //enquiryEntity.set(priority_spinner.getSelectedItem().toString());
                                enquiryEntity.setAssigned_emp_id(((EmployeeEntity) employeeEntityList.get(empl_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);
                                // enquiryEntity.setPaid_type(paid_spinner.getSelectedItem().toString());

                                /*if (TextUtils.isEmpty(edt_amount.getText().toString())) {
                                    enquiryEntity.setPaid_amt("0");
                                } else {
                                    enquiryEntity.setPaid_amt(edt_amount.getText().toString());
                                }*/

                                cnmplViewModel.chngEnquiry(enquiryEntity, 2).observe(ShowEnquiry.this, new Observer<List<Boolean>>() {
                                    public void onChanged(@Nullable List<Boolean> booleen) {
                                        if (!booleen.isEmpty()) {
                                            sweetAlertDialog.dismiss();
                                            d.dismiss();
                                            subscribeToDataStreams(cnmplViewModel, enquiryEntity);
                                        } else if (booleen.size() == 0) {
                                            sweetAlertDialog.dismiss();
                                            d.dismiss();
                                            subscribeToDataStreams(cnmplViewModel, enquiryEntity);
                                        } else {
                                            sweetAlertDialog.show();
                                        }
                                    }
                                });
                            }
                        });
                        d.show();
                    }
                });
            }
        }));
    }


    private void subscribeToDataStreams(EnquiryViewModel viewModel, EnquiryEntity enquiryEntity) {
        try {
            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, 5);
            sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
            sweetAlertDialog.setTitleText("Loading");
            sweetAlertDialog.setCancelable(false);
            sweetAlertDialog.show();
            viewModel.getEnquiry(enquiryEntity).observe(this, new Observer<List<EnquiryEntity>>() {
                public void onChanged(@Nullable List<EnquiryEntity> offerEntityList) {
                    if (!offerEntityList.isEmpty()) {
                        sweetAlertDialog.dismiss();
                        showCompListInUit(offerEntityList);
                        enquiry_recycler.setVisibility(0);
                        no_result_fount.setVisibility(8);
                        list_container.setVisibility(0);
                        if (from.equalsIgnoreCase("1")) {
                            calllog_oper_contnr.setVisibility(View.VISIBLE);
                        } else {
                            calllog_oper_contnr.setVisibility(View.GONE);
                        }
                    } else if (offerEntityList.size() == 0) {
                        sweetAlertDialog.dismiss();
                        showCompListInUit(new ArrayList());
                        enquiry_recycler.setVisibility(8);
                        no_result_fount.setVisibility(0);
                        list_container.setVisibility(8);
                        calllog_oper_contnr.setVisibility(View.GONE);
                    } else {
                        sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showCompListInUit(List<EnquiryEntity> enquiryEntityList) {
        this.enquiryEntityList = enquiryEntityList;
        int pend_cnt = getCont(enquiryEntityList, getResources().getString(R.string.Pending));
        int on_hold_cnt = getCont(enquiryEntityList, getResources().getString(R.string.On_Hold));
        int resolved_closed_cnt = getCont(enquiryEntityList, getResources().getString(R.string.Resolved_Closed));
        int in_progress_cnt = getCont(enquiryEntityList, getResources().getString(R.string.In_Progress));
        int all = ((pend_cnt + on_hold_cnt) + resolved_closed_cnt) + in_progress_cnt;
        this.pending.setText(getResources().getString(R.string.Pending) + " : " + pend_cnt);
        this.on_hold.setText(getResources().getString(R.string.On_Hold) + " : " + on_hold_cnt);
        this.resolved_closed.setText(getResources().getString(R.string.Resolved_Closed) + " : " + resolved_closed_cnt);
        this.in_progress.setText(getResources().getString(R.string.In_Progress) + " : " + in_progress_cnt);
        this.total.setText(getResources().getString(R.string.total) + " : " + all);
        if (from.equalsIgnoreCase("1")) {
            adapter = new EnquiryAdapter(enquiryEntityList, this, 1, all_cb);

        } else {
            adapter = new EnquiryAdapter(enquiryEntityList, this, 0, all_cb);
        }
        this.enquiry_recycler.setLayoutManager(new LinearLayoutManager(this));
        this.enquiry_recycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void showDateView(View view) {
        this.date_expandble_view = (ExpandableRelativeLayout) findViewById(R.id.date_expandble_view);
        this.date_expandble_view.toggle();
    }

    public void showSelectDate(View view) {
        new DatePickerDialog(this, this.mSeleteDate, this.mYear, this.mMonth, this.mDay).show();
    }

    public void showStartDate(View view) {
        new DatePickerDialog(this, this.mStartDate, this.mYear, this.mMonth, this.mDay).show();
    }

    public void showToDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this.mToDate, this.mYear, this.mMonth, this.mDay);
        if (TextUtils.isEmpty(this.start_date.getText().toString())) {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        } else {
            datePickerDialog.getDatePicker().setMinDate(BahetiEnterprises.tStamp(this.start_date.getText().toString()));
        }
        datePickerDialog.show();
    }

    public void resolved(EnquiryEntity enquiryEntity1, int chk) {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, 5);
        sweetAlertDialog.setTitle("Loading");
        sweetAlertDialog.show();
        cnmplViewModel.chngEnquiry(enquiryEntity1, chk).observe(this, new Observer<List<Boolean>>() {
            public void onChanged(@Nullable List<Boolean> booleen) {
                if (!booleen.isEmpty()) {
                    sweetAlertDialog.dismiss();

                    onResume();
                } else if (booleen.size() == 0) {
                    sweetAlertDialog.dismiss();
                } else {
                    sweetAlertDialog.show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (enquiryEntity != null) {
            initViewModel(enquiryEntity);
        }
    }

    public void init() {
        calllog_oper_contnr = (LinearLayout) findViewById(R.id.calllog_oper_contnr);

        this.factory = new EnquiryFactory();
        this.cnmplViewModel = (EnquiryViewModel) ViewModelProviders.of(this, this.factory).get(EnquiryViewModel.class);

        this.etd_cust_mobile_no = (EditText) findViewById(R.id.etd_cust_mobile_no);

        this.btn_new = (Button) findViewById(R.id.btn_new);
        this.btn_assign = (Button) findViewById(R.id.btn_assign);
        this.btn_resolved = (Button) findViewById(R.id.btn_resolved);
        all_cb = (CheckBox) findViewById(R.id.all_cb);
        this.show = (Button) findViewById(R.id.show);
        this.employee_spinner = (Spinner) findViewById(R.id.employee_spinner);
        this.category_spinner = (Spinner) findViewById(R.id.category_spinner);
        this.subcategory_spinner = (Spinner) findViewById(R.id.subcategory_spinner);
        this.status_spinner = (Spinner) findViewById(R.id.status_spinner);
        this.enquiry_recycler = (RecyclerView) findViewById(R.id.enquiry_recycler);
        this.no_result_fount = (TextView) findViewById(R.id.no_result_fount);
        this.sub_cat_container = (TableRow) findViewById(R.id.sub_cat_container);
        this.select_date = (TextView) findViewById(R.id.select_date);
        this.start_date = (TextView) findViewById(R.id.start_date);
        this.to_date = (TextView) findViewById(R.id.to_date);
        this.total = (TextView) findViewById(R.id.total);
        this.pending = (TextView) findViewById(R.id.pending);
        this.on_hold = (TextView) findViewById(R.id.on_hold);
        this.in_progress = (TextView) findViewById(R.id.in_progress);
        this.resolved_closed = (TextView) findViewById(R.id.resolved_closed);
        this.no_result_fount = (TextView) findViewById(R.id.no_result_fount);
        this.list_container = (LinearLayout) findViewById(R.id.list_container);
        this.reset = (ImageButton) findViewById(R.id.reset);
        Calendar c = Calendar.getInstance();
        this.mYear = c.get(1);
        this.mMonth = c.get(2);
        this.mDay = c.get(5);
        this.sh = getSharedPreferences("log", 0);
        this.post = this.sh.getString("post", "");
        this.reset.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                start_date.setText("");
                select_date.setText("");
                to_date.setText("");
                etd_cust_mobile_no.setText("");
            }
        });
        this.sweetAlertDialog = new SweetAlertDialog(this, 5);
        this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
        this.sweetAlertDialog.setTitleText("Loading");
        this.sweetAlertDialog.setCancelable(false);
        btn_reset = (Button) findViewById(R.id.btn_reset);

        btn_reset.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setEmployeeToSpinner();
                status_spinner.setSelection(0);
                select_date.setText("");
                start_date.setText("");
                to_date.setText("");

            }
        });


        this.btn_new.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(ShowEnquiry.this, LodgeEnquiry.class));
            }
        });

        btn_resolved.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String allSelectedIds = getSelectedIds(getSelectedCallLog());
                EnquiryEntity enquiryEntity = new EnquiryEntity();
                enquiryEntity.setUser_id(allSelectedIds);
                enquiryEntity.setStatus(getString(R.string.Resolved_Closed));
                resolved(enquiryEntity, 4);
                //   enquiryEntity.setAssigned_emp_id(((EmployeeEntity) employeeEntityList.get(empl_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);

            }
        });


        this.btn_assign.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (checkSelection()) {
                    final Dialog d = new Dialog(ShowEnquiry.this, R.style.Custom_Dialog);
                    d.getWindow().requestFeature(1);
                    d.setContentView(R.layout.assign_emp_complaint);
                    final Spinner empl_spinner = (Spinner) d.findViewById(R.id.employee_spinner);
                    final Spinner priority_spinner = (Spinner) d.findViewById(R.id.priority_spinner);
                    final Spinner paid_spinner = (Spinner) d.findViewById(R.id.paid_spinner);
                    final EditText edt_amount = (EditText) d.findViewById(R.id.edt_amount);
                    final TableRow amount_to_pay = (TableRow) d.findViewById(R.id.Amount_to_pay);

                    TableRow Paid_cont = (TableRow) d.findViewById(R.id.Paid_cont);
                    amount_to_pay.setVisibility(View.GONE);
                    Paid_cont.setVisibility(View.GONE);
                    setToSpinner(employeeEntityList, empl_spinner);
                    ((TableRow) d.findViewById(R.id.priority_cont)).setVisibility(View.GONE);

                    Button btn_assign_transfer1 = (Button) d.findViewById(R.id.btn_assign_transfer);
                    ((Button) d.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            d.dismiss();
                        }
                    });


                    btn_assign_transfer1.setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            if (empl_spinner.getSelectedItemPosition() == 0) {
                                BahetiEnterprises.retShowAlertDialod("Select Employee", ShowEnquiry.this);
                                return;
                            }


                            String allSelectedIds = getSelectedIds(getSelectedCallLog());
                            EnquiryEntity enquiryEntity = new EnquiryEntity();
                            enquiryEntity.setUser_id(allSelectedIds);
                            enquiryEntity.setAssigned_emp_id(((EmployeeEntity) employeeEntityList.get(empl_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);
                            cnmplViewModel.chngEnquiry(enquiryEntity, 2).observe(ShowEnquiry.this, new Observer<List<Boolean>>() {
                                public void onChanged(@Nullable List<Boolean> booleen) {
                                    if (!booleen.isEmpty()) {
                                        sweetAlertDialog.dismiss();
                                        d.dismiss();
                                        onBackPressed();
                                    } else if (booleen.size() == 0) {
                                        sweetAlertDialog.dismiss();
                                    } else {
                                        sweetAlertDialog.show();
                                    }
                                }
                            });
                        }
                    });
                    d.show();


                }

            }
        });
    }

    public boolean checkSelection() {
        List<EnquiryEntity> installationDemoEntityList = this.adapter.getEnquiryEntity();
        boolean chk = false;
        int i = 0;
        while (i < installationDemoEntityList.size()) {
            if (((EnquiryEntity) installationDemoEntityList.get(i)).isSelected()) {
                chk = true;
                i = installationDemoEntityList.size();
            } else {
                chk = false;
                i++;
            }
        }
        return chk;
    }


    public List<EnquiryEntity> getSelectedCallLog() {
        List<EnquiryEntity> installationDemoEntityList = adapter.getEnquiryEntity();
        List<EnquiryEntity> newList = new ArrayList();
        for (int i = 0; i < installationDemoEntityList.size(); i++) {
            if (((EnquiryEntity) installationDemoEntityList.get(i)).isSelected()) {
                newList.add(installationDemoEntityList.get(i));
            }
        }
        return newList;
    }

    public String getSelectedIds(List<EnquiryEntity> newList) {
        String ids = "";
        for (int i = 0; i < newList.size(); i++) {
            ids = ids + ((EnquiryEntity) newList.get(i)).getId() + ",";
        }
        return ids.substring(0, ids.length() - 1);
    }

    public int getCont(List<EnquiryEntity> enquiryEntityList, String status) {
        int cnt = 0;
        try {
            for (EnquiryEntity ce : enquiryEntityList) {
                if (ce.getStatus().equalsIgnoreCase(status)) {
                    cnt++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cnt;
    }

    public void setEmployeeToSpinner() {
        try {
            EmployeeEntity ee = new EmployeeEntity();
            ee.setAssigned_mgr_type(this.post);
            ((EmployeeViewModel) ViewModelProviders.of(this, new EmployeeFactory()).get(EmployeeViewModel.class)).getEmps(ee).observe(this, new Observer<List<EmployeeEntity>>() {
                public void onChanged(@Nullable List<EmployeeEntity> employeeEntities) {
                    if (!employeeEntities.isEmpty()) {
                        setToSpinner(employeeEntities, employee_spinner);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setToSpinner(List<EmployeeEntity> employeeEntities, Spinner employee_spinner1) {
        this.employeeEntityList = employeeEntities;
        List<String> catList = new ArrayList();
        catList.add("All");
        try {
            for (EmployeeEntity employeeEntity : employeeEntities) {
                catList.add(employeeEntity.getEmp_nm());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        employee_spinner1.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
        setCatgoryToSpinner();
    }

    private void setCatDataToSpinner(List<CategoryEntity> categoryEntities) {
        this.categoryEntityList = categoryEntities;
        List<String> catList = new ArrayList();
        catList.add("All");
        try {
            for (CategoryEntity categoryEntity : categoryEntities) {
                if (this.post.equalsIgnoreCase(getResources().getString(R.string.service_Manager))) {
                    if (categoryEntity.getAssign_to().equalsIgnoreCase("01") || categoryEntity.getAssign_to().equalsIgnoreCase("11")) {
                        catList.add(categoryEntity.getName());
                    }
                } else if (!this.post.equalsIgnoreCase(getResources().getString(R.string.Sales_Manager))) {
                    catList.add(categoryEntity.getName());
                } else if (categoryEntity.getAssign_to().equalsIgnoreCase("10") || categoryEntity.getAssign_to().equalsIgnoreCase("11")) {
                    catList.add(categoryEntity.getName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.category_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
        setSubCatgoryToSpinner();
    }

    private void setToSubCatSpinner(String catId) {
        subCategoryEntityList.clear();

        List<String> catList = new ArrayList();
        catList.add("All");
        try {
            for (SubCategoryEntity subCategoryEntity : this.allSubCatList) {
                if (subCategoryEntity.getCat_id().equalsIgnoreCase(catId)) {
                    catList.add(subCategoryEntity.getSub_cat_name());
                    this.subCategoryEntityList.add(subCategoryEntity);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.subcategory_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }

    public void setSubCatgoryToSpinner() {
        try {
            ((SubCategoryViewModel) ViewModelProviders.of(this, new SubCategoryFactory()).get(SubCategoryViewModel.class)).getSubCats().observe(this, new Observer<List<SubCategoryEntity>>() {
                public void onChanged(@Nullable List<SubCategoryEntity> subCategoryEntities) {
                    if (!subCategoryEntities.isEmpty()) {
                        allSubCatList = subCategoryEntities;
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setCatgoryToSpinner() {
        try {
            ((CategoryViewMdels) ViewModelProviders.of(this, new Factory()).get(CategoryViewMdels.class)).getCats().observe(this, new Observer<List<CategoryEntity>>() {
                public void onChanged(@Nullable List<CategoryEntity> categoryEntities) {
                    if (!categoryEntities.isEmpty()) {
                        setCatDataToSpinner(categoryEntities);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }
}
