package com.orangebyte.bahetiadmin.modules.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "products")
public class ProductEntity extends IdentifiableEntity implements Serializable {
    @SerializedName("brand_id")
    @ColumnInfo(name = "brand_id")
    private String brand_id;
    @SerializedName("cat_id")
    @ColumnInfo(name = "cat_id")
    private String cat_id;
    @SerializedName("features")
    @ColumnInfo(name = "features")
    private String features;
    @SerializedName("image1")
    @ColumnInfo(name = "image1")
    private String image1;
    @SerializedName("image2")
    @ColumnInfo(name = "image2")
    private String image2;
    @SerializedName("image3")
    @ColumnInfo(name = "image3")
    private String image3;
    @SerializedName("image4")
    @ColumnInfo(name = "image4")
    private String image4;
    @SerializedName("image5")
    @ColumnInfo(name = "image5")
    private String image5;
    @SerializedName("model_no")
    @ColumnInfo(name = "model_no")
    private String model_no;
    @SerializedName("price")
    @ColumnInfo(name = "price")
    private String price;
    @SerializedName("status")
    @ColumnInfo(name = "status")
    private String status;
    @SerializedName("sub_cat_id")
    @ColumnInfo(name = "sub_cat_id")
    private String sub_cat_id;
    @SerializedName("title")
    @ColumnInfo(name = "title")
    private String title;

    public ProductEntity() {
    }

    public /* bridge */ /* synthetic */ long getId() {
        return super.getId();
    }

    public /* bridge */ /* synthetic */ void setId(long j) {
        super.setId(j);
    }

    public String getCat_id() {
        return this.cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getSub_cat_id() {
        return this.sub_cat_id;
    }

    public void setSub_cat_id(String sub_cat_id) {
        this.sub_cat_id = sub_cat_id;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBrand_id() {
        return this.brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getModel_no() {
        return this.model_no;
    }

    public void setModel_no(String model_no) {
        this.model_no = model_no;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getFeatures() {
        return this.features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getImage1() {
        return this.image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return this.image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return this.image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return this.image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getImage5() {
        return this.image5;
    }

    public void setImage5(String image5) {
        this.image5 = image5;
    }
}
