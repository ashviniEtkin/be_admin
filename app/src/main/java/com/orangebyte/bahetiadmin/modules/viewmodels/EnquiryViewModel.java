package com.orangebyte.bahetiadmin.modules.viewmodels;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider.NewInstanceFactory;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.modules.db.local.AppDatabase;
import com.orangebyte.bahetiadmin.modules.db.local.DatabaseCreator;
import com.orangebyte.bahetiadmin.modules.entities.EnquiryEntity;
import com.orangebyte.bahetiadmin.modules.repository.EnquiryRepository;
import com.orangebyte.bahetiadmin.modules.repository.RemoteRepository;
import com.orangebyte.bahetiadmin.modules.ui.AllContract.EnquiryStreams;

import java.util.List;

import im.delight.android.webview.BuildConfig;

public class EnquiryViewModel extends ViewModel implements EnquiryStreams {
    private EnquiryRepository enquiryRepository;

    public static class EnquiryFactory extends NewInstanceFactory {
        EnquiryRepository pProductRepository;

        public EnquiryFactory() {
            this(null, null);
        }

        public EnquiryFactory(AppDatabase appDatabase) {
            this(EnquiryRepository.getInstance(RemoteRepository.getInstance(), appDatabase.enquiryDAO()), BuildConfig.VERSION_NAME);
        }

        public EnquiryFactory(@Nullable EnquiryRepository pProductRepository, @Nullable String abc) {
            this.pProductRepository = pProductRepository;
        }

        public <T extends ViewModel> T create(Class<T> cls) {
            return (T) new EnquiryViewModel(this.pProductRepository);
        }
    }

    public EnquiryViewModel() {
        this(null);
    }

    public EnquiryViewModel(@Nullable EnquiryRepository enquiryRepository) {
        if (enquiryRepository != null) {
            this.enquiryRepository = enquiryRepository;
        }
    }

    public LiveData<List<EnquiryEntity>> getEnquiry(EnquiryEntity enquiryEntity) {
        return subscribeToLocalData(enquiryEntity);
    }

    public LiveData<List<Boolean>> chngEnquiry(EnquiryEntity enquiryEntity, int chk) {
        return changeEnqStatus(enquiryEntity, chk);
    }

    private LiveData<List<EnquiryEntity>> subscribeToLocalData(final EnquiryEntity productEntity) {
        return Transformations.switchMap(DatabaseCreator.getInstance().isDatabaseCreated(), new Function<Boolean, LiveData<List<EnquiryEntity>>>() {
            public LiveData<List<EnquiryEntity>> apply(Boolean isDbCreated) {
                if (!Boolean.TRUE.equals(isDbCreated)) {
                    return null;
                }
                EnquiryViewModel.this.onDatabaseCreated();
                return EnquiryViewModel.this.subscribeProductObservable(productEntity);
            }
        });
    }

    private LiveData<List<Boolean>> changeEnqStatus(final EnquiryEntity productEntity, final int chk) {
        return Transformations.switchMap(DatabaseCreator.getInstance().isDatabaseCreated(), new Function<Boolean, LiveData<List<Boolean>>>() {
            public LiveData<List<Boolean>> apply(Boolean isDbCreated) {
                if (!Boolean.TRUE.equals(isDbCreated)) {
                    return null;
                }
                EnquiryViewModel.this.onDatabaseCreated();
                return EnquiryViewModel.this.changeEnquiryStatus(productEntity, chk);
            }
        });
    }

    private LiveData<List<EnquiryEntity>> subscribeProductObservable(EnquiryEntity productEntity) {
        return this.enquiryRepository.loadEnquiryList(productEntity);
    }

    private LiveData<List<Boolean>> changeEnquiryStatus(EnquiryEntity productEntity, int chk) {
        return this.enquiryRepository.actionEnquiry(productEntity, chk);
    }

    private void onDatabaseCreated() {
        AppDatabase appDatabase = DatabaseCreator.getInstance().getDatabase();
        try {
            if (this.enquiryRepository == null) {
                this.enquiryRepository = EnquiryRepository.getInstance(RemoteRepository.getInstance(), appDatabase.enquiryDAO());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
