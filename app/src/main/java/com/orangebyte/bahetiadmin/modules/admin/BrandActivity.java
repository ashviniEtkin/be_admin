package com.orangebyte.bahetiadmin.modules.admin;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.adapters.BrandAdapter;
import com.orangebyte.bahetiadmin.modules.adapters.BrandAdapter.ClickListener;
import com.orangebyte.bahetiadmin.modules.entities.BrandEntity;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener.OnItemClickListener;
import com.orangebyte.bahetiadmin.modules.viewmodels.BrandViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.BrandViewModel.BrandFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog.OnSweetClickListener;
import com.orangebyte.bahetiadmin.views.CircleImageView;
import java.util.ArrayList;
import java.util.List;

public class BrandActivity extends AppCompatActivity implements LifecycleRegistryOwner, ClickListener {
    BrandAdapter adapter;
    List<BrandEntity> brandEntityList = new ArrayList();
    BrandViewModel brandViewModel;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    RecyclerView recy_brand;
    SweetAlertDialog sweetAlertDialog;
    private Toolbar toolbarView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand);
        BahetiEnterprises.setActBar(getResources().getString(R.string.brand_list), this, false);
        initViews();
        listeners();
    }

    public void listeners() {
        this.recy_brand.addOnItemTouchListener(new RecyclerItemClickListener(this, new OnItemClickListener() {
            public void onItemClick(View view, final int position) {
                ImageButton status = (ImageButton) view.findViewById(R.id.chng_status);
                ((ImageButton) view.findViewById(R.id.del)).setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        final BrandEntity ce = (BrandEntity) BrandActivity.this.brandEntityList.get(position);
                        BrandActivity.this.sweetAlertDialog.changeAlertType(3);
                        BrandActivity.this.sweetAlertDialog.setTitleText("Are You sure you want to delete?\n").setContentText("\n").setConfirmText("Yes,delete it!").setCancelText("No").setConfirmClickListener(new OnSweetClickListener() {
                            public void onClick(final SweetAlertDialog sDialog) {
                                BrandActivity.this.brandViewModel.deleteBrandById(ce).observe(BrandActivity.this, new Observer<List<BrandEntity>>() {
                                    public void onChanged(@Nullable final List<BrandEntity> categoryEntities) {
                                        if (categoryEntities.isEmpty()) {
                                            sDialog.show();
                                            return;
                                        }
                                        sDialog.showCancelButton(false);
                                        sDialog.setTitleText("Deleted!").setContentText(ce.getBrand_nm() + " has been deleted!").setConfirmText("OK").setConfirmClickListener(new OnSweetClickListener() {
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.dismiss();
                                                BrandActivity.this.showSubCategoryListInUit(categoryEntities);
                                            }
                                        }).changeAlertType(2);
                                    }
                                });
                            }
                        }).setCancelClickListener(new OnSweetClickListener() {
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        }).show();
                    }
                });
                status.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        BrandActivity.this.sweetAlertDialog.changeAlertType(3);
                        if (((BrandEntity) BrandActivity.this.brandEntityList.get(position)).getStatus().equalsIgnoreCase("1")) {
                            BrandActivity.this.sweetAlertDialog.setTitleText("Are You sure you want to hide?\n");
                        } else {
                            BrandActivity.this.sweetAlertDialog.setTitleText("Are You sure you want to unhide?\n");
                        }
                        BrandActivity.this.sweetAlertDialog.setCancelText("NO").setConfirmText("YES").setContentText("\n");
                        BrandActivity.this.sweetAlertDialog.setConfirmClickListener(new OnSweetClickListener() {
                            public void onClick(final SweetAlertDialog sweetAlertDialog) {
                                BrandEntity sce1 = (BrandEntity) BrandActivity.this.brandEntityList.get(position);
                                if (((BrandEntity) BrandActivity.this.brandEntityList.get(position)).getStatus().equalsIgnoreCase("1")) {
                                    sce1.setStatus("0");
                                    BrandActivity.this.brandViewModel.updateBrandStatus(sce1).observe(BrandActivity.this, new Observer<List<BrandEntity>>() {
                                        public void onChanged(@Nullable List<BrandEntity> categoryEntities) {
                                            if (categoryEntities.isEmpty()) {
                                                sweetAlertDialog.show();
                                                return;
                                            }
                                            sweetAlertDialog.dismiss();
                                            BrandActivity.this.showSubCategoryListInUit(categoryEntities);
                                        }
                                    });
                                    return;
                                }
                                sce1.setStatus("1");
                                BrandActivity.this.brandViewModel.updateBrandStatus(sce1).observe(BrandActivity.this, new Observer<List<BrandEntity>>() {
                                    public void onChanged(@Nullable List<BrandEntity> categoryEntities) {
                                        if (categoryEntities.isEmpty()) {
                                            sweetAlertDialog.show();
                                            return;
                                        }
                                        sweetAlertDialog.dismiss();
                                        BrandActivity.this.showSubCategoryListInUit(categoryEntities);
                                    }
                                });
                            }
                        }).show();
                        BrandActivity.this.onResume();
                    }
                });
            }
        }));
    }

    protected void onResume() {
        super.onResume();
        initViewModel();
    }

    private void initViews() {
        this.toolbarView = (Toolbar) findViewById(R.id.toolbar);
        this.sweetAlertDialog = new SweetAlertDialog(this);
        this.sweetAlertDialog.setCancelable(false);
        this.recy_brand = (RecyclerView) findViewById(R.id.recy_brand);
        if (VERSION.SDK_INT >= 9) {
            this.recy_brand.setHasFixedSize(true);
        }
        if (VERSION.SDK_INT >= 9) {
            this.recy_brand.setLayoutManager(new LinearLayoutManager(this));
        }
        this.adapter = new BrandAdapter(this, this);
        this.recy_brand.setAdapter(this.adapter);
        ((CircleImageView) findViewById(R.id.fab)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(BrandActivity.this, AddBrandActivity.class);
                i.putExtra(BahetiEnterprises.CHK_ADD_UPDATE, "0");
                BrandActivity.this.startActivity(i);
            }
        });
    }

    private void initViewModel() {
        this.brandViewModel = (BrandViewModel) ViewModelProviders.of(this, new BrandFactory()).get(BrandViewModel.class);
        subscribeToDataStreams(this.brandViewModel);
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    public void onItemClicked(BrandEntity subCategoryEntity) {
    }

    private void subscribeToDataStreams(BrandViewModel viewModel) {
        try {
            this.sweetAlertDialog = new SweetAlertDialog(this, 5);
            this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
            this.sweetAlertDialog.setTitleText("Loading");
            this.sweetAlertDialog.setCancelable(false);
            this.sweetAlertDialog.show();
            viewModel.getBrands().observe(this, new Observer<List<BrandEntity>>() {
                public void onChanged(@Nullable List<BrandEntity> brandEntityList) {
                    if (!brandEntityList.isEmpty()) {
                        BrandActivity.this.sweetAlertDialog.dismiss();
                        BrandActivity.this.showSubCategoryListInUit(brandEntityList);
                    } else if (brandEntityList.size() == 0) {
                        BrandActivity.this.sweetAlertDialog.dismiss();
                    } else {
                        BrandActivity.this.sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showSubCategoryListInUit(List<BrandEntity> brandEntityList) {
        this.brandEntityList = brandEntityList;
        this.adapter.setItems(brandEntityList);
    }
}
