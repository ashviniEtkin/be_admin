package com.orangebyte.bahetiadmin.modules.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.admin.AddManagerActivity;
import com.orangebyte.bahetiadmin.modules.admin.ManagerActivity;
import com.orangebyte.bahetiadmin.modules.entities.ManagerEntity;

import java.util.ArrayList;
import java.util.List;

public class ManagerAdapter extends Adapter<ManagerAdapter.ManagerHolder> {
    Context ctx;
    List<ManagerEntity> dataSet = new ArrayList();
    ClickListener listener;

    SharedPreferences sh;
    String post = "";

    public interface ClickListener {
        void onItemClicked(ManagerEntity managerEntity);
    }

    public class ManagerHolder extends ViewHolder {
        ImageButton chng_status, edit;
        TextView mgr_nm;
        TextView mgr_pass;
        TextView mgr_type;
        TextView srl_no;
        LinearLayout manager_container, password_container;

        public ManagerHolder(View itemView) {
            super(itemView);
            this.mgr_pass = (TextView) itemView.findViewById(R.id.mgr_pass);
            this.srl_no = (TextView) itemView.findViewById(R.id.srl_no);
            this.mgr_nm = (TextView) itemView.findViewById(R.id.mgr_nm);
            this.mgr_type = (TextView) itemView.findViewById(R.id.mgr_type);
            this.chng_status = (ImageButton) itemView.findViewById(R.id.chng_status);
            edit = (ImageButton) itemView.findViewById(R.id.mgr_edit);
            manager_container = (LinearLayout) itemView.findViewById(R.id.manager_container);
            password_container = (LinearLayout) itemView.findViewById(R.id.password_container);
        }
    }

    public ManagerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ManagerHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.manager_row, parent, false));
    }

    public void onBindViewHolder(ManagerHolder holder, final int position) {


        holder.srl_no.setText((position + 1) + ".");
        holder.mgr_type.setText(((ManagerEntity) this.dataSet.get(position)).getMgr_type().toString().trim());
        holder.mgr_nm.setText(((ManagerEntity) this.dataSet.get(position)).getName().toString().trim());
        holder.mgr_pass.setText(((ManagerEntity) this.dataSet.get(position)).getPassword().toString().trim());
        if (((ManagerEntity) this.dataSet.get(position)).getStatus().equalsIgnoreCase("1")) {
            holder.chng_status.setImageResource(R.drawable.ic_unhide);
            holder.chng_status.setColorFilter(this.ctx.getResources().getColor(R.color.app_color));
            return;
        }
        holder.chng_status.setImageResource(R.drawable.ic_hide);
        holder.chng_status.setColorFilter(this.ctx.getResources().getColor(R.color.black));



       /* try {
            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx, AddManagerActivity.class);
                    i.putExtra(BahetiEnterprises.CHK_ADD_UPDATE, "1");
                    i.putExtra(BahetiEnterprises.FROM, dataSet.get(position));
                    ctx.startActivity(i);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            holder.manager_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx, AddManagerActivity.class);
                    i.putExtra(BahetiEnterprises.CHK_ADD_UPDATE, "1");
                    i.putExtra(BahetiEnterprises.FROM, dataSet.get(position));
                    ctx.startActivity(i);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
*/

    }

    public void setItems(List<ManagerEntity> itemsList) {
        this.dataSet.clear();
        this.dataSet.addAll(itemsList);
        notifyDataSetChanged();
    }

    public ManagerAdapter(ClickListener listener, Context ctx, List<ManagerEntity> dataSet) {
        this.listener = listener;
        this.dataSet = dataSet;
        this.ctx = ctx;
    }

    public int getItemCount() {
        return this.dataSet.size();
    }
}
