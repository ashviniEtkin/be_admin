package com.orangebyte.bahetiadmin.modules.admin;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.entities.AdminEntity;
import com.orangebyte.bahetiadmin.modules.entities.ForgetEntity;
import com.orangebyte.bahetiadmin.modules.entities.UserEntity;
import com.orangebyte.bahetiadmin.modules.models.ServicesResponse;
import com.orangebyte.bahetiadmin.modules.repository.ApiServices;
import com.orangebyte.bahetiadmin.modules.repository.ServiceFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPassword extends AppCompatActivity {

    Spinner post;
    EditText mobile_number;
    Button btn_fg_psd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        post = (Spinner) findViewById(R.id.post);
        mobile_number = (EditText) findViewById(R.id.mobile_number);
        btn_fg_psd = (Button) findViewById(R.id.btn_fg_psd);
        btn_fg_psd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (post.getSelectedItemPosition() == 0) {
                    BahetiEnterprises.retShowAlertDialod("Select Post", ForgetPassword.this);
                } else if (TextUtils.isEmpty(mobile_number.getText().toString())) {
                    BahetiEnterprises.retShowAlertDialod("Enter Mobile Number", ForgetPassword.this);
                } else {
                    checkMobileNumber();
                }
            }
        });
    }

    public void checkMobileNumber() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.getProgressHelper().setBarColor((getResources().getColor(R.color.app_color)));
        sweetAlertDialog.setTitleText("Please Wait");
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();
        AdminEntity userEntity = new AdminEntity();
        userEntity.setPost(post.getSelectedItem().toString());
        userEntity.setMobile_no(mobile_number.getText().toString());
        //   userList = getUserByname(userEntity);


        ApiServices myApiServices = ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/getUsersByName/");
        try {
            Call<ServicesResponse> call = myApiServices.forgetPassword(userEntity);
            call.enqueue(new Callback<ServicesResponse>() {
                @Override
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    sweetAlertDialog.dismiss();
                    try {
                        final ForgetEntity otp = response.body().getOtp().get(0);
                        if (!otp.getOtp().equalsIgnoreCase("")) {

                            final Dialog passwordDialog = new Dialog(ForgetPassword.this, R.style.Custom_Dialog);
                            passwordDialog.setCancelable(false);
                            passwordDialog.setContentView(R.layout.otp_dialog);

                            Button fp_btn;
                            final EditText edt_otp, pswd, conf_pswd;
                            final LinearLayout password_container, otp_container;
                            edt_otp = (EditText) passwordDialog.findViewById(R.id.edt_otp);
                            pswd = (EditText) passwordDialog.findViewById(R.id.pswd);
                            conf_pswd = (EditText) passwordDialog.findViewById(R.id.conf_pswd);
                            otp_container = (LinearLayout) passwordDialog.findViewById(R.id.otp_container);
                            password_container = (LinearLayout) passwordDialog.findViewById(R.id.password_container);
                            fp_btn = (Button) passwordDialog.findViewById(R.id.fp_btn);


                            fp_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (TextUtils.isEmpty(pswd.getText().toString())) {
                                        BahetiEnterprises.retShowAlertDialod("Please Enter New password", ForgetPassword.this);

                                    } else if (TextUtils.isEmpty(conf_pswd.getText().toString())) {
                                        BahetiEnterprises.retShowAlertDialod("Please Enter Confirm Password", ForgetPassword.this);

                                    } else if (!(pswd.getText().toString().equals(conf_pswd.getText().toString()))) {
                                        BahetiEnterprises.retShowAlertDialod("Please Password Doesn't Match", ForgetPassword.this);

                                    } else {
                                        AdminEntity adminEntity = new AdminEntity();
                                        adminEntity.setId(Long.parseLong(otp.getId()));
                                        adminEntity.setMobile_no(mobile_number.getText().toString());
                                        adminEntity.setPost(otp.getPost());
                                        adminEntity.setPassword(pswd.getText().toString());
                                        updatePassword(adminEntity);
                                        passwordDialog.dismiss();
                                    }

                                }
                            });

                            edt_otp.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                }

                                @Override
                                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                }

                                @Override
                                public void afterTextChanged(Editable editable) {

                                    if (edt_otp.getText().toString().length() == 6) {
                                        if (edt_otp.getText().toString().equalsIgnoreCase(otp.getOtp())) {
                                            otp_container.setVisibility(View.GONE);
                                            password_container.setVisibility(View.VISIBLE);
                                        } else {
                                            otp_container.setVisibility(View.VISIBLE);
                                            password_container.setVisibility(View.GONE);
                                            BahetiEnterprises.retShowAlertDialod("Please Enter Valid Otp", ForgetPassword.this);
                                        }
                                    }
                                }
                            });

                            passwordDialog.show();

                        } else {
                            BahetiEnterprises.retShowAlertDialod(post.getSelectedItem().toString() + " not exist", ForgetPassword.this);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        BahetiEnterprises.retShowAlertDialod(post.getSelectedItem().toString() + " not exist", ForgetPassword.this);

                    }

                }

                @Override
                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    t.printStackTrace();
                    sweetAlertDialog.dismiss();
                }


            });
        } catch (Exception e) {
            sweetAlertDialog.dismiss();
            e.printStackTrace();
        }


    }

    public void updatePassword(AdminEntity adminEntity) {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.getProgressHelper().setBarColor((getResources().getColor(R.color.app_color)));
        sweetAlertDialog.setTitleText("Please Wait");
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();
        try {
            //   RequestBody body = RequestBody.create(MediaType.parse("application/json"), adminEntity.toString());


            ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/updateProfile/")
                    .updatePassword(adminEntity)
                    .enqueue(new Callback<ServicesResponse>() {
                        public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                            boolean t = (Boolean.valueOf(((ServicesResponse) response.body()).isSuccess()));
                            onBackPressed();
                            sweetAlertDialog.dismiss();
                        }

                        public void onFailure(Call<ServicesResponse> call, Throwable t) {
                            t.printStackTrace();
                            sweetAlertDialog.dismiss();
                        }
                    });
        } catch (Exception e) {
            sweetAlertDialog.dismiss();
            e.printStackTrace();
        }
    }
}
