package com.orangebyte.bahetiadmin.modules.models;

public interface EmployeeModel extends IdentifiableModel {
    String getAssigned_mgr_type();

    String getEmp_nm();

    String getMgr_id();

    String getMob_no();

    String getStatus();
}
