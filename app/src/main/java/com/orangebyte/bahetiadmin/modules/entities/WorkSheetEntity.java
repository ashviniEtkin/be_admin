package com.orangebyte.bahetiadmin.modules.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "emp_worksheet")
public class WorkSheetEntity extends IdentifiableEntity implements Serializable {
    @SerializedName("emp_id")
    @ColumnInfo(name = "emp_id")
    String emp_id;
    @SerializedName("emp_job")
    @ColumnInfo(name = "emp_job")
    String emp_job;
    @SerializedName("job_status")
    @ColumnInfo(name = "job_status")
    String job_status;
    @SerializedName("ondate")
    @ColumnInfo(name = "ondate")
    String ondate;
    @SerializedName("product_id")
    @ColumnInfo(name = "product_id")
    String product_id;
    @SerializedName("resolved_date")
    @ColumnInfo(name = "resolved_date")
    String resolved_date;
    @SerializedName("user_id")
    @ColumnInfo(name = "user_id")
    String user_id;

    public WorkSheetEntity() {
    }

    public /* bridge */ /* synthetic */ long getId() {
        return super.getId();
    }

    public /* bridge */ /* synthetic */ void setId(long j) {
        super.setId(j);
    }

    public String getEmp_id() {
        return this.emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getEmp_job() {
        return this.emp_job;
    }

    public void setEmp_job(String emp_job) {
        this.emp_job = emp_job;
    }

    public String getJob_status() {
        return this.job_status;
    }

    public void setJob_status(String job_status) {
        this.job_status = job_status;
    }

    public String getOndate() {
        return this.ondate;
    }

    public void setOndate(String ondate) {
        this.ondate = ondate;
    }

    public String getProduct_id() {
        return this.product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getResolved_date() {
        return this.resolved_date;
    }

    public void setResolved_date(String resolved_date) {
        this.resolved_date = resolved_date;
    }

    public String getUser_id() {
        return this.user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
