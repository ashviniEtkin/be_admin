package com.orangebyte.bahetiadmin.modules.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.db.daos.SubCategoryDAO;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;

import java.util.List;

public class SubCategoryRepository {
    private static SubCategoryRepository sInstance;
    private final RemoteRepository remoteRepository;
    private final SubCategoryDAO subCategoryDAO;

    interface RemoteSubCategoryListener {
        void onCheckResult(boolean z);

        void onRemoteCategoryReceived(SubCategoryEntity subCategoryEntity);

        void onRemoteCategoryReceived(List<SubCategoryEntity> list);
    }

    interface TaskListener {
        void onTaskFinished();
    }

    public static SubCategoryRepository getInstance(RemoteRepository remoteRepository, SubCategoryDAO subCategoryDAO) {
        if (sInstance == null) {
            sInstance = new SubCategoryRepository(remoteRepository, subCategoryDAO);
        }
        return sInstance;
    }

    private SubCategoryRepository(RemoteRepository remoteRepository, SubCategoryDAO subCategoryDAO) {
        this.remoteRepository = remoteRepository;
        this.subCategoryDAO = subCategoryDAO;
    }

    public LiveData<List<SubCategoryEntity>> loadSubCategoryList() {
        fetchSubCategoryList(new RemoteSubCategoryListener() {
            public void onRemoteCategoryReceived(SubCategoryEntity cmEntity) {
            }

            public void onRemoteCategoryReceived(List<SubCategoryEntity> scmEntities) {
                if (scmEntities != null) {
                    SubCategoryRepository.this.insertAllTask(scmEntities);
                }
            }

            public void onCheckResult(boolean b) {
            }
        });
        return this.subCategoryDAO.loadAll();
    }

    public LiveData<List<SubCategoryEntity>> addSubCatgory(final SubCategoryEntity c, final int chk) {
        addSubCate(c, chk, new RemoteSubCategoryListener() {
            public void onRemoteCategoryReceived(SubCategoryEntity cmEntity) {
            }

            public void onRemoteCategoryReceived(List<SubCategoryEntity> list) {
            }

            public void onCheckResult(boolean b) {
                SubCategoryEntity cm = new SubCategoryEntity();
                cm.setSub_cat_name(c.getSubCatName());
                cm.setCat_id(c.getCatId());
                cm.setStatus(c.getStatus());
                if (b && chk == 0) {
                    SubCategoryRepository.this.insertSubCategory(cm);
                } else if (b && chk == 1) {
                    cm.setId(c.getId());
                    SubCategoryRepository.this.updateSubCategory(cm);
                }
            }
        });
        return this.subCategoryDAO.loadAll();
    }

    public LiveData<List<SubCategoryEntity>> updateSubCatStatus(final SubCategoryEntity c) {
        updateSubCatStatus(c, new RemoteSubCategoryListener() {
            public void onRemoteCategoryReceived(SubCategoryEntity cmEntity) {
            }

            public void onRemoteCategoryReceived(List<SubCategoryEntity> list) {
            }

            public void onCheckResult(boolean b) {
                if (b) {
                    SubCategoryEntity cm = new SubCategoryEntity();
                    cm.setId(c.getId());
                    cm.setSub_cat_name(c.getSubCatName());
                    cm.setCat_id(c.getCatId());
                    cm.setStatus(c.getStatus());
                    SubCategoryRepository.this.updateSubCategory(cm);
                }
            }
        });
        return this.subCategoryDAO.loadAll();
    }

    public LiveData<List<SubCategoryEntity>> deleteCategoryByID(final SubCategoryEntity c) {
        deleteCategoryByID(c, new RemoteSubCategoryListener() {
            public void onRemoteCategoryReceived(SubCategoryEntity cmEntity) {
            }

            public void onRemoteCategoryReceived(List<SubCategoryEntity> list) {
            }

            public void onCheckResult(boolean b) {
                if (b) {
                    SubCategoryEntity cm = new SubCategoryEntity();
                    cm.setId(c.getId());
                    cm.setSub_cat_name(c.getSubCatName());
                    cm.setCat_id(c.getCatId());
                    cm.setStatus(c.getStatus());
                    SubCategoryRepository.this.deleteSubCategory(cm);
                }
            }
        });
        return this.subCategoryDAO.loadAll();
    }

    private void insertAllTask(List<SubCategoryEntity> catEntity) {
        insertAllTask(catEntity, null);
    }

    private void insertAllTask(final List<SubCategoryEntity> catEntity, @Nullable final TaskListener listener) {
        if (VERSION.SDK_INT >= 3) {
            new AsyncTask<Context, Void, Void>() {
                protected Void doInBackground(Context... params) {
                    SubCategoryRepository.this.subCategoryDAO.deleteAll();
                    SubCategoryRepository.this.subCategoryDAO.insertAll(catEntity);
                    return null;
                }

                protected void onPostExecute(Void aVoid) {
                    if (VERSION.SDK_INT >= 3) {
                        super.onPostExecute(aVoid);
                    }
                    if (listener != null) {
                        listener.onTaskFinished();
                    }
                }
            }.execute(new Context[]{BahetiEnterprises.getInstance()});
        }
    }

    private void insertSubCategory(SubCategoryEntity catEntity) {
        insertSubCategory(catEntity, null);
    }

    private void insertSubCategory(final SubCategoryEntity subCategoryEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                SubCategoryRepository.this.subCategoryDAO.insert(subCategoryEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteSubCategory(SubCategoryEntity subCategoryEntity) {
        deleteSubCategory(subCategoryEntity, null);
    }

    private void deleteSubCategory(final SubCategoryEntity subCategoryEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                SubCategoryRepository.this.subCategoryDAO.delete(subCategoryEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void updateSubCategory(SubCategoryEntity subCategoryEntity) {
        updateSubCategory(subCategoryEntity, null);
    }

    private void updateSubCategory(final SubCategoryEntity subCategoryEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                SubCategoryRepository.this.subCategoryDAO.update(subCategoryEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteAllSubCategories() {
        deleteAllSubCategories(null);
    }

    private void deleteAllSubCategories(@Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                SubCategoryRepository.this.subCategoryDAO.deleteAll();
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void fetchSubCategoryList(@NonNull final RemoteSubCategoryListener listener) {
        this.remoteRepository.getSubCategoryList(new RemoteCallback<List<SubCategoryEntity>>() {
            public void onSuccess(List<SubCategoryEntity> response) {
                listener.onRemoteCategoryReceived((List) response);
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }

    private void addSubCate(@NonNull SubCategoryEntity c, @NonNull int chk, @NonNull final RemoteSubCategoryListener listener) {
        if (chk == 1) {
            this.remoteRepository.updateSubCategory(c, new RemoteCallback<Boolean>() {
                public void onSuccess(Boolean response) {
                    listener.onCheckResult(response.booleanValue());
                }

                public void onUnauthorized() {
                }

                public void onFailed(Throwable throwable) {
                }
            });
        } else {
            this.remoteRepository.addSubCat(c, new RemoteCallback<Boolean>() {
                public void onSuccess(Boolean response) {
                    listener.onCheckResult(response.booleanValue());
                }

                public void onUnauthorized() {
                }

                public void onFailed(Throwable throwable) {
                }
            });
        }
    }

    private void updateSubCatStatus(@NonNull SubCategoryEntity c, @NonNull final RemoteSubCategoryListener listener) {
        this.remoteRepository.updateSubCatStatus(c, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }

    private void deleteCategoryByID(@NonNull SubCategoryEntity c, @NonNull final RemoteSubCategoryListener listener) {
        this.remoteRepository.deleteSubCategoryByID(c, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }
}
