package com.orangebyte.bahetiadmin.modules.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.admin.CallLogDetails;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;

import java.io.Serializable;
import java.util.List;

public class CallLogInstallAdapter extends Adapter<CallLogInstallAdapter.CallLogHolder> {
    List<CallLogInstallationDemoEntity> callLogInstallationDemoEntities;
    CheckBox cb;
    int chk;
    Context ctx;

    public class CallLogHolder extends ViewHolder {
        TextView asign_to;
        CheckBox checkb;
        TextView comp_date;
        TextView comp_desc;
        TextView comp_status;
        CardView container_id;
        TextView cust_nm;
        Button btn_assign_transfer;
        LinearLayout btn_layout;
        TextView service_request_no;
        LinearLayout service_no_container;


        public CallLogHolder(View itemView) {
            super(itemView);
            this.asign_to = (TextView) itemView.findViewById(R.id.asign_to);
            this.cust_nm = (TextView) itemView.findViewById(R.id.cust_nm);
            this.comp_desc = (TextView) itemView.findViewById(R.id.comp_desc);
            this.comp_date = (TextView) itemView.findViewById(R.id.comp_date);
            this.comp_status = (TextView) itemView.findViewById(R.id.comp_status);
            this.container_id = (CardView) itemView.findViewById(R.id.container_id);
            this.checkb = (CheckBox) itemView.findViewById(R.id.checkb);
            this.btn_layout = (LinearLayout) itemView.findViewById(R.id.btn_layout);
            this.btn_assign_transfer = (Button) itemView.findViewById(R.id.btn_assign_transfer);
            service_no_container = (LinearLayout) itemView.findViewById(R.id.service_no_container);
            service_request_no = (TextView) itemView.findViewById(R.id.service_request_no);
            service_no_container.setVisibility(View.VISIBLE);

        }
    }

    public boolean checkedAllSelected(List<CallLogInstallationDemoEntity> callLogInstallationDemoEntityList) {
        boolean s = false;
        int f = 0;
        for (int y = 0; y < callLogInstallationDemoEntityList.size(); y++) {
            if (callLogInstallationDemoEntityList.get(y).isSelected()) {
                f++;
            }
        }

        if (f == callLogInstallationDemoEntityList.size()) {
            return true;
        } else {
            return false;
        }

    }

    public List<CallLogInstallationDemoEntity> getCallLogInstallationDemoEntities() {
        return this.callLogInstallationDemoEntities;
    }

    public void setCallLogInstallationDemoEntities(List<CallLogInstallationDemoEntity> callLogInstallationDemoEntities) {
        this.callLogInstallationDemoEntities = callLogInstallationDemoEntities;
    }

    public CallLogInstallAdapter(List<CallLogInstallationDemoEntity> callLogInstallationDemoEntities, Context ctx, int chk, CheckBox cb) {
        this.callLogInstallationDemoEntities = callLogInstallationDemoEntities;
        this.ctx = ctx;
        this.chk = chk;
        this.cb = cb;
    }

    public CallLogHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CallLogHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.complaint_row, parent, false));
    }

    public void onBindViewHolder(CallLogHolder holder, final int position) {
        holder.cust_nm.setText(((CallLogInstallationDemoEntity) this.callLogInstallationDemoEntities.get(position)).getUsernm());
        holder.comp_desc.setText(((CallLogInstallationDemoEntity) this.callLogInstallationDemoEntities.get(position)).getDescrip());
        holder.comp_date.setText(BahetiEnterprises.retDate(((CallLogInstallationDemoEntity) this.callLogInstallationDemoEntities.get(position)).getDemodate()));
        holder.comp_status.setText(((CallLogInstallationDemoEntity) this.callLogInstallationDemoEntities.get(position)).getStatus());
        holder.service_request_no.setText(callLogInstallationDemoEntities.get(position).getCom_no());



        try {
            holder.asign_to.setText(((CallLogInstallationDemoEntity) this.callLogInstallationDemoEntities.get(position)).getEmp_nm());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (((CallLogInstallationDemoEntity) this.callLogInstallationDemoEntities.get(position)).getStatus().equalsIgnoreCase("Pending")) {
            if (BahetiEnterprises.showInRed(Long.parseLong(((CallLogInstallationDemoEntity) this.callLogInstallationDemoEntities.get(position)).getDemodate()))) {
                holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.red_900));
            } else {
                holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.red_100));
            }
            holder.btn_assign_transfer.setVisibility(View.VISIBLE);

        } else if (((CallLogInstallationDemoEntity) this.callLogInstallationDemoEntities.get(position)).getStatus().equalsIgnoreCase("On Hold")) {
            holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.green_100));
            holder.btn_assign_transfer.setVisibility(View.VISIBLE);

        } else if (((CallLogInstallationDemoEntity) this.callLogInstallationDemoEntities.get(position)).getStatus().equalsIgnoreCase("In Progress")) {
            holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.teal_100));
            holder.btn_assign_transfer.setVisibility(View.VISIBLE);

        } else {
            holder.container_id.setCardBackgroundColor(this.ctx.getResources().getColor(R.color.amber_200));
            holder.btn_assign_transfer.setVisibility(View.GONE);

        }
        if (this.chk == 1) {
            holder.btn_layout.setVisibility(View.VISIBLE);
            holder.checkb.setVisibility(View.VISIBLE);
        } else {
            holder.btn_layout.setVisibility(View.GONE);
            holder.checkb.setVisibility(View.GONE);
        }
        holder.container_id.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {

                Intent i = new Intent(CallLogInstallAdapter.this.ctx, CallLogDetails.class);
                if (CallLogInstallAdapter.this.chk == 2) {
                    i.putExtra(BahetiEnterprises.FROM, "2");
                }
                i.putExtra(BahetiEnterprises.COMPLAINT_DETAILS, (Serializable) CallLogInstallAdapter.this.callLogInstallationDemoEntities.get(position));
                CallLogInstallAdapter.this.ctx.startActivity(i);
            }
        });
        try {
            holder.checkb.setChecked(((CallLogInstallationDemoEntity) this.callLogInstallationDemoEntities.get(position)).isSelected());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        this.cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //CallLogInstallAdapter.this.setAllCheck(b);
                boolean g = checkedAllSelected(callLogInstallationDemoEntities);
                if (b == false && g == true) {
                    setAllCheck(b);
                } else if (b == false && g == false) {
                    // setAllCheck(b);
                } else if (b == true && g == false) {
                    setAllCheck(b);
                }


            }
        });
        holder.checkb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ((CallLogInstallationDemoEntity) CallLogInstallAdapter.this.callLogInstallationDemoEntities.get(position)).setSelected(isChecked);
                boolean g = checkedAllSelected(callLogInstallationDemoEntities);

                if (g) {
                    cb.setChecked(true);
                } else {
                    cb.setChecked(false);
                }
            }
        });
    }

    public boolean checkAll() {
        boolean a = false;
        int k = 0;
        while (k < this.callLogInstallationDemoEntities.size()) {
            if (((CallLogInstallationDemoEntity) this.callLogInstallationDemoEntities.get(k)).isSelected()) {
                a = true;
                k++;
            } else {
                a = false;
                k = this.callLogInstallationDemoEntities.size();
            }
        }
        return a;
    }

    public void setAllCheck(boolean a) {
        for (int i = 0; i < this.callLogInstallationDemoEntities.size(); i++) {
            ((CallLogInstallationDemoEntity) this.callLogInstallationDemoEntities.get(i)).setSelected(a);
        }
        notifyDataSetChanged();
    }

    public int getItemCount() {
        return this.callLogInstallationDemoEntities.size();
    }
}
