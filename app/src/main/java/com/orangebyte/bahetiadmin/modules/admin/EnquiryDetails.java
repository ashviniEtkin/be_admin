package com.orangebyte.bahetiadmin.modules.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.employee.EnquiryAction;
import com.orangebyte.bahetiadmin.modules.entities.EnquiryEntity;

import im.delight.android.webview.BuildConfig;

public class EnquiryDetails extends AppCompatActivity {
    TextView assigned_to;
    Button btn_ok;
    String chk = BuildConfig.VERSION_NAME;
    TextView comp_desc;
    TextView comp_status;
    TextView cust_address;
    TextView cust_comp_dt;
    TextView cust_mob_no;
    TextView cust_nm;
    EnquiryEntity enquiryEntity;
    TextView prod;
    TextView prod_category;
    TextView prod_model;
    TextView prod_subcat, comp_no;

    public static int chek_backpress = 0;

    @Override
    protected void onResume() {
        super.onResume();

        if (chek_backpress == 1) {
            onBackPressed();
            chek_backpress = 0;
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enquiry_details);
        BahetiEnterprises.setActBar(getResources().getString(R.string.enquiry), this, false);
        this.chk = getIntent().getStringExtra(BahetiEnterprises.FROM);
        initView();
        this.enquiryEntity = (EnquiryEntity) getIntent().getSerializableExtra(BahetiEnterprises.COMPLAINT_DETAILS);
        updateData();
    }

    public void initView() {
        comp_no = (TextView) findViewById(R.id.comp_no);
        this.cust_nm = (TextView) findViewById(R.id.cust_nm);
        this.cust_mob_no = (TextView) findViewById(R.id.cust_mob_no);
        this.cust_address = (TextView) findViewById(R.id.cust_address);
        this.cust_comp_dt = (TextView) findViewById(R.id.cust_comp_dt);
        this.prod_category = (TextView) findViewById(R.id.prod_category);
        this.prod_subcat = (TextView) findViewById(R.id.prod_subcat);
        this.prod = (TextView) findViewById(R.id.prod);
        this.prod_model = (TextView) findViewById(R.id.prod_model);
        this.comp_desc = (TextView) findViewById(R.id.comp_desc);
        this.comp_status = (TextView) findViewById(R.id.comp_status);
        this.assigned_to = (TextView) findViewById(R.id.assigned_to);
        this.btn_ok = (Button) findViewById(R.id.btn_ok);
        this.comp_desc.setMovementMethod(new ScrollingMovementMethod());
        this.btn_ok.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                try {
                    if (EnquiryDetails.this.chk.equalsIgnoreCase("2")) {
                        if (!enquiryEntity.getStatus().equalsIgnoreCase(getResources().getString(R.string.Resolved_Closed))) {

                            Intent enqIntent = new Intent(EnquiryDetails.this, EnquiryAction.class);
                            enqIntent.putExtra(BahetiEnterprises.FROM, EnquiryDetails.this.enquiryEntity);
                            EnquiryDetails.this.startActivity(enqIntent);
                            return;
                        }
                    }
                    EnquiryDetails.this.onBackPressed();
                } catch (Exception e) {
                    e.printStackTrace();
                    EnquiryDetails.this.onBackPressed();
                }
            }
        });
    }

    public void updateData() {
        BahetiEnterprises.setData(this.enquiryEntity.getCom_no(), this.comp_no);
        BahetiEnterprises.setData(this.enquiryEntity.getUsernm(), this.cust_nm);
        BahetiEnterprises.setData(this.enquiryEntity.getMobile_no(), this.cust_mob_no);
        BahetiEnterprises.setData(this.enquiryEntity.getAddress(), this.cust_address);
        BahetiEnterprises.setData(this.enquiryEntity.getOndate(), this.cust_comp_dt);
        BahetiEnterprises.setData(this.enquiryEntity.getDescrip(), this.comp_desc);
        BahetiEnterprises.setData(this.enquiryEntity.getStatus(), this.comp_status);
        BahetiEnterprises.setData(this.enquiryEntity.getEmp_nm(), this.assigned_to);
        BahetiEnterprises.setData(this.enquiryEntity.getTitle(), this.prod);
        BahetiEnterprises.setData(this.enquiryEntity.getModel_no(), this.prod_model);
        BahetiEnterprises.setData(this.enquiryEntity.getSub_cat_name(), this.prod_subcat);
        BahetiEnterprises.setData(this.enquiryEntity.getCat_nm(), this.prod_category);
    }
}
