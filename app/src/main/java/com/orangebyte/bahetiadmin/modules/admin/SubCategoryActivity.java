package com.orangebyte.bahetiadmin.modules.admin;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.adapters.SubCategoryAdapter;
import com.orangebyte.bahetiadmin.modules.adapters.SubCategoryAdapter.ClickListener;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener.OnItemClickListener;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.SubCategoryViewModel.SubCategoryFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog.OnSweetClickListener;
import com.orangebyte.bahetiadmin.views.CircleImageView;
import java.util.ArrayList;
import java.util.List;

public class SubCategoryActivity extends AppCompatActivity implements LifecycleRegistryOwner, ClickListener {
    SubCategoryAdapter adapter;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    RecyclerView recy_subcategory;
    SubCategoryViewModel subCatViewModel;
    List<SubCategoryEntity> subCategoryEntities1 = new ArrayList();
    SweetAlertDialog sweetAlertDialog;
    private Toolbar toolbarView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);
        BahetiEnterprises.setActBar(getResources().getString(R.string.subcategory_list), this, false);
        initViews();
        listeners();
    }

    public void listeners() {
        this.recy_subcategory.addOnItemTouchListener(new RecyclerItemClickListener(this, new OnItemClickListener() {
            public void onItemClick(View view, final int position) {
                ImageButton status = (ImageButton) view.findViewById(R.id.chng_status);
                ((ImageButton) view.findViewById(R.id.del)).setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        final SubCategoryEntity ce = (SubCategoryEntity) SubCategoryActivity.this.subCategoryEntities1.get(position);
                        SubCategoryActivity.this.sweetAlertDialog.changeAlertType(3);
                        SubCategoryActivity.this.sweetAlertDialog.setTitleText("Are You sure you want to delete?\n").setContentText("\n").setConfirmText("Yes,delete it!").setCancelText("No").setConfirmClickListener(new OnSweetClickListener() {
                            public void onClick(final SweetAlertDialog sDialog) {
                                SubCategoryActivity.this.subCatViewModel.deleteSubCategoryById(ce).observe(SubCategoryActivity.this, new Observer<List<SubCategoryEntity>>() {
                                    public void onChanged(@Nullable final List<SubCategoryEntity> categoryEntities) {
                                        if (categoryEntities.isEmpty()) {
                                            sDialog.show();
                                            return;
                                        }
                                        sDialog.showCancelButton(false);
                                        sDialog.setTitleText("Deleted!").setContentText(ce.getSubCatName() + " has been deleted!").setConfirmText("OK").setConfirmClickListener(new OnSweetClickListener() {
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.dismiss();
                                                SubCategoryActivity.this.showSubCategoryListInUit(categoryEntities);
                                            }
                                        }).changeAlertType(2);
                                    }
                                });
                            }
                        }).setCancelClickListener(new OnSweetClickListener() {
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        }).show();
                    }
                });
                status.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        SubCategoryActivity.this.sweetAlertDialog.changeAlertType(3);
                        if (((SubCategoryEntity) SubCategoryActivity.this.subCategoryEntities1.get(position)).getStatus().equalsIgnoreCase("1")) {
                            SubCategoryActivity.this.sweetAlertDialog.setTitleText("Are You sure you want to hide?\n");
                        } else {
                            SubCategoryActivity.this.sweetAlertDialog.setTitleText("Are You sure you want to unhide?\n");
                        }
                        SubCategoryActivity.this.sweetAlertDialog.setCancelText("NO").setConfirmText("YES").setContentText("\n");
                        SubCategoryActivity.this.sweetAlertDialog.setConfirmClickListener(new OnSweetClickListener() {
                            public void onClick(final SweetAlertDialog sweetAlertDialog) {
                                SubCategoryEntity sce1 = (SubCategoryEntity) SubCategoryActivity.this.subCategoryEntities1.get(position);
                                if (((SubCategoryEntity) SubCategoryActivity.this.subCategoryEntities1.get(position)).getStatus().equalsIgnoreCase("1")) {
                                    sce1.setStatus("0");
                                    SubCategoryActivity.this.subCatViewModel.updateSubCatStatus(sce1).observe(SubCategoryActivity.this, new Observer<List<SubCategoryEntity>>() {
                                        public void onChanged(@Nullable List<SubCategoryEntity> categoryEntities) {
                                            if (categoryEntities.isEmpty()) {
                                                sweetAlertDialog.show();
                                                return;
                                            }
                                            sweetAlertDialog.dismiss();
                                            SubCategoryActivity.this.showSubCategoryListInUit(categoryEntities);
                                        }
                                    });
                                    return;
                                }
                                sce1.setStatus("1");
                                SubCategoryActivity.this.subCatViewModel.updateSubCatStatus(sce1).observe(SubCategoryActivity.this, new Observer<List<SubCategoryEntity>>() {
                                    public void onChanged(@Nullable List<SubCategoryEntity> categoryEntities) {
                                        if (categoryEntities.isEmpty()) {
                                            sweetAlertDialog.show();
                                            return;
                                        }
                                        sweetAlertDialog.dismiss();
                                        SubCategoryActivity.this.showSubCategoryListInUit(categoryEntities);
                                    }
                                });
                            }
                        }).show();
                        SubCategoryActivity.this.onResume();
                    }
                });
            }
        }));
    }

    protected void onResume() {
        super.onResume();
        initViewModel();
    }

    private void initViews() {
        this.toolbarView = (Toolbar) findViewById(R.id.toolbar);
        this.sweetAlertDialog = new SweetAlertDialog(this);
        this.sweetAlertDialog.setCancelable(false);
        this.recy_subcategory = (RecyclerView) findViewById(R.id.recy_subcategory);
        if (VERSION.SDK_INT >= 9) {
            this.recy_subcategory.setHasFixedSize(true);
        }
        if (VERSION.SDK_INT >= 9) {
            this.recy_subcategory.setLayoutManager(new LinearLayoutManager(this));
        }
        this.adapter = new SubCategoryAdapter(this, this);
        this.recy_subcategory.setAdapter(this.adapter);
        ((CircleImageView) findViewById(R.id.fab)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(SubCategoryActivity.this, AddSUbCategory.class);
                i.putExtra(BahetiEnterprises.CHK_ADD_UPDATE, "0");
                SubCategoryActivity.this.startActivity(i);
            }
        });
    }

    private void initViewModel() {
        this.subCatViewModel = (SubCategoryViewModel) ViewModelProviders.of(this, new SubCategoryFactory()).get(SubCategoryViewModel.class);
        subscribeToDataStreams(this.subCatViewModel);
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    public void onItemClicked(SubCategoryEntity subCategoryEntity) {
    }

    private void subscribeToDataStreams(SubCategoryViewModel viewModel) {
        try {
            this.sweetAlertDialog = new SweetAlertDialog(this, 5);
            this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
            this.sweetAlertDialog.setTitleText("Loading");
            this.sweetAlertDialog.setCancelable(false);
            this.sweetAlertDialog.show();
            viewModel.getSubCats().observe(this, new Observer<List<SubCategoryEntity>>() {
                public void onChanged(@Nullable List<SubCategoryEntity> subCategoryEntities) {
                    if (!subCategoryEntities.isEmpty()) {
                        SubCategoryActivity.this.sweetAlertDialog.dismiss();
                        SubCategoryActivity.this.showSubCategoryListInUit(subCategoryEntities);
                    } else if (subCategoryEntities.size() == 0) {
                        SubCategoryActivity.this.sweetAlertDialog.dismiss();
                    } else {
                        SubCategoryActivity.this.sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showSubCategoryListInUit(List<SubCategoryEntity> subCategoryEntities) {
        this.subCategoryEntities1 = subCategoryEntities;
        this.adapter.setItems(subCategoryEntities);
    }
}
