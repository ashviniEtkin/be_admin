package com.orangebyte.bahetiadmin.modules.admin;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.expandableview.ExpandableRelativeLayout;
import com.orangebyte.bahetiadmin.modules.adapters.ComplaintServiceAdapter;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener.OnItemClickListener;
import com.orangebyte.bahetiadmin.modules.manager.LodgeComplaint;
import com.orangebyte.bahetiadmin.modules.viewmodels.ComplaintsViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.ComplaintsViewModel.ComplaintFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel.EmployeeFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import com.orangebyte.pdflibrary.PdfDocument;
import com.orangebyte.pdflibrary.viewRenderer.AbstractViewRenderer;
import com.squareup.picasso.Picasso;

import im.delight.android.webview.BuildConfig;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ComplaintServices extends AppCompatActivity implements LifecycleRegistryOwner {
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);

    ComplaintServiceAdapter adapter;
    ComplaintEntity ce;
    ComplaintsViewModel cnmplViewModel;
    List<ComplaintEntity> complaintEntityList = new ArrayList();
    RecyclerView complaint_recycler;
    ExpandableRelativeLayout date_expandble_view;
    List<EmployeeEntity> employeeEntityList = new ArrayList();
    Spinner employee_spinner;
    EditText etd_cust_mobile_no;
    ComplaintFactory factory;
    String from = BuildConfig.VERSION_NAME;
    TextView in_progress;
    String nm = "";

    LinearLayout list_container, calllog_oper_contnr;
    int mDay;
    int mMonth;
    private OnDateSetListener mSeleteDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            select_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    private OnDateSetListener mStartDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            start_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    private OnDateSetListener mToDate = new OnDateSetListener() {
        public void onDateSet(DatePicker arg0, int year, int month, int day) {
            to_date.setText(new StringBuilder().append(day).append("-").append(month + 1).append("-").append(year));
        }
    };
    int mYear;
    TextView no_result_fount;
    TextView on_hold;
    TextView pending;
    String post = BuildConfig.VERSION_NAME;
    ImageButton reset;
    TextView resolved_closed;
    TextView select_date;
    Spinner service_spinner;
    SharedPreferences sh;
    Button show, btn_reset;
    TextView start_date;
    Spinner status_spinner;
    SweetAlertDialog sweetAlertDialog;
    TextView to_date;
    TextView total;

    CheckBox all_cb;
    Button btn_assign;
    Button btn_new;
    Button btn_resolved;
    RadioGroup assigned_grp;
    RadioButton assigned, unassigned;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_services);
        BahetiEnterprises.setActBar(getResources().getString(R.string.Complaint_Service), this, false);
        this.from = getIntent().getStringExtra(BahetiEnterprises.FROM);
        initView();
        setEmployeeToSpinner();
        listeners();
        this.show.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {

                int selectedId = assigned_grp.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                RadioButton ass_uns = (RadioButton) findViewById(selectedId);

                if (status_spinner.getSelectedItemPosition() == 0) {
                    ce.setStatus("null");
                } else {
                    ce.setStatus(status_spinner.getSelectedItem().toString());
                }
                if (employee_spinner.getSelectedItemPosition() == 0) {
                    try {
                        if (ass_uns.getText().toString().equalsIgnoreCase(getString(R.string.assigned))) {
                            ce.setAssigned_empid("yes");

                        } else if (ass_uns.getText().toString().equalsIgnoreCase(getString(R.string.unassigned))) {
                            ce.setAssigned_empid("no");

                        } else {
                            ce.setAssigned_empid("null");
                        }
                    } catch (Exception e) {
                        ce.setAssigned_empid("null");
                        e.printStackTrace();
                    }
                } else if (BahetiEnterprises.checkSpinner(employee_spinner)) {
                    ce.setAssigned_empid("null");
                } else {
                    ce.setAssigned_empid(((EmployeeEntity) employeeEntityList.get(employee_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);
                }
                if (service_spinner.getSelectedItemPosition() == 0) {
                    ce.setService_type("null");
                } else {
                    ce.setService_type(service_spinner.getSelectedItem().toString());
                }
                if (TextUtils.isEmpty(etd_cust_mobile_no.getText().toString())) {
                    ce.setUser_id("null");
                } else {
                    ce.setUser_id(etd_cust_mobile_no.getText().toString());
                }
                if (!TextUtils.isEmpty(start_date.getText().toString()) && !TextUtils.isEmpty(to_date.getText().toString())) {
                    ce.setComp_date(BahetiEnterprises.tStamp(start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    ce.setLast_atnd_on(BahetiEnterprises.lasttStamp(to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                } else if (!TextUtils.isEmpty(start_date.getText().toString()) && TextUtils.isEmpty(to_date.getText().toString())) {
                    ce.setComp_date(BahetiEnterprises.tStamp(start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    ce.setLast_atnd_on(BahetiEnterprises.lasttStamp(start_date.getText().toString()) + BuildConfig.VERSION_NAME);
                } else if (TextUtils.isEmpty(start_date.getText().toString()) && !TextUtils.isEmpty(to_date.getText().toString())) {
                    ce.setComp_date(BahetiEnterprises.tStamp(to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    ce.setLast_atnd_on(BahetiEnterprises.lasttStamp(to_date.getText().toString()) + BuildConfig.VERSION_NAME);
                } else if (TextUtils.isEmpty(select_date.getText().toString())) {
                    ce.setComp_date("null");
                    ce.setLast_atnd_on("null");
                } else {
                    ce.setComp_date(BahetiEnterprises.tStamp(select_date.getText().toString()) + BuildConfig.VERSION_NAME);
                    ce.setLast_atnd_on(BahetiEnterprises.lasttStamp(select_date.getText().toString()) + BuildConfig.VERSION_NAME);
                }
                initViewModel(ce);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ActivityCompat.checkSelfPermission(ComplaintServices.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ComplaintServices.this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE
                    },
                    110);

        }
        all_cb.setChecked(false);
        if (ce.getStatus() != null) {
            initViewModel(ce);
        }
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    private void initViewModel(ComplaintEntity complaintEntity) {
        subscribeToDataStreams(this.cnmplViewModel, complaintEntity);
    }

    private void subscribeToDataStreams(ComplaintsViewModel viewModel, ComplaintEntity complaintEntity) {
        try {
            this.sweetAlertDialog.show();
            viewModel.getComplaints(complaintEntity).observe(this, new Observer<List<ComplaintEntity>>() {
                public void onChanged(@Nullable List<ComplaintEntity> complaintEntitiesList) {
                    if (!complaintEntitiesList.isEmpty()) {
                        sweetAlertDialog.dismiss();
                        list_container.setVisibility(View.VISIBLE);
                        no_result_fount.setVisibility(View.GONE);
                        if (from.equalsIgnoreCase("1")) {
                            calllog_oper_contnr.setVisibility(View.VISIBLE);
                        } else {
                            calllog_oper_contnr.setVisibility(View.GONE);
                        }
                        showCompListInUit(complaintEntitiesList);


                    } else if (complaintEntitiesList.size() == 0) {
                        sweetAlertDialog.dismiss();
                        list_container.setVisibility(View.GONE);
                        no_result_fount.setVisibility(View.VISIBLE);
                        calllog_oper_contnr.setVisibility(View.GONE);
                    } else {
                        sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showCompListInUit(List<ComplaintEntity> complaintEntityList) {
        this.complaintEntityList = complaintEntityList;
        int pend_cnt = getCont(complaintEntityList, getResources().getString(R.string.Pending));
        int on_hold_cnt = getCont(complaintEntityList, getResources().getString(R.string.On_Hold));
        int resolved_closed_cnt = getCont(complaintEntityList, getResources().getString(R.string.Resolved_Closed));
        int in_progress_cnt = getCont(complaintEntityList, getResources().getString(R.string.In_Progress));
        int all = ((pend_cnt + on_hold_cnt) + resolved_closed_cnt) + in_progress_cnt;
        this.pending.setText(getResources().getString(R.string.Pending) + " : " + pend_cnt);
        this.on_hold.setText(getResources().getString(R.string.On_Hold) + " : " + on_hold_cnt);
        this.resolved_closed.setText(getResources().getString(R.string.Resolved_Closed) + " : " + resolved_closed_cnt);
        this.in_progress.setText(getResources().getString(R.string.In_Progress) + " : " + in_progress_cnt);
        this.total.setText(getResources().getString(R.string.total) + " : " + all);
        if (this.from.equalsIgnoreCase("0")) {
            all_cb.setVisibility(View.GONE);
            this.adapter = new ComplaintServiceAdapter(complaintEntityList, this, 0, all_cb);
        } else if (this.from.equalsIgnoreCase("1")) {
            all_cb.setVisibility(View.VISIBLE);
            this.adapter = new ComplaintServiceAdapter(complaintEntityList, this, 1, all_cb);
        } else {
            all_cb.setVisibility(View.GONE);
            this.adapter = new ComplaintServiceAdapter(complaintEntityList, this, 2, all_cb);
        }
        this.complaint_recycler.setLayoutManager(new LinearLayoutManager(this));
        this.complaint_recycler.setAdapter(this.adapter);
        this.adapter.notifyDataSetChanged();
    }

    public void listeners() {
        this.complaint_recycler.addOnItemTouchListener(new RecyclerItemClickListener(this, new OnItemClickListener() {
            public void onItemClick(View view, final int position) {

                /*((CheckBox) view.findViewById(R.id.checkb)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                        if (adapter.checkAll()) {
                            all_cb.setChecked(true);
                        } else {
                            all_cb.setChecked(false);
                        }
                    }
                });*/

                Button btn_details = (Button) view.findViewById(R.id.btn_details);
                ((Button) view.findViewById(R.id.btn_assign_transfer)).setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        final Dialog d = new Dialog(ComplaintServices.this, R.style.Custom_Dialog);
                        d.getWindow().requestFeature(1);
                        d.setContentView(R.layout.assign_emp_complaint);
                        final Spinner empl_spinner = (Spinner) d.findViewById(R.id.employee_spinner);
                        final Spinner priority_spinner = (Spinner) d.findViewById(R.id.priority_spinner);

                        try {
                            PhotoView invoice_url = (PhotoView) d.findViewById(R.id.invoice_url);
                            LinearLayout invoice_note_container = (LinearLayout) d.findViewById(R.id.invoice_note_container);
                            invoice_note_container.setVisibility(View.VISIBLE);
                            TextView invoice_note = (TextView) d.findViewById(R.id.invoice_note);
                            final ImageButton show_invoice = (ImageButton) d.findViewById(R.id.show_invoice);
                            final LinearLayout invoice_container = (LinearLayout) d.findViewById(R.id.invoice_container);

                            show_invoice.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (invoice_container.getVisibility() == View.VISIBLE) {
                                        show_invoice.setImageDrawable(getResources().getDrawable(R.drawable.ic_hide));
                                        invoice_container.setVisibility(View.GONE);
                                    } else {
                                        show_invoice.setImageDrawable(getResources().getDrawable(R.drawable.ic_unhide));
                                        invoice_container.setVisibility(View.VISIBLE);

                                    }
                                }
                            });

                            if (complaintEntityList.get(position).getPurchase_from().equalsIgnoreCase(getString(R.string.Baheti_Enterprises))) {
                                //   invoice_container.setVisibility(View.VISIBLE);
                                invoice_note.setText("This product Purchase From " + getString(R.string.Baheti_Enterprises));
                                show_invoice.setVisibility(View.VISIBLE);
                                Picasso.with(ComplaintServices.this).load(BahetiEnterprises.INVOICE_URL + complaintEntityList.get(position).getInvoice_url()).into(invoice_url);
                            } else {
                                //  invoice_container.setVisibility(View.GONE);
                                invoice_note.setText("This product Purchase From " + getString(R.string.Other));
                                show_invoice.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        final Spinner paid_spinner = (Spinner) d.findViewById(R.id.paid_spinner);
                        final EditText edt_amount = (EditText) d.findViewById(R.id.edt_amount);
                        final TableRow amount_to_pay = (TableRow) d.findViewById(R.id.Amount_to_pay);
                        amount_to_pay.setVisibility(View.GONE);

                        TableRow Paid_cont = (TableRow) d.findViewById(R.id.Paid_cont);
                        Paid_cont.setVisibility(View.VISIBLE);

                        setToSpinner(employeeEntityList, empl_spinner);


                        paid_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i == 1) {
                                    amount_to_pay.setVisibility(View.VISIBLE);
                                } else {
                                    amount_to_pay.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                        Button btn_assign_transfer1 = (Button) d.findViewById(R.id.btn_assign_transfer);
                        ((Button) d.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
                            public void onClick(View view) {
                                d.dismiss();
                            }
                        });
                        btn_assign_transfer1.setOnClickListener(new OnClickListener() {
                            public void onClick(View view) {
                                if (empl_spinner.getSelectedItemPosition() == 0) {
                                    BahetiEnterprises.retShowAlertDialod("Select Employee", ComplaintServices.this);
                                    return;
                                }

                                if (paid_spinner.getSelectedItemPosition() == 0) {
                                    BahetiEnterprises.retShowAlertDialod("Select Paid Type", ComplaintServices.this);
                                    return;
                                }


                                final ComplaintEntity complaintEntity = new ComplaintEntity();
                                complaintEntity.setId(((ComplaintEntity) complaintEntityList.get(position)).getId());
                                complaintEntity.setPriority(priority_spinner.getSelectedItem().toString());
                                complaintEntity.setAssigned_empid(((EmployeeEntity) employeeEntityList.get(empl_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);
                                complaintEntity.setPaid_type(paid_spinner.getSelectedItem().toString());

                                if (TextUtils.isEmpty(edt_amount.getText().toString())) {
                                    complaintEntity.setPaid_amt("0");
                                } else {
                                    complaintEntity.setPaid_amt(edt_amount.getText().toString());
                                }

                                cnmplViewModel.assignedEmp(complaintEntity, 1).observe(ComplaintServices.this, new Observer<List<Boolean>>() {
                                    public void onChanged(@Nullable List<Boolean> booleen) {
                                        if (!booleen.isEmpty()) {
                                            sweetAlertDialog.dismiss();
                                            d.dismiss();

                                            singleJobCart(complaintEntityList.get(position), empl_spinner.getSelectedItem().toString());
                                            subscribeToDataStreams(cnmplViewModel, ce);
                                        } else if (booleen.size() == 0) {
                                            sweetAlertDialog.dismiss();
                                            d.dismiss();
                                            subscribeToDataStreams(cnmplViewModel, ce);
                                        } else {
                                            sweetAlertDialog.show();
                                        }
                                    }
                                });
                            }
                        });
                        d.show();
                    }
                });
            }
        }));
    }

    public void singleJobCart(ComplaintEntity complaintEntity, String assigned_to) {


        AbstractViewRenderer abstractViewRenderer = BahetiEnterprises.getPdf(ComplaintServices.this, complaintEntity
                , null, nm, assigned_to);


        try {

            long ts = System.currentTimeMillis();
            final String file_nm = complaintEntity.getUsernm().replaceAll("\\s", "") + complaintEntity.getMobile_no() + "_report_" + ts + "";
            final String filePathString = Environment.getExternalStorageDirectory() + "/BE/Reports/" + file_nm;

            final File f = new File(filePathString);
            if (f.exists() && !f.isDirectory()) {
                f.delete();
            }

            File folder = new File(Environment.getExternalStorageDirectory() +
                    File.separator + "BE" + File.separator + "Reports");
            boolean success = true;
            if (!folder.exists()) {
                success = folder.mkdirs();
            }

            createPDf(ComplaintServices.this, abstractViewRenderer, file_nm, folder);


        } catch (Exception exea) {
            exea.printStackTrace();
        }


    }

    public void createJobCart(String assigned_to) {
        //   List<AbstractViewRenderer> abstractViewRendererList = new ArrayList<>();
        final int[] i = {0};
        final List<ComplaintEntity> complaintEntityList = getSelectedCallLog();
        for (; i[0] < complaintEntityList.size(); i[0]++) {
            AbstractViewRenderer abstractViewRenderer = BahetiEnterprises.getPdf(ComplaintServices.this, complaintEntityList.get(i[0])
                    , null, nm, assigned_to);
            //  abstractViewRendererList.add(abstractViewRenderer);


            try {

                long ts = System.currentTimeMillis();
                final String file_nm = complaintEntityList.get(i[0]).getUsernm().replaceAll("\\s", "") + complaintEntityList.get(i[0]).getMobile_no() + "_report_" + ts + "";
                final String filePathString = Environment.getExternalStorageDirectory() + "/BE/Reports/" + file_nm;

                final File f = new File(filePathString);
                if (f.exists() && !f.isDirectory()) {
                    f.delete();
                }

                File folder = new File(Environment.getExternalStorageDirectory() +
                        File.separator + "BE" + File.separator + "Reports");
                boolean success = true;
                if (!folder.exists()) {
                    success = folder.mkdirs();
                }

                //  createPDf(ComplaintServices.this, abstractViewRenderer, file_nm, folder);


                final PdfDocument doc = new PdfDocument(ComplaintServices.this);
                doc.addPage(abstractViewRenderer);


                doc.setOrientation(PdfDocument.A4_MODE.PORTRAIT);
                doc.setProgressTitle(R.string.app_name);
                doc.setProgressMessage(R.string.pdf_message);
                doc.setFileName(file_nm);
                doc.setSaveDirectory(folder);
                doc.setInflateOnMainThread(false);
                doc.setListener(new PdfDocument.Callback() {
                    @Override
                    public void onComplete(File file) {
                        Log.i(PdfDocument.TAG_PDF_MY_XML, "Complete");
                        doc.dismissPd();
                        if (i[0] == complaintEntityList.size()) {
                            onResume();
                        }
                        i[0]++;

                    }

                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        Log.i(PdfDocument.TAG_PDF_MY_XML, "Error");
                    }
                });

                doc.createPdf(ComplaintServices.this);


            } catch (Exception exea) {
                exea.printStackTrace();
            }

        }


    }

    public void initView() {
        this.ce = new ComplaintEntity();

        assigned_grp = (RadioGroup) findViewById(R.id.assigned_grp);
        unassigned = (RadioButton) findViewById(R.id.unassigned);
        assigned = (RadioButton) findViewById(R.id.assigned);
        this.employee_spinner = (Spinner) findViewById(R.id.employee_spinner);
        this.status_spinner = (Spinner) findViewById(R.id.status_spinner);
        this.service_spinner = (Spinner) findViewById(R.id.service_spinner);
        this.etd_cust_mobile_no = (EditText) findViewById(R.id.etd_cust_mobile_no);
        this.show = (Button) findViewById(R.id.show);
        this.complaint_recycler = (RecyclerView) findViewById(R.id.complaint_recycler);
        this.select_date = (TextView) findViewById(R.id.select_date);
        this.start_date = (TextView) findViewById(R.id.start_date);
        this.to_date = (TextView) findViewById(R.id.to_date);
        this.total = (TextView) findViewById(R.id.total);
        this.pending = (TextView) findViewById(R.id.pending);
        this.on_hold = (TextView) findViewById(R.id.on_hold);
        this.in_progress = (TextView) findViewById(R.id.in_progress);
        this.resolved_closed = (TextView) findViewById(R.id.resolved_closed);
        this.no_result_fount = (TextView) findViewById(R.id.no_result_fount);
        this.list_container = (LinearLayout) findViewById(R.id.list_container);
        calllog_oper_contnr = (LinearLayout) findViewById(R.id.calllog_oper_contnr);
        this.reset = (ImageButton) findViewById(R.id.reset);
        this.btn_new = (Button) findViewById(R.id.btn_new);
        this.btn_assign = (Button) findViewById(R.id.btn_assign);
        this.btn_resolved = (Button) findViewById(R.id.btn_resolved);
        this.list_container.setVisibility(View.GONE);
        all_cb = (CheckBox) findViewById(R.id.all_cb);
        Calendar c = Calendar.getInstance();
        this.mYear = c.get(Calendar.YEAR);
        this.mMonth = c.get(Calendar.MONTH);
        this.mDay = c.get(Calendar.DAY_OF_MONTH);
        this.sh = getSharedPreferences("log", 0);
        this.post = this.sh.getString("post", BuildConfig.VERSION_NAME);
        nm = sh.getString("unm", "");
        this.factory = new ComplaintFactory();
        this.cnmplViewModel = (ComplaintsViewModel) ViewModelProviders.of(this, this.factory).get(ComplaintsViewModel.class);
        this.sweetAlertDialog = new SweetAlertDialog(this, 5);
        this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
        this.sweetAlertDialog.setTitleText("Loading");
        this.sweetAlertDialog.setCancelable(false);

        btn_reset = (Button) findViewById(R.id.btn_reset);

        btn_reset.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setEmployeeToSpinner();
                status_spinner.setSelection(0);
                service_spinner.setSelection(0);
                select_date.setText("");
                start_date.setText("");
                to_date.setText("");
                etd_cust_mobile_no.setText("");
                assigned.setChecked(false);
                unassigned.setChecked(false);

            }
        });


        this.reset.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                start_date.setText(BuildConfig.VERSION_NAME);
                to_date.setText(BuildConfig.VERSION_NAME);
                select_date.setText(BuildConfig.VERSION_NAME);
            }
        });

        this.btn_new.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(ComplaintServices.this, LodgeComplaint.class));
            }
        });


        this.btn_assign.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (checkSelection()) {
                    final Dialog d = new Dialog(ComplaintServices.this, R.style.Custom_Dialog);
                    d.getWindow().requestFeature(1);
                    d.setContentView(R.layout.assign_emp_complaint);
                    final Spinner empl_spinner = (Spinner) d.findViewById(R.id.employee_spinner);
                    final Spinner priority_spinner = (Spinner) d.findViewById(R.id.priority_spinner);
                    final Spinner paid_spinner = (Spinner) d.findViewById(R.id.paid_spinner);
                    final EditText edt_amount = (EditText) d.findViewById(R.id.edt_amount);
                    final TableRow amount_to_pay = (TableRow) d.findViewById(R.id.Amount_to_pay);

                    TableRow Paid_cont = (TableRow) d.findViewById(R.id.Paid_cont);
                    amount_to_pay.setVisibility(View.GONE);
                    Paid_cont.setVisibility(View.GONE);
                    setToSpinner(employeeEntityList, empl_spinner);


                    Button btn_assign_transfer1 = (Button) d.findViewById(R.id.btn_assign_transfer);
                    ((Button) d.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            d.dismiss();
                        }
                    });


                    btn_assign_transfer1.setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            if (empl_spinner.getSelectedItemPosition() == 0) {
                                BahetiEnterprises.retShowAlertDialod("Select Employee", ComplaintServices.this);
                                return;
                            }


                            String allSelectedIds = getSelectedIds(getSelectedCallLog());
                            ComplaintEntity complaintEntity = new ComplaintEntity();
                            complaintEntity.setUser_id(allSelectedIds);
                            // complaintEntity.setPriority(priority_spinner.getSelectedItem().toString());
                           /* if (TextUtils.isEmpty(edt_amount.getText().toString())) {
                                complaintEntity.setPaid_amt("0");
                            } else {
                                complaintEntity.setPaid_amt(edt_amount.getText().toString());
                            }
*/

                            complaintEntity.setAssigned_empid(((EmployeeEntity) employeeEntityList.get(empl_spinner.getSelectedItemPosition() - 1)).getId() + BuildConfig.VERSION_NAME);
                            cnmplViewModel.assignedEmp(complaintEntity, 2).observe(ComplaintServices.this, new Observer<List<Boolean>>() {
                                public void onChanged(@Nullable List<Boolean> booleen) {
                                    if (!booleen.isEmpty()) {
                                        sweetAlertDialog.dismiss();
                                        d.dismiss();
                                        createJobCart(empl_spinner.getSelectedItem().toString());


                                    } else if (booleen.size() == 0) {
                                        sweetAlertDialog.dismiss();
                                        d.dismiss();
                                        subscribeToDataStreams(cnmplViewModel, ce);
                                    } else {
                                        sweetAlertDialog.show();
                                    }
                                    //subscribeToDataStreams(cnmplViewModel, ce);
                                }
                            });
                        }
                    });
                    d.show();


                }
                    /*final Dialog d = new Dialog(ComplaintServices.this, R.style.Custom_Dialog);
                    d.getWindow().requestFeature(1);
                    d.setContentView(R.layout.assign_emp_complaint);
                    final Spinner empl_spinner = (Spinner) d.findViewById(R.id.employee_spinner);
                    Spinner priority_spinner = (Spinner) d.findViewById(R.id.priority_spinner);
                    ((TableRow) d.findViewById(R.id.priority_cont)).setVisibility(View.GONE);
                    setToSpinner(employeeEntityList, empl_spinner);
                    Button btn_assign_transfer1 = (Button) d.findViewById(R.id.btn_assign_transfer);
                    ((Button) d.findViewById(R.id.btn_cancel)).setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            d.dismiss();
                        }
                    });
                    btn_assign_transfer1.setOnClickListener(new OnClickListener() {
                        public void onClick(View view) {
                            if (empl_spinner.getSelectedItemPosition() == 0) {
                                BahetiEnterprises.retShowAlertDialod("Please Select Employee", ComplaintServices.this);
                                return;
                            }
                            String allSelectedIds = getSelectedIds(getSelectedCallLog());
                            final ComplaintEntity ce = new ComplaintEntity();
                            ce.setUser_id(allSelectedIds);
                            ce.setAssigned_empid(((EmployeeEntity) employeeEntityList.get(empl_spinner.getSelectedItemPosition() - 1)).getId() + "");
                            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ComplaintServices.this, 5);
                            sweetAlertDialog.setTitle("Loading");
                            cnmplViewModel.assignedEmp(ce, 2).observe(ComplaintServices.this, new Observer<List<Boolean>>() {
                                public void onChanged(@Nullable List<Boolean> booleen) {
                                    if (!booleen.isEmpty()) {
                                        d.dismiss();
                                        sweetAlertDialog.dismiss();
                                        initViewModel(ce);
                                    } else if (booleen.size() == 0) {
                                        d.dismiss();
                                        sweetAlertDialog.dismiss();
                                    } else {
                                        sweetAlertDialog.show();
                                    }
                                }
                            });
                        }
                    });
                    d.show();
                    return;
                }
                BahetiEnterprises.retShowAlertDialod("Please Select", ComplaintServices.this);
           */
            }
        });


    }


    public boolean checkSelection() {
        List<ComplaintEntity> installationDemoEntityList = this.adapter.getComplaintServiceEntityList();
        boolean chk = false;
        int i = 0;
        while (i < installationDemoEntityList.size()) {
            if (((ComplaintEntity) installationDemoEntityList.get(i)).isSelected()) {
                chk = true;
                i = installationDemoEntityList.size();
            } else {
                chk = false;
                i++;
            }
        }
        return chk;
    }

    public List<ComplaintEntity> getSelectedCallLog() {
        List<ComplaintEntity> installationDemoEntityList = this.adapter.getComplaintServiceEntityList();
        List<ComplaintEntity> newList = new ArrayList();
        for (int i = 0; i < installationDemoEntityList.size(); i++) {
            if (((ComplaintEntity) installationDemoEntityList.get(i)).isSelected()) {
                newList.add(installationDemoEntityList.get(i));
            }
        }
        return newList;
    }


    public String getSelectedIds(List<ComplaintEntity> newList) {
        String ids = "";
        for (int i = 0; i < newList.size(); i++) {
            ids = ids + ((ComplaintEntity) newList.get(i)).getId() + ",";
        }
        return ids.substring(0, ids.length() - 1);
    }


    public void showDateView(View view) {
        this.date_expandble_view = (ExpandableRelativeLayout) findViewById(R.id.date_expandble_view);
        this.date_expandble_view.toggle();
    }

    public void showSelectDate(View view) {
        new DatePickerDialog(this, this.mSeleteDate, this.mYear, this.mMonth, this.mDay).show();
    }

    public void showStartDate(View view) {
        new DatePickerDialog(this, this.mStartDate, this.mYear, this.mMonth, this.mDay).show();
    }

    public void showToDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this.mToDate, this.mYear, this.mMonth, this.mDay);
        if (TextUtils.isEmpty(this.start_date.getText().toString())) {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        } else {
            datePickerDialog.getDatePicker().setMinDate(BahetiEnterprises.tStamp(this.start_date.getText().toString()));
        }
        datePickerDialog.show();
    }

    public void setEmployeeToSpinner() {
        try {
            EmployeeEntity ee = new EmployeeEntity();
            ee.setAssigned_mgr_type(this.post);
            ((EmployeeViewModel) ViewModelProviders.of(this, new EmployeeFactory()).get(EmployeeViewModel.class))
                    .getEmps(ee).observe(this, new Observer<List<EmployeeEntity>>() {
                public void onChanged(@Nullable List<EmployeeEntity> employeeEntities) {
                    if (!employeeEntities.isEmpty()) {
                        setToSpinner(employeeEntities, employee_spinner);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int getCont(List<ComplaintEntity> complaintEntityList, String status) {
        int cnt = 0;
        try {
            for (ComplaintEntity ce : complaintEntityList) {
                if (ce.getStatus().equalsIgnoreCase(status)) {
                    cnt++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cnt;
    }

    private void setToSpinner(List<EmployeeEntity> employeeEntities, Spinner employee_spinner1) {
        this.employeeEntityList = employeeEntities;
        List<String> catList = new ArrayList();
        catList.add("Select");
        try {
            for (EmployeeEntity employeeEntity : employeeEntities) {
                catList.add(employeeEntity.getEmp_nm());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        employee_spinner1.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));
    }


    // For PDF


    public void createPDf(Context ctx, AbstractViewRenderer page, final String file_nm, final File folder) {
        PdfDocument doc = new PdfDocument(ctx);
        doc.addPage(page);


        doc.setOrientation(PdfDocument.A4_MODE.PORTRAIT);
        doc.setProgressTitle(R.string.app_name);
        doc.setProgressMessage(R.string.pdf_message);
        doc.setFileName(file_nm);
        doc.setSaveDirectory(folder);
        doc.setInflateOnMainThread(false);
        doc.setListener(new PdfDocument.Callback() {
            @Override
            public void onComplete(File file) {
                Log.i(PdfDocument.TAG_PDF_MY_XML, "Complete");


            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                Log.i(PdfDocument.TAG_PDF_MY_XML, "Error");
            }
        });

        doc.createPdf(ctx);


    }


}
