package com.orangebyte.bahetiadmin.modules.ui;

import android.arch.lifecycle.LiveData;
import android.net.Uri;

import com.orangebyte.bahetiadmin.modules.entities.BrandEntity;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.entities.EnquiryEntity;
import com.orangebyte.bahetiadmin.modules.entities.FeedbackEntity;
import com.orangebyte.bahetiadmin.modules.entities.ManagerEntity;
import com.orangebyte.bahetiadmin.modules.entities.NotificationCenter;
import com.orangebyte.bahetiadmin.modules.entities.NotificationEntity;
import com.orangebyte.bahetiadmin.modules.entities.OfferEntity;
import com.orangebyte.bahetiadmin.modules.entities.ProductEntity;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.UserEntity;
import com.orangebyte.bahetiadmin.modules.entities.WorkSheetEntity;
import com.orangebyte.bahetiadmin.modules.models.Cat;

import java.util.List;

public interface AllContract {

    public interface BrandDataStreams {
        LiveData<List<BrandEntity>> addBrand(BrandEntity brandEntity, int i,Uri file);

        LiveData<List<BrandEntity>> deleteBrandById(BrandEntity brandEntity);

        LiveData<List<BrandEntity>> getBrands();

        LiveData<List<BrandEntity>> updateBrandStatus(BrandEntity brandEntity);
    }

    public interface CallLogInstallationStream {
        LiveData<List<Boolean>> changeCallLog(CallLogInstallationDemoEntity callLogInstallationDemoEntity, int i);

        LiveData<List<CallLogInstallationDemoEntity>> getCallLogInstallation(CallLogInstallationDemoEntity callLogInstallationDemoEntity);
    }

    public interface ComplaintsDataStreams {
        LiveData<List<Boolean>> assignedEmp(ComplaintEntity complaintEntity, int i);

        LiveData<List<ComplaintEntity>> getComplaints(ComplaintEntity complaintEntity);
    }

    public interface DataStreams {
        LiveData<List<CategoryEntity>> addCategory(Cat cat, int i);

        LiveData<List<CategoryEntity>> deleteCategoryById(Cat cat);

        LiveData<List<CategoryEntity>> getCats();

        LiveData<List<CategoryEntity>> updateCatStatus(Cat cat);
    }

    public interface EmployeeDataStreams {
        LiveData<List<EmployeeEntity>> addEmp(EmployeeEntity employeeEntity, int i);

        LiveData<List<EmployeeEntity>> deleteEMpById(EmployeeEntity employeeEntity);

        LiveData<List<EmployeeEntity>> getEmps(EmployeeEntity employeeEntity);

        LiveData<List<EmployeeEntity>> updateEmpStatus(EmployeeEntity employeeEntity);
    }

    public interface EmployeeWorksheetStream {
        LiveData<List<WorkSheetEntity>> getEmpWorksheet(WorkSheetEntity workSheetEntity);
    }

    public interface EnquiryStreams {
        LiveData<List<Boolean>> chngEnquiry(EnquiryEntity enquiryEntity, int chk);

        LiveData<List<EnquiryEntity>> getEnquiry(EnquiryEntity enquiryEntity);
    }

    public interface FeedbackStreams {
        LiveData<List<FeedbackEntity>> getFeedback(FeedbackEntity feedbackEntity);
    }

    public interface ManagerDataStreams {
        LiveData<List<ManagerEntity>> addMgr(ManagerEntity managerEntity, int i);

        LiveData<List<ManagerEntity>> deleteMgrById(ManagerEntity managerEntity);

        LiveData<List<ManagerEntity>> getManager(ManagerEntity managerEntity);

        LiveData<List<ManagerEntity>> updateMgrStatus(ManagerEntity managerEntity);
    }

    public interface NotificationDataStreams {
        LiveData<List<NotificationEntity>> addNotifcation(NotificationEntity notificationEntity);
    }

    public interface OfferDataStreams {
        LiveData<List<OfferEntity>> addOffer(int i, OfferEntity offerEntity, Uri uri);

        LiveData<List<OfferEntity>> getOffer(OfferEntity offerEntity);

        LiveData<List<Boolean>> updateOfferStatus(OfferEntity offerEntity);
    }

    public interface ProductDataStreams {
        LiveData<List<ProductEntity>> getProduct(ProductEntity productEntity);
    }

    public interface SubcategoryDataStreams {
        LiveData<List<SubCategoryEntity>> addSubCategory(SubCategoryEntity subCategoryEntity, int i);

        LiveData<List<SubCategoryEntity>> deleteSubCategoryById(SubCategoryEntity subCategoryEntity);

        LiveData<List<SubCategoryEntity>> getSubCats();

        LiveData<List<SubCategoryEntity>> updateSubCatStatus(SubCategoryEntity subCategoryEntity);
    }

    interface NotiDataStreams {
        void addNotifcation(NotificationCenter notificationCenter);
        LiveData<List<NotificationCenter>> getAll();

    }

    public interface UserDataStreams {
        LiveData<List<UserEntity>> getUsersByName(UserEntity userEntity);
        LiveData<List<Boolean>> addUser(UserEntity userEntity);
    }
}
