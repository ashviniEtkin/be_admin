package com.orangebyte.bahetiadmin.modules.entities;

import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;
import com.orangebyte.bahetiadmin.modules.models.IdentifiableModel;

import java.io.Serializable;

abstract class IdentifiableEntity implements IdentifiableModel, Serializable {
    @SerializedName("id")
    @PrimaryKey(autoGenerate = true)
    protected long id;

    IdentifiableEntity() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
