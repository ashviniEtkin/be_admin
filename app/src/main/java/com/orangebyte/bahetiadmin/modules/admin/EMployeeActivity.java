package com.orangebyte.bahetiadmin.modules.admin;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.adapters.EmployeeAdapter;
import com.orangebyte.bahetiadmin.modules.adapters.EmployeeAdapter.ClickListener;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener.OnItemClickListener;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel.EmployeeFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog.OnSweetClickListener;
import com.orangebyte.bahetiadmin.views.CircleImageView;

import im.delight.android.webview.BuildConfig;

import java.util.ArrayList;
import java.util.List;

public class EMployeeActivity extends AppCompatActivity implements LifecycleRegistryOwner, ClickListener {
    EmployeeAdapter adapter;
    List<EmployeeEntity> employeeEntityList = new ArrayList();
    EmployeeViewModel employeeViewModel;
    RecyclerView employee_list;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    TextView no_result_fount;
    String post = BuildConfig.VERSION_NAME;
    SharedPreferences sh;
    SweetAlertDialog sweetAlertDialog;
    private Toolbar toolbarView;
    Spinner type_spinner;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee);
        BahetiEnterprises.setActBar(getResources().getString(R.string.manage_employee), this, false);
        initViews();
        listeners();
    }

    public void listeners() {
        this.employee_list.addOnItemTouchListener(new RecyclerItemClickListener(this, new OnItemClickListener() {
            public void onItemClick(View view, final int position) {

                ((ImageButton) view.findViewById(R.id.edit_emp)).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(EMployeeActivity.this, AddEmployeeActivity.class);
                        i.putExtra(BahetiEnterprises.CHK_ADD_UPDATE, "1");
                        i.putExtra(BahetiEnterprises.FROM, employeeEntityList.get(position));
                        startActivity(i);
                    }
                });


                ((ImageButton) view.findViewById(R.id.chng_status)).setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        EMployeeActivity.this.sweetAlertDialog.changeAlertType(3);
                        if (((EmployeeEntity) EMployeeActivity.this.employeeEntityList.get(position)).getStatus().equalsIgnoreCase("1")) {
                            EMployeeActivity.this.sweetAlertDialog.setTitleText("Are You sure you want to hide?\n");
                        } else {
                            EMployeeActivity.this.sweetAlertDialog.setTitleText("Are You sure you want to unhide?\n");
                        }
                        EMployeeActivity.this.sweetAlertDialog.setCancelText("NO").setConfirmText("YES").setContentText("\n");
                        EMployeeActivity.this.sweetAlertDialog.setConfirmClickListener(new OnSweetClickListener() {
                            public void onClick(final SweetAlertDialog sweetAlertDialog) {
                                EmployeeEntity sce1 = (EmployeeEntity) EMployeeActivity.this.employeeEntityList.get(position);
                                if (((EmployeeEntity) EMployeeActivity.this.employeeEntityList.get(position)).getStatus().equalsIgnoreCase("1")) {
                                    sce1.setStatus("0");
                                    EMployeeActivity.this.employeeViewModel.updateEmpStatus(sce1).observe(EMployeeActivity.this, new Observer<List<EmployeeEntity>>() {
                                        public void onChanged(@Nullable List<EmployeeEntity> employeeEntity) {
                                            if (employeeEntity.isEmpty()) {
                                                sweetAlertDialog.show();
                                                return;
                                            }
                                            sweetAlertDialog.dismiss();
                                            EMployeeActivity.this.showEMployeeListInUit(employeeEntity);
                                        }
                                    });
                                    return;
                                }
                                sce1.setStatus("1");
                                EMployeeActivity.this.employeeViewModel.updateEmpStatus(sce1).observe(EMployeeActivity.this, new Observer<List<EmployeeEntity>>() {
                                    public void onChanged(@Nullable List<EmployeeEntity> employeeEntity) {
                                        if (employeeEntity.isEmpty()) {
                                            sweetAlertDialog.show();
                                            return;
                                        }
                                        sweetAlertDialog.dismiss();
                                        EMployeeActivity.this.showEMployeeListInUit(employeeEntity);
                                    }
                                });
                            }
                        }).show();
                        EMployeeActivity.this.onResume();
                    }
                });
            }
        }));
    }

    protected void onResume() {
        super.onResume();
        EmployeeEntity employeeEntity = new EmployeeEntity();

        if (this.type_spinner.getSelectedItemPosition() != 0) {
            employeeEntity.setAssigned_mgr_type(this.type_spinner.getSelectedItem().toString());
            initViewModel(employeeEntity);
            return;
        } else {
            employeeEntity = new EmployeeEntity();
            employeeEntity.setAssigned_mgr_type(this.post);
            initViewModel(employeeEntity);
        }
    }

    private void initViews() {
        this.no_result_fount = (TextView) findViewById(R.id.no_result_fount);
        this.type_spinner = (Spinner) findViewById(R.id.type_spinner);
        ArrayAdapter<String> typeArray = new ArrayAdapter(this, R.layout.spinner_layout, getResources().getStringArray(R.array.emp_type));
        this.sh = getSharedPreferences("log", 0);
        this.post = this.sh.getString("post", BuildConfig.VERSION_NAME);
        if (this.post.equalsIgnoreCase(getResources().getString(R.string.service_Manager))) {
            typeArray = new ArrayAdapter(this, R.layout.spinner_layout, getResources().getStringArray(R.array.service_emptype));
        } else if (this.post.equalsIgnoreCase(getResources().getString(R.string.Sales_Manager))) {
            typeArray = new ArrayAdapter(this, R.layout.spinner_layout, getResources().getStringArray(R.array.store_emptype));
        }
        this.type_spinner.setAdapter(typeArray);
        this.toolbarView = (Toolbar) findViewById(R.id.toolbar);
        this.sweetAlertDialog = new SweetAlertDialog(this);
        this.sweetAlertDialog.setCancelable(false);
        this.employee_list = (RecyclerView) findViewById(R.id.employee_list);
        if (VERSION.SDK_INT >= 9) {
            this.employee_list.setHasFixedSize(true);
        }
        if (VERSION.SDK_INT >= 9) {
            this.employee_list.setLayoutManager(new LinearLayoutManager(this));
        }
        this.adapter = new EmployeeAdapter(this, this);
        this.employee_list.setAdapter(this.adapter);
        this.type_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                EMployeeActivity.this.onResume();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        ((CircleImageView) findViewById(R.id.fab)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(EMployeeActivity.this, AddEmployeeActivity.class);
                i.putExtra(BahetiEnterprises.CHK_ADD_UPDATE, "0");
                EMployeeActivity.this.startActivity(i);
            }
        });
    }

    private void initViewModel(EmployeeEntity employeeEntity) {
        this.employeeViewModel = (EmployeeViewModel) ViewModelProviders.of(this, new EmployeeFactory()).get(EmployeeViewModel.class);
        subscribeToDataStreams(this.employeeViewModel, employeeEntity);
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    public void onItemClicked(EmployeeEntity employeeEntity) {
    }

    private void subscribeToDataStreams(EmployeeViewModel viewModel, EmployeeEntity employeeEntity) {
        try {
            this.sweetAlertDialog = new SweetAlertDialog(this, 5);
            this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
            this.sweetAlertDialog.setTitleText("Loading");
            this.sweetAlertDialog.setCancelable(false);
            viewModel.getEmps(employeeEntity).observe(this, new Observer<List<EmployeeEntity>>() {
                public void onChanged(@Nullable List<EmployeeEntity> employeeEntityList) {
                    if (!employeeEntityList.isEmpty()) {
                        EMployeeActivity.this.sweetAlertDialog.dismiss();
                        EMployeeActivity.this.showEMployeeListInUit(employeeEntityList);
                        EMployeeActivity.this.employee_list.setVisibility(0);
                        EMployeeActivity.this.no_result_fount.setVisibility(8);
                    } else if (employeeEntityList.size() == 0) {
                        EMployeeActivity.this.sweetAlertDialog.dismiss();
                        EMployeeActivity.this.showEMployeeListInUit(new ArrayList());
                        EMployeeActivity.this.employee_list.setVisibility(8);
                        EMployeeActivity.this.no_result_fount.setVisibility(0);
                    } else {
                        EMployeeActivity.this.sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showEMployeeListInUit(List<EmployeeEntity> employeeEntityList) {
        this.employeeEntityList = employeeEntityList;
        this.adapter.setItems(employeeEntityList);
    }
}
