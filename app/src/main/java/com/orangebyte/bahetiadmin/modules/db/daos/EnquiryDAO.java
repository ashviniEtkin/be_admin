package com.orangebyte.bahetiadmin.modules.db.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.orangebyte.bahetiadmin.modules.entities.EnquiryEntity;

import java.util.List;

@Dao
public interface EnquiryDAO {
    @Delete
    void delete(EnquiryEntity... enquiryEntityArr);

    @Query("DELETE FROM enquiry ")
    void deleteAll();

    @Delete
    void deleteAll(List<EnquiryEntity> list);

    @Insert(onConflict = 1)
    void insert(EnquiryEntity... enquiryEntityArr);

    @Insert(onConflict = 1)
    void insertAll(List<EnquiryEntity> list);

    @Query("SELECT * FROM enquiry ")
    LiveData<List<EnquiryEntity>> loadAll();

    @Update
    void update(EnquiryEntity... enquiryEntityArr);

    @Update
    void updateAll(List<EnquiryEntity> list);
}
