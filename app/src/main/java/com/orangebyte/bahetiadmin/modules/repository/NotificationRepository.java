package com.orangebyte.bahetiadmin.modules.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.db.daos.NotificationDAO;
import com.orangebyte.bahetiadmin.modules.entities.NotificationEntity;

import java.util.List;

public class NotificationRepository {
    private static NotificationRepository sInstance;
    private final NotificationDAO notificationDAO;
    private final RemoteRepository remoteRepository;

    interface RemoteNotificationListener {
        void onCheckResult(boolean z);

        void onRemotReceived(NotificationEntity notificationEntity);

        void onRemoteReceived(List<NotificationEntity> list);
    }

    interface TaskListener {
        void onTaskFinished();
    }

    public static NotificationRepository getInstance(RemoteRepository remoteRepository, NotificationDAO notificationDAO) {
        if (sInstance == null) {
            sInstance = new NotificationRepository(remoteRepository, notificationDAO);
        }
        return sInstance;
    }

    private NotificationRepository(RemoteRepository remoteRepository, NotificationDAO notificationDAO) {
        this.remoteRepository = remoteRepository;
        this.notificationDAO = notificationDAO;
    }

    public LiveData<List<NotificationEntity>> addNoti(NotificationEntity notificationEntity) {
        addNotification(new RemoteNotificationListener() {
            public void onRemotReceived(NotificationEntity cmEntity) {
            }

            public void onRemoteReceived(List<NotificationEntity> list) {
            }

            public void onCheckResult(boolean b) {
            }
        }, notificationEntity);
        return this.notificationDAO.loadAll();
    }

    private void insertTask(NotificationEntity notificationEntity) {
        insertTask(notificationEntity, null);
    }

    private void insertTask(final NotificationEntity notificationEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                NotificationRepository.this.notificationDAO.insert(notificationEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void addNotification(@NonNull final RemoteNotificationListener listener, NotificationEntity notificationEntity) {
        this.remoteRepository.addNotification(notificationEntity, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }
}
