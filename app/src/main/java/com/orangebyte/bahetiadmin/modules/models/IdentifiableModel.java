package com.orangebyte.bahetiadmin.modules.models;

public interface IdentifiableModel {
    long getId();
}
