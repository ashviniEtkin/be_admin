package com.orangebyte.bahetiadmin.modules.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.entities.WorkSheetEntity;
import java.util.ArrayList;
import java.util.List;

public class WorksheetAdapter extends Adapter<WorksheetAdapter.WorksheetHolder> {
    Context context;
    List<WorkSheetEntity> workSheetEntityList = new ArrayList();

    public class WorksheetHolder extends ViewHolder {
        CheckBox cb_wrkst;
        CardView container_id;
        TextView dt_wrst;
        TextView jobs_wrst;
        TextView status_wrkst;

        public WorksheetHolder(View itemView) {
            super(itemView);
            this.cb_wrkst = (CheckBox) itemView.findViewById(R.id.cb_wrkst);
            this.dt_wrst = (TextView) itemView.findViewById(R.id.dt_wrst);
            this.jobs_wrst = (TextView) itemView.findViewById(R.id.jobs_wrst);
            this.status_wrkst = (TextView) itemView.findViewById(R.id.status_wrkst);
            this.container_id = (CardView) itemView.findViewById(R.id.container_id);
        }
    }

    public WorksheetAdapter(Context context) {
        this.context = context;
    }

    public void setItems(List<WorkSheetEntity> itemsList) {
        if (itemsList.size() != 0) {
            this.workSheetEntityList.clear();
            WorkSheetEntity workSheetEntity = new WorkSheetEntity();
            workSheetEntity.setEmp_id("orange");
            this.workSheetEntityList.add(workSheetEntity);
            this.workSheetEntityList.addAll(itemsList);
            notifyDataSetChanged();
        }
    }

    public WorksheetHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WorksheetHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.worksheet_row, parent, false));
    }

    public void onBindViewHolder(WorksheetHolder holder, int position) {
        if (position == 0) {
            holder.dt_wrst.setText("Date");
            holder.jobs_wrst.setText("Jobs");
            holder.status_wrkst.setText("Status");
            holder.status_wrkst.setGravity(17);
            holder.jobs_wrst.setGravity(17);
            holder.dt_wrst.setGravity(17);
        } else {
            WorkSheetEntity wse = (WorkSheetEntity) this.workSheetEntityList.get(position);
            holder.dt_wrst.setText(BahetiEnterprises.retDate(wse.getOndate()));
            holder.jobs_wrst.setText(wse.getEmp_job());
            holder.status_wrkst.setText(wse.getJob_status());
        }
        if (returnEven(position)) {
            holder.container_id.setCardBackgroundColor(this.context.getResources().getColor(R.color.blue_100));
        } else {
            holder.container_id.setCardBackgroundColor(this.context.getResources().getColor(R.color.light_blue_50));
        }
    }

    public boolean returnEven(int pos) {
        if (pos / 2 == 0) {
            return true;
        }
        return false;
    }

    public int getItemCount() {
        return this.workSheetEntityList.size();
    }
}
