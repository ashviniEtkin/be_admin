package com.orangebyte.bahetiadmin.modules.admin;

import android.app.ProgressDialog;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.adapters.CategoryAdapter;
import com.orangebyte.bahetiadmin.modules.adapters.CategoryAdapter.ClickListener;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener;
import com.orangebyte.bahetiadmin.modules.listeners.RecyclerItemClickListener.OnItemClickListener;
import com.orangebyte.bahetiadmin.modules.models.Cat;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels;
import com.orangebyte.bahetiadmin.modules.viewmodels.CategoryViewMdels.Factory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog.OnSweetClickListener;
import com.orangebyte.bahetiadmin.views.CircleImageView;
import im.delight.android.webview.BuildConfig;
import java.util.ArrayList;
import java.util.List;

public class CategoryActivity extends AppCompatActivity implements LifecycleRegistryOwner, ClickListener {
    private CategoryAdapter adapter;
    List<CategoryEntity> categoryEntities1 = new ArrayList();
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    ProgressDialog pd;
    private ContentLoadingProgressBar progress_dialog;
    RecyclerView recyclerView;
    SweetAlertDialog sweetAlertDialog;
    private Toolbar toolbarView;
    private CategoryViewMdels viewModel;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        BahetiEnterprises.setActBar(getResources().getString(R.string.category_list), this, false);
        initViews();
        listeners();
    }

    public void listeners() {
        this.recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new OnItemClickListener() {
            public void onItemClick(View view, final int position) {
                ImageButton status = (ImageButton) view.findViewById(R.id.chng_status);
                ((ImageButton) view.findViewById(R.id.del)).setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        final CategoryEntity ce = (CategoryEntity) CategoryActivity.this.categoryEntities1.get(position);
                        final Cat cat = new Cat(ce.getId() + BuildConfig.VERSION_NAME, ce.getName(), ce.getAssign_to(), ce.getStatus());
                        CategoryActivity.this.sweetAlertDialog.changeAlertType(3);
                        CategoryActivity.this.sweetAlertDialog.setTitleText("Are You sure you want to delete?\n").setContentText("\n").setConfirmText("Yes,delete it!").setCancelText("No").setConfirmClickListener(new OnSweetClickListener() {
                            public void onClick(final SweetAlertDialog sDialog) {
                                CategoryActivity.this.viewModel.deleteCategoryById(cat).observe(CategoryActivity.this, new Observer<List<CategoryEntity>>() {
                                    public void onChanged(@Nullable final List<CategoryEntity> categoryEntities) {
                                        if (categoryEntities.isEmpty()) {
                                            sDialog.show();
                                            return;
                                        }
                                        sDialog.showCancelButton(false);
                                        sDialog.setTitleText("Deleted!").setContentText(ce.getName() + " has been deleted!").setConfirmText("OK").setConfirmClickListener(new OnSweetClickListener() {
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.dismiss();
                                                CategoryActivity.this.showCategoryListInUit(categoryEntities);
                                            }
                                        }).changeAlertType(2);
                                    }
                                });
                            }
                        }).setCancelClickListener(new OnSweetClickListener() {
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        }).show();
                    }
                });
                status.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        CategoryActivity.this.sweetAlertDialog.changeAlertType(3);
                        if (((CategoryEntity) CategoryActivity.this.categoryEntities1.get(position)).getStatus().equalsIgnoreCase("1")) {
                            CategoryActivity.this.sweetAlertDialog.setTitleText("Are You sure you want to hide?\n");
                        } else {
                            CategoryActivity.this.sweetAlertDialog.setTitleText("Are You sure you want to unhide?\n");
                        }
                        CategoryActivity.this.sweetAlertDialog.setCancelText("NO").setConfirmText("YES").setContentText("\n");
                        CategoryActivity.this.sweetAlertDialog.setConfirmClickListener(new OnSweetClickListener() {
                            public void onClick(final SweetAlertDialog sweetAlertDialog) {
                                if (((CategoryEntity) CategoryActivity.this.categoryEntities1.get(position)).getStatus().equalsIgnoreCase("1")) {
                                    CategoryActivity.this.viewModel.updateCatStatus(new Cat(((CategoryEntity) CategoryActivity.this.categoryEntities1.get(position)).getId() + BuildConfig.VERSION_NAME, ((CategoryEntity) CategoryActivity.this.categoryEntities1.get(position)).getName(), ((CategoryEntity) CategoryActivity.this.categoryEntities1.get(position)).getAssign_to() + BuildConfig.VERSION_NAME, "0")).observe(CategoryActivity.this, new Observer<List<CategoryEntity>>() {
                                        public void onChanged(@Nullable List<CategoryEntity> categoryEntities) {
                                            if (categoryEntities.isEmpty()) {
                                                sweetAlertDialog.show();
                                                return;
                                            }
                                            sweetAlertDialog.dismiss();
                                            CategoryActivity.this.showCategoryListInUit(categoryEntities);
                                        }
                                    });
                                    return;
                                }
                                CategoryActivity.this.viewModel.updateCatStatus(new Cat(((CategoryEntity) CategoryActivity.this.categoryEntities1.get(position)).getId() + BuildConfig.VERSION_NAME, ((CategoryEntity) CategoryActivity.this.categoryEntities1.get(position)).getName(), ((CategoryEntity) CategoryActivity.this.categoryEntities1.get(position)).getAssign_to() + BuildConfig.VERSION_NAME, "1")).observe(CategoryActivity.this, new Observer<List<CategoryEntity>>() {
                                    public void onChanged(@Nullable List<CategoryEntity> categoryEntities) {
                                        if (categoryEntities.isEmpty()) {
                                            sweetAlertDialog.show();
                                            return;
                                        }
                                        sweetAlertDialog.dismiss();
                                        CategoryActivity.this.showCategoryListInUit(categoryEntities);
                                    }
                                });
                            }
                        }).show();
                        CategoryActivity.this.onResume();
                    }
                });
            }
        }));
    }

    protected void onResume() {
        super.onResume();
        initViewModel();
    }

    private void initViews() {
        this.pd = new ProgressDialog(this);
        this.pd.setMessage("Please Wait");
        this.toolbarView = (Toolbar) findViewById(R.id.toolbar);
        this.progress_dialog = (ContentLoadingProgressBar) findViewById(R.id.progress_dialog);
        this.sweetAlertDialog = new SweetAlertDialog(this);
        this.sweetAlertDialog.setCancelable(false);
        this.recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        if (VERSION.SDK_INT >= 9) {
            this.recyclerView.setHasFixedSize(true);
        }
        if (VERSION.SDK_INT >= 9) {
            this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        }
        this.adapter = new CategoryAdapter(this, this);
        this.recyclerView.setAdapter(this.adapter);
        ((CircleImageView) findViewById(R.id.fab)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(CategoryActivity.this, AddCategory.class);
                i.putExtra(BahetiEnterprises.CHK_ADD_UPDATE, "0");
                CategoryActivity.this.startActivity(i);
            }
        });
    }

    private void initViewModel() {
        this.viewModel = (CategoryViewMdels) ViewModelProviders.of(this, new Factory()).get(CategoryViewMdels.class);
        subscribeToDataStreams(this.viewModel);
    }

    private void subscribeToDataStreams(CategoryViewMdels viewModel) {
        try {
            viewModel.getCats().observe(this, new Observer<List<CategoryEntity>>() {
                public void onChanged(@Nullable List<CategoryEntity> categoryEntities) {
                    if (categoryEntities.isEmpty()) {
                        CategoryActivity.this.pd.show();
                        return;
                    }
                    CategoryActivity.this.pd.dismiss();
                    CategoryActivity.this.showCategoryListInUit(categoryEntities);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgress() {
        this.progress_dialog.show();
    }

    private void hideProgress() {
        this.progress_dialog.hide();
    }

    private void showCategoryListInUit(List<CategoryEntity> catEntities) {
        this.categoryEntities1 = catEntities;
        this.adapter.setItems(catEntities);
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    public void onItemClicked(CategoryEntity catEntity) {
    }
}
