package com.orangebyte.bahetiadmin.applications;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.StrictMode;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics.Event;
import com.orangebyte.bahetiadmin.MainActivity;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.modules.adapters.TypeAdapter;
import com.orangebyte.bahetiadmin.modules.employee.JobDone;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;
import com.orangebyte.bahetiadmin.modules.entities.SubCategoryEntity;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog.OnSweetClickListener;
import com.orangebyte.bahetiadmin.utils.TypefaceUtil;
import com.orangebyte.pdflibrary.viewRenderer.AbstractViewRenderer;

import im.delight.android.webview.BuildConfig;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class BahetiEnterprises extends Application {
    //    public static String BASE_URL = "http://be.orangebytetech.com/mobile_app/webservice";

    public static String BASE_URL = "http://be.orangebytetech.com/mobile_app/webservice";
    private static final float BLUR_RADIUS = 5.0f;
    public static String CATEGORY = "CATEGORY";
    public static String CHK_ADD_UPDATE = "CHK_ADD_UPDATE";
    public static String COMPLAINT_DETAILS = "COMPLAINT_DETAILS";
    public static String FROM = "FROM";
    public static String IMAGE_URL = "http://be.orangebytetech.com/uploads/";
    public static String Brand_URL = "http://be.orangebytetech.com/brands/";
    public static String SIGNATURE_URL = "http://be.orangebytetech.com/signature/";
    public static String PROOF_URL = "http://be.orangebytetech.com/proofs/";
    public static String INVOICE_URL = "http://be.orangebytetech.com/invoices/";

    public static String INTNT = "INTENT";
    public static String PRODUCT_IMAGE_URL = "http://bahetiproduct.orangebytetech.com/images/products/";
    public static String SUBCATEGORY = "SUBCATEGORY";
    private static final RetryPolicy VOLLEY_REQUEST_RETRY_POLICY = new DefaultRetryPolicy(25000, 1, 1.0f);
    public static BahetiEnterprises mInstance = null;
    private static Context myContext;
    public static SweetAlertDialog sweetAlertDialog;
    public static long two_days = 172800000;
    private RequestQueue mRequestQueue;

    static RecyclerView status_list;
    static TextView type, job_cart_no, invoice, assigned_by_nm, assigned_date, tm_out, tm_in, job_cart_prod_nm, job_cart_prod_model_no,
            job_cart_cat, job_cart_sub_cat, job_cart_prod_brand, warnty, assigned_to, cust_nm, cust_address, desc, cust_mob_no, comp_reg_dt;


    public void onCreate() {
        super.onCreate();
        mInstance = this;
        sweetAlertDialog = new SweetAlertDialog(this);
        sweetAlertDialog.changeAlertType(3);
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Rupee_Foradian.ttf");


        TypefaceUtil.overrideFont(getApplicationContext(), "DEFAULT", "fonts/BebasNeueBook.ttf");
        TypefaceUtil.overrideFont(getApplicationContext(), "MONOSPACE", "fonts/BebasNeueRegular.ttf");
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/BebasNeueRegular.ttf");
        TypefaceUtil.overrideFont(getApplicationContext(), "SANS_SERIF", "fonts/BebasNeueRegular.ttf");


        try {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

            builder.detectFileUriExposure();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static Bitmap blur(Bitmap image, Context ctx) {
        if (image == null) {
            return null;
        }
        Bitmap outputBitmap = Bitmap.createBitmap(image);
        RenderScript renderScript = RenderScript.create(ctx);
        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
        Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);
        return outputBitmap;
    }

    public static synchronized BahetiEnterprises getInstance() {
        BahetiEnterprises bahetiEnterprises;
        synchronized (BahetiEnterprises.class) {
            bahetiEnterprises = mInstance;
        }
        return bahetiEnterprises;
    }

    public RequestQueue getRequestQueue() {
        if (this.mRequestQueue == null) {
            this.mRequestQueue = Volley.newRequestQueue(getApplicationContext(), new OkHttpStack());
        }
        return this.mRequestQueue;
    }

    public static boolean showInRed(long dtstmp) {
        if (System.currentTimeMillis() - dtstmp > two_days) {
            return true;
        }
        return false;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        if (TextUtils.isEmpty(tag)) {
            tag = "ContentValues";
        }
        req.setTag(tag);
        req.setRetryPolicy(VOLLEY_REQUEST_RETRY_POLICY);
        VolleyLog.d("Adding request to queue: %s", new Object[]{req.getUrl()});
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag("ContentValues");
        req.setRetryPolicy(VOLLEY_REQUEST_RETRY_POLICY);
        getRequestQueue().add(req);
    }



    public static SpannableString customeString(String str1) {

       /* if (str1.length() > 1) {
            String text = getInstance().getResources().getString(R.string.first_letter) + str1.substring(0, 1) +getInstance().getResources().getString(R.string.close_font)+ ""+getInstance().getResources().getString(R.string.second_letter) + str1.substring(1, str1.length()) +  getInstance().getResources().getString(R.string.close_font);
            return text.replace(" ","");
        } else {
            String text =  getInstance().getResources().getString(R.string.first_letter) + str1.substring(0, str1.length()) + getInstance().getResources().getString(R.string.close_font);//+ "</font> <font color=#ffcc00>" + str1.substring(1, str1.length()) + "</font>";
            return text;
        }*/

        SpannableString text2 = new SpannableString(str1);
        text2.setSpan(new ForegroundColorSpan(getInstance().getResources().getColor(R.color.app_color)), 0, 1, 0);
        text2.setSpan(new ForegroundColorSpan(Color.BLACK), 1, str1.length(), 0);

        return text2;
    }

    public static List<SpannableString> getStr(String s1) {

        List<SpannableString> spannableStrings = new ArrayList<>();
        String fstr = "";
        String[] parts = s1.split(" ");
        for (int i = 0; i < parts.length; i++) {
            // fstr = fstr + customeString(parts[i]) + " ";

            spannableStrings.add(customeString(parts[i]+" "));
        }

        return spannableStrings;
    }

    public static void setActBar(String scr_nm, final Context ctx, boolean chk_logout) {
        ImageView back_arrow = (ImageView) ((Activity) ctx).findViewById(R.id.back_arrow);
        ImageView logout = (ImageView) ((Activity) ctx).findViewById(R.id.logout);
        TextView screen_nm = ((TextView) ((Activity) ctx).findViewById(R.id.screen_nm));//.setText(scr_nm);


        try {
            List<SpannableString> spannableStringList = getStr(scr_nm);
            CharSequence ch11 = "";

            for (int i = 0; i < spannableStringList.size(); i++) {
                // mTextView.setText(TextUtils.concat(span1, " ", span2));
                try {
                    ch11 = TextUtils.concat(ch11, spannableStringList.get(i));
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            screen_nm.setText(ch11, TextView.BufferType.SPANNABLE);
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (chk_logout) {
            logout.setVisibility(View.INVISIBLE);
        }
        logout.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                BahetiEnterprises.logout(ctx);
            }
        });
        back_arrow.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                ((Activity) ctx).onBackPressed();
            }
        });
        hiddenKBoard(ctx);
    }

    public static void hiddenKBoard(Context ctx) {
        ((Activity) ctx).getWindow().setSoftInputMode(3);
    }

    public static void logout(final Context ctx) {
        Builder dlg = new Builder(ctx);
        dlg.setTitle(com.orangebyte.pdflibrary.R.string.app_name);
        dlg.setMessage("Are You Sure You Want To Logout ?");
        dlg.setIcon(R.drawable.logo);
        dlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    SharedPreferences sh = ((Activity) ctx).getSharedPreferences("log", 0);
                    sh.edit().remove(Event.LOGIN).commit();
                    sh.edit().remove("users").commit();
                    sh.edit().remove("post").commit();
                    Editor editor = sh.edit();
                    editor.clear();
                    editor.commit();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                Intent intent1 = new Intent(ctx, MainActivity.class);
                intent1.setFlags(268435456);
                ctx.startActivity(IntentCompat.makeRestartActivityTask(intent1.getComponent()));
                ((Activity) ctx).finish();
            }
        });
        dlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        dlg.create().show();
    }

    public static long tStamp(String change_date) {
        long ts = 0;
        try {
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aa");
            formatter.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
            ts = formatter.parse(change_date + " 12:00:00 AM").getTime();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ts;
    }


    public static long tStampO(String change_date) {
        long ts = 0;
        try {
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aa");
            formatter.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
            ts = formatter.parse(change_date + " 00:00:00 AM").getTime();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ts;
    }

    public static long lasttStamp(String change_date) {
        long ts = 0;
        try {
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aa");
            formatter.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
            ts = formatter.parse(change_date + " 23:59:59 AM").getTime();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ts;
    }

    public static void retShowAlertDialod(String alert_text, Context ctx) {
        sweetAlertDialog = new SweetAlertDialog((Activity) ctx);
        sweetAlertDialog.changeAlertType(3);
        sweetAlertDialog.setTitleText("\n" + alert_text).setContentText("\n").setConfirmText("OK").setConfirmClickListener(new OnSweetClickListener() {
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
            }
        });
        sweetAlertDialog.show();
    }

    public static RequestBody retRequestBody(String text) {
        return RequestBody.create(MediaType.parse("text/plain"), text);
    }

    public static String retDate(String tstamp) {
        String dt = BuildConfig.VERSION_NAME;
        try {
            return new SimpleDateFormat("dd-MM-yyyy").format(new Date(Long.parseLong(tstamp)));
        } catch (Exception ex) {
            ex.printStackTrace();
            return dt;
        }
    }

    public static void dismissAlertDialog() {
        sweetAlertDialog.dismiss();
    }

    public static String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getInstance().getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        }
        cursor.moveToFirst();
        return cursor.getString(cursor.getColumnIndex("_data"));
    }

    public static void setData(String data, TextView txtView) {
        if (TextUtils.isEmpty(data)) {
            txtView.setText("-");
        } else if (data.equalsIgnoreCase("null")) {
            txtView.setText("-");
        } else if (data == null) {
            txtView.setText("-");
        } else {
            txtView.setText(data);
        }
    }

    public static String getData(String data) {
        if (TextUtils.isEmpty(data)) {
            return "-";
        } else if (data.equalsIgnoreCase("null")) {
            return "-";
        } else if (data == null) {
            return "-";
        } else {
            return data;
        }
    }


    public static boolean checkSpinner(Spinner sp) {
        if (sp.getAdapter().getCount() == 0) {
            return true;
        }
        return false;
    }

    public static String setDataOnSpinner(Spinner sp, String data) {
        return !checkSpinner(sp) ? data : "null";
    }

    public static String getIdFromCatgory(List<CategoryEntity> categoryEntityList1, String catNm) {
        String catId = BuildConfig.VERSION_NAME;
        int i = 0;
        while (i < categoryEntityList1.size()) {
            if (((CategoryEntity) categoryEntityList1.get(i)).getName().equalsIgnoreCase(catNm)) {
                catId = ((CategoryEntity) categoryEntityList1.get(i)).getId() + BuildConfig.VERSION_NAME;
                i = categoryEntityList1.size();
            } else {
                i++;
            }
        }
        return catId;
    }

    public static String getIdFromSubCatgory(List<SubCategoryEntity> subCategoryEntities, String catNm) {
        String catId = BuildConfig.VERSION_NAME;
        int i = 0;
        while (i < subCategoryEntities.size()) {
            if (((SubCategoryEntity) subCategoryEntities.get(i)).getSub_cat_name().equalsIgnoreCase(catNm)) {
                catId = ((SubCategoryEntity) subCategoryEntities.get(i)).getId() + BuildConfig.VERSION_NAME;
                i = subCategoryEntities.size();
            } else {
                i++;
            }
        }
        return catId;
    }


    public static AbstractViewRenderer getPdf(final Context ctx, final ComplaintEntity ce, final CallLogInstallationDemoEntity clde, final String assigned_by, final String assign_to) {
        AbstractViewRenderer abstractViewRenderer = new AbstractViewRenderer(ctx, R.layout.job_cart) {
            protected void initView(View view) {
                initPdfView(view);
                if (ce != null) {
                    updateComplaintData(ce, ctx, assigned_by, assign_to);
                    return;
                }
                if (clde != null) {
                    updateInstallationData(clde, ctx, assigned_by, assign_to);
                }
            }
        };
        return abstractViewRenderer;
    }


    public static void initPdfView(View view) {
        status_list = (RecyclerView) view.findViewById(R.id.status_list);
        job_cart_no = (TextView) view.findViewById(R.id.job_cart_no);
        invoice = (TextView) view.findViewById(R.id.invoice);
        assigned_by_nm = (TextView) view.findViewById(R.id.assigned_by_nm);
        assigned_date = (TextView) view.findViewById(R.id.assigned_date);
        tm_out = (TextView) view.findViewById(R.id.tm_out);
        tm_in = (TextView) view.findViewById(R.id.tm_in);
        job_cart_prod_nm = (TextView) view.findViewById(R.id.job_cart_prod_nm);
        job_cart_prod_model_no = (TextView) view.findViewById(R.id.job_cart_prod_model_no);
        job_cart_cat = (TextView) view.findViewById(R.id.job_cart_cat);
        job_cart_sub_cat = (TextView) view.findViewById(R.id.job_cart_sub_cat);
        job_cart_prod_brand = (TextView) view.findViewById(R.id.job_cart_prod_brand);
        warnty = (TextView) view.findViewById(R.id.warnty);
        assigned_to = (TextView) view.findViewById(R.id.assigned_to);


        cust_nm = (TextView) view.findViewById(R.id.cust_nm);
        cust_address = (TextView) view.findViewById(R.id.cust_address);
        cust_mob_no = (TextView) view.findViewById(R.id.cust_mob_no);
        comp_reg_dt = (TextView) view.findViewById(R.id.comp_reg_dt);
        desc = (TextView) view.findViewById(R.id.desc);
        type = (TextView) view.findViewById(R.id.type);


    }

    public static void updateComplaintData(ComplaintEntity complaintEntity, Context ctx, String assugned_by, String assig_to) {


        job_cart_no.setText(ctx.getResources().getString(R.string.service_req_no) + " : " + complaintEntity.getCom_no());
        invoice.setText(ctx.getResources().getString(R.string.service_req_report));
        assigned_by_nm.setText(ctx.getResources().getString(R.string.assign_by) + " : " + assugned_by);
        assigned_date.setText(ctx.getResources().getString(R.string.Date) + " " + retDate(System.currentTimeMillis() + ""));
        job_cart_prod_nm.setText(ctx.getResources().getString(R.string.Product) + " " + complaintEntity.getTitle());
        job_cart_prod_model_no.setText(ctx.getResources().getString(R.string.product_model) + " : " + complaintEntity.getModel_no());
        job_cart_cat.setText(ctx.getResources().getString(R.string.Category) + " " + complaintEntity.getCat_nm());
        job_cart_sub_cat.setText(ctx.getResources().getString(R.string.SubCategory) + " " + complaintEntity.getCat_nm());
        job_cart_prod_brand.setText(ctx.getResources().getString(R.string.brand) + " : " + complaintEntity.getCat_nm());

        cust_nm.setText(ctx.getResources().getString(R.string.Name_of_Customer) + " " + complaintEntity.getUsernm());
        cust_address.setText(ctx.getResources().getString(R.string.Address) + " " + complaintEntity.getAddress());
        cust_mob_no.setText(ctx.getResources().getString(R.string.MobileNum) + " " + complaintEntity.getMobile_no());
        comp_reg_dt.setText(ctx.getResources().getString(R.string.call_log_registration_no) + " " + retDate(complaintEntity.getComp_date()));
        desc.setText(ctx.getResources().getString(R.string.customer_complaint) + " : " + complaintEntity.getDesc());

        if (complaintEntity.getProduct_in().equalsIgnoreCase("0")) {
            warnty.setText(ctx.getResources().getString(R.string.warranty) + " : NO ");
        } else {
            warnty.setText(ctx.getResources().getString(R.string.warranty) + " : Yes ");
        }

        assigned_to.setText("Assigned to \n Name : " + assig_to);
        List<String> araList = Arrays.asList(complaintEntity.getService_type().split(","));

        TypeAdapter adapter = new TypeAdapter(araList);
        status_list.setLayoutManager(new LinearLayoutManager(ctx));
        status_list.setAdapter(adapter);
        type.setText("Service Type ");


    }

    public static void updateInstallationData(CallLogInstallationDemoEntity callLogInstallationDemoEntity, Context ctx, String assugned_by, String assig_to) {


        job_cart_no.setText(ctx.getResources().getString(R.string.call_log_no) + " " + callLogInstallationDemoEntity.getCom_no());
        invoice.setText(ctx.getResources().getString(R.string.call_log_report));
        assigned_by_nm.setText(ctx.getResources().getString(R.string.assign_by) + " : " + assugned_by);
        assigned_date.setText(ctx.getResources().getString(R.string.Date) + " " + retDate(System.currentTimeMillis() + ""));
        job_cart_prod_nm.setText(ctx.getResources().getString(R.string.Product) + " " + callLogInstallationDemoEntity.getTitle());
        job_cart_prod_model_no.setText(ctx.getResources().getString(R.string.product_model) + " : " + callLogInstallationDemoEntity.getModel_no());
        job_cart_cat.setText(ctx.getResources().getString(R.string.Category) + " : " + callLogInstallationDemoEntity.getCat_nm());
        job_cart_sub_cat.setText(ctx.getResources().getString(R.string.SubCategory) + " " + callLogInstallationDemoEntity.getCat_nm());
        job_cart_prod_brand.setText(ctx.getResources().getString(R.string.brand) + " : " + callLogInstallationDemoEntity.getCat_nm());

        cust_nm.setText(ctx.getResources().getString(R.string.Name_of_Customer) + " " + callLogInstallationDemoEntity.getUsernm());
        cust_address.setText(ctx.getResources().getString(R.string.Address) + " " + callLogInstallationDemoEntity.getAddress());
        cust_mob_no.setText(ctx.getResources().getString(R.string.MobileNum) + " " + callLogInstallationDemoEntity.getMobile_no());
        comp_reg_dt.setText(ctx.getResources().getString(R.string.call_log_registration_no) + " : " + retDate(callLogInstallationDemoEntity.getDemodate()));
        desc.setText(ctx.getResources().getString(R.string.customer_complaint) + " : " + callLogInstallationDemoEntity.getDescrip());


        if (callLogInstallationDemoEntity.getProduct_in().equalsIgnoreCase("0")) {
            warnty.setText(ctx.getResources().getString(R.string.warranty) + " : NO ");
        } else {
            warnty.setText(ctx.getResources().getString(R.string.warranty) + " : Yes ");
        }

        assigned_to.setText("Assigned to \n Name : " + assig_to);
        List<String> araList = Arrays.asList(callLogInstallationDemoEntity.getType().split(","));

        TypeAdapter adapter = new TypeAdapter(araList);
        status_list.setLayoutManager(new LinearLayoutManager(ctx));
        status_list.setAdapter(adapter);
        type.setText("Call Log Type");
    }
}
