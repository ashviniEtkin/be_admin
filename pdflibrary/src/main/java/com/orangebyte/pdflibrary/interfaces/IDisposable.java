package com.orangebyte.pdflibrary.interfaces;

public interface IDisposable {
  /**
   * dispose the item
   */
  void dispose();
}
