package com.orangebyte.bahetiadmin.modules.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.orangebyte.bahetiadmin.modules.entities.UserEntity;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    private static UserRepository sInstance;
    private final RemoteRepository remoteRepository;

    interface RemoteUserListener {
        void onCheckResult(boolean z);

        void onRemotReceived(UserEntity userEntity);

        void onRemoteReceived(List<UserEntity> list);
    }

    public static UserRepository getInstance(RemoteRepository remoteRepository) {
        if (sInstance == null) {
            sInstance = new UserRepository(remoteRepository);
        }
        return sInstance;
    }

    private UserRepository(RemoteRepository remoteRepository) {
        this.remoteRepository = remoteRepository;
    }

    public LiveData<List<UserEntity>> worksheetList(UserEntity userEntity) {
        final MutableLiveData<List<UserEntity>> data = new MutableLiveData();
        fetchUserList(new RemoteUserListener() {
            public void onRemotReceived(UserEntity cmEntity) {
            }

            public void onRemoteReceived(List<UserEntity> workSheetEntityList1) {
                if (workSheetEntityList1 != null) {
                    data.setValue(workSheetEntityList1);
                }
            }

            public void onCheckResult(boolean b) {
            }
        }, userEntity);
        return data;
    }

    public LiveData<List<Boolean>> addUsr(UserEntity userEntity) {
        final MutableLiveData<List<Boolean>> data = new MutableLiveData<>();
        addUser(new RemoteUserListener() {
            @Override
            public void onRemotReceived(UserEntity cmEntity) {

            }

            @Override
            public void onRemoteReceived(final List<UserEntity> userEntityList1) {


            }

            @Override
            public void onCheckResult(boolean b) {
                if (b) {
                    List<Boolean> da = new ArrayList<>();
                    da.add(b);
                    data.setValue(da);

                }
            }


        }, userEntity);
        return data;
    }

    public void addUser(@NonNull final RemoteUserListener listener, UserEntity userEntity) {
        remoteRepository.addUser(userEntity, new RemoteCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response);
            }

            @Override
            public void onUnauthorized() {

            }

            @Override
            public void onFailed(Throwable throwable) {

            }
        });
    }


    public void fetchUserList(@NonNull final RemoteUserListener listener, UserEntity userEntity) {
        this.remoteRepository.getUserByname(userEntity, new RemoteCallback<List<UserEntity>>() {
            public void onSuccess(List<UserEntity> response) {
                listener.onRemoteReceived(response);
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }
}
