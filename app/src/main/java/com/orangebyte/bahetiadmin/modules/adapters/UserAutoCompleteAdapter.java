package com.orangebyte.bahetiadmin.modules.adapters;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.entities.UserEntity;
import com.orangebyte.bahetiadmin.modules.models.ServicesResponse;
import com.orangebyte.bahetiadmin.modules.repository.ApiServices;
import com.orangebyte.bahetiadmin.modules.repository.ServiceFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.UserViewModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ashvini on 1/10/2018.
 */

public class UserAutoCompleteAdapter extends BaseAdapter implements Filterable, LifecycleRegistryOwner {

    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    private static final int MAX_RESULTS = 10;
    private Context mContext;
    private List<UserEntity> resultList = new ArrayList<UserEntity>();
    ProgressBar pd;

    UserViewModel viewModel2;

    public UserAutoCompleteAdapter(Context context, ProgressBar pd, UserViewModel viewModel2) {
        mContext = context;
        this.pd = pd;
        this.viewModel2 = viewModel2;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public UserEntity getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.search_row, parent, false);
        }
        ((TextView) convertView.findViewById(R.id.text1)).setText(getItem(position).getUsernm());
        ((TextView) convertView.findViewById(R.id.text2)).setText(getItem(position).getAddress());
        return convertView;
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(final CharSequence constraint) {
                final FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // List<UserEntity> userList = new ArrayList<>();
                    UserEntity userEntity = new UserEntity();
                    userEntity.setUsernm(constraint.toString());
                    //   userList = getUserByname(userEntity);



                    ApiServices myApiServices = ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/getUsersByName/");
                    try {
                        Call<ServicesResponse> call = myApiServices.getUsersByName(userEntity);
                        call.enqueue(new Callback<ServicesResponse>() {
                            @Override
                            public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                                List<UserEntity> userEntityList1 = response.body().getUserEntityList();
                                filterResults.values = userEntityList1;
                                filterResults.count = userEntityList1.size();
                                publishResults(constraint,filterResults);

                            }

                            @Override
                            public void onFailure(Call<ServicesResponse> call, Throwable t) {

                            }


                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                /*    try {
                        UserEntity userEntity = new UserEntity();
                        userEntity.setUsernm(constraint.toString());

                        viewModel2.getUsersByName(userEntity).observe(UserAutoCompleteAdapter.this, new Observer<List<UserEntity>>() {
                            @Override
                            public void onChanged(@Nullable List<UserEntity> categoryEntities) {
                                if (!categoryEntities.isEmpty()) {
                                    filterResults.values = userList;
                                    filterResults.count = userList.size();
                                    //  setToSpinner(categoryEntities);
                                } else {

                                }


                            }


                        });
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                return filterResults;*/
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults
                    results) {
                if (results != null && results.count > 0) {
                    resultList = (List<UserEntity>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }

    /**
     * Returns a search result for the given book title.
     */


    public List<UserEntity> getUserByname(final UserEntity userEntity) {
        final List<UserEntity> userEntityList = new ArrayList<>();
        ApiServices myApiServices = ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/getUsersByName/");
        try {
            Call<ServicesResponse> call = myApiServices.getUsersByName(userEntity);
            call.enqueue(new Callback<ServicesResponse>() {
                @Override
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    List<UserEntity> userEntityList1 = response.body().getUserEntityList();
                    userEntityList.addAll(userEntityList1);

                }

                @Override
                public void onFailure(Call<ServicesResponse> call, Throwable t) {

                }


            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return userEntityList;

    }


}
