package com.orangebyte.bahetiadmin.modules.models;

public interface CategoryModel extends IdentifiableModel {
    String getAssign_to();

    String getCat_nm();

    String getStatus();
}
