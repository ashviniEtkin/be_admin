package com.orangebyte.bahetiadmin.modules.admin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.entities.FeedbackEntity;

public class FeedbackDetails extends AppCompatActivity {
    TextView assigned_to;
    Button btn_ok;
    TextView comp_desc;
    TextView cust_address;
    TextView cust_comp_dt;
    TextView cust_mob_no;
    TextView cust_nm;
    FeedbackEntity feedbackEntity;
    TextView prod;
    TextView prod_category;
    TextView prod_model;
    TextView prod_subcat;
    TextView rating;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_details);
        BahetiEnterprises.setActBar(getResources().getString(R.string.feedback_details), this, false);
        initView();
        this.feedbackEntity = (FeedbackEntity) getIntent().getSerializableExtra(BahetiEnterprises.COMPLAINT_DETAILS);
        updateData();
    }

    public void initView() {
        this.cust_nm = (TextView) findViewById(R.id.cust_nm);
        this.cust_mob_no = (TextView) findViewById(R.id.cust_mob_no);
        this.cust_address = (TextView) findViewById(R.id.cust_address);
        this.cust_comp_dt = (TextView) findViewById(R.id.cust_comp_dt);
        this.prod_category = (TextView) findViewById(R.id.prod_category);
        this.prod_subcat = (TextView) findViewById(R.id.prod_subcat);
        this.prod = (TextView) findViewById(R.id.prod);
        this.prod_model = (TextView) findViewById(R.id.prod_model);
        this.comp_desc = (TextView) findViewById(R.id.comp_desc);
        this.rating = (TextView) findViewById(R.id.rating);
        this.assigned_to = (TextView) findViewById(R.id.assigned_to);
        this.btn_ok = (Button) findViewById(R.id.btn_ok);
        this.comp_desc.setMovementMethod(new ScrollingMovementMethod());
        this.btn_ok.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                FeedbackDetails.this.onBackPressed();
            }
        });
    }

    public void updateData() {
        BahetiEnterprises.setData(this.feedbackEntity.getUsernm(), this.cust_nm);
        BahetiEnterprises.setData(this.feedbackEntity.getMobile_no(), this.cust_mob_no);
        BahetiEnterprises.setData(this.feedbackEntity.getAddress(), this.cust_address);
        BahetiEnterprises.setData(this.feedbackEntity.getOndate(), this.cust_comp_dt);
        BahetiEnterprises.setData(this.feedbackEntity.getDesp(), this.comp_desc);
        BahetiEnterprises.setData(this.feedbackEntity.getRating(), this.rating);
        BahetiEnterprises.setData(this.feedbackEntity.getEmp_nm(), this.assigned_to);
        BahetiEnterprises.setData(this.feedbackEntity.getTitle(), this.prod);
        BahetiEnterprises.setData(this.feedbackEntity.getModel_no(), this.prod_model);
        BahetiEnterprises.setData(this.feedbackEntity.getSub_cat_name(), this.prod_subcat);
        BahetiEnterprises.setData(this.feedbackEntity.getCat_nm(), this.prod_category);
    }
}
