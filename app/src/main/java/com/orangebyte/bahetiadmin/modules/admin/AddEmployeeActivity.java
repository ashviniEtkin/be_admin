package com.orangebyte.bahetiadmin.modules.admin;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.entities.EmployeeEntity;
import com.orangebyte.bahetiadmin.modules.entities.ManagerEntity;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.EmployeeViewModel.EmployeeFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.ManagerViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.ManagerViewModel.ManagerFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;

import im.delight.android.webview.BuildConfig;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddEmployeeActivity extends AppCompatActivity implements LifecycleRegistryOwner {
    Button add;
    EditText emp_mob_no;
    EditText emp_name, incentive;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    List<ManagerEntity> managerEntityList;
    Spinner manager_spinner;
    Spinner mgr_name_spinner;
    Spinner mgr_type_spinner;
    SweetAlertDialog sweetAlertDialog;
    EmployeeViewModel viewModel;
    String chk_add_update = "";
    EmployeeEntity ee = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_employee);
        initViews();


        chk_add_update = getIntent().getStringExtra(BahetiEnterprises.CHK_ADD_UPDATE);
        if (chk_add_update.equals("1")) {
            BahetiEnterprises.setActBar(getResources().getString(R.string.Update_emp), this, false);
            ee = (EmployeeEntity) getIntent().getSerializableExtra(BahetiEnterprises.FROM);
            add.setText(getString(R.string.update));

            updateData();
        } else {
            add.setText(getString(R.string.add));
            BahetiEnterprises.setActBar(getResources().getString(R.string.Add_emp), this, false);
        }
        this.add.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (TextUtils.isEmpty(emp_mob_no.getText().toString())) {
                    BahetiEnterprises.retShowAlertDialod("Enter Mobile Number", AddEmployeeActivity.this);

                } else if (emp_mob_no.getText().toString().length() < 10) {
                    BahetiEnterprises.retShowAlertDialod("Enter Valid Mobile Number", AddEmployeeActivity.this);

                } else if (TextUtils.isEmpty(emp_name.getText().toString())) {
                    BahetiEnterprises.retShowAlertDialod("Enter Name", AddEmployeeActivity.this);

                } else if (TextUtils.isEmpty(incentive.getText().toString())) {
                    BahetiEnterprises.retShowAlertDialod("Enter Incentive", AddEmployeeActivity.this);
                } else {
                    initModelView();
                }
            }
        });
    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    public void initViews() {
        incentive = (EditText) findViewById(R.id.incentive);
        this.mgr_name_spinner = (Spinner) findViewById(R.id.mgr_name_spinner);
        this.manager_spinner = (Spinner) findViewById(R.id.manager_spinner);
        this.mgr_type_spinner = (Spinner) findViewById(R.id.mgr_type_spinner);
        this.emp_mob_no = (EditText) findViewById(R.id.emp_mob_no);
        this.emp_name = (EditText) findViewById(R.id.emp_name);
        this.add = (Button) findViewById(com.orangebyte.pdflibrary.R.id.add);
        this.sweetAlertDialog = new SweetAlertDialog(this, 5);
        this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
        this.sweetAlertDialog.setTitleText("Loading");
        this.sweetAlertDialog.setCancelable(false);
        this.manager_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 1 || i == 2) {
                    mgr_type_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, Arrays.asList(getResources().getStringArray(R.array.store_emp_type))));
                    setMgrList(manager_spinner.getSelectedItem().toString());
                } else if (i == 3) {
                    mgr_type_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, Arrays.asList(getResources().getStringArray(R.array.service_emp_type))));
                    setMgrList(manager_spinner.getSelectedItem().toString());
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void setMgrList(String mrg_type) {
        try {
            ManagerViewModel viewModel2 = (ManagerViewModel) ViewModelProviders.of(this, new ManagerFactory()).get(ManagerViewModel.class);
            ManagerEntity managerEntity = new ManagerEntity();
            managerEntity.setMgr_type(mrg_type);
            viewModel2.getManager(managerEntity).observe(this, new Observer<List<ManagerEntity>>() {
                public void onChanged(@Nullable List<ManagerEntity> managerEntityList) {
                    if (!managerEntityList.isEmpty()) {
                        sweetAlertDialog.dismiss();
                        setToSpinner(managerEntityList);
                    } else if (managerEntityList.size() == 0) {
                        sweetAlertDialog.dismiss();
                        setToSpinner(new ArrayList());
                    } else {
                        sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setToSpinner(List<ManagerEntity> managerEntities) {
        this.managerEntityList = managerEntities;
        List<String> catList = new ArrayList();
        try {
            for (ManagerEntity managerEntity : managerEntities) {
                catList.add(managerEntity.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.mgr_name_spinner.setAdapter(new ArrayAdapter(getApplicationContext(), R.layout.spinner_layout, catList));


        if (ee != null) {
            for (int i = 0; i < managerEntityList.size(); ) {
                if (managerEntityList.get(i).getId() == Integer.parseInt(ee.getMgr_id())) {

                    mgr_name_spinner.setSelection(i);
                    try {
                        String mgrTy = managerEntityList.get(i).getMgr_type();

                        List<String> mgrTypeList = Arrays.asList(getResources().getStringArray(R.array.mgr_type));

                        for (int j = 0; j < mgrTypeList.size(); ) {
                            if (mgrTypeList.get(j).equalsIgnoreCase(mgrTy)) {
                                manager_spinner.setSelection(j);
                                j = mgrTypeList.size();
                            } else {
                                j++;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    i = managerEntityList.size();


                } else {
                    i++;
                }
            }

        }
    }

    public void initModelView() {
        this.viewModel = (EmployeeViewModel) ViewModelProviders.of(this, new EmployeeFactory()).get(EmployeeViewModel.class);
        EmployeeEntity ee1 = new EmployeeEntity();
        ee1.setAssigned_mgr_type(this.mgr_type_spinner.getSelectedItem().toString());
        ee1.setMob_no(this.emp_mob_no.getText().toString());
        ee1.setEmp_nm(this.emp_name.getText().toString());
        ee1.setStatus("1");
        ee1.setIncentive(incentive.getText().toString());
        ee1.setPassword(this.emp_mob_no.getText().toString());
        ee1.setMgr_id(((ManagerEntity) this.managerEntityList.get(this.mgr_name_spinner.getSelectedItemPosition())).getId() + BuildConfig.VERSION_NAME);
        if (ee != null) {

            ee1.setId(ee.getId());
            addEmp(this.viewModel, ee1, 1);
        } else {
            addEmp(this.viewModel, ee1, 0);
        }
    }

    private void addEmp(EmployeeViewModel viewModel, EmployeeEntity ee, int chk) {
        try {
            this.sweetAlertDialog.show();
            viewModel.addEmp(ee, chk).observe(this, new Observer<List<EmployeeEntity>>() {
                public void onChanged(@Nullable List<EmployeeEntity> employeeEntities) {
                    if (!employeeEntities.isEmpty()) {
                        sweetAlertDialog.dismiss();
                        onBackPressed();
                    } else if (employeeEntities.size() == 0) {
                        sweetAlertDialog.dismiss();
                        onBackPressed();
                    } else {
                        sweetAlertDialog.show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void updateData() {

        emp_mob_no.setText(ee.getMob_no());
        emp_name.setText(ee.getEmp_nm());
        incentive.setText(ee.getIncentive());

        setMgrList("null");

        //ArrayList<String> typeList = getResources().getStringArray(R.string.empl)

    }
}
