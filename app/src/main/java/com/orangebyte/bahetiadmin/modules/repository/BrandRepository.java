package com.orangebyte.bahetiadmin.modules.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.db.daos.BrandDAO;
import com.orangebyte.bahetiadmin.modules.entities.BrandEntity;

import java.util.List;

public class BrandRepository {
    private static BrandRepository sInstance;
    private final BrandDAO brandDAO;
    private final RemoteRepository remoteRepository;

    interface RemoteBrandListener {
        void onCheckResult(boolean z);

        void onRemotReceived(BrandEntity brandEntity);

        void onRemoteReceived(List<BrandEntity> list);
    }

    interface TaskListener {
        void onTaskFinished();
    }

    public static BrandRepository getInstance(RemoteRepository remoteRepository, BrandDAO brandDAO) {
        if (sInstance == null) {
            sInstance = new BrandRepository(remoteRepository, brandDAO);
        }
        return sInstance;
    }

    private BrandRepository(RemoteRepository remoteRepository, BrandDAO brandDAO) {
        this.remoteRepository = remoteRepository;
        this.brandDAO = brandDAO;
    }

    public LiveData<List<BrandEntity>> loadBrandList() {
        fetchBrandList(new RemoteBrandListener() {
            public void onRemotReceived(BrandEntity cmEntity) {
            }

            public void onRemoteReceived(List<BrandEntity> scmEntities) {
                if (scmEntities != null) {
                    BrandRepository.this.insertAllTask(scmEntities);
                }
            }

            public void onCheckResult(boolean b) {
            }
        });
        return this.brandDAO.loadAll();
    }

    public LiveData<List<BrandEntity>> addUpdateBrand(final BrandEntity c, final int chk) {
        addUpdateBrand(c, chk, new RemoteBrandListener() {
            public void onRemotReceived(BrandEntity cmEntity) {
            }

            public void onRemoteReceived(List<BrandEntity> list) {
            }

            public void onCheckResult(boolean b) {
                BrandEntity cm = new BrandEntity();
                cm.setBrand_nm(c.getBrand_nm());
                cm.setCat_id(c.getCat_id());
                cm.setStatus(c.getStatus());
                if (b && chk == 0) {
                    BrandRepository.this.insertSubCategory(cm);
                } else if (b && chk == 1) {
                    cm.setId(c.getId());
                    BrandRepository.this.updateSubCategory(cm);
                }
            }
        });
        return this.brandDAO.loadAll();
    }


    public LiveData<List<BrandEntity>> addEditBrnd(final BrandEntity c, final int chk, Uri file) {
        addBrand( chk,c,file, new RemoteBrandListener() {
            public void onRemotReceived(BrandEntity cmEntity) {
            }

            public void onRemoteReceived(List<BrandEntity> list) {
            }

            public void onCheckResult(boolean b) {
                BrandEntity cm = new BrandEntity();
                cm.setBrand_nm(c.getBrand_nm());
                cm.setCat_id(c.getCat_id());
                cm.setStatus(c.getStatus());
                cm.setImg(c.getImg());
                cm.setToll_free_no(c.getToll_free_no());
                if (b && chk == 0) {
                    BrandRepository.this.insertSubCategory(cm);
                } else if (b && chk == 1) {
                    cm.setId(c.getId());
                    BrandRepository.this.updateSubCategory(cm);
                }
            }
        });
        return this.brandDAO.loadAll();
    }

    public LiveData<List<BrandEntity>> updateBrandStatus(final BrandEntity c) {
        updateBrandStatus(c, new RemoteBrandListener() {
            public void onRemotReceived(BrandEntity cmEntity) {
            }

            public void onRemoteReceived(List<BrandEntity> list) {
            }

            public void onCheckResult(boolean b) {
                if (b) {
                    BrandEntity brandEntity = new BrandEntity();
                    brandEntity.setId(c.getId());
                    brandEntity.setBrand_nm(c.getBrand_nm());
                    brandEntity.setCat_id(c.getCat_id());
                    brandEntity.setStatus(c.getStatus());
                    BrandRepository.this.updateSubCategory(brandEntity);
                }
            }
        });
        return this.brandDAO.loadAll();
    }

    public LiveData<List<BrandEntity>> deleteBrandByID(final BrandEntity brandEntity) {
        deleteBrandByID(brandEntity, new RemoteBrandListener() {
            public void onRemotReceived(BrandEntity brandEntity) {
            }

            public void onRemoteReceived(List<BrandEntity> list) {
            }

            public void onCheckResult(boolean b) {
                if (b) {
                    BrandEntity be = new BrandEntity();
                    be.setId(brandEntity.getId());
                    be.setBrand_nm(brandEntity.getBrand_nm());
                    be.setCat_id(brandEntity.getCat_id());
                    be.setStatus(brandEntity.getStatus());
                    BrandRepository.this.deleteSubCategory(be);
                }
            }
        });
        return this.brandDAO.loadAll();
    }

    private void insertAllTask(List<BrandEntity> brandEntity) {
        insertAllTask(brandEntity, null);
    }

    private void insertAllTask(final List<BrandEntity> brandEntity, @Nullable final TaskListener listener) {
        if (VERSION.SDK_INT >= 3) {
            new AsyncTask<Context, Void, Void>() {
                protected Void doInBackground(Context... params) {
                    BrandRepository.this.brandDAO.deleteAll();
                    BrandRepository.this.brandDAO.insertAll(brandEntity);
                    return null;
                }

                protected void onPostExecute(Void aVoid) {
                    if (VERSION.SDK_INT >= 3) {
                        super.onPostExecute(aVoid);
                    }
                    if (listener != null) {
                        listener.onTaskFinished();
                    }
                }
            }.execute(new Context[]{BahetiEnterprises.getInstance()});
        }
    }

    private void insertSubCategory(BrandEntity brandEntity) {
        insertSubCategory(brandEntity, null);
    }

    private void insertSubCategory(final BrandEntity brandEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                BrandRepository.this.brandDAO.insert(brandEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteSubCategory(BrandEntity brandEntity) {
        deleteSubCategory(brandEntity, null);
    }


    private void addBrand(@NonNull int chk, @NonNull BrandEntity brandEntity, @NonNull Uri fileUri, @NonNull final RemoteBrandListener listener) {
        this.remoteRepository.addBrand(chk, brandEntity, fileUri, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }


    private void deleteSubCategory(final BrandEntity brandEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                BrandRepository.this.brandDAO.delete(brandEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void updateSubCategory(BrandEntity brandEntity) {
        updateSubCategory(brandEntity, null);
    }

    private void updateSubCategory(final BrandEntity brandEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                BrandRepository.this.brandDAO.update(brandEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteAllSubCategories() {
        deleteAllSubCategories(null);
    }

    private void deleteAllSubCategories(@Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                BrandRepository.this.brandDAO.deleteAll();
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void fetchBrandList(@NonNull final RemoteBrandListener listener) {
        this.remoteRepository.getBrandyList(new RemoteCallback<List<BrandEntity>>() {
            public void onSuccess(List<BrandEntity> response) {
                listener.onRemoteReceived(response);
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }

    private void addUpdateBrand(@NonNull BrandEntity brandEntity, @NonNull int chk, @NonNull final RemoteBrandListener listener) {
        if (chk == 1) {
            this.remoteRepository.updateBrand(brandEntity, new RemoteCallback<Boolean>() {
                public void onSuccess(Boolean response) {
                    listener.onCheckResult(response.booleanValue());
                }

                public void onUnauthorized() {
                }

                public void onFailed(Throwable throwable) {
                }
            });
        } else {
            this.remoteRepository.addBrand(brandEntity, new RemoteCallback<Boolean>() {
                public void onSuccess(Boolean response) {
                    listener.onCheckResult(response.booleanValue());
                }

                public void onUnauthorized() {
                }

                public void onFailed(Throwable throwable) {
                }
            });
        }
    }

    private void updateBrandStatus(@NonNull BrandEntity brandEntity, @NonNull final RemoteBrandListener listener) {
        this.remoteRepository.updateBrandStatus(brandEntity, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }

    private void deleteBrandByID(@NonNull BrandEntity brandEntity, @NonNull final RemoteBrandListener listener) {
        this.remoteRepository.deleteBrandByID(brandEntity, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }
}
