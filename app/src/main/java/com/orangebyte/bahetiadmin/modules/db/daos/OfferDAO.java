package com.orangebyte.bahetiadmin.modules.db.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.orangebyte.bahetiadmin.modules.entities.OfferEntity;

import java.util.List;

@Dao
public interface OfferDAO {
    @Delete
    void delete(OfferEntity... offerEntityArr);

    @Query("DELETE FROM offers ")
    void deleteAll();

    @Delete
    void deleteAll(List<OfferEntity> list);

    @Insert(onConflict = 1)
    void insert(OfferEntity... offerEntityArr);

    @Insert(onConflict = 1)
    void insertAll(List<OfferEntity> list);

    @Query("SELECT * FROM offers ORDER BY status DESC")
    LiveData<List<OfferEntity>> loadAll();

    @Update
    void update(OfferEntity... offerEntityArr);

    @Update
    void updateAll(List<OfferEntity> list);
}
