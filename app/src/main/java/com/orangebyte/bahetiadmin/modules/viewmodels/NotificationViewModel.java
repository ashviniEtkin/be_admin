package com.orangebyte.bahetiadmin.modules.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider.NewInstanceFactory;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.modules.db.local.AppDatabase;
import com.orangebyte.bahetiadmin.modules.db.local.DatabaseCreator;
import com.orangebyte.bahetiadmin.modules.entities.NotificationEntity;
import com.orangebyte.bahetiadmin.modules.repository.NotificationRepository;
import com.orangebyte.bahetiadmin.modules.repository.RemoteRepository;
import com.orangebyte.bahetiadmin.modules.ui.AllContract.NotificationDataStreams;

import java.util.List;

import im.delight.android.webview.BuildConfig;

public class NotificationViewModel extends ViewModel implements NotificationDataStreams {
    private NotificationRepository notificationRepository;

    public static class NotificationFactory extends NewInstanceFactory {
        NotificationRepository mNotificationRepository;

        public NotificationFactory() {
            this(null, null);
        }

        public NotificationFactory(AppDatabase appDatabase) {
            this(NotificationRepository.getInstance(RemoteRepository.getInstance(), appDatabase.notificationDAO()), BuildConfig.VERSION_NAME);
        }

        public NotificationFactory(@Nullable NotificationRepository mNotificationRepository, @Nullable String abc) {
            this.mNotificationRepository = mNotificationRepository;
        }

        public <T extends ViewModel> T create(Class<T> cls) {
            return (T) new NotificationViewModel(this.mNotificationRepository);
        }
    }

    public NotificationViewModel() {
        this(null);
    }

    public NotificationViewModel(@Nullable NotificationRepository notificationRepository) {
        if (notificationRepository != null) {
            this.notificationRepository = notificationRepository;
        }
    }

    public LiveData<List<NotificationEntity>> addNotifcation(NotificationEntity notificationEntity) {
        onDatabaseCreated();
        return this.notificationRepository.addNoti(notificationEntity);
    }

    private void onDatabaseCreated() {
        AppDatabase appDatabase = DatabaseCreator.getInstance().getDatabase();
        try {
            if (this.notificationRepository == null) {
                this.notificationRepository = NotificationRepository.getInstance(RemoteRepository.getInstance(), appDatabase.notificationDAO());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
