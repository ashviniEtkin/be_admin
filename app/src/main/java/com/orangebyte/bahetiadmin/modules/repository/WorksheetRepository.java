package com.orangebyte.bahetiadmin.modules.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.orangebyte.bahetiadmin.modules.entities.WorkSheetEntity;

import java.util.List;

public class WorksheetRepository {
    private static WorksheetRepository sInstance;
    private final RemoteRepository remoteRepository;

    interface RemoteWorksheetListener {
        void onCheckResult(boolean z);

        void onRemotReceived(WorkSheetEntity workSheetEntity);

        void onRemoteReceived(List<WorkSheetEntity> list);
    }

    public static WorksheetRepository getInstance(RemoteRepository remoteRepository) {
        if (sInstance == null) {
            sInstance = new WorksheetRepository(remoteRepository);
        }
        return sInstance;
    }

    private WorksheetRepository(RemoteRepository remoteRepository) {
        this.remoteRepository = remoteRepository;
    }

    public LiveData<List<WorkSheetEntity>> worksheetList(WorkSheetEntity workSheetEntity) {
        final MutableLiveData<List<WorkSheetEntity>> data = new MutableLiveData();
        fetchProductList(new RemoteWorksheetListener() {
            public void onRemotReceived(WorkSheetEntity cmEntity) {
            }

            public void onRemoteReceived(List<WorkSheetEntity> workSheetEntityList1) {
                if (workSheetEntityList1 != null) {
                    data.setValue(workSheetEntityList1);
                }
            }

            public void onCheckResult(boolean b) {
            }
        }, workSheetEntity);
        return data;
    }

    public void fetchProductList(@NonNull final RemoteWorksheetListener listener, WorkSheetEntity workSheetEntity) {
        this.remoteRepository.getEmpWorksheet(workSheetEntity, new RemoteCallback<List<WorkSheetEntity>>() {
            public void onSuccess(List<WorkSheetEntity> response) {
                listener.onRemoteReceived(response);
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }
}
