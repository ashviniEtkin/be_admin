package com.orangebyte.bahetiadmin.modules.db.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.orangebyte.bahetiadmin.modules.entities.CategoryEntity;

import java.util.List;

@Dao
public interface CategoryDAO {
    @Delete
    void delete(CategoryEntity... categoryEntityArr);

    @Query("DELETE FROM category ")
    void deleteAll();

    @Delete
    void deleteAll(List<CategoryEntity> list);

    @Insert(onConflict = 1)
    void insert(CategoryEntity... categoryEntityArr);

    @Insert(onConflict = 1)
    void insertAll(List<CategoryEntity> list);

    @Query("SELECT * FROM category ")
    LiveData<List<CategoryEntity>> loadAll();

    @Update
    void update(CategoryEntity... categoryEntityArr);

    @Update
    void updateAll(List<CategoryEntity> list);
}
