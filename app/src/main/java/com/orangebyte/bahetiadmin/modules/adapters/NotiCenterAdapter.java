package com.orangebyte.bahetiadmin.modules.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.entities.NotificationCenter;

import java.util.List;

/**
 * Created by Ashvini on 4/2/2018.
 */

public class NotiCenterAdapter extends RecyclerView.Adapter<NotiCenterAdapter.NotiCenterHolder> {

    List<NotificationCenter> notificationCenterList;
    Context ctx;

    public NotiCenterAdapter(List<NotificationCenter> notificationCenterList, Context ctx) {
        this.notificationCenterList = notificationCenterList;
        this.ctx = ctx;
    }

    @Override
    public NotiCenterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.noti_row, parent, false);
        return new NotiCenterHolder(view);
    }

    @Override
    public void onBindViewHolder(NotiCenterHolder holder, int position) {

        NotificationCenter notificationCenter = notificationCenterList.get(position);

        holder.txt_date.setText(BahetiEnterprises.retDate(notificationCenter.getDt()));
        holder.txt_msg.setText(notificationCenter.getMsg());
        holder.txt_title.setText(notificationCenter.getTitle());

    }

    @Override
    public int getItemCount() {
        return notificationCenterList.size();
    }

    public class NotiCenterHolder extends ViewHolder {


        TextView txt_date, txt_msg, txt_title;

        public NotiCenterHolder(View itemView) {
            super(itemView);
            txt_date = (TextView) itemView.findViewById(R.id.txt_date);
            txt_msg = (TextView) itemView.findViewById(R.id.txt_msg);
            txt_title = (TextView) itemView.findViewById(R.id.txt_title);

        }
    }
}
