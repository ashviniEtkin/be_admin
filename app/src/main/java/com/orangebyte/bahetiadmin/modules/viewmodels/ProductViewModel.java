package com.orangebyte.bahetiadmin.modules.viewmodels;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider.NewInstanceFactory;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.modules.db.local.AppDatabase;
import com.orangebyte.bahetiadmin.modules.db.local.DatabaseCreator;
import com.orangebyte.bahetiadmin.modules.entities.ProductEntity;
import com.orangebyte.bahetiadmin.modules.repository.ProductRepository;
import com.orangebyte.bahetiadmin.modules.repository.RemoteRepository;
import com.orangebyte.bahetiadmin.modules.ui.AllContract.ProductDataStreams;

import java.util.List;

import im.delight.android.webview.BuildConfig;

public class ProductViewModel extends ViewModel implements ProductDataStreams {
    private ProductRepository productRepository;

    public static class ProductFactory extends NewInstanceFactory {
        ProductRepository pProductRepository;

        public ProductFactory() {
            this(null, null);
        }

        public ProductFactory(AppDatabase appDatabase) {
            this(ProductRepository.getInstance(RemoteRepository.getInstance(), appDatabase.productDAO()), BuildConfig.VERSION_NAME);
        }

        public ProductFactory(@Nullable ProductRepository pProductRepository, @Nullable String abc) {
            this.pProductRepository = pProductRepository;
        }

        public <T extends ViewModel> T create(Class<T> cls) {
            return (T) new ProductViewModel(this.pProductRepository);
        }
    }

    public ProductViewModel() {
        this(null);
    }

    public ProductViewModel(@Nullable ProductRepository productRepository) {
        if (productRepository != null) {
            this.productRepository = productRepository;
        }
    }

    public LiveData<List<ProductEntity>> getProduct(ProductEntity productEntity) {
        onDatabaseCreated();
        return subscribeToLocalData(productEntity);
    }

    private LiveData<List<ProductEntity>> subscribeToLocalData(final ProductEntity productEntity) {
        return Transformations.switchMap(DatabaseCreator.getInstance().isDatabaseCreated(), new Function<Boolean, LiveData<List<ProductEntity>>>() {
            public LiveData<List<ProductEntity>> apply(Boolean isDbCreated) {
                if (!Boolean.TRUE.equals(isDbCreated)) {
                    return null;
                }
                ProductViewModel.this.onDatabaseCreated();
                return ProductViewModel.this.subscribeProductObservable(productEntity);
            }
        });
    }

    private LiveData<List<ProductEntity>> subscribeProductObservable(ProductEntity productEntity) {
        return this.productRepository.loadProductList(productEntity);
    }

    private void onDatabaseCreated() {
        AppDatabase appDatabase = DatabaseCreator.getInstance().getDatabase();
        try {
            if (this.productRepository == null) {
                this.productRepository = ProductRepository.getInstance(RemoteRepository.getInstance(), appDatabase.productDAO());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
