package com.orangebyte.bahetiadmin.modules.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;

import com.google.gson.annotations.SerializedName;
import com.orangebyte.bahetiadmin.modules.models.ManagerModel;

import java.io.Serializable;

@Entity(tableName = "managers")
public class ManagerEntity extends IdentifiableEntity implements ManagerModel, Serializable {
    @SerializedName("mgr_type")
    @ColumnInfo(name = "mgr_type")
    private String mgr_type;
    @SerializedName("mobile_no")
    @ColumnInfo(name = "mobile_no")
    private String mobile_no;
    @SerializedName("name")
    @ColumnInfo(name = "name")
    private String name;
    @SerializedName("permi")
    @ColumnInfo(name = "permi")
    private String permi;
    @SerializedName("status")
    @ColumnInfo(name = "status")
    private String status;

    @SerializedName("password")
    @ColumnInfo(name = "password")
    private String password;

    public ManagerEntity() {
    }

    public /* bridge */ /* synthetic */ long getId() {
        return super.getId();
    }

    public /* bridge */ /* synthetic */ void setId(long j) {
        super.setId(j);
    }

    @Ignore
    public ManagerEntity(long id, String name, String mobile_no, String mgr_type, String status) {
        this.id = id;
        this.name = name;
        this.mobile_no = mobile_no;
        this.mgr_type = mgr_type;
        this.status = status;
    }

    @Ignore
    public ManagerEntity(ManagerModel mm) {
        this.id = mm.getId();
        this.name = mm.getName();
        this.mobile_no = mm.getMobile_no();
        this.mgr_type = mm.getMgr_type();
        this.status = mm.getStatus();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPermi() {
        return this.permi;
    }

    public void setPermi(String permi) {
        this.permi = permi;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_no() {
        return this.mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getMgr_type() {
        return this.mgr_type;
    }

    public void setMgr_type(String mgr_type) {
        this.mgr_type = mgr_type;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return this.name;
    }

    public String getStatus() {
        return this.status;
    }
}
