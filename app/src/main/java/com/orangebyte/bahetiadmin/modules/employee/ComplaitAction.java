package com.orangebyte.bahetiadmin.modules.employee;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.orangebyte.bahetiadmin.R;
import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.applications.PathUtil;
import com.orangebyte.bahetiadmin.modules.admin.ComplaintDetails;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;
import com.orangebyte.bahetiadmin.modules.entities.OfferEntity;
import com.orangebyte.bahetiadmin.modules.manager.LodgeComplaint;
import com.orangebyte.bahetiadmin.modules.models.ServicesResponse;
import com.orangebyte.bahetiadmin.modules.repository.ApiServices;
import com.orangebyte.bahetiadmin.modules.repository.RemoteCallback;
import com.orangebyte.bahetiadmin.modules.repository.ServiceFactory;
import com.orangebyte.bahetiadmin.modules.viewmodels.ComplaintsViewModel;
import com.orangebyte.bahetiadmin.modules.viewmodels.ComplaintsViewModel.ComplaintFactory;
import com.orangebyte.bahetiadmin.sweetalertDialog.SweetAlertDialog;
import com.orangebyte.pdflibrary.PdfDocument;
import com.orangebyte.pdflibrary.viewRenderer.AbstractViewRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import im.delight.android.webview.BuildConfig;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class ComplaitAction extends AppCompatActivity implements LifecycleRegistryOwner {
    Spinner action_tk_txt;
    Button btn_send, btn_proof;
    ComplaintEntity ce;
    ComplaintsViewModel cnmplViewModel;
    ComplaintFactory factory;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    int mDay;
    int mMonth;
    int mYear;
    TextView name;
    TextView select_date;
    Spinner status;
    SweetAlertDialog sweetAlertDialog;
    File file;
    LinearLayout mContent;
    View view;
    signature mSignature;
    Bitmap bitmap;
    ImageButton mClear, cancel;
    LinearLayout resolved_container, action_taken_container, total_amount_container, amount_to_pay_container, extra_expenses;
    ImageButton close;
    EditText paid_amt, extra_amt, action_descriptiom;
    String DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/.DigitSign/";
    String pic_name = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
    String StoredPath = DIRECTORY + pic_name + ".png";
    static String IMAGE_DIRECTORY_NAME = ".PROOF";
    int REQUEST_CAMERA = 330;
    Uri selectedImageUri = null;
    int RESULT_LOAD_IMG = 360;
    TextView total_amt, txt_signature, txt_additional_desc, txt_proof;
    Dialog signDialog;
    TextView service_no, rec_dt, recp_name, recp_amt, recp_rs_n_dt, rep_rs;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complait_action);
        BahetiEnterprises.setActBar(getResources().getString(R.string.change_status), this, false);
        this.ce = (ComplaintEntity) getIntent().getSerializableExtra(BahetiEnterprises.FROM);

        init();

        this.btn_send.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (ComplaitAction.this.status.getSelectedItemPosition() == 0) {
                    BahetiEnterprises.retShowAlertDialod("Please Status", ComplaitAction.this);
                } else /*if (TextUtils.isEmpty(ComplaitAction.this.action_tk_txt.getSelectedItem().toString())) {
                    BahetiEnterprises.retShowAlertDialod("Please Enter Action", ComplaitAction.this);
                } else*/ {
                    ComplaitAction.this.updateStatus();
                }
            }
        });


        status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 3) {
                    total_amount_container.setVisibility(View.VISIBLE);
                    amount_to_pay_container.setVisibility(View.VISIBLE);
                    resolved_container.setVisibility(View.VISIBLE);
                    action_taken_container.setVisibility(View.VISIBLE);
                    List<String> actionTaken_resolved = Arrays.asList(getResources().getStringArray(R.array.action_taken_resolved));
                    ArrayAdapter adapter = new ArrayAdapter(ComplaitAction.this, R.layout.spinner_layout, actionTaken_resolved);
                    action_tk_txt.setAdapter(adapter);
                } else if (i == 1) {
                    total_amount_container.setVisibility(View.GONE);
                    amount_to_pay_container.setVisibility(View.GONE);
                    resolved_container.setVisibility(View.GONE);
                    action_taken_container.setVisibility(View.VISIBLE);
                    List<String> actionTaken_resolved = Arrays.asList(getResources().getStringArray(R.array.action_taken_on_hold));
                    ArrayAdapter adapter = new ArrayAdapter(ComplaitAction.this, R.layout.spinner_layout, actionTaken_resolved);
                    action_tk_txt.setAdapter(adapter);
                } else {
                    total_amount_container.setVisibility(View.GONE);
                    amount_to_pay_container.setVisibility(View.GONE);
                    action_taken_container.setVisibility(View.GONE);
                    resolved_container.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    public void chooseImage(View view) {

        final CharSequence[] options = {"Camera", "Choose from Gallery", "Cancel"};


        AlertDialog.Builder builder = new AlertDialog.Builder(ComplaitAction.this);

        builder.setTitle("Add Invoice!");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Camera"))

                {

                    Intent intents = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    selectedImageUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

                    intents.putExtra(MediaStore.EXTRA_OUTPUT, selectedImageUri);


                    startActivityForResult(intents, REQUEST_CAMERA);


                } else if (options[item].equals("Choose from Gallery")) {

                    /*Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(intent, 2);*/

                    if (ActivityCompat.checkSelfPermission(ComplaitAction.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ComplaitAction.this,
                                new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE
                                },
                                110);

                    } else {
          /*  Intent intent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("pdf*//*");
            startActivityForResult(Intent.createChooser(intent, "Select File"), this.RESULT_LOAD_IMG);

*/
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        intent.setType("image/*");
                        startActivityForResult(intent, RESULT_LOAD_IMG);
                    }


                } else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }

            }

        });

        builder.show();

    }

    public Uri getOutputMediaFileUri(int type) {
        File fl = getOutputMediaFile(type);
        Uri si = Uri.fromFile(fl);
        return si;
    }

    private static File getOutputMediaFile(int type) {


        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);


        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }


        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA && resultCode == -1) {


        } else if (requestCode == this.RESULT_LOAD_IMG && resultCode == -1 && data != null) {
            this.selectedImageUri = data.getData();
            Cursor cursor = managedQuery(this.selectedImageUri, new String[]{"_data"}, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow("_data");
            cursor.moveToFirst();
            String selectedImagePath = cursor.getString(column_index);
            String filePath = BahetiEnterprises.getRealPathFromURI(data.getDataString());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;

        } else {
            Toast.makeText(getApplicationContext(), "you haven't select image", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(ComplaitAction.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ComplaitAction.this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE
                    },
                    110);

        }
    }

    public void updateStatus() {
        ComplaintEntity complaintEntity = new ComplaintEntity();
        complaintEntity.setId(this.ce.getId());
        complaintEntity.setLast_atnd_on(BahetiEnterprises.tStamp(this.select_date.getText().toString()) + BuildConfig.VERSION_NAME);
        if (status.getSelectedItemPosition() == 2) {
            complaintEntity.setActn_tkn_st("-");

        } else {
            complaintEntity.setActn_tkn_st(this.action_tk_txt.getSelectedItem().toString());
        }
        complaintEntity.setStatus(this.status.getSelectedItem().toString());
        complaintEntity.setSignature_url(ce.getUsernm().replace(" ", "_") + System.currentTimeMillis() + ".png");

        complaintEntity.setProof_url(ce.getUsernm().replace(" ", "_") + "proof_irl" + System.currentTimeMillis() + ".jpeg");


        if (TextUtils.isEmpty(paid_amt.getText().toString())) {
            complaintEntity.setPaid_amt("0");
        } else {
            complaintEntity.setPaid_amt(paid_amt.getText().toString());
        }

        if (TextUtils.isEmpty(extra_amt.getText().toString())) {
            complaintEntity.setExtra_charges("0");
        } else {
            complaintEntity.setExtra_charges(extra_amt.getText().toString());
        }

        if (TextUtils.isEmpty(action_descriptiom.getText().toString())) {
            complaintEntity.setExtra_charges_desc("null");
        } else {
            complaintEntity.setExtra_charges_desc(action_descriptiom.getText().toString());
        }


        this.factory = new ComplaintFactory();
        this.cnmplViewModel = (ComplaintsViewModel) ViewModelProviders.of(this, this.factory).get(ComplaintsViewModel.class);
        if (complaintEntity.getStatus().equalsIgnoreCase(getString(R.string.Resolved_Closed))) {


            view.setDrawingCacheEnabled(true);
            mSignature.save(view, StoredPath);

            if (selectedImageUri == null) {
                BahetiEnterprises.retShowAlertDialod("Please Take Proof", ComplaitAction.this);
            } else {
                updateResolvedComplaint(complaintEntity);
                sendInvoice();
            }
        } else {

            sweetAlertDialog.show();
            this.cnmplViewModel.assignedEmp(complaintEntity, 4).observe(this, new Observer<List<Boolean>>() {
                public void onChanged(@Nullable List<Boolean> booleen) {
                    if (!booleen.isEmpty()) {
                        ComplaitAction.this.sweetAlertDialog.dismiss();

                        ComplaitAction.this.onBackPressed();

                    } else if (booleen.size() == 0) {
                        ComplaitAction.this.sweetAlertDialog.dismiss();
                        ComplaitAction.this.onBackPressed();

                    } else {
                        ComplaitAction.this.sweetAlertDialog.show();
                    }

                    sendInvoice();

                }
            });

        }
        ComplaintDetails.chek_backpress = 1;
    }


    public void updateResolvedComplaint(ComplaintEntity complaintEntity) {
        //  File file = null;
        File file = null;
        File proof_file = null;
        RequestBody requestBody = null;
        RequestBody proof_requestBody = null;
        MultipartBody.Part fileToUpload = null;
        MultipartBody.Part fileToUpload2 = null;
        try {
            // file = new File(PathUtil.getPath(BahetiEnterprises.getInstance(), fileUri));
            file = new File(StoredPath);

        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            proof_file = new File(PathUtil.getPath(BahetiEnterprises.getInstance(), selectedImageUri));
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        } catch (Exception e2) {
            e2.printStackTrace();
        }


        try {
            proof_requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), proof_file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            fileToUpload = MultipartBody.Part.createFormData("uploaded_file", file.getName(), requestBody);
        } catch (Exception e22) {
            e22.printStackTrace();
        }

        try {
            fileToUpload2 = MultipartBody.Part.createFormData("proof_uploaded_file", proof_file.getName(), proof_requestBody);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ApiServices myApiServices = ServiceFactory.makeService(BahetiEnterprises.BASE_URL + "/addOffer/");
        try {
            Call<ServicesResponse> call;
            HashMap<String, RequestBody> map = new HashMap();
            map.put("id", BahetiEnterprises.retRequestBody(complaintEntity.getId() + ""));
            map.put("status", BahetiEnterprises.retRequestBody(complaintEntity.getStatus()));
            map.put("actn_tkn_st", BahetiEnterprises.retRequestBody(complaintEntity.getActn_tkn_st()));
            map.put("last_atnd_on", BahetiEnterprises.retRequestBody(complaintEntity.getLast_atnd_on()));
            map.put("signature_url", BahetiEnterprises.retRequestBody(complaintEntity.getSignature_url()));
            map.put("extra_charges", BahetiEnterprises.retRequestBody(complaintEntity.getExtra_charges()));
            map.put("extra_charges_desc", BahetiEnterprises.retRequestBody(complaintEntity.getExtra_charges_desc()));
            map.put("proof_url", BahetiEnterprises.retRequestBody(complaintEntity.getProof_url()));
            map.put("paid_amt", BahetiEnterprises.retRequestBody(complaintEntity.getPaid_amt()));

            call = myApiServices.actionTakenComplaint(fileToUpload, fileToUpload2, map);

            sweetAlertDialog.show();
            call.enqueue(new Callback<ServicesResponse>() {
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    sweetAlertDialog.dismiss();
                    onBackPressed();
                    try {
                        Boolean.valueOf(((ServicesResponse) response.body()).isSuccess());
                    } catch (Exception e) {
                        e.printStackTrace();
                        //remoteCallback.onSuccess(Boolean.valueOf(true));
                    }
                }

                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    t.printStackTrace();
                    sweetAlertDialog.dismiss();
                    onBackPressed();
                    //  remoteCallback.onSuccess(Boolean.valueOf(true));
                }
            });
        } catch (Exception e222) {
            sweetAlertDialog.dismiss();
            onBackPressed();
            e222.printStackTrace();
        }
    }


    public void init() {

        signDialog = new Dialog(ComplaitAction.this);
        signDialog.setCancelable(false);
        signDialog.setContentView(R.layout.dialog_signature);

        extra_expenses = (LinearLayout) findViewById(R.id.extra_expenses);
        total_amount_container = (LinearLayout) findViewById(R.id.total_amount_container);
        amount_to_pay_container = (LinearLayout) findViewById(R.id.amount_to_pay_container);
        txt_proof = (TextView) findViewById(R.id.txt_proof);
        txt_additional_desc = (TextView) findViewById(R.id.txt_additional_desc);
        txt_signature = (TextView) findViewById(R.id.txt_signature);

        txt_proof.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage(view);
            }
        });


        txt_additional_desc.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (extra_expenses.getVisibility() == View.VISIBLE) {
                    extra_expenses.setVisibility(View.GONE);
                } else {
                    extra_expenses.setVisibility(View.VISIBLE);
                }
            }
        });
        txt_signature.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                signDialog.show();

            }
        });


        this.select_date = (TextView) findViewById(R.id.select_date);
        this.status = (Spinner) findViewById(R.id.status);
        this.name = (TextView) findViewById(R.id.name);
        this.action_tk_txt = (Spinner) findViewById(R.id.action_tk_txt);
        this.btn_send = (Button) findViewById(R.id.btn_send);
        Calendar c = Calendar.getInstance();
        this.mYear = c.get(Calendar.YEAR);
        this.mMonth = c.get(Calendar.MONTH);
        this.mDay = c.get(Calendar.DATE);
        this.select_date.setText(this.mDay + "-" + (this.mMonth + 1) + "-" + this.mYear);
        this.factory = new ComplaintFactory();
        this.cnmplViewModel = (ComplaintsViewModel) ViewModelProviders.of(this, this.factory).get(ComplaintsViewModel.class);
        this.sweetAlertDialog = new SweetAlertDialog(this, 5);
        this.sweetAlertDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.app_color));
        this.sweetAlertDialog.setTitleText("Loading");
        this.sweetAlertDialog.setCancelable(false);
        resolved_container = (LinearLayout) findViewById(R.id.resolved_container);
        mContent = (LinearLayout) signDialog.findViewById(R.id.linearLayout);
        mSignature = new signature(getApplicationContext(), null);
        mSignature.setBackgroundColor(Color.WHITE);
        // Dynamically generating Layout through java code
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mClear = (ImageButton) signDialog.findViewById(R.id.clear);
        cancel = (ImageButton) signDialog.findViewById(R.id.cancel);
        action_taken_container = (LinearLayout) findViewById(R.id.action_taken_container);
        view = mContent;

        mClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("tag", "Panel Cleared");
                mSignature.clear();
                //mGetSign.setEnabled(false);
            }
        });

        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                signDialog.dismiss();
            }
        });


        file = new File(DIRECTORY);
        if (!file.exists()) {
            file.mkdir();
        }

        total_amt = (TextView) findViewById(R.id.total_amt);
        close = (ImageButton) findViewById(R.id.close);
        action_descriptiom = (EditText) findViewById(R.id.action_descriptiom);
        extra_amt = (EditText) findViewById(R.id.extra_amt);
        paid_amt = (EditText) findViewById(R.id.paid_amt);
        btn_proof = (Button) findViewById(R.id.btn_proof);
        this.name.setText(this.ce.getUsernm());
        paid_amt.setText(ce.getPaid_amt());
        total_amt.setText(ce.getPaid_amt());
        extra_amt.setText(ce.getExtra_charges());
        btn_proof.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage(view);
            }
        });
        extra_amt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int eAmt = 0;
                try {
                    eAmt = Integer.parseInt(extra_amt.getText().toString());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                int pamt = 0;

                try {
                    pamt = Integer.parseInt(paid_amt.getText().toString());
                } catch (NumberFormatException e) {
                    pamt = 0;
                    e.printStackTrace();
                }

                try {
                    int total_value = pamt + eAmt;
                    total_amt.setText(total_value + "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        paid_amt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int eAmt = 0;
                try {
                    eAmt = Integer.parseInt(extra_amt.getText().toString());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                int pamt = 0;

                try {
                    pamt = Integer.parseInt(paid_amt.getText().toString());
                } catch (NumberFormatException e) {
                    pamt = 0;
                    e.printStackTrace();
                }

                try {
                    int total_value = pamt + eAmt;
                    total_amt.setText(total_value + "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                action_descriptiom.setText("");
                extra_amt.setText("");
            }
        });

    }

    public LifecycleRegistry getLifecycle() {
        return this.lifecycleRegistry;
    }

    public void updateComplaintData(ComplaintEntity complaintEntity) {
      /*  this.iv_name.setText(complaintEntity.getUsernm());
        this.iv_product_nm.setText(complaintEntity.getTitle());
        this.iv_product_model_no.setText(complaintEntity.getModel_no());
        this.iv_status.setText(getResources().getString(R.string.resolved));
        this.iv_resloved_date.setText(this.mDay + "/" + this.mMonth + "/" + this.mYear);
        this.iv_paied_amt.setText(this.amt_to_pay.getText().toString());*/

        // TextView service_no, rec_dt, recp_name, recp_amt, recp_rs_n_dt, rep_rs;


        this.recp_name.setText(complaintEntity.getUsernm());
        this.service_no.setText(getString(R.string.service_request_no) + " " + complaintEntity.getCom_no());
        this.rec_dt.setText(getString(R.string.Date) + this.mDay + "/" + this.mMonth + "/" + this.mYear);
        this.recp_rs_n_dt.setText(" 388293 Date : " + this.mDay + "/" + this.mMonth + "/" + this.mYear);
        this.recp_amt.setText(this.total_amt.getText().toString());
    }

    public void updateInstallationData(CallLogInstallationDemoEntity cdemo) {
      /*  this.iv_name.setText(cdemo.getUsernm());
        this.iv_product_nm.setText(cdemo.getTitle());
        this.iv_product_model_no.setText(cdemo.getModel_no());
        this.iv_status.setText(getResources().getString(R.string.resolved));
        this.iv_resloved_date.setText(this.mDay + "/" + this.mMonth + "/" + this.mYear);
        this.iv_paied_amt.setText(this.amt_to_pay.getText().toString());*/

        this.recp_name.setText(cdemo.getUsernm());
        this.service_no.setText(getString(R.string.service_request_no) + " " + cdemo.getCom_no());
        this.rec_dt.setText(getString(R.string.Date) + this.mDay + "/" + this.mMonth + "/" + this.mYear);
        this.recp_rs_n_dt.setText(" 388293 Date : " + this.mDay + "/" + this.mMonth + "/" + this.mYear);
        this.recp_amt.setText(this.total_amt.getText().toString());
    }

    public void sendInvoice() {
        final AbstractViewRenderer abstractViewRenderer = new AbstractViewRenderer(this, R.layout.invoice_after_job_complete) {
            protected void initView(View view) {
                initPdfView(view);

                updateComplaintData((ComplaintEntity) getIntent().getSerializableExtra(BahetiEnterprises.FROM));

                return;
            }
        };
        ProgressDialog pd = new ProgressDialog(ComplaitAction.this);
        pd.setMessage("Creating Invoice");
        pd.show();
        createMulPDf(ComplaitAction.this, abstractViewRenderer, pd);
    }


    public void initPdfView(View pdfView) {


        this.service_no = (TextView) pdfView.findViewById(R.id.service_no);
        this.rec_dt = (TextView) pdfView.findViewById(R.id.rec_dt);
        this.recp_name = (TextView) pdfView.findViewById(R.id.recp_name);
        this.recp_amt = (TextView) pdfView.findViewById(R.id.recp_amt);
        this.recp_rs_n_dt = (TextView) pdfView.findViewById(R.id.recp_rs_n_dt);
        this.rep_rs = (TextView) pdfView.findViewById(R.id.rep_rs);


    }


    public void createMulPDf(Context ctx, AbstractViewRenderer pages, final ProgressDialog progressDialog) {
        PdfDocument doc = new PdfDocument(ctx);
        String file_nm = "invoice_" + System.currentTimeMillis() + BuildConfig.VERSION_NAME;
        File f = new File(Environment.getExternalStorageDirectory() + "/.Orange/.InvoiceSummary/" + file_nm);
        if (f.exists() && !f.isDirectory()) {
            f.delete();
        }
        File folder = new File(Environment.getExternalStorageDirectory() + File.separator + ".Orange" + File.separator + ".InvoiceSummary");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        if (success) {
            Log.e("Dir", "Created");
        } else {
            Log.e("Dir", "Not Created");
        }
        doc.addPage(pages);
        doc.setOrientation(PdfDocument.A4_MODE.PORTRAIT);
        doc.setProgressTitle(com.orangebyte.pdflibrary.R.string.app_name);
        doc.setProgressMessage(R.string.pdf_message);
        doc.setFileName(file_nm);
        doc.setSaveDirectory(folder);
        doc.setInflateOnMainThread(false);
        doc.setListener(new PdfDocument.Callback() {
            public void onComplete(File file) {
                Log.i(PdfDocument.TAG_PDF_MY_XML, "Complete");
                new AsyncTask() {
                    protected void onPreExecute() {
                        super.onPreExecute();
                    }

                    protected Object doInBackground(Object[] objects) {
                        return null;
                    }

                    protected void onPostExecute(Object o) {
                        super.onPostExecute(o);
                        progressDialog.dismiss();
                    }
                }.execute(new Object[0]);
            }

            public void onError(Exception e) {
                e.printStackTrace();
                Log.i(PdfDocument.TAG_PDF_MY_XML, "Error");
            }
        });
        doc.createPdf(ctx);
    }


    public class signature extends View {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public void save(View v, String StoredPath) {
            Log.v("tag", "Width: " + v.getWidth());
            Log.v("tag", "Height: " + v.getHeight());
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);
            }
            Canvas canvas = new Canvas(bitmap);
            try {
                // Output the file
                FileOutputStream mFileOutStream = new FileOutputStream(StoredPath);
                v.draw(canvas);
                // Convert the output file to Image such as .png
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                mFileOutStream.flush();
                mFileOutStream.close();
            } catch (Exception e) {
                Log.v("log_tag", e.toString());
            }
        }

        public void clear() {
            path.reset();
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();
            //mGetSign.setEnabled(true);

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:
                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;
                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string) {
            Log.v("log_tag", string);
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }

    public void dialog_action() {


      /*  mGetSign.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Log.v("tag", "Panel Saved");
                view.setDrawingCacheEnabled(true);
                mSignature.save(view, StoredPath);
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), "Successfully Saved", Toast.LENGTH_SHORT).show();
                // Calling the same class
                recreate();
            }
        });*/

        //dialog.show();
    }
}
