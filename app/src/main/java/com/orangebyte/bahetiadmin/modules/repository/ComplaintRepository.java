package com.orangebyte.bahetiadmin.modules.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.orangebyte.bahetiadmin.modules.entities.ComplaintEntity;

import java.util.ArrayList;
import java.util.List;

public class ComplaintRepository {
    private static ComplaintRepository sInstance;
    private final RemoteRepository remoteRepository;

    interface RemoteComplaintListener {
        void onCheckResult(boolean z);

        void onRemotReceived(ComplaintEntity complaintEntity);

        void onRemoteReceived(List<ComplaintEntity> list);
    }

    interface TaskListener {
        void onTaskFinished();
    }

    public static ComplaintRepository getInstance(RemoteRepository remoteRepository) {
        if (sInstance == null) {
            sInstance = new ComplaintRepository(remoteRepository);
        }
        return sInstance;
    }

    private ComplaintRepository(RemoteRepository remoteRepository) {
        this.remoteRepository = remoteRepository;
    }

    public LiveData<List<Boolean>> assingnedEmployee(ComplaintEntity complaintEntity, int chk) {
        final MutableLiveData<List<Boolean>> data = new MutableLiveData();
        assignedEmployee(chk, new RemoteComplaintListener() {
            public void onRemotReceived(ComplaintEntity complaintEntity) {
            }

            public void onRemoteReceived(List<ComplaintEntity> list) {
            }

            public void onCheckResult(boolean b) {
                List<Boolean> dt = new ArrayList();
                dt.add(Boolean.valueOf(b));
                data.setValue(dt);
            }
        }, complaintEntity);
        return data;
    }

    public LiveData<List<ComplaintEntity>> loadComplaintsList(ComplaintEntity complaintEntity) {
        final MutableLiveData<List<ComplaintEntity>> data = new MutableLiveData();
        fetchComplaintsList(new RemoteComplaintListener() {
            public void onRemotReceived(ComplaintEntity cmEntity) {
            }

            public void onRemoteReceived(List<ComplaintEntity> scmEntities) {
                if (scmEntities != null) {
                    data.setValue(scmEntities);
                }
            }

            public void onCheckResult(boolean b) {
            }
        }, complaintEntity);
        return data;
    }

    private void fetchComplaintsList(@NonNull final RemoteComplaintListener listener, ComplaintEntity complaintEntity) {
        this.remoteRepository.getComplaints(complaintEntity, new RemoteCallback<List<ComplaintEntity>>() {
            public void onSuccess(List<ComplaintEntity> response) {
                listener.onRemoteReceived(response);
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }

    private void assignedEmployee(@NonNull int chk, @NonNull final RemoteComplaintListener listener, ComplaintEntity complaintEntity) {
        this.remoteRepository.assigEMp(chk, complaintEntity, new RemoteCallback<Boolean>() {
            public void onSuccess(Boolean response) {
                listener.onCheckResult(response.booleanValue());
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }
}
