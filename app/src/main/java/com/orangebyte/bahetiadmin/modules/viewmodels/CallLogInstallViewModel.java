package com.orangebyte.bahetiadmin.modules.viewmodels;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider.NewInstanceFactory;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.modules.db.local.DatabaseCreator;
import com.orangebyte.bahetiadmin.modules.entities.CallLogInstallationDemoEntity;
import com.orangebyte.bahetiadmin.modules.repository.CallLogInstallationRepository;
import com.orangebyte.bahetiadmin.modules.repository.RemoteRepository;
import com.orangebyte.bahetiadmin.modules.ui.AllContract.CallLogInstallationStream;

import java.util.List;

public class CallLogInstallViewModel extends ViewModel implements CallLogInstallationStream {
    private CallLogInstallationRepository callLogInstallationRepository;

    public static class CallLogInstallFactory extends NewInstanceFactory {
        CallLogInstallationRepository cCallLogInstallationRepository;

        public CallLogInstallFactory() {
            this(null);
        }

        public CallLogInstallFactory(@Nullable CallLogInstallationRepository cCallLogInstallationRepository) {
            this.cCallLogInstallationRepository = cCallLogInstallationRepository;
        }

        public <T extends ViewModel> T create(Class<T> cls) {
             return (T) new CallLogInstallViewModel(this.cCallLogInstallationRepository);
        }
    }

    public CallLogInstallViewModel() {
        this(null);
    }

    public CallLogInstallViewModel(@Nullable CallLogInstallationRepository callLogInstallationRepository) {
        if (callLogInstallationRepository != null) {
            this.callLogInstallationRepository = callLogInstallationRepository;
        }
    }

    public LiveData<List<CallLogInstallationDemoEntity>> getCallLogInstallation(CallLogInstallationDemoEntity callLogInstallationDemoEntity) {
        return subscribeToLocalData(callLogInstallationDemoEntity);
    }

    public LiveData<List<Boolean>> changeCallLog(CallLogInstallationDemoEntity callLogInstallationDemoEntity, int chk) {
        onInstanceCreated();
        return this.callLogInstallationRepository.changeCallLog(chk, callLogInstallationDemoEntity);
    }

    private LiveData<List<CallLogInstallationDemoEntity>> subscribeToLocalData(final CallLogInstallationDemoEntity callLogInstallationDemoEntity) {
        return Transformations.switchMap(DatabaseCreator.getInstance().isDatabaseCreated(), new Function<Boolean, LiveData<List<CallLogInstallationDemoEntity>>>() {
            public LiveData<List<CallLogInstallationDemoEntity>> apply(Boolean isDbCreated) {
                if (!Boolean.TRUE.equals(isDbCreated)) {
                    return null;
                }
                CallLogInstallViewModel.this.onInstanceCreated();
                return CallLogInstallViewModel.this.subscribeProductObservable(callLogInstallationDemoEntity);
            }
        });
    }

    private LiveData<List<CallLogInstallationDemoEntity>> subscribeProductObservable(CallLogInstallationDemoEntity callLogInstallationDemoEntity) {
        return this.callLogInstallationRepository.loadCallLogInstallsList(callLogInstallationDemoEntity);
    }

    private void onInstanceCreated() {
        try {
            if (this.callLogInstallationRepository == null) {
                this.callLogInstallationRepository = CallLogInstallationRepository.getInstance(RemoteRepository.getInstance());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
