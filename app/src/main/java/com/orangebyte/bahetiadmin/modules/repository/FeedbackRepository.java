package com.orangebyte.bahetiadmin.modules.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.orangebyte.bahetiadmin.applications.BahetiEnterprises;
import com.orangebyte.bahetiadmin.modules.db.daos.FeedbackDAO;
import com.orangebyte.bahetiadmin.modules.entities.FeedbackEntity;

import java.util.List;

public class FeedbackRepository {
    private static FeedbackRepository sInstance;
    private final FeedbackDAO feedbackDAO;
    private final RemoteRepository remoteRepository;

    interface RemoteFeedbackListener {
        void onCheckResult(boolean z);

        void onRemotReceived(FeedbackEntity feedbackEntity);

        void onRemoteReceived(List<FeedbackEntity> list);
    }

    interface TaskListener {
        void onTaskFinished();
    }

    public static FeedbackRepository getInstance(RemoteRepository remoteRepository, FeedbackDAO feedbackDAO) {
        if (sInstance == null) {
            sInstance = new FeedbackRepository(remoteRepository, feedbackDAO);
        }
        return sInstance;
    }

    private FeedbackRepository(RemoteRepository remoteRepository, FeedbackDAO feedbackDAO) {
        this.remoteRepository = remoteRepository;
        this.feedbackDAO = feedbackDAO;
    }

    public LiveData<List<FeedbackEntity>> loadFeedbackList(FeedbackEntity feedbackEntity) {
        fetchFeedbackList(new RemoteFeedbackListener() {
            public void onRemotReceived(FeedbackEntity cmEntity) {
            }

            public void onRemoteReceived(List<FeedbackEntity> scmEntities) {
                if (scmEntities != null) {
                    FeedbackRepository.this.insertAllTask(scmEntities);
                }
            }

            public void onCheckResult(boolean b) {
            }
        }, feedbackEntity);
        return this.feedbackDAO.loadAll();
    }

    private void insertAllTask(List<FeedbackEntity> feedbackEntity) {
        insertFeedback((List) feedbackEntity, null);
    }

    private void insertFeedback(final List<FeedbackEntity> feedbackEntity, @Nullable final TaskListener listener) {
        if (VERSION.SDK_INT >= 3) {
            new AsyncTask<Context, Void, Void>() {
                protected Void doInBackground(Context... params) {
                    FeedbackRepository.this.feedbackDAO.deleteAll();
                    FeedbackRepository.this.feedbackDAO.insertAll(feedbackEntity);
                    return null;
                }

                protected void onPostExecute(Void aVoid) {
                    if (VERSION.SDK_INT >= 3) {
                        super.onPostExecute(aVoid);
                    }
                    if (listener != null) {
                        listener.onTaskFinished();
                    }
                }
            }.execute(new Context[]{BahetiEnterprises.getInstance()});
        }
    }

    private void insertFeedback(FeedbackEntity feedbackEntity) {
        insertFeedback(feedbackEntity, null);
    }

    private void insertFeedback(final FeedbackEntity feedbackEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                FeedbackRepository.this.feedbackDAO.insert(feedbackEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteFeedback(FeedbackEntity feedbackEntity) {
        deletFback(feedbackEntity, null);
    }

    private void deletFback(final FeedbackEntity feedbackEntity, @Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                FeedbackRepository.this.feedbackDAO.delete(feedbackEntity);
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void deleteAllFeedback() {
        deleteAllFeedback(null);
    }

    private void deleteAllFeedback(@Nullable final TaskListener listener) {
        new AsyncTask<Context, Void, Void>() {
            protected Void doInBackground(Context... params) {
                FeedbackRepository.this.feedbackDAO.deleteAll();
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (listener != null) {
                    listener.onTaskFinished();
                }
            }
        }.execute(new Context[]{BahetiEnterprises.getInstance()});
    }

    private void fetchFeedbackList(@NonNull final RemoteFeedbackListener listener, FeedbackEntity feedbackEntity) {
        this.remoteRepository.getFeedback(feedbackEntity, new RemoteCallback<List<FeedbackEntity>>() {
            public void onSuccess(List<FeedbackEntity> response) {
                listener.onRemoteReceived(response);
            }

            public void onUnauthorized() {
            }

            public void onFailed(Throwable throwable) {
            }
        });
    }
}
